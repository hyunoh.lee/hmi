본 문서는 현재 테스트를 위해 생성해둔 define 변수에 대해서 정리를 하기 위해 작성되었다.

1. NOT_USED_DEVICE
- 장비 없이 tams 통신 테스트를 하기 위해 설정
- 보드, 차상장치 관련 통신 에러 없음
- TAMS와 테스트를 위해서 redis 데이터를 강제로 변경하는 부분 있음
- Board_TCP_Client.cs, TAMS_Redis_Pub.cs, CV_Control.cs, Lift_IO_Control.cs, PanelComm_Brazil.cs
* 실 장비 테스트에서는 무조건 주석 필요

2. MOVE_COMPLETE
- ST_MoveComplete 대신 bMoveComplete을 이용
- 현재 보드에서 MoveComplete 활성화 후 꺼지는 시점이 Moving이 꺼지는 시점보다 빨라서 생성
- MainBrazil.cs, PanelComm_Brazil.cs
* 보드 단에서 확인 후 삭제

3. COMMUNICATION_250_TO_100
- 통신 부하 테스트를 위해서 적용
- 기존 메인 타이머 주기를 250 ms -> 100 ms 으로 변경
- MainBrazil.cs
 실 장비 테스트에서는 무조건 주석 필요

4. USED_TAMS_TCP_LOG
- TAMS와 TCP 통신을 하면서 이에 대한 로그를 남겨서 디버그 할 목적으로 생성
- 생성 경로 "C:\MOBYUS\BRAZIL\Log", yyyy-mm-dd_recv.log, yyyy-mm-dd_send.log
- TAMS_TCP_Server.cs

5. NOT_USED_PIN_ERROR
- 메뉴얼 데스크 없이 lift up/down 작업을 진행하기 위해 생성
- 이를 활성화 시키면, up limit 활성화시 적재 상태로 판단하며, up limit 비활성화시 짐이 없다고 판단한다.
- Lift_IO_Control.cs