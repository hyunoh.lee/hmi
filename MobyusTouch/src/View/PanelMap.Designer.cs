﻿using System.Drawing;

namespace MobyusTouch
{
    partial class PanelMap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Map_pictureBox = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.CurrentLinkBtn = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.NextNodeBtn = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.TargetNodeBtn = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.CurrentNodeBtn = new System.Windows.Forms.Button();
            this.LayerBtn = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.NavPHIBtn = new System.Windows.Forms.Button();
            this.NavYBtn = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.NavXBtn = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.PalletThetaBtn = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.PalletYBtn = new System.Windows.Forms.Button();
            this.PalletXBtn = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.StackingThetaBtn = new System.Windows.Forms.Button();
            this.StackingYBtn = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.StackingXBtn = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.OrderBtn = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Map_pictureBox)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Map_pictureBox
            // 
            this.Map_pictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Map_pictureBox.Location = new System.Drawing.Point(2, 12);
            this.Map_pictureBox.Name = "Map_pictureBox";
            this.Map_pictureBox.Size = new System.Drawing.Size(1277, 558);
            this.Map_pictureBox.TabIndex = 0;
            this.Map_pictureBox.TabStop = false;
            this.Map_pictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.Map_pictureBox_Paint);
            this.Map_pictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Map_pictureBox_MouseDown);
            this.Map_pictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Map_pictureBox_MouseMove);
            this.Map_pictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Map_pictureBox_MouseUp);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.panel2.Controls.Add(this.CurrentLinkBtn);
            this.panel2.Controls.Add(this.button10);
            this.panel2.Controls.Add(this.button7);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.NextNodeBtn);
            this.panel2.Controls.Add(this.button13);
            this.panel2.Controls.Add(this.TargetNodeBtn);
            this.panel2.Controls.Add(this.button40);
            this.panel2.Controls.Add(this.button42);
            this.panel2.Controls.Add(this.CurrentNodeBtn);
            this.panel2.Controls.Add(this.LayerBtn);
            this.panel2.Controls.Add(this.button25);
            this.panel2.Controls.Add(this.button17);
            this.panel2.Controls.Add(this.NavPHIBtn);
            this.panel2.Controls.Add(this.NavYBtn);
            this.panel2.Controls.Add(this.button15);
            this.panel2.Controls.Add(this.NavXBtn);
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.PalletThetaBtn);
            this.panel2.Controls.Add(this.button5);
            this.panel2.Controls.Add(this.PalletYBtn);
            this.panel2.Controls.Add(this.PalletXBtn);
            this.panel2.Controls.Add(this.button9);
            this.panel2.Controls.Add(this.button6);
            this.panel2.Controls.Add(this.StackingThetaBtn);
            this.panel2.Controls.Add(this.StackingYBtn);
            this.panel2.Controls.Add(this.button4);
            this.panel2.Controls.Add(this.StackingXBtn);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Location = new System.Drawing.Point(-1, 571);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1280, 74);
            this.panel2.TabIndex = 29;
            // 
            // CurrentLinkBtn
            // 
            this.CurrentLinkBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.CurrentLinkBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CurrentLinkBtn.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.CurrentLinkBtn.ForeColor = System.Drawing.Color.White;
            this.CurrentLinkBtn.Location = new System.Drawing.Point(179, 27);
            this.CurrentLinkBtn.Name = "CurrentLinkBtn";
            this.CurrentLinkBtn.Size = new System.Drawing.Size(61, 46);
            this.CurrentLinkBtn.TabIndex = 32;
            this.CurrentLinkBtn.Text = "-";
            this.CurrentLinkBtn.UseVisualStyleBackColor = false;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button10.ForeColor = System.Drawing.Color.White;
            this.button10.Location = new System.Drawing.Point(179, 0);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(61, 28);
            this.button10.TabIndex = 31;
            this.button10.Text = "CUR LINK";
            this.button10.UseVisualStyleBackColor = false;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Location = new System.Drawing.Point(0, 0);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(60, 73);
            this.button7.TabIndex = 21;
            this.button7.Text = "MAP";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(59, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(61, 28);
            this.button1.TabIndex = 22;
            this.button1.Text = "LAYER";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // NextNodeBtn
            // 
            this.NextNodeBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.NextNodeBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NextNodeBtn.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.NextNodeBtn.ForeColor = System.Drawing.Color.White;
            this.NextNodeBtn.Location = new System.Drawing.Point(299, 27);
            this.NextNodeBtn.Name = "NextNodeBtn";
            this.NextNodeBtn.Size = new System.Drawing.Size(61, 46);
            this.NextNodeBtn.TabIndex = 19;
            this.NextNodeBtn.Text = "-";
            this.NextNodeBtn.UseVisualStyleBackColor = false;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button13.Cursor = System.Windows.Forms.Cursors.Default;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button13.ForeColor = System.Drawing.Color.White;
            this.button13.Location = new System.Drawing.Point(359, 0);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(61, 28);
            this.button13.TabIndex = 8;
            this.button13.Text = "NAV X";
            this.button13.UseVisualStyleBackColor = false;
            // 
            // TargetNodeBtn
            // 
            this.TargetNodeBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.TargetNodeBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TargetNodeBtn.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.TargetNodeBtn.ForeColor = System.Drawing.Color.White;
            this.TargetNodeBtn.Location = new System.Drawing.Point(239, 27);
            this.TargetNodeBtn.Name = "TargetNodeBtn";
            this.TargetNodeBtn.Size = new System.Drawing.Size(61, 46);
            this.TargetNodeBtn.TabIndex = 20;
            this.TargetNodeBtn.Text = "-";
            this.TargetNodeBtn.UseVisualStyleBackColor = false;
            // 
            // button40
            // 
            this.button40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button40.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button40.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button40.ForeColor = System.Drawing.Color.White;
            this.button40.Location = new System.Drawing.Point(419, 0);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(61, 28);
            this.button40.TabIndex = 6;
            this.button40.Text = "NAV Y";
            this.button40.UseVisualStyleBackColor = false;
            // 
            // button42
            // 
            this.button42.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button42.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button42.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button42.ForeColor = System.Drawing.Color.White;
            this.button42.Location = new System.Drawing.Point(479, 0);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(61, 28);
            this.button42.TabIndex = 4;
            this.button42.Text = "NAV θ";
            this.button42.UseVisualStyleBackColor = false;
            // 
            // CurrentNodeBtn
            // 
            this.CurrentNodeBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.CurrentNodeBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CurrentNodeBtn.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.CurrentNodeBtn.ForeColor = System.Drawing.Color.White;
            this.CurrentNodeBtn.Location = new System.Drawing.Point(119, 27);
            this.CurrentNodeBtn.Name = "CurrentNodeBtn";
            this.CurrentNodeBtn.Size = new System.Drawing.Size(61, 46);
            this.CurrentNodeBtn.TabIndex = 23;
            this.CurrentNodeBtn.Text = "-";
            this.CurrentNodeBtn.UseVisualStyleBackColor = false;
            // 
            // LayerBtn
            // 
            this.LayerBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.LayerBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LayerBtn.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.LayerBtn.ForeColor = System.Drawing.Color.White;
            this.LayerBtn.Location = new System.Drawing.Point(59, 27);
            this.LayerBtn.Name = "LayerBtn";
            this.LayerBtn.Size = new System.Drawing.Size(61, 46);
            this.LayerBtn.TabIndex = 24;
            this.LayerBtn.Text = "-";
            this.LayerBtn.UseVisualStyleBackColor = false;
            // 
            // button25
            // 
            this.button25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button25.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button25.ForeColor = System.Drawing.Color.White;
            this.button25.Location = new System.Drawing.Point(299, 0);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(61, 28);
            this.button25.TabIndex = 16;
            this.button25.Text = "NEXT";
            this.button25.UseVisualStyleBackColor = false;
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button17.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button17.ForeColor = System.Drawing.Color.White;
            this.button17.Location = new System.Drawing.Point(239, 0);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(61, 28);
            this.button17.TabIndex = 17;
            this.button17.Text = "TARGET";
            this.button17.UseVisualStyleBackColor = false;
            // 
            // NavPHIBtn
            // 
            this.NavPHIBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.NavPHIBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NavPHIBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.NavPHIBtn.ForeColor = System.Drawing.Color.White;
            this.NavPHIBtn.Location = new System.Drawing.Point(479, 27);
            this.NavPHIBtn.Name = "NavPHIBtn";
            this.NavPHIBtn.Size = new System.Drawing.Size(61, 46);
            this.NavPHIBtn.TabIndex = 3;
            this.NavPHIBtn.Text = "-";
            this.NavPHIBtn.UseVisualStyleBackColor = false;
            // 
            // NavYBtn
            // 
            this.NavYBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.NavYBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NavYBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.NavYBtn.ForeColor = System.Drawing.Color.White;
            this.NavYBtn.Location = new System.Drawing.Point(419, 27);
            this.NavYBtn.Name = "NavYBtn";
            this.NavYBtn.Size = new System.Drawing.Size(61, 46);
            this.NavYBtn.TabIndex = 5;
            this.NavYBtn.Text = "-";
            this.NavYBtn.UseVisualStyleBackColor = false;
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button15.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.ForeColor = System.Drawing.Color.White;
            this.button15.Location = new System.Drawing.Point(119, 0);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(61, 28);
            this.button15.TabIndex = 18;
            this.button15.Text = "CUR NODE";
            this.button15.UseVisualStyleBackColor = false;
            // 
            // NavXBtn
            // 
            this.NavXBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.NavXBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NavXBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.NavXBtn.ForeColor = System.Drawing.Color.White;
            this.NavXBtn.Location = new System.Drawing.Point(359, 27);
            this.NavXBtn.Name = "NavXBtn";
            this.NavXBtn.Size = new System.Drawing.Size(61, 46);
            this.NavXBtn.TabIndex = 7;
            this.NavXBtn.Text = "-";
            this.NavXBtn.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold);
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(839, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(61, 28);
            this.button3.TabIndex = 30;
            this.button3.Text = "PALLET θ";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // PalletThetaBtn
            // 
            this.PalletThetaBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.PalletThetaBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PalletThetaBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.PalletThetaBtn.ForeColor = System.Drawing.Color.White;
            this.PalletThetaBtn.Location = new System.Drawing.Point(839, 27);
            this.PalletThetaBtn.Name = "PalletThetaBtn";
            this.PalletThetaBtn.Size = new System.Drawing.Size(61, 46);
            this.PalletThetaBtn.TabIndex = 29;
            this.PalletThetaBtn.Text = "-";
            this.PalletThetaBtn.UseVisualStyleBackColor = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold);
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(779, 0);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(61, 28);
            this.button5.TabIndex = 26;
            this.button5.Text = "PALLET Y";
            this.button5.UseVisualStyleBackColor = false;
            // 
            // PalletYBtn
            // 
            this.PalletYBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.PalletYBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PalletYBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.PalletYBtn.ForeColor = System.Drawing.Color.White;
            this.PalletYBtn.Location = new System.Drawing.Point(779, 27);
            this.PalletYBtn.Name = "PalletYBtn";
            this.PalletYBtn.Size = new System.Drawing.Size(61, 46);
            this.PalletYBtn.TabIndex = 25;
            this.PalletYBtn.Text = "-";
            this.PalletYBtn.UseVisualStyleBackColor = false;
            // 
            // PalletXBtn
            // 
            this.PalletXBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.PalletXBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PalletXBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.PalletXBtn.ForeColor = System.Drawing.Color.White;
            this.PalletXBtn.Location = new System.Drawing.Point(719, 27);
            this.PalletXBtn.Name = "PalletXBtn";
            this.PalletXBtn.Size = new System.Drawing.Size(61, 46);
            this.PalletXBtn.TabIndex = 27;
            this.PalletXBtn.Text = "-";
            this.PalletXBtn.UseVisualStyleBackColor = false;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold);
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.Location = new System.Drawing.Point(719, 0);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(61, 28);
            this.button9.TabIndex = 28;
            this.button9.Text = "PALLET X";
            this.button9.UseVisualStyleBackColor = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Century Gothic", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(539, 0);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(61, 28);
            this.button6.TabIndex = 14;
            this.button6.Text = "STACKING X";
            this.button6.UseVisualStyleBackColor = false;
            // 
            // StackingThetaBtn
            // 
            this.StackingThetaBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.StackingThetaBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StackingThetaBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.StackingThetaBtn.ForeColor = System.Drawing.Color.White;
            this.StackingThetaBtn.Location = new System.Drawing.Point(659, 27);
            this.StackingThetaBtn.Name = "StackingThetaBtn";
            this.StackingThetaBtn.Size = new System.Drawing.Size(61, 46);
            this.StackingThetaBtn.TabIndex = 9;
            this.StackingThetaBtn.Text = "-";
            this.StackingThetaBtn.UseVisualStyleBackColor = false;
            // 
            // StackingYBtn
            // 
            this.StackingYBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.StackingYBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StackingYBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.StackingYBtn.ForeColor = System.Drawing.Color.White;
            this.StackingYBtn.Location = new System.Drawing.Point(599, 27);
            this.StackingYBtn.Name = "StackingYBtn";
            this.StackingYBtn.Size = new System.Drawing.Size(61, 46);
            this.StackingYBtn.TabIndex = 11;
            this.StackingYBtn.Text = "-";
            this.StackingYBtn.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Century Gothic", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(599, 0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(61, 28);
            this.button4.TabIndex = 12;
            this.button4.Text = "STACKING Y";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // StackingXBtn
            // 
            this.StackingXBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.StackingXBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StackingXBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.StackingXBtn.ForeColor = System.Drawing.Color.White;
            this.StackingXBtn.Location = new System.Drawing.Point(539, 27);
            this.StackingXBtn.Name = "StackingXBtn";
            this.StackingXBtn.Size = new System.Drawing.Size(61, 46);
            this.StackingXBtn.TabIndex = 13;
            this.StackingXBtn.Text = "-";
            this.StackingXBtn.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Century Gothic", 5.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(659, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(61, 28);
            this.button2.TabIndex = 10;
            this.button2.Text = "STACKING θ";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.panel1.Controls.Add(this.OrderBtn);
            this.panel1.Controls.Add(this.button8);
            this.panel1.Location = new System.Drawing.Point(-1, 499);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(120, 74);
            this.panel1.TabIndex = 33;
            // 
            // OrderBtn
            // 
            this.OrderBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.OrderBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OrderBtn.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.OrderBtn.ForeColor = System.Drawing.Color.White;
            this.OrderBtn.Location = new System.Drawing.Point(0, 28);
            this.OrderBtn.Name = "OrderBtn";
            this.OrderBtn.Size = new System.Drawing.Size(120, 46);
            this.OrderBtn.TabIndex = 33;
            this.OrderBtn.Text = "-";
            this.OrderBtn.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button8.ForeColor = System.Drawing.Color.White;
            this.button8.Location = new System.Drawing.Point(0, 0);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(120, 28);
            this.button8.TabIndex = 33;
            this.button8.Text = "ORDER";
            this.button8.UseVisualStyleBackColor = false;
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button11.ForeColor = System.Drawing.Color.White;
            this.button11.Location = new System.Drawing.Point(118, 499);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(61, 28);
            this.button11.TabIndex = 33;
            this.button11.Text = "TARGET";
            this.button11.UseVisualStyleBackColor = false;
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.button12.ForeColor = System.Drawing.Color.White;
            this.button12.Location = new System.Drawing.Point(118, 527);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(61, 46);
            this.button12.TabIndex = 33;
            this.button12.Text = "-";
            this.button12.UseVisualStyleBackColor = false;
            // 
            // PanelMap
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1280, 800);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.Map_pictureBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PanelMap";
            this.Text = "PanelMap";
            ((System.ComponentModel.ISupportInitialize)(this.Map_pictureBox)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.PictureBox Map_pictureBox;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button NextNodeBtn;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button TargetNodeBtn;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button CurrentNodeBtn;
        private System.Windows.Forms.Button LayerBtn;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button NavPHIBtn;
        private System.Windows.Forms.Button NavYBtn;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button NavXBtn;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button PalletThetaBtn;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button PalletYBtn;
        private System.Windows.Forms.Button PalletXBtn;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button StackingThetaBtn;
        private System.Windows.Forms.Button StackingYBtn;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button StackingXBtn;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button CurrentLinkBtn;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button OrderBtn;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
    }
}