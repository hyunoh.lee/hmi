﻿using MobyusTouch.src.Communication;
using MobyusTouch.src.Global;
using MobyusTouch.Src.Utility;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
//using MapDLL;
using EditorLibrary;
using System.Data;
using System.IO;
using LumenWorks.Framework.IO.Csv;
using System.Collections.ObjectModel;
using MobyusTouch.src.Communication;

namespace MobyusTouch.src.View
{
    /// <summary>
    /// PanelSetting.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class PanelSetting : UserControl
    {
        private static Logger logger = LogManager.GetLogger("PanelSetting");
        /// <summary>
        /// ////////////////////////////
        /// </summary>
        List<WorklistFiles> worklistFiles = null;
        List<WorklistInfo> worklistInfo = null;
        List<Param> param = null;
        ObservableCollection<Param> obParam;

        public string transferID;
        public string workID;
        public string workMode; // A: AMR Path Planning, M: ACS Path Planning
        public int workType; // LOAD, UNLOAD, MOVE
        public byte workState;
        public byte workStateUnit;
        public int workLevel;
        public int workTime;

        public ushort curLink;
        public ushort curNode;
        public ushort toNode;
        public ushort nextNode;

        public List<AstarNodeData> realPath;
        public Queue<WorkStep> workQueue;
        public Queue<WorkStep> workQueueCompleted;

        public DataTable workDataTable;

        public int palletType;
        public int frontRetryCnt;
        private int alarm;

        private int nWorklistIdx;
        WorklistFiles wlf;
        Param pr;
        /// <summary>
        /// //////////////////////////////
        /// </summary>
        DispatcherTimer timer = new DispatcherTimer();

        public PanelSetting()
        {
            InitializeComponent();
            //  obParam = this.Resources["obParam"] as ObservableCollection<Param>;

            SetConfig();
            GetWorklist();

            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += new EventHandler(Result_Tick);
            timer.Start();
        }

        public void BtnCAN_ON_OFF()
        {

        }


        private void Result_Tick(object sender, EventArgs e)
        {
            try
            {
                StatusCharge.Content = Globals.VehicleSystem.SystemCharge == true ? "충전 중" : "충전 해제";
                ChargeBattery.Content = Globals.BMSSystem.SOC.ToString() + "%";

                RecognitionCameraRightValue.Content = "(" + Globals.RecognitionSystem.RightServoMotorAngle + ")";
                RecognitionCameraLeftValue.Content = "(" + Globals.RecognitionSystem.LeftServoMotorAngle + ")";

                if (Globals.bRefresh_worklistView)
                {
                    GetWorklist();

                    int ntmp = worklistFiles.Count;
                    for (int i = 0; i < ntmp; i++)
                    {
                        if (worklistFiles[i].fileName == Globals.strWL_WORK + "_" + Globals.strWL_PALLETE_ID + "_" + Globals.strWL_LEVEL + "_" + Globals.strWL_NODE)
                        {
                            worklisFilesListView.SelectedItem = worklisFilesListView.Items[i];
                            worklisFilesListView.UpdateLayout();

                            var sc = worklisFilesListView.Items[i];
                            worklisFilesListView.ScrollIntoView(sc);
                        }
                    }
                    Globals.bRefresh_worklistView = false;
                }

                if (Globals.bAdd_newParam)
                {
                    param.Add(new Param() { param_name = Globals.strWL_PR_NAME, param_value = Globals.strWL_PR_VALUE, param_sleep = Globals.strWL_PR_SLEEP, param_sync = Globals.strWL_PR_SYNC });
                    paramListView.ItemsSource = param;

                    paramListView.Items.Refresh();
                    Globals.bAdd_newParam = false;
                }

                if (Globals.bEdit_Param)
                {
                    int index = paramListView.SelectedIndex;

                    Param item = new Param();
                    item.param_name = Globals.strWL_PR_NAME;
                    item.param_value = Globals.strWL_PR_VALUE;
                    item.param_sleep = Globals.strWL_PR_SLEEP;
                    item.param_sync = Globals.strWL_PR_SYNC;

                    if (index < paramListView.Items.Count)
                    {
                        param.RemoveAt(index);
                        param.Insert(index, item);
                        paramListView.Items.Refresh();
                    }

                    Globals.bEdit_Param = false;
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }

        public void SetConfig()
        {
            comboBoxPort.Text = Globals.serialPortName;
            comboBoxBaudRate.Text = Globals.serialBaudRate.ToString();

            int tempDataBits = Globals.serialDataBits;
            if (tempDataBits >= 5)
            {
                tempDataBits = tempDataBits - 5;
            }

            comboBoxDataBits.SelectedIndex = tempDataBits;
            comboBoxParity.SelectedIndex = (int)Globals.serialParityBits;
            comboBoxStopBits.SelectedIndex = (int)Globals.serialStopBits;

            if (Globals.fmsIpAddress != "" && Globals.fmsIpAddress != null)
            {
                string[] words = Globals.fmsIpAddress.Split('.');
                ipACS1.Text = words[0];
                ipACS2.Text = words[1];
                ipACS3.Text = words[2];
                ipACS4.Text = words[3];
            }
            portACS.Text = Globals.fmsIpPort.ToString();

            RecognitionMinDistance.Text = Globals.RecognitionParameter.MinDistance.ToString();
            RecognitionReceiverGain.Text = Globals.RecognitionParameter.ReceiverGain.ToString();
            RecognitionPostSharpening.Text = Globals.RecognitionParameter.PostSharpening.ToString();
            RecognitionConfidence.Text = Globals.RecognitionParameter.Confidence.ToString();
            RecognitionPreSharpening.Text = Globals.RecognitionParameter.PreSharpening.ToString();
            RecognitionNoiseFiltering.Text = Globals.RecognitionParameter.NoiseFiltering.ToString();
            RecognitionPalletDistanceUp.Text = Globals.RecognitionParameter.PalletDistanceUp.ToString();
            RecognitionPalletDistanceDown.Text = Globals.RecognitionParameter.PalletDistanceDown.ToString();

            VehicleStackingAngle.Text = Globals.VehicleParameter.StackingAngle.ToString();
            VehicleStackingDistance.Text = Globals.VehicleParameter.StackingDistance.ToString();
            VehicleMinSpeed.Text = Globals.VehicleParameter.MinSpeed.ToString();
            VehicleMaxSpeed.Text = Globals.VehicleParameter.MaxSpeed.ToString();


            SettingFileLocation.Content = Globals.HMI_SettingFilePath;
            MapFileLocation.Content = Globals.MAP_SettingFilePath;
        }
        public class WorklistFiles
        {
            public string fileName { get; set; }

        }
        public class WorklistInfo
        {
            public string worklist_id { get; set; }
            public string worklist_work { get; set; }
            public string worklist_palleteid { get; set; }
            public string worklist_level { get; set; }
            public string worklist_node { get; set; }
            public string worklist_link_type { get; set; }
            public string worklist_cnt { get; set; }
            

        }

        public class Param
        {
            public string param_name { get; set; }
            public string param_value { get; set; }
            public string param_sleep { get; set; }
            public string param_sync { get; set; }

        }
        public void GetWorklist()
        {
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(Globals.WORK_SettingDirPath);
            string[] files = System.IO.Directory.GetFiles(Globals.WORK_SettingDirPath, "*.csv");

            worklistFiles = new List<WorklistFiles>();
            foreach (var file in files)
            {
                string f = file.ToString().Replace(Globals.WORK_SettingDirPath + @"\", "");
                f = f.Replace(".csv", "");
                worklistFiles.Add(new WorklistFiles() { fileName = f });
            }

            worklisFilesListView.ItemsSource = worklistFiles;
        }
        private void worklisFilesListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            nWorklistIdx = worklisFilesListView.SelectedIndex;

            if (nWorklistIdx > -1)
                LoadWorklistParam();
        }

        private void LoadWorklistParam()
        {
            wlf = (WorklistFiles)worklisFilesListView.SelectedItems[0];
            string strFile = wlf.fileName;
            string strPath = Globals.WORK_SettingDirPath + @"\" + strFile + ".csv";

            FileInfo item = new FileInfo(strPath);
            if (!item.Exists)
            {
                workID = workID.Remove(workID.LastIndexOf('_'), workID.Length - workID.LastIndexOf('_')) + "_0";
                item = new FileInfo(Globals.WORK_SettingDirPath + "\\" + workID + ".csv");
            }

            using (CsvReader csvReader = new CsvReader(new StreamReader(System.IO.File.OpenRead(item.FullName)), false, ';'))
            {
                workDataTable = new DataTable();
                try
                {
                    workDataTable.Load(csvReader);
                }
                catch
                {
                    MessageBox.Show("파일 내용이 잘못되었습니다.");
                }

                for (int i = 0; i <= 12; i++)
                {
                    if (i >= workDataTable.Columns.Count)
                    {
                        workDataTable.Columns.Add(new DataColumn());
                    }
                    workDataTable.Columns[i].ReadOnly = false;
                }
            }

            if (workDataTable.Rows.Count > 1)
            {
                string FILE_ID = workDataTable.Rows[1][0].ToString();
                string FILE_WORK = workDataTable.Rows[1][1].ToString();
                string FILE_PALLET_ID = workDataTable.Rows[1][2].ToString();
                string FILE_LEVEL = workDataTable.Rows[1][3].ToString();
                string FILE_NODE = workDataTable.Rows[1][4].ToString();
                string FILE_LINK_TYPE = workDataTable.Rows[1][5].ToString();
                string FILE_CNT = workDataTable.Rows[1][6].ToString();

                string fileWorkID = FILE_WORK + "_" + FILE_PALLET_ID + "_" + FILE_LEVEL + "_" + FILE_NODE;

                Globals.strWL_WORK = FILE_WORK;
                Globals.strWL_PALLETE_ID = FILE_PALLET_ID;
                Globals.strWL_LEVEL = FILE_LEVEL;
                Globals.strWL_NODE = FILE_NODE;
                Globals.strWL_LINK_TYPE = FILE_LINK_TYPE;

                worklistInfo = new List<WorklistInfo>();
                worklistInfo.Add(new WorklistInfo() { worklist_id = FILE_ID, worklist_work = FILE_WORK, worklist_palleteid = FILE_PALLET_ID, worklist_level = FILE_LEVEL, worklist_node = FILE_NODE, worklist_link_type = FILE_LINK_TYPE, worklist_cnt = FILE_CNT });

                worklisInfoView.ItemsSource = worklistInfo;


                param = new List<Param>();
                for (int i = 0; i < workDataTable.Rows.Count; i++)
                {
                    if (i == 5)
                    {
                        /*
                        string PALLET_NAME = csvTable.Rows[i][0].ToString();
                        int PALLET_WIDTH = int.Parse(csvTable.Rows[i][1].ToString());
                        int PALLET_HEIGHT = int.Parse(csvTable.Rows[i][2].ToString());
                        int PALLET_LENGTH = int.Parse(csvTable.Rows[i][3].ToString());
                        int PALLET_LENGTH_HOLE = int.Parse(csvTable.Rows[i][4].ToString());
                        */
                    }
                    else if (i >= 10)


                    {
                        string name = workDataTable.Rows[i][0].ToString();
                        string value = workDataTable.Rows[i][1].ToString();
                        string sleep = workDataTable.Rows[i][2].ToString();
                        string sync = workDataTable.Rows[i][3].ToString();

                        param.Add(new Param() { param_name = name, param_value = value, param_sleep = sleep, param_sync = sync });

                    }
                }

                paramListView.ItemsSource = param;

            }
        }



        private void btnConnectControlBoard_Click(object sender, EventArgs e)
        {
            Globals.serialPortName = comboBoxPort.Text;
            Globals.serialBaudRate = Int32.Parse(comboBoxBaudRate.Text);
            Globals.serialDataBits = comboBoxDataBits.SelectedIndex + 5;
            Globals.serialParityBits = (Parity)comboBoxParity.SelectedIndex;
            Globals.serialStopBits = (StopBits)comboBoxStopBits.SelectedIndex + 1;

            SerialPortBoard.Disconnect();

            SerialPortBoard.bSetConnect = true;
            if (SerialPortBoard.Connect())
            {
                ConfigSerialPortWrite();
            }
            else
            {
                MessageBox.Show("통신이 연결되지 않았습니다.");
            }
        }
        private void btnDisConnectControlBoard_Click(object sender, EventArgs e)
        {
            SerialPortBoard.bSetConnect = false;

            SerialPortBoard.Disconnect();
            MessageBox.Show("통신 연결을 해제하였습니다.");

        }

        private void BtnConnectACS_Click(object sender, EventArgs e)
        {
            if (ipACS1.Text.ToString() == "" || ipACS2.Text.ToString() == "" || ipACS3.Text.ToString() == "" || ipACS4.Text.ToString() == "")
            {
                MessageBox.Show("IP주소를 입력해주세요.");
                return;
            }
            if (portACS.Text.ToString() == "")
            {
                MessageBox.Show("PORT를 입력해주세요.");
                return;
            }
            Globals.IgnoreACS = true;
            Globals.fmsIpAddress = ipACS1.Text + "." + ipACS2.Text + "." + ipACS3.Text + "." + ipACS4.Text;
            Globals.fmsIpPort = Int16.Parse(portACS.Text);

            MqttClientHyundai.Disconnect();
            Thread.Sleep(500);
            if (MqttClientHyundai.Connect())
            {
                ConfigACSWrite();
            }
            else
            {
                MessageBox.Show("통신이 연결되지 않았습니다.");

            }
        }

        private void BtnDisconnectACS_Click(object sender, EventArgs e)
        {
            MqttClientHyundai.Disconnect();
            MessageBox.Show("통신 연결을 해제하였습니다.");
        }

        private void BtnConnectCharge_Click(object sender, EventArgs e)
        {
            Globals.HMISystem.ChargeEnable = true;
            Globals.currentWork.workState = (byte)WorkState.CHARGING; // CHARGE
            MessageBox.Show("충전을 시작합니다.");
        }

        private void BtnDIsconnectCharge_Click(object sender, EventArgs e)
        {
            Globals.HMISystem.ChargeEnable = false;
            Globals.nChargingCount = 0;
            Globals.SOCCount = 0;
            Globals.CurrentTimeCheck = 0;
            Globals.currentWork.workState = (byte)WorkState.READY; // READY
            MessageBox.Show("충전을 해제합니다.");
        }

        public void BtnRecognitionUpdate_Click(object sender, EventArgs e)
        {
            Globals.RecognitionParameter.MinDistance = ushort.Parse(RecognitionMinDistance.Text);
            Globals.RecognitionParameter.ReceiverGain = byte.Parse(RecognitionReceiverGain.Text);
            Globals.RecognitionParameter.PostSharpening = byte.Parse(RecognitionPostSharpening.Text);
            Globals.RecognitionParameter.Confidence = byte.Parse(RecognitionConfidence.Text);
            Globals.RecognitionParameter.PreSharpening = byte.Parse(RecognitionPreSharpening.Text);
            Globals.RecognitionParameter.NoiseFiltering = byte.Parse(RecognitionNoiseFiltering.Text);
            Globals.RecognitionParameter.PalletDistanceUp = byte.Parse(RecognitionPalletDistanceUp.Text);
            Globals.RecognitionParameter.PalletDistanceDown = byte.Parse(RecognitionPalletDistanceDown.Text);
            Globals.RecognitionParameter.ObstacleX = ushort.Parse(Obstacle_X.Text); // kdy
            Globals.RecognitionParameter.ObstacleY = ushort.Parse(Obstacle_Y.Text); // kdy

            CanForklift.nWriteCan_Cnt = 0;

            Globals.RecognitionParameter.UpdateFlag = true;
            DateTime StartTime = DateTime.Now;

            while (Globals.RecognitionParameter.UpdateFlag)
            {
                DateTime EndTime = DateTime.Now;
                long elapsedTicks = EndTime.Ticks - StartTime.Ticks;
                TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);
                if (elapsedSpan.TotalSeconds >= 5)
                {
                    MessageBox.Show("인식 파라미터 업데이트에 실패하였습니다.");
                    break;
                }

                if (Globals.RecognitionParameter.UpdateResult)
                {
                    ConfigWriteRecognition();
                    Globals.RecognitionParameter.UpdateFlag = false;
                    MessageBox.Show("인식 파라미터 업데이트가 완료되었습니다.");
                    break;
                }

                Thread.Sleep(100);
            }
        }

        public void BtnReconitionCameraUpdate_Click(object sender, EventArgs e)
        {

            if (Math.Abs(short.Parse(RecognitionCameraLeft.Text)) > 60 || Math.Abs(short.Parse(RecognitionCameraRight.Text)) > 60)
            {
                MessageBox.Show("60도 이상 동작하지 않습니다.");
                return;
            }

            Globals.RecognitionParameter.RotateMotorLeft = (short)(2048 - 11.3778 * short.Parse(RecognitionCameraLeft.Text));
            Globals.RecognitionParameter.RotateMotorRight = (short)(2048 - 11.3778 * short.Parse(RecognitionCameraRight.Text));

            Globals.RecognitionParameter.RotateMotorUpdateFlag = true;
            DateTime StartTime = DateTime.Now;


            while (Globals.RecognitionParameter.RotateMotorUpdateFlag)
            {
                DateTime EndTime = DateTime.Now;
                long elapsedTicks = EndTime.Ticks - StartTime.Ticks;
                TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);
                if (elapsedSpan.TotalSeconds >= 5)
                {
                    MessageBox.Show("인식 카메라 모터 파라미터 업데이트에 실패하였습니다.");
                    break;
                }

                if (Globals.RecognitionParameter.RotateMotorLeftUpdateResult && Globals.RecognitionParameter.RotateMotorRightUpdateResult)
                {
                    ConfigWriteRecognition();
                    //Globals.RecognitionParameter.RotateMotorUpdateFlag = false;
                    MessageBox.Show("인식 카메라 모터 파라미터 업데이트가 완료되었습니다.");
                    break;
                }

                Thread.Sleep(100);
            }
        }

        private void BtnVehicleUpdate_Click(object sender, RoutedEventArgs e)
        {
            Globals.VehicleParameter.StackingAngle = byte.Parse(VehicleStackingAngle.Text);
            Globals.VehicleParameter.StackingDistance = byte.Parse(VehicleStackingDistance.Text);
            Globals.VehicleParameter.MinSpeed = byte.Parse(VehicleMinSpeed.Text);
            Globals.VehicleParameter.MaxSpeed = byte.Parse(VehicleMaxSpeed.Text);

            Globals.VehicleParameter.UpdateFlag = true;
            DateTime StartTime = DateTime.Now;

            while (Globals.VehicleParameter.UpdateFlag)
            {
                DateTime EndTime = DateTime.Now;
                long elapsedTicks = EndTime.Ticks - StartTime.Ticks;
                TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);
                if (elapsedSpan.TotalSeconds >= 5)
                {
                    MessageBox.Show("주행 파라미터 업데이트에 실패하였습니다.");
                    break;
                }

                if (Globals.VehicleParameter.UpdateResult)
                {
                    ConfigWriteVehicle();
                    Globals.VehicleParameter.UpdateFlag = false;
                    MessageBox.Show("주행 파라미터 업데이트가 완료되었습니다.");
                    break;
                }

                Thread.Sleep(100);
            }
        }

        public void ConfigWriteVehicle()
        {
            IniFileParsing.SetFilePath(Globals.HMI_SettingFilePath);
            IniFileParsing.WriteValue("Vehicle", "STACKING_ANGLE", Globals.VehicleParameter.StackingAngle);
            IniFileParsing.WriteValue("Vehicle", "STACKING_DISTANCE", Globals.VehicleParameter.StackingDistance);
            IniFileParsing.WriteValue("Vehicle", "MIN_SPEED", Globals.VehicleParameter.MinSpeed);
            IniFileParsing.WriteValue("Vehicle", "MAX_SPEED", Globals.VehicleParameter.MaxSpeed);
        }

        public void ConfigWriteRecognition()
        {
            IniFileParsing.SetFilePath(Globals.HMI_SettingFilePath);
            IniFileParsing.WriteValue("Recognition", "MIN_DISTANCE", Globals.RecognitionParameter.MinDistance);
            IniFileParsing.WriteValue("Recognition", "RECEIVER_GAIN", Globals.RecognitionParameter.ReceiverGain);
            IniFileParsing.WriteValue("Recognition", "POST_SHARPENING", Globals.RecognitionParameter.PostSharpening);
            IniFileParsing.WriteValue("Recognition", "PRE_SHARPENING", Globals.RecognitionParameter.PreSharpening);
            IniFileParsing.WriteValue("Recognition", "CONFIDENCE", Globals.RecognitionParameter.Confidence);
            IniFileParsing.WriteValue("Recognition", "NOISE_FILTERING", Globals.RecognitionParameter.NoiseFiltering);
            IniFileParsing.WriteValue("Recognition", "PALLET_DISTANCE_UP", Globals.RecognitionParameter.PalletDistanceUp);
            IniFileParsing.WriteValue("Recognition", "PALLET_DISTANCE_DOWN", Globals.RecognitionParameter.PalletDistanceDown);
        }

        public void ConfigSerialPortWrite()
        {
            IniFileParsing.SetFilePath(Globals.HMI_SettingFilePath);
            IniFileParsing.WriteValue("SerialPort", "PORT_NAME", comboBoxPort.Text);
            IniFileParsing.WriteValue("SerialPort", "PORT_BAUDRATE", comboBoxBaudRate.Text);
            IniFileParsing.WriteValue("SerialPort", "PORT_DATABITS", comboBoxDataBits.SelectedIndex + 5);
            IniFileParsing.WriteValue("SerialPort", "PORT_PARITY", comboBoxParity.SelectedIndex);
            IniFileParsing.WriteValue("SerialPort", "PORT_STOPBITS", comboBoxStopBits.SelectedIndex + 1);
        }

        public void ConfigACSWrite()
        {
            IniFileParsing.SetFilePath(Globals.HMI_SettingFilePath);
            IniFileParsing.WriteValue("FmsTcp", "FMS_IP", Globals.fmsIpAddress);
            IniFileParsing.WriteValue("FmsTcp", "FMS_PORT", Globals.fmsIpPort);
        }

        private void BtnNewWorkList_Click(object sender, RoutedEventArgs e)
        {
            DialogWorklist dialogWorklist = new DialogWorklist();
            if (dialogWorklist.ShowDialog() == true)
            {
            }

        }

        private void BtnSaveWorkList_Click(object sender, RoutedEventArgs e)
        {

            DirectoryInfo dtif = new DirectoryInfo(Globals.WORK_SettingDirPath + @"\");

            string filename, filepath;
            filename = wlf.fileName;
            filepath = dtif + /*"\\" +*/ filename + ".csv";

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(filepath))
            {
                // 각 필드에 사용될 제목을 정의 

                file.WriteLine("ID;WORK;PALLET_ID;LEVEL;NODE;LINK_TYPE;CNT");
                // 필드값
                file.WriteLine("{0}", filename + ";" + Globals.strWL_WORK + ";" + Globals.strWL_PALLETE_ID + ";" + Globals.strWL_LEVEL + ";" + Globals.strWL_NODE + ";" + Globals.strWL_LINK_TYPE + ";" + paramListView.Items.Count.ToString());
                file.WriteLine(";;;;;;");
                file.WriteLine(";;;;;;");
                file.WriteLine("PALLET_NAME;PALLET_WIDTH;PALLET_HEIGHT;PALLET_LENGTH;PALLET_LENGTH_HOLE;;");
                file.WriteLine("CS101;1500;1300;1750;700;;");
                file.WriteLine(";;;;;;");
                file.WriteLine(";;;;;;");
                file.WriteLine(";;;;;;");
                file.WriteLine("LIST;VALUE;SLEEP;SYNC;;;");

                Param item;
                for (int i = 0; i < paramListView.Items.Count; i++)
                {
                    item = (Param)paramListView.Items[i];
                    file.WriteLine("{0}", item.param_name + ";" + item.param_value + ";" + item.param_sleep + ";" + item.param_sync + ";;;");
                }

            }

            MessageBox.Show(filename + " 수정 완료.");

        }

        private void BtnDeleteWorkList_Click(object sender, RoutedEventArgs e)
        {
            string strFile = wlf.fileName;
            string strPath = Globals.WORK_SettingDirPath + @"\" + strFile + ".csv";

            bool result = File.Exists(strPath); 
            if (result == true)
            {
                File.Delete(strPath);
                MessageBox.Show(wlf.fileName + "삭제 완료.");

                GetWorklist();
            }
            else
            {
                MessageBox.Show("파일을 찾을 수 없습니다.");
            }
        }

        private void BtnAddWorkParam_Click(object sender, RoutedEventArgs e)
        {
            DialogWorklistAddParam dialogWorklistAddParam = new DialogWorklistAddParam();
            if (dialogWorklistAddParam.ShowDialog() == true)
            {
            }

        }

        private void BtnEditWorkParam_Click(object sender, RoutedEventArgs e)
        {
            if (paramListView.SelectedItems.Count > 0)
            {
                DialogWorklistEditParam dialogWorklistEditParam = new DialogWorklistEditParam();
                if (dialogWorklistEditParam.ShowDialog() == true)
                {
                }

            }
            else
                MessageBox.Show("파라미터를 선택해야 합니다.");

        }
        private void BtnDelWorkParam_Click(object sender, RoutedEventArgs e)
        {
            if (paramListView.SelectedItems.Count > 0)
            {
                int index = paramListView.SelectedIndex;
                param.RemoveAt(index);

                paramListView.Items.Refresh();
            }
            else
                MessageBox.Show("파라미터를 선택해야 합니다.");
        }

        private void paramListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (paramListView.SelectedItems.Count > 0)
            {
                pr = (Param)paramListView.SelectedItems[0];

                Globals.strWL_PR_NAME = pr.param_name;
                Globals.strWL_PR_VALUE = pr.param_value;
                Globals.strWL_PR_SLEEP = pr.param_sleep;
                Globals.strWL_PR_SYNC = pr.param_sync;
            }
        }

        private void BtnIndexUpWorkParam_Click(object sender, RoutedEventArgs e)
        {
            int index = paramListView.SelectedIndex;

            Param item = (Param)paramListView.Items[index];
            if (index > 0)
            {
                param.RemoveAt(index);
                param.Insert(index - 1, item);
                paramListView.Items.Refresh();
            }
        }

        private void BtnIndexDownWorkParam_Click(object sender, RoutedEventArgs e)
        {
            int index = paramListView.SelectedIndex;

            Param item = (Param)paramListView.Items[index];
            if (index < paramListView.Items.Count - 1)
            {
                param.RemoveAt(index);
                param.Insert(index + 1, item);
                paramListView.Items.Refresh();
            }
        }

        private void BtnSensorCheck_Click(object sender, RoutedEventArgs e)
        {
            //List<PathDataAFL> serialPathData = SerialPortBoard.CreatePathSerial(Astar.StartAstarAFL(6520, -467, 0, 60));
            //SerialPortBoard.SendPathSerial(serialPathData);
        }

        private void BtnManualComplete_Click(object sender, RoutedEventArgs e)
        {
            if (MqttClientHyundai.IsConnected())
            {
                Globals.nManualComplete = 1;
                MessageBox.Show("매뉴얼 컴플리트 송신 완료");
                logger.Info("   Manual Complete Button Click");
            }
            else if (!MqttClientHyundai.IsConnected())
                MessageBox.Show("Mqtt 통신 연결 확인");
            else if (Globals.HMISystem.SystemMode == SystemMode.AUTO || Globals.HMISystem.SystemMode == SystemMode.SEMI_AUTO)
                MessageBox.Show("매뉴얼 모드 확인");
        }

        private void BtnVehicleLogUpdate_Click(object sender, RoutedEventArgs e)
        {
             SerialPortBoard.SendLogSerial(0x70, 5020);
        }

        private void BtnForceComplete_Click(object sender, RoutedEventArgs e)
        {
            if (MqttClientHyundai.IsConnected())
            {
                Globals.nForceComplete = 1;
                MessageBox.Show("강제 완료 신호 송신 완료");
                logger.Info("   Force Complete Button Click");
            }
            else if (!MqttClientHyundai.IsConnected())
                MessageBox.Show("Mqtt 통신 연결 확인");
            else if (Globals.HMISystem.SystemMode == SystemMode.AUTO || Globals.HMISystem.SystemMode == SystemMode.SEMI_AUTO)
                MessageBox.Show("매뉴얼 모드 확인");

        }

        private void IgnoreACS_Click(object sender, RoutedEventArgs e)
        {
            Globals.IgnoreACS = false;
            MessageBox.Show("MQTT연결 무시.");
        }
    }
}
