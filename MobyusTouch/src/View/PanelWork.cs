﻿
using LumenWorks.Framework.IO.Csv;
using MobyusTouch.src.Global;
using NLog;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace MobyusTouch
{
    public partial class PanelWork : Form
    {
        private static Logger logger = LogManager.GetLogger("PanelWork");

        //delegate void TimerEventFiredDelegate();
        //bool bReadWork = true;
        //System.Threading.Timer timer;

        public PanelWork()
        {
            InitializeComponent();
            /*
            workGridView.Columns.Add("Column", "");
            workGridView.Rows.Insert(0, "현재 작업이 존재하지 않습니다.");

            workGridView.Columns[0].Width = 240;
            workGridView.Font = new Font("Tahoma", 9, FontStyle.Regular);
            workGridView.ForeColor = Color.Black;

            timer = new System.Threading.Timer(SelectCurrentWorkCallback);
            timer.Change(0, 100);
            */
        }

        ~PanelWork()
        {
            //timer.Dispose();
        }

        /*
        private void SelectCurrentWorkCallback(object state)
        {
            if (InvokeRequired)
            {
                //BeginInvoke(new TimerEventFiredDelegate(SelectCurrentWork));
            }
        }

        int startCount = 0;
     
        private void SelectCurrentWork()
        {
            try
            {
                
                if (Globals.currentWork.workQueue != null && Globals.currentWork.workQueue.Count > 0)
                {
                    if (bReadWork)
                    {
                        startCount = 10;
                        bReadWork = false;

                        workGridView.Columns.Clear();
                        workGridView.DataSource = Globals.currentWork.workDataTable;

                        workGridView.Columns[0].Width = 240;
                        workGridView.Columns[1].Width = 70;
                        workGridView.Columns[2].Width = 70;
                        workGridView.Columns[3].Width = 70;
                        workGridView.Columns[4].Width = 70;
                        workGridView.Columns[5].Width = 70;
                        workGridView.Columns[6].Width = 70;
                        workGridView.Columns[7].Width = 70;
                        workGridView.Columns[8].Width = 70;
                        workGridView.Columns[9].Width = 70;
                        workGridView.Columns[10].Width = 70;
                        workGridView.Columns[11].Width = 70; 
                        workGridView.Columns[12].Width = 70;

                        workGridView.Columns[0].HeaderText = "";
                        workGridView.Columns[1].HeaderText = "";
                        workGridView.Columns[2].HeaderText = "";
                        workGridView.Columns[3].HeaderText = "";
                        workGridView.Columns[4].HeaderText = "";
                        workGridView.Columns[5].HeaderText = "";
                        workGridView.Columns[6].HeaderText = "";
                        workGridView.Columns[7].HeaderText = "";
                        workGridView.Columns[8].HeaderText = "";
                        workGridView.Columns[9].HeaderText = "";
                        workGridView.Columns[10].HeaderText = "";
                        workGridView.Columns[11].HeaderText = "";
                        workGridView.Columns[12].HeaderText = "";

                        workGridView.CurrentCell = workGridView.Rows[10].Cells[0];
                    }

                    // START QUEUE
                    WorkStep workStep = Globals.workStep;

                    if (workGridView.Rows[startCount].Cells[0].Value != null && "".Equals(workGridView.Rows[startCount].Cells[4].Value.ToString().Trim()) && Globals.workStep.name.Equals(workGridView.Rows[startCount].Cells[0].Value.ToString().Trim()))
                    {
                        //Console.WriteLine("START: " + workStep.name);
                        workGridView.Rows[startCount].HeaderCell.Style.BackColor = Color.LightBlue;
                        workGridView.CurrentCell = workGridView.Rows[startCount].Cells[0];

                        workGridView.Rows[startCount].Cells[4].Value = true;
                        startCount++;
                    }
                }

                // END QUEUE
                if (Globals.currentWork.workQueueCompleted != null && Globals.currentWork.workQueueCompleted.Count > 0)
                {
                    WorkStep workStep = Globals.currentWork.workQueueCompleted.Dequeue();
                    
                    for (int i = 0; i < workGridView.Rows.Count - 10; i++)
                    {
                        if (workGridView.Rows[i + 10].Cells[0].Value != null && workStep.name.Equals(workGridView.Rows[i + 10].Cells[0].Value.ToString().Trim()))
                        {
                            workGridView.Rows[i + 10].Cells[5].Value = true;
                            workGridView.Rows[i + 10].Cells[6].Value = Globals.currentWork.curNode;
                            workGridView.Rows[i + 10].Cells[7].Value = Globals.VehicleSystem.CurrentSpeed / 250;
                            workGridView.Rows[i + 10].Cells[8].Value = Globals.VehicleSystem.CurrentAngle;
                            workGridView.Rows[i + 10].Cells[9].Value = Globals.ForkLiftSystem.LiftPosition;
                            workGridView.Rows[i + 10].Cells[10].Value = Globals.ForkLiftSystem.TiltPosition;
                            workGridView.Rows[i + 10].Cells[11].Value = (Globals.ForkLiftSystem.ShiftRightPosition - Globals.ForkLiftSystem.ShiftLeftPosition) / 2;
                            workGridView.Rows[i + 10].Cells[12].Value = Globals.ForkLiftSystem.MoverPosition;

                            workGridView.Refresh();
                        }
                    }   
                }

                // Complete
                if ((Globals.currentWork.workQueue == null || Globals.currentWork.workQueueCompleted== null) && bReadWork == false)
                {
                    bReadWork = true;
                }
            }
            catch (Exception ex)
            {
                logger.Error (ex.Message);
            }
        }
        */
    }
}