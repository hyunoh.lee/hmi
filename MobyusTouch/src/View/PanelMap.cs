﻿
//using MapDLL;
using EditorLibrary;
using MobyusTouch.src.Global;
using MobyusTouch.Src.Utility;
using MobyusTouch.src.Global.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;
using NLog;

namespace MobyusTouch
{
    public partial class PanelMap : Form
    {

        // 화면조정 관련
        private short radius = 700;
        public ZoomInfo Zoom_data;
        Graphics g;

        // 링크와 노드들에 관한 정보를 저장하려고 만든 리스트
        List<NodeInfo> Node_Data = new List<NodeInfo>();
        List<LinkInfo> Link_Data = new List<LinkInfo>();
        List<Point> Gridmap_Data = new List<Point>();

        private static Logger logger = LogManager.GetLogger("PanelMap");
        // 디스플레이 관련 플래그
        bool Display_node_num_flag = false;
        bool Display_link_num_flag = false;
        bool Display_transfer_flag = false;
        private bool MouseLbuttonFlag = false;
        private bool bTrackingCurrentScreen = false;
        private bool bOnlyOneTracking = true;

        // Path 관련 함수
        //Astar Astar_path = new Astar();
        public List<AstarNodeData> astar_path = new List<AstarNodeData>();
        private int path_start_node = -1;

        //forkflit 이미지
        Bitmap forklfit = new Bitmap(Properties.Resources.forklift, new Size(35, 25));

        delegate void TimerEventFiredDelegate();

        //맵 드로잉
        private static Pen pnDrawLine = new Pen(Color.FromArgb(50, 0, 0, 0));
        private static Pen pnDrawNode = new Pen(Color.FromArgb(255, 0, 0, 0), 1);
        private static Pen pnDrawNode_Red = new Pen(Color.FromArgb(255, 255, 0, 0), 1);
        private static Pen pnDrawLink = new Pen(Color.FromArgb(255, 0, 0, 0), 3);

        private static Brush blckBrush_Red = new SolidBrush(Color.Red);
        private static FontFamily familyname = new FontFamily("Calibri");
        private static Font myFont = new Font(familyname, 12, FontStyle.Bold, GraphicsUnit.Pixel);
        private static Brush blckBrush_Blue = new SolidBrush(Color.Blue);

        //링크 드로잉 
        private static Pen link_pen = new Pen(Color.FromArgb(255, 0, 0, 0), 3);
        private static Point link_RP_start = new Point();
        private static Point link_RP_end = new Point();
        private static Point link_DP_start = new Point();
        private static Point link_DP_End = new Point();
        private static Point link_DP = new Point();
        private static Point link_Base_P = new Point();
        private static SolidBrush link_solidBrush = new SolidBrush(Color.FromArgb(255, 255, 0, 0));
        private static Pen link_pen_ = new Pen(Color.FromArgb(255, 255, 0, 0), 2);
        private static Point link_Draw = new Point();
        private static Point link_Rect_P = new Point();
        private static int link_Radius = 0;
        private static float link_radiusWidth = 0;
        private static float link_radiusHeight = 0;


        //노드 드로잉
        private static Point node_RP = new Point();
        private static Point node_tmp = new Point();
        private static Byte node_type = 0;

        //노드 넘버 드로잉 
        private static Point nodeNum_DP = new Point();
        private static Point nodeNum_RP = new Point();
        private static Point nodeNum_Draw = new Point();

        //링크 넘버 드로잉
        private static Point linkNum_DP1 = new Point();
        private static Point linkNum_DP2 = new Point();
        private static Point linkNum_RP1 = new Point();
        private static Point linkNum_RP2 = new Point();
        private static Point linkNum_Draw = new Point();

        //지게차 위치 드로잉
        private static Point transfer_RP = new Point();
        private static Point transfer_tmp = new Point();
        private static Rectangle transfer_rec = new Rectangle();
        private static Pen red_pen = new Pen(Color.FromArgb(255, 0, 255, 0), 2);

        public PanelMap()
        {
            InitializeComponent();

            //맵 드로잉
            pnDrawLine.DashStyle = DashStyle.DashDot;
            pnDrawLink.StartCap = LineCap.ArrowAnchor;
            pnDrawLink.EndCap = LineCap.RoundAnchor;
            myFont = new Font(familyname, 12, FontStyle.Bold, GraphicsUnit.Pixel);

            // 현재 화면 배율 계산 및 적용
            Map_pictureBox.MouseWheel += new MouseEventHandler(pictureBox_MouseWheel); // 마우스 휠 이벤트 추가  
            Map_pictureBox.Invalidate();
            Map_pictureBox.BackColor = Color.Transparent;
            Zoom_data = new ZoomInfo(Map_pictureBox.Size.Width, Map_pictureBox.Size.Height);

            // 더블버퍼 적용
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            UpdateStyles();

            // Map 열기
            Button mapOpen = new Button();
            mapOpen.ForeColor = Color.Black;
            mapOpen.Name = "MapOpen";
            mapOpen.Text = "맵 열기";
            mapOpen.Size = new Size(80, 30);
            mapOpen.Parent = Map_pictureBox;
            mapOpen.FlatStyle = FlatStyle.Standard;
            mapOpen.Location = new Point(20, 20);
            mapOpen.BackColor = Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            mapOpen.FlatAppearance.BorderSize = 0;
            mapOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            mapOpen.Font = new Font("Calibri", 9F, FontStyle.Bold);
            mapOpen.ForeColor = Color.White;
            mapOpen.Click += new EventHandler(MapOpen);


            // Transfer 표시 CheckBox 생성 
            CheckBox transferCheckBox = new CheckBox();
            transferCheckBox.Name = "nodeCheckBox";
            transferCheckBox.Text = "Transfer";
            transferCheckBox.Size = new Size(90, 25);
            transferCheckBox.Parent = Map_pictureBox;
            transferCheckBox.FlatStyle = FlatStyle.Flat;
            transferCheckBox.Location = new Point(130, 20);
            transferCheckBox.Checked = true;
            transferCheckBox.ForeColor = Color.Black;
            transferCheckBox.CheckedChanged += new EventHandler(ShowTransfer);
            Display_transfer_flag = true;

            // Node 표시 CheckBox 생성 
            CheckBox nodeCheckBox = new CheckBox();
            nodeCheckBox.Name = "nodeCheckBox";
            nodeCheckBox.Text = "Node";
            nodeCheckBox.Size = new Size(70, 25);
            nodeCheckBox.Parent = Map_pictureBox;
            nodeCheckBox.FlatStyle = FlatStyle.Flat;
            nodeCheckBox.Location = new Point(220, 20);
            nodeCheckBox.Checked = true;
            nodeCheckBox.ForeColor = Color.Black;
            nodeCheckBox.CheckedChanged += new EventHandler(ShowNode);
            Display_node_num_flag = true;

            // Link 표시 CheckBox 생성 
            CheckBox linkCheckBox = new CheckBox();
            linkCheckBox.Name = "linkCheckBox";
            linkCheckBox.Text = "Link";
            linkCheckBox.Size = new Size(70, 25);
            linkCheckBox.Parent = Map_pictureBox;
            linkCheckBox.FlatStyle = FlatStyle.Flat;
            linkCheckBox.Location = new Point(290, 20);
            linkCheckBox.Checked = false;
            linkCheckBox.ForeColor = Color.Black;
            linkCheckBox.CheckedChanged += new EventHandler(ShowLink);
            Display_link_num_flag = false;

            Zoom_data.InitZoomInfo();
            Map_pictureBox.Refresh();
            Node_Data.Clear();
            Link_Data.Clear();

            System.Threading.Timer timer = new System.Threading.Timer(GetNavAndPalletDataCallback);
            timer.Change(0, 800);

            System.Threading.Timer timer2 = new System.Threading.Timer(MapRefreshCallback);
            timer2.Change(0, 2000);

            try
            {
                String fileName = Globals.MAP_SettingFilePath;
                if (File.Exists(fileName))
                {
                    Stream stream = File.Open(fileName, FileMode.Open);
                    BinaryFormatter formatter = new BinaryFormatter();

                    Globals.MapData = (SaveData)formatter.Deserialize(stream);
                    Globals.MapData.LoadMapData(ref Node_Data, ref Link_Data);
                    Astar.GetMapData(Node_Data, Link_Data);

                    if (Globals.NavigationSystem.NAV_X != 0 && Globals.NavigationSystem.NAV_Y != 0)
                        Zoom_data.InitZoomInfo(Map_pictureBox.Size.Width, Map_pictureBox.Size.Height, Node_Data, checked((int)Globals.NavigationSystem.NAV_X), checked((int)Globals.NavigationSystem.NAV_Y));
                    else
                        Zoom_data.InitZoomInfo(Map_pictureBox.Size.Width, Map_pictureBox.Size.Height, Node_Data);

                    Globals.nodeData = Node_Data;
                    Globals.linkData = Link_Data;
                    //for (int i = 0; i < 3; i++)
                    //    Zoom_data.IncreaseRatio();

                    Map_pictureBox.Invalidate();
                    Map_pictureBox.Update();
                    stream.Close();
                }

                //for (int i = 0; i < Globals.nodeData.Count; i++)
                //{
                //    Console.WriteLine("Node : " + Global.nodeData[i].GetNodeNumber() +  " X : " + Global.nodeData[i].GetPoint().X + " Y: " + Global.nodeData[i].GetPoint().Y);
                //}


            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        //private static ushort find_current_node(System.Drawing.Point RP, ref double distance, ref ushort node_num)
        //{
        //    double min_distance = double.MaxValue;
        //    ushort curr_node = 0;

        //    for (int i = 0; i < Globals.nodeData.Count; i++)
        //    {
        //        System.Drawing.Point tmpF = Globals.nodeData[i].GetNodePoint();
        //        distance = Math.Sqrt((RP.X - tmpF.X) * (RP.X - tmpF.X) + (RP.Y - tmpF.Y) * (RP.Y - tmpF.Y));
        //        if ((min_distance > distance) && (Globals.nodeData[i].GetNodeNumber() != node_num))
        //        {
        //            curr_node = Globals.nodeData[i].GetNodeNumber();
        //            min_distance = distance;

        //            Console.WriteLine(" NodeNum : " + curr_node + " distance : " + distance);
        //        }
        //    }
        //    return curr_node;
        //}

        private void GetNavAndPalletDataCallback(object state)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new TimerEventFiredDelegate(GetNavAndIPalletData));
            }
        }

        private void MapRefreshCallback(object state)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new TimerEventFiredDelegate(MapRefresh));
            }
        }

        private void GetNavAndIPalletData()
        {
            WorkEntity workCommand = new WorkEntity();

            // 1129 kdy 현재 acs 명령 시각화 하기
            
            OrderBtn.Text = Globals.tmp_text;
            LayerBtn.Text = Globals.NavigationSystem.NAVLayer.ToString();
            CurrentNodeBtn.Text = Globals.currentWork.curNode.ToString();
            CurrentLinkBtn.Text = Globals.currentWork.curLink.ToString();

            TargetNodeBtn.Text = Globals.currentWork.toNode.ToString();
            button12.Text = Globals.currentWork.toNode.ToString();
            NextNodeBtn.Text = Globals.currentWork.nextNode.ToString();

            NavXBtn.Text = Globals.NavigationSystem.NAV_X.ToString();
            NavYBtn.Text = Globals.NavigationSystem.NAV_Y.ToString();
            NavPHIBtn.Text = Globals.NavigationSystem.NAV_PHI.ToString();

            StackingXBtn.Text = Globals.RecognitionSystem.TargetX.ToString();
            StackingYBtn.Text = Globals.RecognitionSystem.TargetY.ToString();
            StackingThetaBtn.Text = Globals.RecognitionSystem.TargetTheta.ToString();

            PalletXBtn.Text = Globals.RecognitionSystem.FrontPalletX.ToString();
            PalletYBtn.Text = Globals.RecognitionSystem.FrontPalletY.ToString();
            PalletThetaBtn.Text = Globals.RecognitionSystem.FrontPalletTheta.ToString();

        }
        private void MapRefresh()
        {
            Map_pictureBox.Invalidate();
        }

        private void Map_pictureBox_Paint(object sender, PaintEventArgs e)
        {
            g = e.Graphics;
            MapDrawLine();
            MapDrawNode();
            MapDrawLink();
            MapDrawNodeNumber();
            MapDrawLinkNumber();
            MapDrawTransfer();

        }

        private void PathPaintOnMap(object sender, EventArgs e)
        {
            if (Globals.currentWork.realPath.Count > 0)
            {
                Pen path_pen = new Pen(Color.Red, 5);

                int start_node = Globals.currentWork.realPath[Globals.currentWork.realPath.Count - 1].GetCurrentNodeID();

                while (start_node != path_start_node)
                {
                    for (int i = 0; i < Globals.currentWork.realPath.Count; i++)
                    {
                        if (start_node == Globals.currentWork.realPath[i].GetCurrentNodeID())
                        {

                            if (Globals.currentWork.realPath[i].GetLinkType() == 1 || Globals.currentWork.realPath[i].GetLinkType() == 2)
                            {
                                for (int k = 0; k < Link_Data.Count; k++)
                                {
                                    if (Globals.currentWork.realPath[i].GetLinkNumber() == Link_Data[k].GetLinkNumber())
                                    {
                                        Point DP = new Point();
                                        Point Base_P = Link_Data[k].GetBasePoint();
                                        Zoom_data.RPtoDP(Base_P, ref DP);
                                        //SolidBrush solidBrush = new SolidBrush(Color.FromArgb(255, 255, 0, 0));
                                        //g.FillRectangle(solidBrush, DP.X - 5, DP.Y - 5, 15, 15);
                                        Point Draw = new Point();
                                        Point Rect_P = new Point();
                                        Rect_P.X = Base_P.X - (int)radius;
                                        Rect_P.Y = Base_P.Y + (int)radius;
                                        Zoom_data.RPtoDP(Rect_P, ref Draw);

                                        g.DrawArc(path_pen, Draw.X, Draw.Y, (int)((radius * 2) * Zoom_data.GetFScale()), (int)((radius * 2) * Zoom_data.GetFScale()), Link_Data[k].GetCurveStartAng(), (Link_Data[k].GetCurveSweepAng()));
                                    }
                                }
                            }
                            else
                            {
                                Point RP_start = Node_Data[find_node(Globals.currentWork.realPath[i].GetCurrentNodeID())].GetNodePoint();
                                Point RP_end = Node_Data[find_node(Globals.currentWork.realPath[i].GetParentNodeID())].GetNodePoint();
                                Point DP_start = new Point();
                                Point DP_End = new Point();
                                Zoom_data.RPtoDP(RP_start, ref DP_start);
                                Zoom_data.RPtoDP(RP_end, ref DP_End);
                                g.DrawLine(path_pen, DP_start.X, DP_start.Y, DP_End.X, DP_End.Y);

                            }
                            start_node = Globals.currentWork.realPath[i].GetParentNodeID();

                            break;
                        }
                    }
                }
            }
        }

        private void Map_pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            Point Mouse = new Point();
            Point RP = new Point();
            Mouse.X = e.X;
            Mouse.Y = e.Y;

            if (MouseLbuttonFlag == true)
            {
                Zoom_data.MouseMoving(Mouse);
                Map_pictureBox.Invalidate();
                Map_pictureBox.Update();
            }

            Zoom_data.DPtoRP(Mouse, ref RP);
        }

        private void MapDrawLine()
        {
            int Xn = Map_pictureBox.Width;
            int Yn = Map_pictureBox.Height;


            for (int y = -3; y < Yn; y += 60)
            {
                g.DrawLine(pnDrawLine, 0, y, Xn, y);
            }
            for (int x = -3; x < Xn; x += 60)
            {
                g.DrawLine(pnDrawLine, x, 0, x, Yn);
            }
        }
        private void MapDrawNode()
        {
            if (Node_Data.Count > 0) // 노드 Draw
            {
                for (int i = 0; i < Node_Data.Count; i++)
                {
                    node_RP = Node_Data[i].GetNodePoint();

                    Zoom_data.RPtoDP(node_RP, ref node_tmp);
                    node_type = Node_Data[i].GetNodeType();

                    switch (node_type)
                    {
                        case 0:
                            var rec = new Rectangle((int)node_tmp.X - 5, (int)node_tmp.Y - 5, 10, 10);
                            g.DrawEllipse(pnDrawNode, rec);
                            break;
                        case 1:
                            rec = new Rectangle((int)node_tmp.X - 5, (int)node_tmp.Y - 5, 10, 10);
                            g.DrawRectangle(pnDrawNode, rec);
                            break;
                        case 2:
                            rec = new Rectangle((int)node_tmp.X - 5, (int)node_tmp.Y - 5, 10, 10);
                            g.DrawRectangle(pnDrawNode_Red, rec);
                            break;
                        case 3:
                            rec = new Rectangle((int)node_tmp.X - 5, (int)node_tmp.Y - 5, 10, 10);
                            g.DrawRectangle(pnDrawNode, rec);
                            break;
                    }
                }
            }

        }

        private void MapDrawLink()
        {
            if (Link_Data.Count > 0) // 링크 Draw 
            {
                for (int i = 0; i < Link_Data.Count; i++)
                {
                    link_pen.StartCap = LineCap.ArrowAnchor;
                    link_pen.EndCap = LineCap.RoundAnchor;

                    int start = Astar.FindNodeIndex(Link_Data[i].GetStartNodeNumber());
                    int end = Astar.FindNodeIndex(Link_Data[i].GetTargetNodeNumber());

                    link_RP_start = Node_Data[Astar.FindNodeIndex(Link_Data[i].GetStartNodeNumber())].GetNodePoint();
                    link_RP_end = Node_Data[Astar.FindNodeIndex(Link_Data[i].GetTargetNodeNumber())].GetNodePoint();

                    Zoom_data.RPtoDP(link_RP_start, ref link_DP_start);
                    Zoom_data.RPtoDP(link_RP_end, ref link_DP_End);

                    //lhw hmi에서 커브 직선으료 디스플레이
                   /* if ((Link_Data[i].GetLinkType() == 1 || Link_Data[i].GetLinkType() == 2) &&  Link_Data[i].GetLinkDistance() > 500)
                    {
                        //lhw 수정 후
                        link_Base_P = Link_Data[i].GetBasePoint();
                        Zoom_data.RPtoDP(link_Base_P, ref link_DP);
                        link_Radius = (int)Link_Data[i].GetCurveRadius();
                        link_Rect_P.X = link_Base_P.X - link_Radius;
                        link_Rect_P.Y = link_Base_P.Y + link_Radius;
                        Zoom_data.RPtoDP(link_Rect_P, ref link_Draw);

                        link_radiusWidth = (float)((link_Radius * 2) * Zoom_data.GetFScale());
                        link_radiusHeight = (float)((link_Radius * 2) * Zoom_data.GetFScale());


                        //if (link_radiusWidth < 10)
                        //    link_radiusWidth = 10;
                        //if (link_radiusHeight < 10)
                        //    link_radiusHeight = 10;

                        g.DrawArc(link_pen, link_Draw.X, link_Draw.Y, link_radiusWidth, link_radiusHeight, Link_Data[i].GetCurveStartAng(), Link_Data[i].GetCurveSweepAng());

                        //lhw 수정 전
                        //Point link_DP = new Point();
                        //Point link_Base_P = Link_Data[i].GetBasePoint();
                        //Zoom_data.RPtoDP(link_Base_P, ref link_DP);
                        //SolidBrush solidBrush = new SolidBrush(Color.FromArgb(255, 255, 0, 0));
                        //Pen pen_ = new Pen(Color.FromArgb(255, 255, 0, 0), 2);
                        //// g.DrawRectangle(pen_, DP.X - 5, DP.Y - 5, 10, 10);
                        //Point Draw = new Point();
                        //Point Rect_P = new Point();
                        //int Radius = (int)Link_Data[i].GetCurveRadius();
                        //Rect_P.X = link_Base_P.X - Radius;
                        //Rect_P.Y = link_Base_P.Y + Radius;
                        //Zoom_data.RPtoDP(Rect_P, ref Draw);

                        //float radiusWidth = (float)((Radius * 2) * Zoom_data.GetFScale());
                        //float radiusHeight = (float)((Radius * 2) * Zoom_data.GetFScale());

                        //if (Link_Data[i].GetLinkType() == 1 || Link_Data[i].GetLinkType() == 2)
                        //{
                        //    if (radiusWidth < 60)
                        //        radiusWidth = 60;
                        //    if (radiusHeight < 60)
                        //        radiusHeight = 60;
                        //}

                        //g.DrawArc(link_pen, Draw.X, Draw.Y, radiusWidth, radiusHeight, Link_Data[i].GetCurveStartAng(), Link_Data[i].GetCurveSweepAng());
                    }
                    else*/
                        g.DrawLine(link_pen, link_DP_End.X, link_DP_End.Y, link_DP_start.X, link_DP_start.Y);
                }
            }
        }
        private void MapDrawNodeNumber()
        {
            if (Display_node_num_flag) // 노드번호 맵 표시
            {
                for (int i = 0; i < Node_Data.Count; i++)
                {
                    nodeNum_RP = Node_Data[i].GetNodePoint();
                    Zoom_data.RPtoDP(nodeNum_RP, ref nodeNum_DP);

                    nodeNum_Draw = new Point((int)nodeNum_DP.X - (int)((double)100 * Zoom_data.GetRatio()), (int)nodeNum_DP.Y - (int)((double)200 * Zoom_data.GetRatio()));
                    g.DrawString(Node_Data[i].GetNodeNumber().ToString(), myFont, blckBrush_Red, nodeNum_Draw);

                }
            }
        }


        private void MapDrawLinkNumber()
        {
            if (Display_link_num_flag) // 링크번호 맵 표시
            {
                for (int i = 0; i < Link_Data.Count; i++)
                {                    
                    linkNum_RP1 = Node_Data[Astar.FindNodeIndex(Link_Data[i].GetStartNodeNumber())].GetNodePoint();
                    linkNum_RP2 = Node_Data[Astar.FindNodeIndex(Link_Data[i].GetTargetNodeNumber())].GetNodePoint();
                    Zoom_data.RPtoDP(linkNum_RP1, ref linkNum_DP1);
                    Zoom_data.RPtoDP(linkNum_RP2, ref linkNum_DP2);
                    linkNum_Draw = new Point((int)(linkNum_DP1.X + linkNum_DP2.X) / 2, (int)(linkNum_DP1.Y + linkNum_DP2.Y) / 2 - (int)((double)2500 * Zoom_data.GetRatio()));
                    g.DrawString(Link_Data[i].GetLinkNumber().ToString(), myFont, blckBrush_Blue, linkNum_Draw);
                }            
            }
        }
        private void MapDrawTransfer()
        {
            try
            {
                if (Display_transfer_flag) // 현재 지게차 표시
                {
                    transfer_RP = new Point(checked((int)Globals.NavigationSystem.NAV_X), checked((int)Globals.NavigationSystem.NAV_Y));
                    Zoom_data.RPtoDP(transfer_RP, ref transfer_tmp);
                    transfer_rec = new Rectangle((int)transfer_tmp.X - 7, (int)transfer_tmp.Y - 7, 18, 18);
                    g.DrawRectangle(red_pen, transfer_rec);
                                
                    if (Globals.NavigationSystem.NAV_X != 0 && Globals.NavigationSystem.NAV_Y != 0)
                        bTrackingCurrentScreen = true;

                    if (bTrackingCurrentScreen && bOnlyOneTracking)
                    {
                        bOnlyOneTracking = false;
                        bTrackingCurrentScreen = false;
                        Zoom_data.InitZoomInfo(Map_pictureBox.Size.Width, Map_pictureBox.Size.Height, Node_Data, checked((int)Globals.NavigationSystem.NAV_X), checked((int)Globals.NavigationSystem.NAV_Y));

                        Map_pictureBox.Invalidate();
                        Map_pictureBox.Update();
                    }

                    /*
                    g.TranslateTransform((int)tmp.X, (int)tmp.Y); 
                    g.RotateTransform(Globals.NavigationSystem.NAV_PHI * -1);
                    g.DrawImage(forklfit, new PointF(0, 0));
                     */

                }
            }
            catch (Exception e)
            {
                logger.Error("   " + e.ToString());
                Globals.currentWork.alarm = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1알 수 없 는 /C1  에    러 !]";
            }

        }

        public int find_node(int node)
        {
            int node_n = 0;

            for (int i = 0; i < Node_Data.Count; i++)
            {
                if (Node_Data[i].GetNodeNumber() == node)
                    node_n = i;

            }

            return node_n;


        }

        public int find_link(int link)
        {
            int link_n = 0;

            for (int i = 0; i < Link_Data.Count; i++)
            {
                if (Link_Data[i].GetLinkNumber() == link)
                    link_n = i;

            }

            return link_n;
        }


        private void create_curve(int Link_num)
        {

            Point Start = Node_Data[Link_Data[Link_num].GetStartNodeNumber()].GetNodePoint();
            Point End = Node_Data[Link_Data[Link_num].GetTargetNodeNumber()].GetNodePoint();

            int start_num = 0;
            int end_num = 0;

            for (int i = 0; i < Link_Data.Count; i++)
            {
                if (Link_Data[Link_num].GetStartNodeNumber() == Link_Data[i].GetTargetNodeNumber())
                {
                    start_num = i;
                    break;
                }
            }
            for (int i = 0; i < Link_Data.Count; i++)
            {
                if (Link_Data[Link_num].GetTargetNodeNumber() == Link_Data[i].GetStartNodeNumber())
                {
                    end_num = i;
                    break;
                }
            }
            LinkInfo Start_Link = Link_Data[start_num];
            LinkInfo End_Link = Link_Data[end_num];

            // 두 링크의 직선방정식을 통해 교차점을 계산

            Point AP1, AP2, BP1, BP2;

            AP1 = Node_Data[Start_Link.GetStartNodeNumber()].GetNodePoint();
            AP2 = Node_Data[Start_Link.GetTargetNodeNumber()].GetNodePoint();
            BP1 = Node_Data[End_Link.GetStartNodeNumber()].GetNodePoint();
            BP2 = Node_Data[End_Link.GetTargetNodeNumber()].GetNodePoint();

            Point Base_P = new Point();
            Point Start_P = new Point();
            Point End_P = new Point();

            //Find_Base_Point(AP1, AP2, BP1, BP2, Start_Link, End_Link, ref Start_P, ref End_P, ref Base_P); // 교차점
        } // 커브생성

        private void Map_pictureBox_SizeChanged(object sender, EventArgs e)
        {
            Zoom_data.ChangeImageCenter(Map_pictureBox.Width, Map_pictureBox.Height);
            Map_pictureBox.Invalidate();
            Map_pictureBox.Update();
        }

        private void pictureBox_MouseWheel(object sender, MouseEventArgs e) // 맵 화면 확대 축소 이벤트 
        {

            Map_pictureBox.Refresh();

            int lines = e.Delta * SystemInformation.MouseWheelScrollLines / 120;
            PictureBox pb = (PictureBox)sender;

            Point Mouse = new Point();
            Mouse.X = e.X;
            Mouse.Y = e.Y;

            double dRatio = Zoom_data.GetRatio();


            if (lines > 0)
            {
                Zoom_data.IncreaseRatio();
            }
            else if (lines < 0 /*&& dRatio >= 0.03*/)
            {
                Zoom_data.DecreaseRatio();
            }

            Zoom_data.CalculateRPOffSet(Mouse);
            Map_pictureBox.Invalidate();
            Map_pictureBox.Update();


        }
        private void ShowCurrentTransfer()
        {
        }

        private void ShowTransfer(object sender, EventArgs e) // 링크 표시
        {
            if (Display_transfer_flag)
                Display_transfer_flag = false;
            else
                Display_transfer_flag = true;


            Map_pictureBox.Invalidate();
            Map_pictureBox.Update();
        }

        private void ShowLink(object sender, EventArgs e) // 링크 표시
        {
            if (Display_link_num_flag)
                Display_link_num_flag = false;
            else
                Display_link_num_flag = true;


            Map_pictureBox.Invalidate();
            Map_pictureBox.Update();
        }

        private void ShowNode(object sender, EventArgs e) // 노드 표시
        {
            if (Display_node_num_flag)
                Display_node_num_flag = false;
            else
                Display_node_num_flag = true;

            Map_pictureBox.Invalidate();
            Map_pictureBox.Update();
        }



        private void MapOpen(object sender, EventArgs e)
        {
            try
            {
                String FileName;
                OpenFileDialog OpenFiledlg = new OpenFileDialog();
                OpenFiledlg.DefaultExt = "dat";
                OpenFiledlg.Filter = "Data File|*.dat";
                if (OpenFiledlg.ShowDialog() == DialogResult.OK)
                {
                    FileName = OpenFiledlg.FileName;
                    Stream stream = File.Open(FileName, FileMode.Open);
                    BinaryFormatter formatter = new BinaryFormatter();
                    Globals.MapData = (SaveData)formatter.Deserialize(stream);
                    Globals.MapData.LoadMapData(ref Node_Data, ref Link_Data);
                    Astar.GetMapData(Node_Data, Link_Data);

                    Globals.nodeData = Node_Data;
                    Globals.linkData = Link_Data;

                    //Init_Listview();
                    //Zoom_data.SetImageRect(Node_Data);
                    if (Globals.NavigationSystem.NAV_X != 0 && Globals.NavigationSystem.NAV_Y != 0)
                        Zoom_data.InitZoomInfo(Map_pictureBox.Size.Width, Map_pictureBox.Size.Height, Node_Data, checked((int)Globals.NavigationSystem.NAV_X), checked((int)Globals.NavigationSystem.NAV_Y));
                    else
                        Zoom_data.InitZoomInfo(Map_pictureBox.Size.Width, Map_pictureBox.Size.Height, Node_Data);

                    //Update_node_listview();
                    //Update_link_listview();
                    Map_pictureBox.Invalidate();
                    Map_pictureBox.Update();
                    stream.Close();

                    SaveFileDialog sfd = new SaveFileDialog();
                    sfd.FileName = Globals.MAP_SettingFilePath;
                    System.IO.File.Copy(OpenFiledlg.FileName, sfd.FileName, true);

                    MessageBox.Show("읽기 완료!");
                }
            }
            catch(Exception ee)
            {
                MessageBox.Show(ee.ToString());
            }
        }

        private void Map_pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            MouseLbuttonFlag = false;
        }

        private void MovingWindow(Point In_)
        {
            if (MouseLbuttonFlag == false)
            {
                MouseLbuttonFlag = true;
                Zoom_data.SetMouseDownPoint(In_);
            }


        }

        private void Map_pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            Point Mouse = new Point();
            Mouse.X = e.X;
            Mouse.Y = e.Y;
            MovingWindow(Mouse);
        }

        public ushort FindNodeIndex(ushort node_)
        {
            int index = 0;
            for (int i = 0; i < Node_Data.Count; i++)
            {
                if (Node_Data[i].GetNodeNumber() == node_)
                    index = i;
            }
            return (ushort)index;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Zoom_data.InitZoomInfo(Map_pictureBox.Size.Width, Map_pictureBox.Size.Height, Node_Data, checked((int)Globals.NavigationSystem.NAV_X), checked((int)Globals.NavigationSystem.NAV_Y));
        }
    }
}
