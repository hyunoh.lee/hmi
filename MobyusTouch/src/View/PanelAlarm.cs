﻿using MobyusTouch.src.Global;
using MobyusTouch.src.Global.Entity;
using MobyusTouch.src.Utility;
using NLog;
using NLog.Config;
using NLog.Targets;
using NLog.Targets.Wrappers;
using NLog.Windows.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Threading;

namespace MobyusTouch
{
    public partial class PanelAlarm : Form
    {
        private static Logger logger = LogManager.GetLogger("PanelAlarm");

        delegate void TimerEventFiredDelegate();
        
        System.Threading.Timer workListTimer;
        System.Threading.Timer alarmListTimer;

        bool bReadWork = true;
        int startCount = 0;

        public PanelAlarm()
        {
            InitializeComponent();

            SetWorkConfig();
            SetAlarmLatestConfig();

            SetLoggingConfig();
            //SetLoggingConfigPanel();

            workListTimer = new System.Threading.Timer(SelectCurrentWorkCallback);
            workListTimer.Change(0, 500);

            alarmListTimer = new System.Threading.Timer(GetCurrentAlarmCallback);
            alarmListTimer.Change(0, 5000);

        }

        private void GetCurrentAlarm()
        {
            int counter = 0;
            try
            {
                if (File.Exists(@"c:\MOBYUS\logs\error.txt"))
                {
                    alarmGridView.Rows.Clear();
                    foreach (string line in System.IO.File.ReadLines(@"c:\MOBYUS\logs\error.txt", Encoding.Default).Reverse())
                    {
                        int length = line.Split('[').Length;

                        if(length > 8)
                        {                     
                            String error = line.Split('[')[1];
                            if (error.Substring(0, 5).Equals("ERROR"))
                            {
                                String date = line.Split('[')[0];
                                String workEntity = line.Split('[')[8];

                                String workType = workEntity.Split(':')[5];
                                workType = Regex.Replace(workType, @"[^0-9]", "");

                                String workLevel = workEntity.Split(':')[6];
                                workLevel = Regex.Replace(workLevel, @"[^0-9]", "");

                                //String curNode = workEntity.Split(':')[9];
                                //curNode = Regex.Replace(curNode, @"[^0-9]", "");

                                String toNode = workEntity.Split(':')[11];
                                toNode = Regex.Replace(toNode, @"[^0-9]", "");

                                String alarm = workEntity.Split(':')[17];
                                alarm = Regex.Replace(alarm, @"[^0-9]", "");

                                if (int.Parse(alarm) > 0) 
                                { 
                                    String alarmInfo = ((String)Globals.alarmTable[int.Parse(alarm)]).Split('|')[0];
                                    String alarmSolve = ((String)Globals.alarmTable[int.Parse(alarm)]).Split('|')[1];
                                
                                    alarmGridView.Rows.Insert(0, (counter + 1).ToString(), date, alarm, alarmInfo, workType, toNode, workLevel, alarmSolve);
                                    
                                    counter++;
                                }

                            }
                        }
                        if (counter == 10)
                        {
                            break;
                        }

                    }
                    List<DataGridViewRow> rows = new List<DataGridViewRow>();
                    rows.AddRange(alarmGridView.Rows.Cast<DataGridViewRow>());
                    rows.Reverse();
                    alarmGridView.Rows.Clear();
                    alarmGridView.Rows.AddRange(rows.ToArray());
                    alarmGridView.Rows.RemoveAt(0);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ex: " + ex.StackTrace);
            }
        }

        private void AlarmCellClick(object sender, DataGridViewCellEventArgs e)
        {           
            if(alarmGridView[1, e.RowIndex].Value != null) 
            {
                WorkEntity workEntity = new WorkEntity();
                workEntity.workStartTime = alarmGridView[1, e.RowIndex].Value.ToString();
                workEntity.alarm = int.Parse(alarmGridView[2, e.RowIndex].Value.ToString());
                workEntity.workType = int.Parse(alarmGridView[4, e.RowIndex].Value.ToString());
                workEntity.toNode = ushort.Parse(alarmGridView[5, e.RowIndex].Value.ToString());
                workEntity.workLevel = int.Parse(alarmGridView[6, e.RowIndex].Value.ToString());

                DialogEMO dialogEMO = new DialogEMO(workEntity);
                dialogEMO.ShowDialog();
            }
        }

        private void SetAlarmLatestConfig()
        {
            alarmGridView.Columns.Add("Column", "번호");
            alarmGridView.Columns.Add("Column", "일시");
            alarmGridView.Columns.Add("Column", "코드번호");
            alarmGridView.Columns.Add("Column", "알람명");
            alarmGridView.Columns.Add("Column", "작업");
            alarmGridView.Columns.Add("Column", "목적지");
            alarmGridView.Columns.Add("Column", "다단높이");
            alarmGridView.Columns.Add("Column", "조치사항");

            alarmGridView.Columns[0].Width = 50;
            alarmGridView.Columns[1].Width = 180;
            alarmGridView.Columns[2].Width = 70;
            alarmGridView.Columns[3].Width = 160;
            alarmGridView.Columns[4].Width = 70;
            alarmGridView.Columns[5].Width = 70;
            alarmGridView.Columns[6].Width = 70;
            alarmGridView.Columns[7].Width = 340;

            alarmGridView.Font = new Font("Tahoma", 9, FontStyle.Regular);
            alarmGridView.ForeColor = Color.Black;

        }

        private void SetWorkConfig()
        {
            workGridView.Columns.Add("Column", "");
            workGridView.Rows.Insert(0, "현재 작업이 존재하지 않습니다.");

            workGridView.Columns[0].Width = 240;
            workGridView.Font = new Font("Tahoma", 9, FontStyle.Regular);
            workGridView.ForeColor = Color.Black;
        }

        private void SelectCurrentWork()
        {
            try
            {
                if (Globals.currentWork.workQueue != null && Globals.currentWork.workQueue.Count > 0)
                {
                    if (bReadWork)
                    {
                        startCount = 10;
                        bReadWork = false;

                        workGridView.Columns.Clear();
                        workGridView.DataSource = Globals.currentWork.workDataTable;

                        workGridView.Columns[0].Width = 240;
                        workGridView.Columns[1].Width = 70;
                        workGridView.Columns[2].Width = 70;
                        workGridView.Columns[3].Width = 70;
                        workGridView.Columns[4].Width = 70;
                        workGridView.Columns[5].Width = 70;
                        workGridView.Columns[6].Width = 70;
                        workGridView.Columns[7].Width = 70;
                        workGridView.Columns[8].Width = 70;
                        workGridView.Columns[9].Width = 70;
                        workGridView.Columns[10].Width = 70;
                        workGridView.Columns[11].Width = 70;
                        workGridView.Columns[12].Width = 70;

                        workGridView.Columns[0].HeaderText = "";
                        workGridView.Columns[1].HeaderText = "";
                        workGridView.Columns[2].HeaderText = "";
                        workGridView.Columns[3].HeaderText = "";
                        workGridView.Columns[4].HeaderText = "";
                        workGridView.Columns[5].HeaderText = "";
                        workGridView.Columns[6].HeaderText = "";
                        workGridView.Columns[7].HeaderText = "";
                        workGridView.Columns[8].HeaderText = "";
                        workGridView.Columns[9].HeaderText = "";
                        workGridView.Columns[10].HeaderText = "";
                        workGridView.Columns[11].HeaderText = "";
                        workGridView.Columns[12].HeaderText = "";

                        workGridView.CurrentCell = workGridView.Rows[10].Cells[0];
                    }

                    // START QUEUE
                    WorkStep workStep = Globals.workStep;

                    if (workGridView.Rows[startCount].Cells[0].Value != null && "".Equals(workGridView.Rows[startCount].Cells[4].Value.ToString().Trim()) && Globals.workStep.name.Equals(workGridView.Rows[startCount].Cells[0].Value.ToString().Trim()))
                    {
                        //Console.WriteLine("START: " + workStep.name + " startCount : " + startCount);
                        workGridView.Rows[startCount].HeaderCell.Style.BackColor = Color.LightBlue;
                        workGridView.CurrentCell = workGridView.Rows[startCount].Cells[0];

                        workGridView.Rows[startCount].Cells[4].Value = true;

                        workGridView.Refresh();

                        startCount++;
                    }
                }

                // END QUEUE
                if (Globals.currentWork.workQueueCompleted != null && Globals.currentWork.workQueueCompleted.Count > 0)
                {
                    WorkStep workStep = Globals.currentWork.workQueueCompleted.Dequeue();

                    for (int i = 0; i < workGridView.Rows.Count - 10; i++)
                    {
                        if (workGridView.Rows[i + 10].Cells[0].Value != null 
                            && !"".Equals(workGridView.Rows[i + 10].Cells[6].Value) 
                            && workStep.name.Equals(workGridView.Rows[i + 10].Cells[0].Value.ToString().Trim()))
                        {
                            workGridView.Rows[i + 10].Cells[5].Value = true;
                            workGridView.Rows[i + 10].Cells[6].Value = Globals.currentWork.curNode;
                            workGridView.Rows[i + 10].Cells[7].Value = Globals.VehicleSystem.CurrentSpeed / 250;
                            workGridView.Rows[i + 10].Cells[8].Value = Globals.VehicleSystem.CurrentAngle;
                            workGridView.Rows[i + 10].Cells[9].Value = Globals.ForkLiftSystem.LiftPosition;
                            workGridView.Rows[i + 10].Cells[10].Value = Globals.ForkLiftSystem.TiltPosition;
                            workGridView.Rows[i + 10].Cells[11].Value = (Globals.ForkLiftSystem.ShiftRightPosition - Globals.ForkLiftSystem.ShiftLeftPosition) / 2;
                            workGridView.Rows[i + 10].Cells[12].Value = Globals.ForkLiftSystem.MoverPosition;

                            workGridView.Refresh();
                        }
                    }
                }

                // Complete
                if ((Globals.currentWork.workQueue == null || Globals.currentWork.workQueueCompleted == null) && bReadWork == false)
                {
                    bReadWork = true;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
        }

        public void SetLoggingConfig()
        {
            LoggingConfiguration config = LogManager.Configuration;
            if (config == null)
            {
                config = new LoggingConfiguration();
            }

            // INFO
            {
                NLog.Targets.FileTarget fileTarget = new NLog.Targets.FileTarget
                {
                    FileName = @"C:\MOBYUS\logs\${shortdate}.txt",
                    Layout = @"${time} [${level:uppercase=false:padding=-5}] [${callsite:includeNamespace=false}] - ${message} ${exception:format=tostring}",
                    KeepFileOpen = false,
                    ArchiveDateFormat = "yyyy-MM-dd",
                    ArchiveFileName = @"C:\MOBYUS\logs\{#}.txt",
                    ArchiveNumbering = ArchiveNumberingMode.Date,
                    ArchiveEvery = FileArchivePeriod.Day,
                    ArchiveAboveSize = 10000000,
                    MaxArchiveFiles = 20
                };

                AsyncTargetWrapper fileWrapper = new AsyncTargetWrapper();
                fileWrapper.WrappedTarget = fileTarget;
                fileWrapper.QueueLimit = 5000;
                fileWrapper.OverflowAction = AsyncTargetWrapperOverflowAction.Discard;

                var consoleTarget = new ColoredConsoleTarget();
                config.AddTarget("console", consoleTarget);

                config.LoggingRules.Add(new LoggingRule("*", LogLevel.Info, fileWrapper));
                //config.LoggingRules.Add(new LoggingRule("*", LogLevel.Debug, consoleTarget));
            }

            

            // firmware
            {
                NLog.Targets.FileTarget fileTarget = new NLog.Targets.FileTarget
                {
                    FileName = @"C:\MOBYUS\logs\firmware\firmware_${shortdate}.txt",
                    Layout = @"${longdate} [${level:uppercase=true:padding=-5}] [${callsite:includeNamespace=false}] - ${message} ${exception:format=tostring}",
                    KeepFileOpen = false,
                    ArchiveDateFormat = "yyyy-MM-dd",
                    ArchiveFileName = @"C:\MOBYUS\logs\firmware\firmware_{#}.txt",
                    ArchiveNumbering = ArchiveNumberingMode.Date,
                    ArchiveEvery = FileArchivePeriod.Day,
                    ArchiveAboveSize = 100000,
                    MaxArchiveFiles = 20
                };

                AsyncTargetWrapper fileWrapper = new AsyncTargetWrapper();
                fileWrapper.WrappedTarget = fileTarget;
                fileWrapper.QueueLimit = 5000;
                fileWrapper.OverflowAction = AsyncTargetWrapperOverflowAction.Discard;

                config.LoggingRules.Add(new LoggingRule("*", LogLevel.Debug, LogLevel.Debug, fileWrapper));
            }

            // Status
            {
                NLog.Targets.FileTarget fileTarget = new NLog.Targets.FileTarget
                {
                    FileName = @"C:\MOBYUS\logs\status\status_${shortdate}.txt",
                    Layout = @"${longdate} [${level:uppercase=true:padding=-5}] [${callsite:includeNamespace=false}] - ${message} ${exception:format=tostring}",
                    KeepFileOpen = false,
                    ArchiveDateFormat = "yyyy-MM-dd",
                    ArchiveFileName = @"C:\MOBYUS\logs\status\status_{#}.txt",
                    ArchiveNumbering = ArchiveNumberingMode.Date,
                    ArchiveEvery = FileArchivePeriod.Day,
                    ArchiveAboveSize = 100000,
                    MaxArchiveFiles = 20
                };

                AsyncTargetWrapper fileWrapper = new AsyncTargetWrapper();
                fileWrapper.WrappedTarget = fileTarget;
                fileWrapper.QueueLimit = 5000;
                fileWrapper.OverflowAction = AsyncTargetWrapperOverflowAction.Discard;

                config.LoggingRules.Add(new LoggingRule("*", LogLevel.Trace, LogLevel.Trace, fileWrapper));
            }

            // ERROR
            {
                NLog.Targets.FileTarget fileTarget = new NLog.Targets.FileTarget
                {
                    FileName = @"C:\MOBYUS\logs\error.txt",
                    Layout = @"${longdate} [${level:uppercase=true:padding=-5}] [${callsite:includeNamespace=false}] - ${message} ${exception:format=tostring}",
                    KeepFileOpen = false,
                    ArchiveAboveSize = 100000,
                    MaxArchiveFiles = 1
                };

                AsyncTargetWrapper fileWrapper = new AsyncTargetWrapper();
                fileWrapper.WrappedTarget = fileTarget;
                fileWrapper.QueueLimit = 5000;
                fileWrapper.OverflowAction = AsyncTargetWrapperOverflowAction.Discard;

                config.LoggingRules.Add(new LoggingRule("*", LogLevel.Error, fileWrapper));
            }

            //// CONSOLE
            //{
            //    NLog.Targets.ConsoleTarget consoleTarget = new NLog.Targets.ConsoleTarget()
            //    {
            //        Layout = @"${time} [${level:uppercase=true:padding=-5}] [${callsite:includeNamespace=false}] - ${message} ${exception:format=tostring}"
            //    };

            //    AsyncTargetWrapper consoleWrapper = new AsyncTargetWrapper();
            //    consoleWrapper.WrappedTarget = consoleTarget;
            //    consoleWrapper.QueueLimit = 5000;
            //    consoleWrapper.OverflowAction = AsyncTargetWrapperOverflowAction.Discard;

            //    config.LoggingRules.Add(new LoggingRule("*", LogLevel.Debug, consoleWrapper));
            //}

            LogManager.Configuration = config;
        }

        public void SetLoggingConfigPanel()
        {
            LoggingConfiguration config = LogManager.Configuration;
            if (config == null)
            {
                config = new LoggingConfiguration();
            }

            {
                NLog.Windows.Forms.RichTextBoxTarget target = new NLog.Windows.Forms.RichTextBoxTarget();

                target.TargetRichTextBox = this.LogViewer;
                target.Name = "RichTextBox";
                target.Layout = @"${time} [${level:uppercase=true:padding=-5}] [${callsite:includeNamespace=false}] - ${message} ${exception:format=tostring}";
                //target.AutoScroll = true;
                target.MaxLines = 10000;
                target.UseDefaultRowColoringRules = false;
                target.RowColoringRules.Add(new RichTextBoxRowColoringRule("level == LogLevel.Info", "Black", "Control", FontStyle.Regular));
                target.RowColoringRules.Add(new RichTextBoxRowColoringRule("level == LogLevel.Error", "White", "DarkRed", FontStyle.Regular));

                AsyncTargetWrapper asyncWrapper = new AsyncTargetWrapper();
                asyncWrapper.Name = "AsyncRichTextBox";
                asyncWrapper.WrappedTarget = target;

                config.LoggingRules.Add(new LoggingRule("*", LogLevel.Info, asyncWrapper));
            }

            /* ERROR 관련 부분
            {
                NLog.Windows.Forms.RichTextBoxTarget target = new NLog.Windows.Forms.RichTextBoxTarget();

                target.TargetRichTextBox = this.LogViewerError;
                target.Name = "RichTextBox";
                target.Layout = @"${time} [${level:uppercase=true:padding=-5}] [${callsite:includeNamespace=false}] - ${message} ${exception:format=tostring}";
                target.AutoScroll = true;
                target.MaxLines = 10000;
                target.UseDefaultRowColoringRules = false;
                target.RowColoringRules.Add(new RichTextBoxRowColoringRule("level == LogLevel.Error", "White", "DarkRed", FontStyle.Regular));

                AsyncTargetWrapper asyncWrapper = new AsyncTargetWrapper();
                asyncWrapper.Name = "AsyncRichTextBox";
                asyncWrapper.WrappedTarget = target;

                config.LoggingRules.Add(new LoggingRule("*", LogLevel.Error, asyncWrapper));

            }
            */
            LogManager.Configuration = config;
        }

        private void SelectCurrentWorkCallback(object state)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new TimerEventFiredDelegate(SelectCurrentWork));
            }
        }

        private void GetCurrentAlarmCallback(object state)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new TimerEventFiredDelegate(GetCurrentAlarm));
            }
        }

    }
}
