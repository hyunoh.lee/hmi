﻿using MobyusTouch.src.Communication;
using MobyusTouch.src.Control.Sub;
using MobyusTouch.src.Global;
using MobyusTouch.src.Global.Entity;
using NLog;
using System;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace MobyusTouch
{
    public partial class DialogEMO : Window
    {
        private static Logger logger = LogManager.GetLogger("DialogEMO");

        WorkEntity workEntity;
        public DialogEMO(WorkEntity workEntity)
        {
            InitializeComponent();

            ModeControl.dlgEMOCloseEvent += new DialogEMOCloseEvent(DlgEMO_Close);
            MqttClientHyundai.dlgEMOCloseEvent += new DialogEMOCloseEvent2(DlgEMO_Close2);

            try
            {
                if (workEntity != null)
                {
                    switch (workEntity.workType)
                    {
                        case Globals.TRANSFER_LOAD:
                            this.workType.Content = "적재";
                            break;
                        case Globals.TRANSFER_UNLOAD:
                            this.workType.Content = "이재";
                            break;
                        case Globals.TRANSFER_MOVE:
                            this.workType.Content = "이동";
                            break;
                        case Globals.TRANSFER_CHARGE:
                            this.workType.Content = "충전";
                            break;
                        default:
                            this.workType.Content = "대기";
                            break;
                    }
                    if (!"".Equals(Globals.alarmTable[workEntity.alarm]))
                    {
                        String alarmInfo = "";
                        String alarmSolve = "";

                        //logger.Info("Dialog EMO WorkEntity Alarm :" + Globals.alarmTable[workEntity.alarm].ToString());

                        if (Globals.alarmTable[workEntity.alarm].ToString().IndexOf("|", 0, Globals.alarmTable[workEntity.alarm].ToString().Length) != -1)
                        {
                            alarmInfo = Globals.alarmTable[workEntity.alarm].ToString().Split('|')[0];
                            alarmSolve = Globals.alarmTable[workEntity.alarm].ToString().Split('|')[1];

                        }

                        this.alarm.Content = "No." + workEntity.alarm + "(" + alarmInfo + ")";
                        this.toLevel.Content = workEntity.workLevel == 0 ? "-" : workEntity.workLevel + "단";
                        this.toNode.Content = workEntity.toNode == 0 ? "-" : workEntity.toNode.ToString();
                        this.curNode.Content = workEntity.curNode;
                        this.solve.Text = alarmSolve;


                    }
                    if (new System.Diagnostics.StackTrace().GetFrame(1).GetMethod().Name.IndexOf("AlarmCellClick") == 0)
                    {
                        this.alarmDate.Content = workEntity.workStartTime;

                        this.DeleteBtn.IsEnabled = false;
                        this.SendBtn.IsEnabled = false;

                    }
                    else
                    {
                        this.alarmDate.Content = System.DateTime.Now.ToString();
                    }

                    this.workEntity = workEntity;
                }
                else
                {
                    this.workType.Content = "No Work";
                    logger.Error("[ERROR] " + this.workType.Content);
                }


            }
            catch (Exception ex)
            {
                logger.Error("[ERROR] " + ex.ToString());
            }
        }

        private void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            Globals.currentWork.alarm = Alarms.NO_ALARM;
            Globals.HMISystem.SystemMode = SystemMode.SEMI_AUTO;
            Thread.Sleep(200);

            Globals.bDlgEMOShow = false;
            this.DialogResult = false;
        }

        public void DlgEMO_Close()
        {
            try
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate
                {
                    if (PresentationSource.FromVisual(this) != null)
                    //if(Application.Current.Windows.Cast<Window>().Contains(this) == false)
                    {
                        logger.Info("   Dialog EMO Close");

                        Globals.bDlgEMOShow = false;
                        this.DialogResult = false;

                    }
                }));
            }
            catch (Exception e)
            {
                logger.Error(e.ToString());
            }
        }
        public void DlgEMO_Close2()
        {
            try
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate
                {
                    if (PresentationSource.FromVisual(this) != null)
                    //if(Application.Current.Windows.Cast<Window>().Contains(this) == false)
                    {
                        logger.Info("   Dialog EMO Close");

                        Globals.bDlgEMOShow = false;
                        this.DialogResult = false;

                    }
                }));
            }
            catch (Exception e)
            {
                logger.Error(e.ToString());
            }
        }

        private void SendBtn_Click(object sender, RoutedEventArgs e)
        {
            if (this.workEntity.workType == Globals.TRANSFER_MOVE) // 작업이 이동이면 
            {
                Globals.currentWork.workState = 44; // 도착
            }
            else if (this.workEntity.workType == Globals.TRANSFER_LOAD) // 작업이 적재이면 
            {
                Globals.currentWork.workState = 55; // 적재 완료
            }
            else if (this.workEntity.workType == Globals.TRANSFER_UNLOAD) // 작업이 이재이면 
            {
                Globals.currentWork.workState = 66; // 이재 완료
            }
            else if (this.workEntity.workType == Globals.TRANSFER_CHARGE) // 작업이 충전이면 
            {
                Globals.currentWork.workState = 99; // 충전 중
            }
            Thread.Sleep(1000);

            Globals.bDlgEMOShow = false;
            this.DialogResult = false;
        }

        private void ResumeBtn_Click(object sender, RoutedEventArgs e)
        {
            Globals.currentWork.alarm = Alarms.NO_ALARM;
            Globals.HMISystem.SystemMode = SystemMode.AUTO;
            Thread.Sleep(200);

            Globals.bDlgEMOShow = false;
            this.DialogResult = false;

            if (Globals.currentWork.workState == (byte)WorkState.UNLOADING || Globals.currentWork.workState == (byte)WorkState.LOADING)
            {

            }
        }

    }
}
