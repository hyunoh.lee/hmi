﻿using MobyusTouch.src.Global;
using MobyusTouch.src.Global.Entity;
using NLog;
using System;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace MobyusTouch
{
    public partial class DialogACSOrderInfo : Window
    {
        private static Logger logger = LogManager.GetLogger("DialogACSOrderInfo");

        WorkEntity workEntity;
        DispatcherTimer acsOrderDlgTimer = new DispatcherTimer();
        public DialogACSOrderInfo(WorkEntity workEntity)
        {
            InitializeComponent();

            this.workEntity = workEntity;

            //acsOrderDlgTimer = new System.Threading.Timer(ACSOrderTimer);
            //acsOrderDlgTimer.Change(0, 1000);
            acsOrderDlgTimer.Interval = TimeSpan.FromSeconds(1.0);
            acsOrderDlgTimer.Tick += new EventHandler(ACSOrderTimer);
            acsOrderDlgTimer.Start();
        }

        private void ACSOrderTimer(object sender, EventArgs e)//Object state)
        {
            this.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
            {
                ACSOrderUIGetInfo();
            }));

          
        }
        private void ACSOrderUIGetInfo()
        {
            if (workEntity != null)
            {
                switch (workEntity.workType)
                {
                    case Globals.TRANSFER_LOAD:
                        this.workType.Content = "적재";
                        break;
                    case Globals.TRANSFER_UNLOAD:
                        this.workType.Content = "이재";
                        break;
                    case Globals.TRANSFER_MOVE:
                        this.workType.Content = "이동";
                        break;
                    case Globals.TRANSFER_CHARGE:
                        this.workType.Content = "충전";
                        break;
                    default:
                        this.workType.Content = "대기";
                        break;
                }
                try
                {
                    if (!"".Equals(Globals.alarmTable[workEntity.alarm]))
                    {
                        this.toLevel.Content = workEntity.workLevel == 0 ? "-" : workEntity.workLevel + "단";
                        this.toNode.Content = workEntity.toNode == 0 ? "-" : workEntity.toNode.ToString();
                        this.curNode.Content = workEntity.curNode;

                    }

                    this.workEntity = workEntity;
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString() + "[ERROR]");
                }
            }
            else
                this.workType.Content = "-";



            if (!Globals.bACSOrderDlgShow)
            {
                Globals.bACSOrderDlgShow = true;
                acsOrderDlgTimer.Stop();
                acsOrderDlgTimer = null;

                this.DialogResult = false;
            }
        }
    }
}
