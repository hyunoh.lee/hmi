﻿
using MobyusTouch.src.Global;
using MobyusTouch.src.Global.Entity;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
//using Save_data_dll_v1;

//using MapDLL;
using EditorLibrary;

namespace MobyusTouch
{
    public partial class DialogOrderWork : Window
    {
        private short workType;
        private ushort workLevel;
        private ushort toNode;
        private int palletType;

        ListBoxItem workItem;
        ListBoxItem levelItem;
        ListBoxItem palletTypeItem;

        DispatcherTimer timer = new DispatcherTimer();

        public DialogOrderWork()
        {
            InitializeComponent();
            this.DataContext = this;

            this.stopBtn.IsEnabled = false;
            this.pauseBtn.IsEnabled = false;

            timer.Interval = TimeSpan.FromMilliseconds(0.3);
            timer.Tick += new EventHandler(ResultWork_Tick);
            timer.Start();
        }

        public void TimerStop()
        {
            timer.Stop();
        }

        void Work_Clicked(object sender, RoutedEventArgs e)
        {
            RadioButton radioBtn = (sender as RadioButton);

        }

        bool IsPauseChecked = false;
        private void MouseClickEvent(object sender, EventArgs e)
        {
            Button button = sender as Button;

            switch (button.CommandParameter.ToString())
            {
                case "START":
                    if (Globals.HMISystem.SystemMode != SystemMode.SEMI_AUTO)
                    {
                        timer.Stop();
                        MessageBox.Show("SEMI AUTO모드 시에만 작동합니다.");
                        this.DialogResult = false;
                        return;
                    }

                    workItem = (ListBoxItem)Work.SelectedItem;
                    levelItem = (ListBoxItem)Level.SelectedItem;
                    palletTypeItem = (ListBoxItem)PalletType.SelectedItem;
                    // Target Node
                    NodeInfo toNodeInfo = new NodeInfo();
                    if (Globals.nodeData == null || Globals.nodeData.Count == 0)
                    {
                        MessageBox.Show("MAP 정보가 없습니다.");
                        return;
                    }

                    if (toNode <= 0)
                    {
                        MessageBox.Show("Target Node를 입력해주세요.");
                        return;
                    }

                    if (toNode > 0)
                    {
                        bool existNode = false;
                        for (int i = 0; i < Globals.nodeData.Count; i++)
                        {
                            toNodeInfo = Globals.nodeData[i];
                            if (toNode == toNodeInfo.GetNodeNumber())
                            {
                                existNode = true;
                                break;
                            }
                        }
                        if (!existNode)
                        {
                            MessageBox.Show("Map Node 정보가 존재하지 않습니다.");
                            return;
                        }
                    }

                    // Work Type
                    if (workItem == null)
                    {
                        MessageBox.Show("작업 명령을 선택해 주세요.");
                        return;
                    }
                    if (workItem.Name.ToString() == "TRANSFER_MOVE")
                    {
                        workType = Globals.TRANSFER_MOVE;
                    }
                    else if (workItem.Name.ToString() == "TRANSFER_LOAD")
                    {
                        workType = Globals.TRANSFER_LOAD;
                    }
                    else if (workItem.Name.ToString() == "TRANSFER_UNLOAD")
                    {
                        workType = Globals.TRANSFER_UNLOAD;
                    }
                    else if (workItem.Name.ToString() == "TRANSFER_SCENARIO")
                    {
                        workType = Globals.TRANSFER_SCENARIO;
                    }

                    if (workType > 0)
                    {
                        if (workType == Globals.TRANSFER_MOVE && toNodeInfo.GetNodeType() == 1) // Move명령에 Node가 station인 경우
                        {
                            MessageBox.Show("이동 명령이나, 목적지 위치가 이·적재장소입니다.");
                            return;
                        }

                        if ((workType == Globals.TRANSFER_LOAD || workType == Globals.TRANSFER_UNLOAD) && toNodeInfo.GetNodeType() == 0 && palletType > 0) // Load, Unload는 Station만 주행 가능
                        {
                            MessageBox.Show("목적지 위치가 이·적재장소인 경우 적재 또는 이재 명령만 가능합니다.");
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("작업 명령을 선택해 주세요.");
                        return;
                    }

                    if (workType != Globals.TRANSFER_MOVE)
                    {
                        // Pallet Type
                        if (palletTypeItem == null)
                        {
                            MessageBox.Show("팔렛 종류를 선택해 주세요.");
                            return;
                        }
                        else
                        {
                            palletType = short.Parse(palletTypeItem.Name.ToString().Substring(5));
                        }

                        // Work Level
                        if (levelItem == null)
                        {
                            MessageBox.Show("팔렛 레벨을 선택해 주세요.");
                            return;
                        }
                        else
                        {
                            workLevel = ushort.Parse(levelItem.Name.ToString().Substring(6));
                        }
                    }

                    WorkEntity workCommand = new WorkEntity();
                    workCommand.curNode = Globals.currentWork.curNode;
                    workCommand.toNode = toNode;
                    workCommand.workType = workType;
                    workCommand.workLevel = workLevel;
                    workCommand.palletType = palletType;

                    if (workType == Globals.TRANSFER_MOVE)
                    {
                        workCommand.workLevel = 0;
                        workCommand.palletType = 0;
                    }

                    if (workCommand.EnqueueWork())
                    {
                        this.Target.IsEnabled = false;
                        this.startBtn.IsEnabled = false;

                        this.stopBtn.IsEnabled = true;
                        this.pauseBtn.IsEnabled = true;

                        Globals.currentWork = workCommand;

                        Globals.HMISystem.PalletLevel = (byte)workCommand.workLevel;
                        Globals.HMISystem.PalletType = (byte)workCommand.palletType;
                        Globals.HMISystem.WorkType = workCommand.workState;
                        Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_FIRMWARE_NONE;
                        Globals.HMISystem.ACStoHMI_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_ACS_NONE;

                        //AutoMessageBox.Show("작업 전송 완료","알림", 3000);
                    }
                    else
                    {
                        MessageBox.Show("작업이 존재하지 않습니다. [NO FOUND WORK FILE]");
                    }
                    break;

                case "STOP":

                    Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_STOP;
                    Globals.HMISystem.ACStoHMI_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_STOP;
                    Thread.Sleep(200);
                    Globals.workStep.Clear();
                    Globals.currentWork.Clear();

                    Globals.HMISystem.LiftEnable = false;
                    Globals.HMISystem.TiltEnable = false;
                    Globals.HMISystem.ReachEnable = false;
                    Globals.HMISystem.SideShiftEnable = false;
                    Globals.HMISystem.MoverEnable = false;
                    Globals.HMISystem.ChargeEnable = false;
                    Globals.HMISystem.ClampEnable = false;
                    Globals.HMISystem.TaggingEnable = false;
                    Globals.HMISystem.PalletStackingEnable = false;
                    Globals.HMISystem.PalletFrontEnable = false;
                    Globals.HMISystem.PalletHeightEnable = false;
                    Globals.HMISystem.PalletStackingSuccessEnable = false;
                    Globals.HMISystem.PalletFrontExistEnable = false;

                    if (workItem != null)
                    {
                        workItem.IsSelected = false;
                        workType = 0;
                    }
                    if (levelItem != null)
                    {
                        levelItem.IsSelected = false;
                        workLevel = 0;
                    }

                    if (palletTypeItem != null)
                    {
                        palletTypeItem.IsSelected = false;
                        palletType = 0;
                    }

                    this.Target.IsEnabled = true;
                    this.startBtn.IsEnabled = true;

                    this.stopBtn.IsEnabled = false;
                    this.pauseBtn.IsEnabled = false;
                    MessageBox.Show("현재 작업을 삭제하였습니다.");
                    break;

                case "PAUSE":

                    if (IsPauseChecked == false)
                    {
                        IsPauseChecked = true;

                        Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_PAUSE;
                        Globals.HMISystem.HMItoACS_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_PAUSE;
                        this.pauseBtn.Content = "RESUME";
                        MessageBox.Show("작업을 일시정지 하였습니다.");
                    }
                    else
                    {
                        IsPauseChecked = false;
                        Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_RESUME;
                        Globals.HMISystem.HMItoACS_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_FIRMWARE_NONE;
                        this.pauseBtn.Content = "PAUSE";
                        MessageBox.Show("작업을 재개 하였습니다.");
                    }
                    break;

                case "TARGET":

                    DialogKeypad dialog = new DialogKeypad();
                    if (dialog.ShowDialog() == true)
                    {
                        Target.Content = dialog.Result;
                        if (Target.Content.ToString() == "")
                        {
                            Target.Content = 0;
                        }
                        toNode = UInt16.Parse(Target.Content.ToString());
                        //kdy 여기에 이동 박스 눌리게 표시
                        workItem = (ListBoxItem)Work.FindName("TRANSFER_MOVE");
                        workItem.IsSelected = true;
                    }
                    break;


                case "EXIT":
                    this.DialogResult = false;
                    break;

                default:
                    break;
            }
        }

        private void  ResultWork_Tick(object sender, EventArgs e)
        {
            this.Current.Content = Globals.currentWork.curNode;

            if (Globals.currentWork.workType != Globals.TRANSFER_READY && (Globals.currentWork.workQueue == null || Globals.currentWork.workQueue.Count == 0)) // 현재위치가 목적지면
            {

                if (workItem != null)
                {
                    workItem.IsSelected = false;
                    workType = 0;
                }
                if (levelItem != null)
                {
                    levelItem.IsSelected = false;
                    workLevel = 0;
                }

                if (palletTypeItem != null)
                {
                    palletTypeItem.IsSelected = false;
                    palletType = 0;
                }

                this.Target.IsEnabled = true;
                this.startBtn.IsEnabled = true;
                this.stopBtn.IsEnabled = false;
                this.pauseBtn.IsEnabled = false;
            }
        }
    }
}