﻿
using System;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.Threading;
using System.Windows.Threading;
using MobyusTouch.src.Global;
using System.IO;

namespace MobyusTouch
{
    public partial class DialogWorklist : Window
    {
        DispatcherTimer timer = new DispatcherTimer();
        DialogKeyboard dialogKeyboard;
        DialogKeypad dialogKeypad;
        public DialogWorklist()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        private void button_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            switch (button.CommandParameter.ToString())
            {
                case "WL_PALLETE_ID":
                    Input_WL_PALLETE_ID();
                    break;
                case "WL_LEVEL":
                    Input_WL_LEVEL();
                    break;
                case "WL_NODE":
                    Input_WL_NODE();
                    break;

                case "OK":
                    Make_New_Worklist();
                    this.DialogResult = true;
                    break;
                case "Cancel":
                    this.DialogResult = false;
                    break;

                default:
                    break;
            }
        }

        private void Input_WL_PALLETE_ID()
        {
            dialogKeypad = new DialogKeypad();
            if (dialogKeypad.ShowDialog() == true)
            {
                WL_PALLETE_ID.Content = dialogKeypad.Result;


                if (WL_PALLETE_ID.Content.ToString() == "")
                {
                    MessageBox.Show("입력이 제대로 되지 않았습니다.");
                }

                Globals.strWL_PALLETE_ID = WL_PALLETE_ID.Content.ToString();
            }
        }
        private void Input_WL_LEVEL()
        {
            dialogKeypad = new DialogKeypad();
            if (dialogKeypad.ShowDialog() == true)
            {
                WL_LEVEL.Content = dialogKeypad.Result;


                if (WL_LEVEL.Content.ToString() == "")
                {
                    MessageBox.Show("입력이 제대로 되지 않았습니다.");
                }

                Globals.strWL_LEVEL = WL_LEVEL.Content.ToString();
            }
        }
        private void Input_WL_NODE()
        {
            dialogKeypad = new DialogKeypad();
            if (dialogKeypad.ShowDialog() == true)
            {
                WL_NODE.Content = dialogKeypad.Result;


                if (WL_NODE.Content.ToString() == "")
                {
                    MessageBox.Show("입력이 제대로 되지 않았습니다.");
                }

                Globals.strWL_NODE = WL_NODE.Content.ToString();
            }

        }

        private void Make_New_Worklist()
        {
            DirectoryInfo dtif = new DirectoryInfo(Globals.WORK_SettingDirPath + @"\");

            string filename, filepath;
            filename = Globals.strWL_WORK + "_" + Globals.strWL_PALLETE_ID + "_" + Globals.strWL_LEVEL + "_" + Globals.strWL_NODE;
            filepath = dtif + /*"\\" +*/ filename + ".csv";

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(filepath))
            {
                // 각 필드에 사용될 제목을 정의 

                file.WriteLine("ID;WORK;PALLET_ID;LEVEL;NODE;CNT");
                // 필드값
                file.WriteLine("{0}", filename + ";" + Globals.strWL_WORK + ";" + Globals.strWL_PALLETE_ID + ";" + Globals.strWL_LEVEL + ";" + Globals.strWL_NODE + ";" + Globals.strWL_LINK_TYPE + ";0");
                file.WriteLine(";;;;;;");
                file.WriteLine(";;;;;;");
                file.WriteLine("PALLET_NAME;PALLET_WIDTH;PALLET_HEIGHT;PALLET_LENGTH;PALLET_LENGTH_HOLE;;");
                file.WriteLine("CS101;1500;1300;1750;700;;");
                file.WriteLine(";;;;;;");
                file.WriteLine(";;;;;;");
                file.WriteLine(";;;;;;");
            }

            MessageBox.Show(filename + " 파일 생성 완료.");

            Globals.bRefresh_worklistView = true;


        }

        private void cbWL_WORK_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox currentComboBox = sender as ComboBox;
            if (currentComboBox != null)
            {
                ComboBoxItem currentItem = currentComboBox.SelectedItem as ComboBoxItem;
                if (currentItem != null)
                {
                    switch (currentItem.Content.ToString())
                    {
                        case "MOVE":
                            Globals.strWL_WORK = "MOVE";
                            break;
                        case "LOAD":
                            Globals.strWL_WORK = "LOAD";
                            break;
                        case "UNLOAD":
                            Globals.strWL_WORK = "UNLOAD";
                            break;

                        default:
                            break;
                    }

                }
            }
        }
        private void cbWL_LINK_TYPE_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox currentComboBox = sender as ComboBox;
            if (currentComboBox != null)
            {
                ComboBoxItem currentItem = currentComboBox.SelectedItem as ComboBoxItem;
                if (currentItem != null)
                {
                    switch (currentItem.Content.ToString())
                    {
                        case "STRAIGHT":
                            Globals.strWL_LINK_TYPE = "0";
                            break;
                        case "CURVE":
                            Globals.strWL_LINK_TYPE = "1";
                            break;
                        case "LOADING":
                            if (Globals.strWL_LEVEL == "1")
                                Globals.strWL_LINK_TYPE = "4";
                            else
                                Globals.strWL_LINK_TYPE = "5";
                            break;
                        case "UNLOADING":
                            if (Globals.strWL_LEVEL == "1")
                                Globals.strWL_LINK_TYPE = "0";
                            else
                                Globals.strWL_LINK_TYPE = "6";
                            break;

                        default:
                            break;
                    }

                }
            }
        }
    }
}

