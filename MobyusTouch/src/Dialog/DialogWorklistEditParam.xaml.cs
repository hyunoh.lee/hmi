﻿
using System;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.Threading;
using System.Windows.Threading;
using MobyusTouch.src.Global;
using System.IO;

namespace MobyusTouch
{
    public partial class DialogWorklistEditParam : Window
    {
        DispatcherTimer timer = new DispatcherTimer();
        DialogKeyboard dialogKeyboard;
        DialogKeypad dialogKeypad;
        public DialogWorklistEditParam()
        {
            InitializeComponent();

            switch(Globals.strWL_PR_NAME)
            {
                case "WORK_MOVE_1ST_PATH_PLANNING":
                    cbWL_PR_NAME.SelectedIndex = 0;
                    break;
                case "WORK_MOVE_2ND_PATH_PLANNING":
                    cbWL_PR_NAME.SelectedIndex = 1;
                    break;
                case "WORK_MOVE_PATH_SENDING":
                    cbWL_PR_NAME.SelectedIndex = 2;
                    break;
                case "WORK_MOVE_FROM_STATION":
                    cbWL_PR_NAME.SelectedIndex = 3;
                    break;
                case "WORK_MOVE_TO_STATION":
                    cbWL_PR_NAME.SelectedIndex = 4;
                    break;
                case "WORK_MOVE_COMPLETE":
                    cbWL_PR_NAME.SelectedIndex = 5;
                    break;
                case "WORK_MOVE_END":
                    cbWL_PR_NAME.SelectedIndex = 6;
                    break;
                case "WORK_FORK_MOVER":
                    cbWL_PR_NAME.SelectedIndex = 7;
                    break;
                case "WORK_FORK_LIFTING":
                    cbWL_PR_NAME.SelectedIndex = 8;
                    break;
                case "WORK_FORK_TILTING":
                    cbWL_PR_NAME.SelectedIndex = 9;
                    break;
                case "WORK_FORK_SIDESHIFT":
                    cbWL_PR_NAME.SelectedIndex = 10;
                    break;
                case "WORK_PALLET_FRONT_ENABLE":
                    cbWL_PR_NAME.SelectedIndex = 11;
                    break;
                case "WORK_PALLET_FRONT_DISABLE":
                    cbWL_PR_NAME.SelectedIndex = 12;
                    break;
                case "WORK_PALLET_STACKING_ENABLE":
                    cbWL_PR_NAME.SelectedIndex = 13;
                    break;
                case "WORK_PALLET_STACKING_DISABLE":
                    cbWL_PR_NAME.SelectedIndex = 14;
                    break;
                case "WORK_PALLET_HEIGHT_ENABLE":
                    cbWL_PR_NAME.SelectedIndex = 15;
                    break;
                case "WORK_PALLET_HEIGHT_DISABLE":
                    cbWL_PR_NAME.SelectedIndex = 16;
                    break;
            }
            WL_PR_VALUE.Content = Globals.strWL_PR_VALUE;
            WL_PR_SLEEP.Content = Globals.strWL_PR_SLEEP;
            switch(Globals.strWL_PR_SYNC) // kdy
            {
                case "TRUE":
                    cbWL_PR_SYNC.SelectedIndex = 0;
                    break;
                case "FALSE":
                    cbWL_PR_SYNC.SelectedIndex = 1;
                    break;

            }

            this.DataContext = this;
        }
        private void button_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            switch (button.CommandParameter.ToString())
            {                 
                case "WL_PR_VALUE":
                    Input_WL_PR_VALUE();
                    break;
                case "WL_PR_SLEEP":
                    Input_WL_PR_SLEEP();
                    break;
                // kdy
                //case "WL_PR_SYNC":
                //    Input_WL_PR_SYNC();
                //    break;
                case "OK":
                    Edit_Param();
                    this.DialogResult = true;
                    break;
                case "Cancel":
                    this.DialogResult = false;
                    break;

                default:
                    break;
            }
        }
        
        
        private void Input_WL_PR_VALUE()
        {
            dialogKeypad = new DialogKeypad();
            if (dialogKeypad.ShowDialog() == true)
            {
                WL_PR_VALUE.Content = dialogKeypad.Result;


                if (WL_PR_VALUE.Content.ToString() == "")
                {
                    MessageBox.Show("입력이 제대로 되지 않았습니다.");
                }

                Globals.strWL_PR_VALUE = WL_PR_VALUE.Content.ToString();
            }
        }
        private void Input_WL_PR_SLEEP()
        {
            dialogKeypad = new DialogKeypad();
            if (dialogKeypad.ShowDialog() == true)
            {
                WL_PR_SLEEP.Content = dialogKeypad.Result;


                if (WL_PR_SLEEP.Content.ToString() == "")
                {
                    MessageBox.Show("입력이 제대로 되지 않았습니다.");
                }

                Globals.strWL_PR_SLEEP = WL_PR_SLEEP.Content.ToString();
            }
        }
        //private void Input_WL_PR_SYNC() // kdy가 바꾸기 전 기존 함수
        //{
        //    dialogKeypad = new DialogKeypad();
        //    if (dialogKeypad.ShowDialog() == true)
        //    {
        //        WL_PR_SYNC.Content = dialogKeypad.Result;


        //        if (WL_PR_SYNC.Content.ToString() == "")
        //        {
        //            MessageBox.Show("입력이 제대로 되지 않았습니다.");
        //        }

        //        Globals.strWL_PR_SYNC = WL_PR_SYNC.Content.ToString();
        //    }

        //}
        private void cbWL_PR_SYNC_SelectionChanged(object sender, SelectionChangedEventArgs e) // kdy
        {
            ComboBox currentComboBox = sender as ComboBox;
            if (currentComboBox != null)
            {
                ComboBoxItem currentItem = currentComboBox.SelectedItem as ComboBoxItem;
                if (currentItem != null)
                {
                    switch (currentItem.Content.ToString())
                    {
                        case "TRUE":
                            Globals.strWL_PR_SYNC = "TRUE";
                            break;
                        case "FALSE":
                            Globals.strWL_PR_SYNC = "FALSE";
                            break;
                        default:
                            break;
                    }

                }
            }
        }
        private void Edit_Param()
        {        
            Globals.bEdit_Param = true;

        }

        private void cbWL_PR_NAME_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox currentComboBox = sender as ComboBox;
            if (currentComboBox != null)
            {
                ComboBoxItem currentItem = currentComboBox.SelectedItem as ComboBoxItem;
                if (currentItem != null)
                {
                    switch (currentItem.Content.ToString())
                    {
                        case "[MOVE]1ST_PATH_PLANNING":
                            Globals.strWL_PR_NAME = "WORK_MOVE_1ST_PATH_PLANNING";
                            break;
                        case "[MOVE]2ND_PATH_PLANNING":
                            Globals.strWL_PR_NAME = "WORK_MOVE_2ND_PATH_PLANNING";
                            break;
                        case "[MOVE]PATH_SENDING":
                            Globals.strWL_PR_NAME = "WORK_MOVE_PATH_SENDING";
                            break;
                        case "[MOVE]FROM_STATION":
                            Globals.strWL_PR_NAME = "WORK_MOVE_FROM_STATION";
                            break;
                        case "[MOVE]TO_STATION":
                            Globals.strWL_PR_NAME = "WORK_MOVE_TO_STATION";
                            break;
                        case "[MOVE]COMPLETE":
                            Globals.strWL_PR_NAME = "WORK_MOVE_COMPLETE";
                            break;
                        case "[MOVE]END":
                            Globals.strWL_PR_NAME = "WORK_MOVE_END";
                            break;
                        case "[FORK]MOVER":
                            Globals.strWL_PR_NAME = "WORK_FORK_MOVER";
                            break;
                        case "[FORK]LIFTING":
                            Globals.strWL_PR_NAME = "WORK_FORK_LIFTING";
                            break;
                        case "[FORK]TILTING":
                            Globals.strWL_PR_NAME = "WORK_FORK_TILTING";
                            break;
                        case "[FORK]SIDESHIFT":
                            Globals.strWL_PR_NAME = "WORK_FORK_SIDESHIFT";
                            break;
                        case "[PALLETE]FRONT_ENABLE":
                            Globals.strWL_PR_NAME = "WORK_PALLET_FRONT_ENABLE";
                            break;
                        case "[PALLETE]FRONT_DISABLE":
                            Globals.strWL_PR_NAME = "WORK_PALLET_FRONT_DISABLE";
                            break;
                        case "[PALLETE]STACKING_ENABLE":
                            Globals.strWL_PR_NAME = "WORK_PALLET_STACKING_ENABLE";
                            break;
                        case "[PALLETE]STACKING_DISABLE":
                            Globals.strWL_PR_NAME = "WORK_PALLET_STACKING_DISABLE";
                            break;
                        case "[PALLETE]HEIGHT_ENABLE":
                            Globals.strWL_PR_NAME = "WORK_PALLET_HEIGHT_ENABLE";
                            break;
                        case "[PALLETE]HEIGHT_DISABLE":
                            Globals.strWL_PR_NAME = "WORK_PALLET_HEIGHT_DISABLE";
                            break;
                        default:
                            break;
                    }

                }
            }
        }

   
    }
}

