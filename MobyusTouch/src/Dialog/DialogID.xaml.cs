﻿using System;
using NLog;
using System.Windows;
using System.Drawing;
using System.Windows.Controls;
using System.ComponentModel;
using MobyusTouch.src.Global;
using MobyusTouch.Src.Utility;
using MobyusTouch.src.Communication;
using System.Threading;

namespace MobyusTouch
{
    /// <summary>
    /// DialogID.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class DialogID : Window
    {
        private static Logger logger = LogManager.GetLogger("DialogID");
        public DialogID()
        {
            InitializeComponent();
            //if (Globals.AFL_ID == 1)
            //{
            //    var converter = new System.Windows.Media.BrushConverter();
            //    _1호차.Background = (Brush)converter.ConvertFromString("#FFFFFF90");
            //    _2호차.IsEnabled = false;
            //}
            //if (Globals.AFL_ID == 2)
            //{
            //    _1호차.IsEnabled = false;
            //}
        }

        public static bool Change_AFL_ID_MessageBox()
        {
            bool tmp = false;
            Globals.HMISystem.AFLIDEnable = true;
            MessageBoxResult dialogResult = MessageBox.Show("1.고장난차량을 3호차로 바꾸고 안전한 위치로 옮기고 전원을 끄셨습니까?              ", "3호차로 변경", MessageBoxButton.YesNo);
            if (dialogResult == MessageBoxResult.Yes)
            {
                MessageBoxResult dialogResult2 = MessageBox.Show("2. 해당차량의 안전센서를 " + Globals.AFL_ID + "호차 버젼으로 변경 하셨습니까? ", "안전센서 변경", MessageBoxButton.YesNo);
                if (dialogResult2 == MessageBoxResult.Yes)
                {
                    MessageBoxResult dialogResult3 = MessageBox.Show("3. 혹시 좌,우 안전센서 모두 장애물이 걸렸다고 나오나요? \n\n   그렇다면 안전센서 버젼과 차량 번호가 일치하지 않았습니다. \n\n   장애물 감지가 안되었을 경우에만 '예(Y)' 클릭 ", "안전센서 변경 확인", MessageBoxButton.YesNo);
                    if (dialogResult3 == MessageBoxResult.Yes)
                    {
                        MessageBoxResult dialogResult4 = MessageBox.Show("4. 해당차량에 맞는 포크발로 교체하셨습니까?                                      ", "포크발 교체", MessageBoxButton.YesNo);
                        if (dialogResult4 == MessageBoxResult.Yes)
                        {
                            MessageBoxResult dialogResult5 = MessageBox.Show("5. 해당차량을 " + Globals.AFL_ID + "호차로 변경하시겠습니까?                ", "차량선택", MessageBoxButton.YesNo);
                            if (dialogResult5 == MessageBoxResult.Yes)
                            {
                                if (Globals.NavigationSystem.CheckAFLID == 2)
                                {
                                    Globals.currentWork.alarm = Alarms.ALARM_NAVI_CHANGE_AFL_ID_ERROR;
                                    Globals.currentWork.alarm_name = "![000/E5151/C1호 차 변 경 /C1  에    러 !]"; //호차변경에러
                                }
                                else
                                {
                                    Globals.HMISystem.AFLIDEnable = false;
                                    tmp = true;
                                }
                                
                            }
                        }
                    }
                }
            }
            return tmp;
        }
        private void button_Click(object sender, RoutedEventArgs e)
        {
            bool tmp = false;
            try
            {
                Button button = sender as Button;
                Globals.pre_AFL_ID = Globals.AFL_ID;
                Globals.AFL_ID = Convert.ToInt32(button.CommandParameter);
                tmp = Change_AFL_ID_MessageBox();
                if (tmp)
                {
                    Globals.ID = "AMR00" + button.CommandParameter.ToString();
                    Globals.AFL_ID = Convert.ToInt32(button.CommandParameter);
                    IniFileParsing.WriteValue("Type", "ID", Globals.ID);
                    this.DialogResult = true;
                }
                else
                {
                    Globals.HMISystem.AFLIDEnable = false;
                    Globals.AFL_ID = Globals.pre_AFL_ID;
                    this.DialogResult = false;
                }

                logger.Info("Confirm Changed Forklift Globals.ID : " + Globals.ID);
                MqttClientHyundai.Disconnect();
                Thread.Sleep(1000);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }
    }
}
