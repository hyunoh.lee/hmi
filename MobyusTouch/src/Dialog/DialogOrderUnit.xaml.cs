﻿
using System;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.Threading;
using System.Windows.Threading;
using MobyusTouch.src.Global;

namespace MobyusTouch
{
    public partial class DialogOrderUnit : Window
    {
        DispatcherTimer timer = new DispatcherTimer();
        DialogKeypad dialogKeypad;
        public DialogOrderUnit()
        {
            InitializeComponent();
            this.DataContext = this;

            timer.Interval = TimeSpan.FromMilliseconds(0.2);
            timer.Tick += new EventHandler(BtnPressed_Tick);
            timer.Start();
        }

        public void TimerStop()
        {
            timer.Stop();
        }
        private void button_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            switch (button.CommandParameter.ToString())
            {
                case "LIFT_POSITION":
                    Lifting();
                    break;
                case "TILT_POSITION":
                    Tilting();
                    break;
                case "REACH_POSITION":
                    Reaching();
                    break;
                case "SIDESHIFT_POSITION":
                    SideShifting();
                    break;
                case "FORKMOVER_POSITION":
                    ForkMover();
                    break;
                case "CHARGE_START":
                    ChargeStart();
                    break;
                case "CHARGE_END":
                    ChargeEnd();
                    break;
                case "CLAMP_START":
                    ClampStart();
                    break;
                case "CLAMP_END":
                    ClampEnd();
                    break;
                case "Cancel":
                    this.DialogResult = false;
                    timer.Stop();
                    break;

                default:
                    break;
            }
        }

        private void Lifting()
        {
            dialogKeypad = new DialogKeypad();
            if (dialogKeypad.ShowDialog() == true)
            {
                double targetPosition = 0;
                LIFT_TARGET.Content = dialogKeypad.Result;
                if (LIFT_TARGET.Content.ToString() == "")
                {
                    LIFT_TARGET.Content = "0";
                }
                targetPosition = Double.Parse(LIFT_TARGET.Content.ToString());
                if (targetPosition > Globals.FORK_LIFT_MAX_LIMIT)
                {
                    LIFT_TARGET.Content = Globals.FORK_LIFT_MAX_LIMIT.ToString();
                    MessageBox.Show("최대 " + Globals.FORK_LIFT_MAX_LIMIT.ToString() + "mm까지 입력 가능합니다.");
                    return;
                }
                if (targetPosition < Globals.FORK_LIFT_MIN_LIMIT)
                {
                    LIFT_TARGET.Content = Globals.FORK_LIFT_MIN_LIMIT.ToString();
                    MessageBox.Show("최소 " + Globals.FORK_LIFT_MIN_LIMIT.ToString() + "mm부터 입력 가능합니다.");
                    return;
                }

                this.LIFT_POSITION.IsEnabled = false;
                this.LIFT_UP.IsEnabled = false;
                this.LIFT_DOWN.IsEnabled = false;

                Globals.HMISystem.LiftEnable = true;
                Globals.HMISystem.LiftingValue = (UInt16)targetPosition;
            }
        }

        private void Tilting()
        {
            dialogKeypad = new DialogKeypad();
            if (dialogKeypad.ShowDialog() == true)
            {
                double targetPosition = 0;
                TILT_TARGET.Content = dialogKeypad.Result;
                if (TILT_TARGET.Content.ToString() == "")
                {
                    TILT_TARGET.Content = "0";
                }
                targetPosition = Double.Parse(TILT_TARGET.Content.ToString());
                if (targetPosition > Globals.FORK_TILT_MAX_LIMIT)
                {
                    TILT_TARGET.Content = Globals.FORK_TILT_MAX_LIMIT.ToString();
                    MessageBox.Show("최대 "+ Globals.FORK_TILT_MAX_LIMIT.ToString() + "도 까지 입력 가능합니다.");
                    return;
                }
                if (targetPosition < Globals.FORK_TILT_MIN_LIMIT)
                {
                    TILT_TARGET.Content = Globals.FORK_TILT_MIN_LIMIT.ToString();
                    MessageBox.Show("최소 "+ Globals.FORK_TILT_MIN_LIMIT.ToString()+ "도 부터 입력 가능합니다.");
                    return;
                }

                this.TILT_POSITION.IsEnabled = false;
                this.TILT_UP.IsEnabled = false;
                this.TILT_DOWN.IsEnabled = false;

                Globals.HMISystem.TiltEnable = true;
                Globals.HMISystem.TiltingValue = (sbyte)targetPosition;
            }
        }

        private void Reaching()
        {
            dialogKeypad = new DialogKeypad();
            if (dialogKeypad.ShowDialog() == true)
            {
                double targetPosition = 0;
                REACH_TARGET.Content = dialogKeypad.Result;
                if (REACH_TARGET.Content.ToString() == "")
                {
                    REACH_TARGET.Content = "0";
                }
                targetPosition = Double.Parse(REACH_TARGET.Content.ToString());
                if (targetPosition > Globals.FORK_REACH_MAX_LIMIT)
                {
                    REACH_TARGET.Content = Globals.FORK_REACH_MAX_LIMIT.ToString();
                    MessageBox.Show("최대 "+ Globals.FORK_REACH_MAX_LIMIT.ToString() + "mm 까지 입력 가능합니다.");
                    return;
                }
                if (targetPosition < Globals.FORK_REACH_MIN_LIMIT)
                {
                    REACH_TARGET.Content = Globals.FORK_REACH_MAX_LIMIT.ToString();
                    MessageBox.Show("최소 "+ Globals.FORK_REACH_MAX_LIMIT.ToString() + "mm 부터 입력 가능합니다.");
                    return;
                }

                this.REACH_POSITION.IsEnabled = false;
                this.REACH_UP.IsEnabled = false;
                this.REACH_DOWN.IsEnabled = false;

                Globals.HMISystem.ReachEnable = true;
                Globals.HMISystem.ReachingValue = (ushort)targetPosition;
            }
        }

        private void SideShifting()
        {
            dialogKeypad = new DialogKeypad();
            if (dialogKeypad.ShowDialog() == true)
            {
                double targetPosition = 0;
                int forkShiftLeftLimit = 0;
                int forkShiftRightLimit = 0;
                if (Globals.ForkLiftSystem.ShiftLeftPosition <= Globals.FORK_SHIFT_LIMIT - Globals.ForkLiftSystem.ShiftRightPosition)
                {
                    forkShiftLeftLimit = Globals.ForkLiftSystem.ShiftLeftPosition;
                    forkShiftRightLimit = Globals.ForkLiftSystem.ShiftRightPosition;
                }
                else
                {
                    forkShiftLeftLimit = Globals.FORK_SHIFT_LIMIT - Globals.ForkLiftSystem.ShiftRightPosition;
                    forkShiftRightLimit = Globals.FORK_SHIFT_LIMIT - Globals.ForkLiftSystem.ShiftLeftPosition;
                }
                Globals.beforeSideShiftValue = (Globals.ForkLiftSystem.ShiftRightPosition - Globals.ForkLiftSystem.ShiftLeftPosition) / 2;

                SIDESHIFT_TARGET.Content = dialogKeypad.Result;
                if (SIDESHIFT_TARGET.Content.ToString() == "")
                {
                    SIDESHIFT_TARGET.Content = "0";
                }
                targetPosition = Double.Parse(SIDESHIFT_TARGET.Content.ToString());

                if (targetPosition > forkShiftLeftLimit)
                {
                    SIDESHIFT_TARGET.Content = forkShiftLeftLimit.ToString();
                    MessageBox.Show("좌측으로 " + forkShiftLeftLimit.ToString()+ "mm 까지 입력 가능합니다.");
                    return;
                }
                if ((targetPosition < 0) && (Math.Abs(targetPosition) > forkShiftRightLimit))
                {
                    SIDESHIFT_TARGET.Content = forkShiftRightLimit.ToString();
                    MessageBox.Show("우측으로 " + forkShiftRightLimit.ToString() + "mm 까지 입력 가능합니다.");
                    return;
                }

                this.SIDESHIFT_POSITION.IsEnabled = false;
                this.SIDESHIFT_UP.IsEnabled = false;
                this.SIDESHIFT_DOWN.IsEnabled = false;

                Globals.HMISystem.SideShiftEnable = true;
                Globals.HMISystem.SideShiftValue = (Int16)(targetPosition);
            }
        }

        private void ForkMover()
        {
            dialogKeypad = new DialogKeypad();
            if (dialogKeypad.ShowDialog() == true)
            {
                double targetPosition = 0;

                FORKMOVER_TARGET.Content = dialogKeypad.Result;
                if (FORKMOVER_TARGET.Content.ToString() == "")
                {
                    FORKMOVER_TARGET.Content = "0";
                }

                targetPosition = Double.Parse(FORKMOVER_TARGET.Content.ToString());
                if (targetPosition > Globals.FORK_MOVER_MAX_LIMIT)
                {
                    FORKMOVER_TARGET.Content = Globals.FORK_MOVER_MAX_LIMIT.ToString();
                    MessageBox.Show("최대 " + Globals.FORK_MOVER_MAX_LIMIT.ToString() + "mm 까지 입력 가능합니다.");
                    return;
                }
                if (targetPosition < Globals.FORK_MOVER_MIN_LIMIT)
                {
                    FORKMOVER_TARGET.Content = Globals.FORK_MOVER_MIN_LIMIT.ToString();
                    MessageBox.Show("최소 " + Globals.FORK_MOVER_MIN_LIMIT.ToString() + "mm 부터 입력 가능합니다.");
                    return;
                }

                this.FORKMOVER_POSITION.IsEnabled = false;
                this.FORKMOVER_UP.IsEnabled = false;
                this.FORKMOVER_DOWN.IsEnabled = false;

                Globals.HMISystem.MoverEnable = true;
                Globals.HMISystem.MoverValue = (char)targetPosition;
            }
        }

        private void ChargeStart()
        {
            Globals.HMISystem.ChargeEnable = true;
        }

        private void ChargeEnd()
        {
            Globals.HMISystem.ChargeEnable = false;
        }

        private void ClampStart()
        {
            Globals.HMISystem.ClampEnable = true;
        }

        private void ClampEnd()
        {
            Globals.HMISystem.ClampEnable = false;
        }


        private void BtnPressed_Tick(object sender, EventArgs e)
        {
            LIFT_CURRENT.Content = (ushort)Globals.ForkLiftSystem.LiftPosition;
            TILT_CURRENT.Content = (sbyte)Globals.ForkLiftSystem.TiltPosition;
            REACH_CURRENT.Content = (ushort)Globals.ForkLiftSystem.ReachPosition;
            SIDESHIFT_CURRENT.Content = (short)(Globals.ForkLiftSystem.ShiftRightPosition- Globals.ForkLiftSystem.ShiftLeftPosition) / 2;
            FORKMOVER_CURRENT.Content = (ushort)Globals.ForkLiftSystem.MoverPosition;
            BATTERY_CURRENT.Content = Globals.BMSSystem.Battery + "%";
            CHARGE_STATUS.Content = Globals.VehicleSystem.SystemCharge == true ? "충전중" : "충전해제";

            if (Globals.ForkLiftSystem.LiftState == true || Globals.ForkLiftSystem.TiltState == true || Globals.ForkLiftSystem.ReachState == true || Globals.ForkLiftSystem.SideShiftState == true || Globals.ForkLiftSystem.MoverState == true)
            {
                this.LIFT_POSITION.IsEnabled = true;
                this.TILT_POSITION.IsEnabled = true;
                this.REACH_POSITION.IsEnabled = true;
                this.FORKMOVER_POSITION.IsEnabled = true;
                this.SIDESHIFT_POSITION.IsEnabled = true;

                this.LIFT_UP.IsEnabled = true;
                this.LIFT_DOWN.IsEnabled = true;
                this.TILT_UP.IsEnabled = true;
                this.TILT_DOWN.IsEnabled = true;
                this.REACH_UP.IsEnabled = true;
                this.REACH_DOWN.IsEnabled = true;
                this.SIDESHIFT_UP.IsEnabled = true;
                this.SIDESHIFT_DOWN.IsEnabled = true;
                this.FORKMOVER_UP.IsEnabled = true;
                this.FORKMOVER_DOWN.IsEnabled = true;
            }

            if (this.LIFT_UP.IsPressed)
            {
                Globals.HMISystem.LiftEnable = true;
                Globals.HMISystem.ForkliftValue = 0x01;
            }
            else if (this.LIFT_DOWN.IsPressed)
            {
                Globals.HMISystem.LiftEnable = true;
                Globals.HMISystem.ForkliftValue = 0x02;
            }
            else if (this.TILT_UP.IsPressed)
            {
                Globals.HMISystem.TiltEnable = true;
                Globals.HMISystem.ForkliftValue = 0x01;
            }
            else if (this.TILT_DOWN.IsPressed)
            {
                Globals.HMISystem.TiltEnable = true;
                Globals.HMISystem.ForkliftValue = 0x02;
            }
            else if (this.REACH_UP.IsPressed)
            {
                Globals.HMISystem.ReachEnable = true;
                Globals.HMISystem.ForkliftValue = 0x01;
            }
            else if (this.REACH_DOWN.IsPressed)
            {
                Globals.HMISystem.ReachEnable = true;
                Globals.HMISystem.ForkliftValue = 0x02;
            }
            else if (this.SIDESHIFT_UP.IsPressed)
            {
                Globals.HMISystem.SideShiftEnable = true;
                Globals.HMISystem.ForkliftValue = 0x01;
            }
            else if (this.SIDESHIFT_DOWN.IsPressed)
            {
                Globals.HMISystem.SideShiftEnable = true;
                Globals.HMISystem.ForkliftValue = 0x02;
            }
            else if (this.FORKMOVER_UP.IsPressed)
            {
                Globals.HMISystem.MoverEnable = true;
                Globals.HMISystem.ForkliftValue = 0x01;
            }
            else if (this.FORKMOVER_DOWN.IsPressed)
            {
                Globals.HMISystem.MoverEnable = true;
                Globals.HMISystem.ForkliftValue = 0x02;
            }
            else
            {
                if(this.LIFT_POSITION.IsEnabled == true && this.TILT_POSITION.IsEnabled == true && this.REACH_POSITION.IsEnabled == true && this.SIDESHIFT_POSITION.IsEnabled == true && this.FORKMOVER_POSITION.IsEnabled == true) { 
                    Globals.HMISystem.ReachEnable = false;
                    Globals.HMISystem.LiftEnable = false;
                    Globals.HMISystem.TiltEnable = false;
                    Globals.HMISystem.MoverEnable = false;
                    Globals.HMISystem.SideShiftEnable = false;
                    Globals.HMISystem.ForkliftValue = 0x00;
                }
            }
        }
    }
}

