﻿
using System;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.Threading;
using System.Windows.Threading;
using MobyusTouch.src.Global;

namespace MobyusTouch
{
    public partial class DialogBMS : Window
    {
        DispatcherTimer timer = new DispatcherTimer();
        public DialogBMS()
        {
            InitializeComponent();
            this.DataContext = this;

            timer.Interval = TimeSpan.FromMilliseconds(0.3);
            timer.Tick += new EventHandler(BtnPressed_Tick);
            timer.Start();
        }

        public void TimerStop()
        {
            timer.Stop();
        }
        
        private void BtnPressed_Tick(object sender, EventArgs e)
        {
            //CHARGE_VOLT.Content = Globals.BMSSystem.ChargeVolt;
            CELL_VOLT.Content = Globals.BMSSystem.CellVolt;
            //PACK_VOLT.Content = Globals.BMSSystem.PackVolt;
            //PACK_CURRENT.Content = Globals.BMSSystem.PackCurrent;
            TEMPERATURE.Content = Globals.BMSSystem.Temperature;

            V.Content = (Double)Globals.BMSSystem.Bat_V/100;
            I.Content = (double)Globals.BMSSystem.Bat_i/100;
            SOC.Content = Globals.BMSSystem.SOC;
            SOH.Content = Globals.BMSSystem.SOH;
            

            //STATUS.Content = Globals.BMSSystem.Status;
            //BATTERY.Content = Globals.BMSSystem.Battery;
            //FAULT.Content = Globals.BMSSystem.Fault;
            //WARNING.Content = Globals.BMSSystem.Warning;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

