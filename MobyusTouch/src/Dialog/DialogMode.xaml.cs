﻿
using System;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using MobyusTouch.src.Global;

namespace MobyusTouch
{
    public partial class DialogMode : Window, INotifyPropertyChanged
    {

        private string _result;
        public string Result
        {
            get { return _result; }
            private set { _result = value; this.OnPropertyChanged("Result"); }
        }

        private SystemMode _modeIndex;
        public SystemMode ModeIndex
        {
            get { return _modeIndex; }
            private set { _modeIndex = value; }
        }

        
        public DialogMode(string mode)
        {
            InitializeComponent();
            this.DataContext = this;
            _result = mode;
            if(_result.Equals("OFFLINE"))
            {
                _modeIndex = SystemMode.OFFLINE;
            }
            else if (_result.Equals("MANUAL MODE"))
            {
                _modeIndex = SystemMode.MANUAL;
            }
            else if (_result.Equals("SEMI AUTO MODE"))
            {
                _modeIndex = SystemMode.SEMI_AUTO;
            }
            else if (_result.Equals("AUTO MODE"))
            {
                _modeIndex = SystemMode.AUTO;
            }

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            switch (button.CommandParameter.ToString())
            {
                case "OFFLINE":
                    Result = "OFFLINE";
                    ModeIndex = SystemMode.OFFLINE;
                    break;

                case "MANUAL":
                    Result = "MANUAL MODE";
                    ModeIndex = SystemMode.MANUAL;
                    this.DialogResult = true;
                    break;

                case "SEMI":
                    Result = "SEMI AUTO MODE";
                    ModeIndex = SystemMode.SEMI_AUTO;
                    this.DialogResult = true;
                    break;

                case "AUTO":
                    Result = "AUTO MODE";
                    ModeIndex = SystemMode.AUTO;
                    this.DialogResult = true;
                    break;

                case "OK":
                    this.DialogResult = true;
                    break;

                case "Cancel":
                    this.DialogResult = false;
                    break;

                default:
                    break;
            }
        }

        #region INotifyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }




        #endregion


    }
}

