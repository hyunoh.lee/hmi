﻿using EditorLibrary;
using MobyusTouch.src.Communication;
using MobyusTouch.src.Global;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MobyusTouch.src.Control
{
    [Serializable()]
    class ExceptionControlHyundai : System.Exception
    {
        private static Logger logger = LogManager.GetLogger("ExceptionControlHyundai");

        private static int ObstacleCnt = 0;
        private static int WT100Cnt = 0;
        private static int BMSChargeCnt = 0;

        private static int CAN_220_CNT = 0;
        private static int CAN_221_CNT = 0;

        private static int CAN_241_CNT = 0;
        private static int CAN_242_CNT = 0;

        private static int CAN_260_CNT = 0;
        private static int CAN_261_CNT = 0;
        private static int CAN_263_CNT = 0;
        private static int CAN_264_CNT = 0;
        private static int CAN_265_CNT = 0;
        private static int CAN_266_CNT = 0;
        private static int CAN_267_CNT = 0;
        private static int CAN_268_CNT = 0;
        private static int CAN_269_CNT = 0;

        private static int CAN_290_CNT = 0;
        private static int CAN_291_CNT = 0;
        private static int CAN_293_CNT = 0;

        private static int CAN_330_CNT = 0;
        private static int CAN_333_CNT = 0;
        private static int CAN_334_CNT = 0;
        private static int CAN_340_CNT = 0;
        private static int CAN_341_CNT = 0;
        private static int CAN_342_CNT = 0;

        System.Threading.Timer bmsTimer;
        System.Threading.Timer bmsChargeTimer;
        System.Threading.Timer bmsWarnningErrorCheck;
        System.Threading.Timer canTimer;
        System.Threading.Timer obstacleStopTimer;
        System.Threading.Timer cameraRecognitionTimer;
        System.Threading.Timer navTimer;
        System.Threading.Timer navigationTimer;
        System.Threading.Timer forkliftTimer;
        System.Threading.Timer vehicleTimer;
        System.Threading.Timer fmsTimer;
        System.Threading.Timer workDurationOverTimer;
        System.Threading.Timer timerRCUCommCheckTimer;

        private static bool bNav350err = false;


        public ExceptionControlHyundai()
        {
            AddAlarmInfo();
                        
            TimerStart();
        }



        ~ExceptionControlHyundai()
        {
            TimerStop();
        }

        public void TimerStart()
        {
            if (!Globals.debug)
            {
                //TAMS와의 통신이 20분이상 연결 안될 시 알람 발생 개발 ------------------------------------------------------------------------

                // CAN 통신 알람
                canTimer = new System.Threading.Timer(CANAlarm);
                canTimer.Change(3000, 3000); // 15초

                // 장애물 지속 알람  // WT100 지속 알람 23.04.21kdy
                obstacleStopTimer = new System.Threading.Timer(ObstacleStopAlarm);
                obstacleStopTimer.Change(3000, 5000); //3초기다린후 5초마다 체크


                // 인식 시스템(Nano) 팔렛 알람
                cameraRecognitionTimer = new System.Threading.Timer(CameraRecognitionAlarm);
                cameraRecognitionTimer.Change(3000, 500);

                // Navigation Board 알람
                navigationTimer = new System.Threading.Timer(NavigationAlarm);
                navigationTimer.Change(3000, 2000);

                // Forklift Board 알람
                forkliftTimer = new System.Threading.Timer(ForkliftAlarm);
                forkliftTimer.Change(3000, 2000);

                // Vehicle Board 알람
                vehicleTimer = new System.Threading.Timer(VehicleAlarm);
                vehicleTimer.Change(3000, 500);

                // NAV 알람
                navTimer = new System.Threading.Timer(NAVAlarm);
                navTimer.Change(3000, 5000);

                // ACS(FMS) 통신 연결 경고
                fmsTimer = new System.Threading.Timer(FMSWarning);
                fmsTimer.Change(3000, 3000);

                // BMS Warnning Error 체크
                bmsWarnningErrorCheck = new System.Threading.Timer(BMSWarningErrorCheck);
                bmsWarnningErrorCheck.Change(3000, 1000);

                // 작업 제한 시간 오버 
                workDurationOverTimer = new System.Threading.Timer(WorkDurationOverAlarm);
                workDurationOverTimer.Change(3000, 10000);

                timerRCUCommCheckTimer = new System.Threading.Timer(CheckRCUCommAlarm);
                timerRCUCommCheckTimer.Change(3000, 1000);

                ////////////////////////////////////////////
                ///

                // BMS 알람  
                bmsTimer = new System.Threading.Timer(BMSAlarm);
                bmsTimer.Change(3000, 3000); //프로그램 시작 후 10분뒤에 동작

            }
        }

        public void TimerStop()
        {
            if (!Globals.debug)
            {
                canTimer.Dispose();
                obstacleStopTimer.Dispose();
                cameraRecognitionTimer.Dispose();
                navigationTimer.Dispose();
                navTimer.Dispose();
                fmsTimer.Dispose();
                // bmsChargeTimer.Dispose();
                // bmsTimer.Dispose();
                workDurationOverTimer.Dispose();

                if (timerRCUCommCheckTimer != null)
                    timerRCUCommCheckTimer.Dispose();

            }
        }

        ////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////
        // ACS(FMS) 통신 연결 경고
        /// Warning

        public void FMSWarning(Object state)
        {
            if (Globals.HMISystem.SystemMode == SystemMode.AUTO && !MqttClientHyundai.IsConnected())
            {
                Globals.currentWork.alarm = Alarms.ALARM_MQTT_CONNECTION_ERROR;
                Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_PAUSE;
                Globals.HMISystem.ACStoHMI_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_PAUSE;
            }
        }


        ////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////
        /// Alarm

        public void WorkDurationOverAlarm(Object state)
        {
            Globals.currentWork.workDuration++;
            if (Globals.currentWork.workQueue != null && Globals.currentWork.workDuration >= 120) // 20분 이상 시 알람
            {
                Globals.currentWork.alarm = Alarms.ALARM_WORK_TIME_OVER_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1작업시간초과/C1  에    러 !]";
            }
        }
        // ACS RC ALRARM
        public void CheckRCUCommAlarm(Object state)
        {
            // ACS RCU 통신 체크
            if (Globals.HMISystem.SystemMode == SystemMode.AUTO)
            {
                Globals.nRCUConnectCheck++;

                if (Globals.nRCUConnectCheck > 3) // 3초
                {
                    logger.Info("   RCU Connected Warning  " + "   NAV [X] " + Globals.NavigationSystem.NAV_X + " [Y] " + Globals.NavigationSystem.NAV_Y + " [Theta] " + Globals.NavigationSystem.NAV_PHI + " [NODE] " + Globals.currentWork.curNode);
                    Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_PAUSE;
                    Globals.HMISystem.HMItoACS_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_RCU_PAUSE;
                    Thread.Sleep(200);

                    if (!Globals.bRCUAlarmCheck)
                    {
                        Globals.bRCUAlarmCheck = true;

                        Globals.currentWork.alarm = Alarms.ALARM_ACS_RCU_COMM_WARNING;
                        Globals.currentWork.alarm_name = "![000/E5151/C1관 제 통 신 /C1  경    고 !]";

                    }

                    if (Globals.nRCUConnectCheck > 600) // 10분
                    {
                        logger.Info("   RCU Connected Error");
                        Globals.currentWork.alarm = Alarms.ALARM_ACS_COMM_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1관 제 통 신 /C1  에    러 !]";
                        Globals.nRCUConnectCheck = 0;
                        Globals.bRCUAlarmCheck = false;
                    }
                    Globals.nRCUConnectCheck = 0;
                }
            }
        }

        // CAN ALARM
        public void CANAlarm(Object state)
        {
            #region CAN 220 연결 확인
            if (Globals.CAN_220_FLAG == false)
            {
                //logger.Info(" CAN 220 ERROR COUNT START" + CAN_220_CNT);
                CAN_220_CNT++;
            }
            else
            {
                CAN_220_CNT = 0;
            }
            if (CAN_220_CNT >= 10)
            {
                //logger.Info(" CAN 220 ERROR COUNT END" + CAN_220_CNT);
                CAN_220_CNT = 0;
                logger.Info(" [CAN] CAN_220 Connection Error ");
                Globals.currentWork.alarm = Alarms.ALARM_CAN_220_VEHICLE_CONNECTION_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1차 량 통 신 /C1  에러220 !]";
            }
            #endregion

            #region CAN 221 연결 확인
            if (Globals.CAN_221_FLAG == false)
            {
                //logger.Info(" CAN 221 ERROR COUNT START" + CAN_221_CNT);
                CAN_221_CNT++;
            }
            else
            {
                CAN_221_CNT = 0;
            }
            if (CAN_221_CNT >= 10)
            {
                //logger.Info(" CAN 221 ERROR COUNT END" + CAN_221_CNT);
                CAN_221_CNT = 0;
                logger.Info(" [CAN] CAN_221 Connection Error ");
                Globals.currentWork.alarm = Alarms.ALARM_CAN_221_VEHICLE_CONNECTION_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1차 량 통 신 /C1  에러221 !]";
            }
            #endregion

            #region CAN 241 연결 확인
            if (Globals.CAN_241_FLAG == false)
            {
                //logger.Info(" CAN 241 ERROR COUNT START" + CAN_241_CNT);
                CAN_241_CNT++;
            }
            else
            {
                CAN_241_CNT = 0;
            }
            if (CAN_241_CNT >= 10)
            {
                //logger.Info(" CAN 241 ERROR COUNT END" + CAN_241_CNT);
                CAN_241_CNT = 0;
                logger.Info(" [CAN] CAN_241 Connection Error ");
                Globals.currentWork.alarm = Alarms.ALARM_CAN_241_FORKLIFT_CONNECTION_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1포 크 통 신 /C1  에러241 !]";
            }
            #endregion

            #region CAN 242 연결 확인
            if (Globals.CAN_242_FLAG == false)
            {
                CAN_242_CNT++;
            }
            else
            {
                CAN_242_CNT = 0;
            }
            if (CAN_242_CNT >= 10)
            {
                CAN_242_CNT = 0;
                logger.Info(" [CAN] CAN_242 Connection Error ");
                Globals.currentWork.alarm = Alarms.ALARM_CAN_242_FORKLIFT_CONNECTION_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1포 크 통 신 /C1  에러242 !]";
            }
            #endregion

            #region CAN 260 연결 확인
            if (Globals.CAN_260_FLAG == false)
            {
                CAN_260_CNT++;
            }
            else
            {
                CAN_260_CNT = 0;
            }
            if (CAN_260_CNT >= 10)
            {
                CAN_260_CNT = 0;
                logger.Info(" [CAN] CAN_260 Connection Error ");
                Globals.currentWork.alarm = Alarms.ALARM_CAN_260_NAVIGATION_CONNECTION_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1보 드 통 신 /C1  에러260 !]";
            }
            #endregion

            #region CAN 261 연결 확인
            if (Globals.CAN_261_FLAG == false)
            {
                //logger.Info(" CAN 261 ERROR COUNT START" + CAN_261_CNT);
                CAN_261_CNT++;
            }
            else
            {
                CAN_261_CNT = 0;
            }
            if (CAN_261_CNT >= 10)
            {
                //logger.Info(" CAN 261 ERROR COUNT END" + CAN_261_CNT);
                CAN_261_CNT = 0;
                logger.Info(" [CAN] CAN_261 Connection Error ");
                Globals.currentWork.alarm = Alarms.ALARM_CAN_261_NAVIGATION_CONNECTION_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1위 치 통 신 /C1  에러261 !]";
            }
            #endregion

            #region CAN 263 연결 확인
            if (Globals.CAN_263_FLAG == false)
            {
                //logger.Info(" CAN 261 ERROR COUNT START" + CAN_263_CNT);
                CAN_263_CNT++;
            }
            else
            {
                CAN_263_CNT = 0;
            }
            if (CAN_263_CNT >= 10)
            {
                //logger.Info(" CAN 263 ERROR COUNT END" + CAN_263_CNT);
                CAN_263_CNT = 0;
                logger.Info(" [CAN] CAN_263 Connection Error ");
                Globals.currentWork.alarm = Alarms.ALARM_CAN_261_NAVIGATION_CONNECTION_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1보 드 통 신 /C1  에러263 !]";
            }
            #endregion

            #region CAN 264 연결 확인
            if (Globals.CAN_264_FLAG == false)
            {
                //logger.Info(" CAN 264 ERROR COUNT START" + CAN_264_CNT);
                CAN_264_CNT++;
            }
            else
            {
                CAN_264_CNT = 0;
            }
            if (CAN_264_CNT >= 10)
            {
                //logger.Info(" CAN 264 ERROR COUNT END" + CAN_264_CNT);
                CAN_264_CNT = 0;
                logger.Info(" [CAN] CAN_264 Connection Error ");
                Globals.currentWork.alarm = Alarms.ALARM_CAN_261_NAVIGATION_CONNECTION_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1보 드 통 신 /C1  에러264 !]";
            }
            #endregion

            #region CAN 265 연결 확인
            if (Globals.CAN_265_FLAG == false)
            {
                //logger.Info(" CAN 265 ERROR COUNT START" + CAN_265_CNT);
                CAN_265_CNT++;
            }
            else
            {
                CAN_265_CNT = 0;
            }
            if (CAN_265_CNT >= 10)
            {
                //logger.Info(" CAN 265 ERROR COUNT END" + CAN_265_CNT);
                CAN_265_CNT = 0;
                logger.Info(" [CAN] CAN_265 Connection Error ");
                Globals.currentWork.alarm = Alarms.ALARM_CAN_261_NAVIGATION_CONNECTION_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1보 드 통 신 /C1  에러265 !]";
            }
            #endregion

            #region CAN 266 연결 확인
            if (Globals.CAN_266_FLAG == false)
            {
                //logger.Info(" CAN 266 ERROR COUNT START" + CAN_266_CNT);
                CAN_266_CNT++;
            }
            else
            {
                CAN_266_CNT = 0;
            }
            if (CAN_266_CNT >= 10)
            {
                //logger.Info(" CAN 266 ERROR COUNT END" + CAN_266_CNT);
                CAN_266_CNT = 0;
                logger.Info(" [CAN] CAN_266 Connection Error ");
                Globals.currentWork.alarm = Alarms.ALARM_CAN_261_NAVIGATION_CONNECTION_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1보 드 통 신 /C1  에러266 !]";
            }
            #endregion

            #region CAN 267 연결 확인
            if (Globals.CAN_267_FLAG == false)
            {
                //logger.Info(" CAN 267 ERROR COUNT START" + CAN_267_CNT);
                CAN_267_CNT++;
            }
            else
            {
                CAN_267_CNT = 0;
            }
            if (CAN_267_CNT >= 10)
            {
                //logger.Info(" CAN 267 ERROR COUNT END" + CAN_267_CNT);
                CAN_267_CNT = 0;
                logger.Info(" [CAN] CAN_267 Connection Error ");
                Globals.currentWork.alarm = Alarms.ALARM_CAN_261_NAVIGATION_CONNECTION_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1보 드 통 신 /C1  에러267 !]";
            }
            #endregion
            #region CAN 268 연결 확인
            if (Globals.CAN_268_FLAG == false)
            {
                //logger.Info(" CAN 268 ERROR COUNT START" + CAN_268_CNT);
                CAN_268_CNT++;
            }
            else
            {
                CAN_268_CNT = 0;
            }
            if (CAN_268_CNT >= 10)
            {
                //logger.Info(" CAN 268 ERROR COUNT END" + CAN_268_CNT);
                CAN_268_CNT = 0;
                logger.Info(" [CAN] CAN_268 Connection Error ");
                Globals.currentWork.alarm = Alarms.ALARM_CAN_261_NAVIGATION_CONNECTION_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1보 드 통 신 /C1  에러268 !]";
            }
            #endregion
            #region CAN 269 연결 확인
            if (Globals.CAN_269_FLAG == false)
            {
                //logger.Info(" CAN 269 ERROR COUNT START" + CAN_269_CNT);
                CAN_269_CNT++;
            }
            else
            {
                CAN_269_CNT = 0;
            }
            if (CAN_269_CNT >= 10)
            {
                //logger.Info(" CAN 269 ERROR COUNT END" + CAN_269_CNT);
                CAN_269_CNT = 0;
                logger.Info(" [CAN] CAN_269 Connection Error ");
                Globals.currentWork.alarm = Alarms.ALARM_CAN_261_NAVIGATION_CONNECTION_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1보 드 통 신 /C1  에러269 !]";
            }
            #endregion

            #region CAN 290 연결 확인
            if (Globals.HMISystem.PalletStackingEnable || Globals.HMISystem.PalletFrontEnable)
            {
                if (Globals.CAN_290_FLAG == false)
                {
                    CAN_290_CNT++;
                }
                else
                {
                    CAN_290_CNT = 0;
                }
            }
            if (CAN_290_CNT >= 10)
            {
                CAN_290_CNT = 0;
                logger.Info(" [CAN] CAN_290 Connection Error ");
                Globals.currentWork.alarm = Alarms.ALARM_CAN_290_RECOGNITION_CONNECTION_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1인 식 통 신 /C1  에러290 !]";
            }
            #endregion

            //#region CAN 293 연결 확인
            //if (Globals.HMISystem.PalletStackingEnable || Globals.HMISystem.PalletFrontEnable)
            //{
            //    if (Globals.CAN_293_FLAG == false)
            //    {
            //        CAN_293_CNT++;
            //    }
            //    else
            //    {
            //        CAN_293_CNT = 0;
            //    }
            //}
            //if (CAN_293_CNT >= 10)
            //{
            //    CAN_293_CNT = 0;
            //    logger.Info(" [CAN] CAN_293 Connection Error ");
            //    Globals.currentWork.alarm = Alarms.ALARM_CAN_293_RECOGNITION_CONNECTION_ERROR;
            //    Globals.currentWork.alarm_name = "![000/E5151/C1인 식 통 신 /C1  에러293 !]";
            //}
            //#endregion

            #region CAN 330 연결 확인
            if (Globals.CAN_330_FLAG == false)
            {
                CAN_330_CNT++;
            }
            else
            {
                CAN_330_CNT = 0;
            }
            if (CAN_330_CNT >= 10)
            {
                CAN_330_CNT = 0;
                logger.Info(" [CAN] CAN_330 Connection Error ");
                Globals.currentWork.alarm = Alarms.ALARM_CAN_330_SAFETY_CONNECTION_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1안 전 통 신 /C1  에러330 !]";
            }
            #endregion

            //#region CAN 333 연결 확인
            ////if (Globals.CAN_333_FLAG == false)
            ////{
            ////    CAN_333_CNT++;
            ////}
            ////else
            ////{
            ////    CAN_333_CNT = 0;
            ////}
            ////if (CAN_333_CNT >= 10)
            ////{
            ////    CAN_333_CNT = 0;
            ////    logger.Info(" [CAN] CAN_333 Connection Error ");
            ////    //Globals.currentWork.alarm = Alarms.ALARM_CAN_333_GATEWAY_CONNECTION_ERROR;
            ////}
            //#endregion

            #region CAN 334 연결 확인
            //if (Globals.CAN_334_FLAG == false)
            //{
            //    CAN_334_CNT++;
            //}
            //else
            //{
            //    CAN_334_CNT = 0;
            //}
            //if (CAN_334_CNT >= 10)
            //{
            //    CAN_334_CNT = 0;
            //    logger.Info(" [CAN] CAN_334 Connection Error ");
            //    Globals.currentWork.alarm = Alarms.ALARM_CAN_334_SAFETY_CONNECTION_ERROR;
            //    Globals.currentWork.alarm_name = "![000/E5151/C1안 전 통 신 /C1  에러334 !]";
            //}
            #endregion

            #region CAN 340 연결 확인
            if (Globals.CAN_340_FLAG == false)
            {
                CAN_340_CNT++;
            }
            else
            {
                CAN_340_CNT = 0;
            }
            if (CAN_340_CNT >= 10)
            {
                CAN_340_CNT = 0;
                logger.Info(" [CAN] CAN_340 Connection Error ");
                Globals.currentWork.alarm = Alarms.ALARM_CAN_340_BMS_CONNECTION_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1 배터리통신 /C1  에러340 !]";
            }
            #endregion

            #region CAN 341 연결 확인
            if (Globals.CAN_341_FLAG == false)
            {
                CAN_341_CNT++;
            }
            else
            {
                CAN_341_CNT = 0;
            }
            if (CAN_341_CNT >= 10)
            {
                CAN_341_CNT = 0;
                //lhw 341 에러
                logger.Info(" [CAN] CAN_341 Connection Error ");
                Globals.currentWork.alarm = Alarms.ALARM_CAN_341_BMS_CONNECTION_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1 배터리통신 /C1  에러341 !]";
            }
            #endregion

            #region CAN 342 연결 확인
            if (Globals.CAN_342_FLAG == false)
            {
                CAN_342_CNT++;
            }
            else
            {
                CAN_342_CNT = 0;
            }
            if (CAN_342_CNT >= 10)
            {
                CAN_342_CNT = 0;
                //lhw 342 에러
                logger.Info(" [CAN] CAN_342 Connection Error ");
                Globals.currentWork.alarm = Alarms.ALARM_CAN_342_BMS_CONNECTION_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1 배터리통신 /C1  에러342 !]";
            }
            #endregion

            Globals.CAN_181_FLAG = false;
            Globals.CAN_220_FLAG = false;
            Globals.CAN_221_FLAG = false;
            Globals.CAN_241_FLAG = false;
            Globals.CAN_242_FLAG = false;
            Globals.CAN_260_FLAG = false;
            Globals.CAN_261_FLAG = false;
            Globals.CAN_263_FLAG = false;
            Globals.CAN_264_FLAG = false;
            Globals.CAN_265_FLAG = false;
            Globals.CAN_266_FLAG = false;
            Globals.CAN_267_FLAG = false;
            Globals.CAN_290_FLAG = false;
            Globals.CAN_293_FLAG = false;
            Globals.CAN_330_FLAG = false;
            Globals.CAN_333_FLAG = false;
            Globals.CAN_334_FLAG = false;
            Globals.CAN_340_FLAG = false;
            Globals.CAN_341_FLAG = false;
            Globals.CAN_342_FLAG = false;
        }

        // NAV 알람
        public void NAVAlarm(Object state)
        {
            if (Globals.NavigationSystem.NAVState != 0)
            {
                logger.Info(" ###### Nav State " + Globals.NavigationSystem.NAVState);
                switch (Globals.NavigationSystem.NAVState)
                {

                    case 1:
                        try
                        {
                            // kdy 23.05.08 nav에러 펌웨어에서 보기때문에 hmi는 삭제 
                            //if (!bNav350err)
                            //{
                            //    bNav350err = true;
                            //    byte tmpline = 0;
                            //    byte tmpdr = 0;

                            //    if (Globals.linkData.Count > 0)
                            //    { 
                            //        for (int i = 0; i < Globals.linkData.Count; i++)
                            //        {
                            //            if (Globals.currentWork.curLink == Globals.linkData[i].GetLinkNumber())
                            //            {
                            //                tmpline = (byte)((Globals.linkData[i].GetBoption() >> 1) & (0x01));
                            //                tmpdr = (byte)((Globals.linkData[i].GetBoption()) & (0x01));
                            //            }
                            //        }
                            //    }

                            //    if (tmpline == 0 && tmpdr ==0)  // DR, 라인트래킹 이외 체크 되있을 경우
                            //    {
                            //        Globals.currentWork.alarm = Alarms.ALARM_NAV_350_ERROR;
                            //        Globals.currentWork.alarm_name = "위치에러";
                            //    }
                            //    bNav350err = false;
                            //}
                            if (Globals.bNAV_StateCheck)
                            {
                                Globals.bNAV_StateCheck = false;
                                logger.Info("  Nav Alarm Receive [NAVState] 1");
                            }
                            break;
                        }
                        catch(Exception e)
                        {
                            logger.Error(e.ToString());
                            Globals.currentWork.alarm = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                            Globals.currentWork.alarm_name = "![000/E5151/C1알 수 없 는 /C1  에    러 !]";
                            break;
                        }
                    case 2:
                        //Globals.currentWork.alarm = Alarms.ALARM_NAV_GYRO_ERROR;
                        break;
                    case 3:
                        Globals.currentWork.alarm = Alarms.ALARM_NAV_NO_DATA_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1위치센서데이터에러!]";
                        break;
                    case 4:
                        Globals.currentWork.alarm = Alarms.ALARM_NAV_CAN_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1위 치 통 신 /C1  에    러 !]";
                        break;
                    default:
                        break;

                }
            }
            else
            {
                Globals.bNAV_StateCheck = true;
            }
        }

        // 장애물 지속 알람
        public void ObstacleStopAlarm(Object state)
        {
            if ((Globals.NavigationSystem.VStateBeamStopArea == true || Globals.NavigationSystem.bUltraSonic == true || Globals.SafetySystem.VisioneryT) && Globals.currentWork.workType != Globals.TRANSFER_CHARGE && Globals.currentWork.workType != Globals.TRANSFER_READY)
            {
                ObstacleCnt++;
                if (ObstacleCnt >= 36 && Globals.NavigationSystem.Alarm == 0) //3분
                {
                    ObstacleCnt = 0;
                    Globals.currentWork.alarm = Alarms.ALARM_NAVI_OBSTACLE_STOP_KEEP_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1 장애물감지 /C1  에    러 !]";
                }
            }
            else
            {
                ObstacleCnt = 0;
            }

            //23.04.21kdy wt100(포크센서)가 15초 지속되면 알람치기
            if ((Globals.SafetySystem.LeftWT100 || Globals.SafetySystem.RightWT100) && Globals.NavigationSystem.bWT100 && Globals.currentWork.workType != Globals.TRANSFER_CHARGE && Globals.currentWork.workType != Globals.TRANSFER_READY)
            {
                WT100Cnt++;
                if (WT100Cnt > 3) // 15초
                {
                    WT100Cnt = 0;
                    Globals.currentWork.alarm = Alarms.ALARM_NAVI_OBSTACLE_WT100_STOP_KEEP_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1포크센서감지/C1  에    러 !]";
                }
            }
            else
            {
                WT100Cnt = 0;
            }
        }

        // 인식 시스템(Nano) 팔렛 알람
        public void CameraRecognitionAlarm(Object state)
        {
            if (Globals.RecognitionSystem.Alarm != 0)
            {
                logger.Info("   Recognition Alarm  [X] " + Globals.RecognitionSystem.TargetX + " [Y] " + Globals.RecognitionSystem.TargetY + " [Theta] " + Globals.RecognitionSystem.TargetTheta);


                switch (Globals.RecognitionSystem.Alarm)
                {
                    case 1:
                        Globals.currentWork.alarm = Alarms.ALARM_RECOGNITION_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 팔레트인식 /C1  에    러 !]";
                        break;
                    case 2:
                        Globals.currentWork.alarm = Alarms.ALARM_RECOGNITION_PALLET_BOTTOM_LEFT_ROI_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1팔레트왼쪽인식센서에러!]";
                        break;
                    case 3:
                        Globals.currentWork.alarm = Alarms.ALARM_RECOGNITION_PALLET_BOTTOM_RIGHT_ROI_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1팔레트오른쪽인식센서에러!]";
                        break;
                    case 4:
                        Globals.currentWork.alarm = Alarms.ALARM_RECOGNITION_PALLET_BOTTOM_LEFT_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1팔레트왼쪽인식센서에러!]";
                        break;
                    case 5:
                        Globals.currentWork.alarm = Alarms.ALARM_RECOGNITION_PALLET_BOTTOM_RIGHT_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1팔레트왼쪽인식센서에러!]";
                        break;
                    case 6:
                        Globals.currentWork.alarm = Alarms.ALARM_RECOGNITION_PALLET_TOP_LEFT_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1팔레트왼쪽인식센서에러!]";
                        break;
                    case 7:
                        Globals.currentWork.alarm = Alarms.ALARM_RECOGNITION_PALLET_TOP_RIGHT_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1팔레트왼쪽인식센서에러!]";
                        break;
                    case 8:
                        Globals.currentWork.alarm = Alarms.ALARM_RECOGNITION_ITEM_HEIGHT_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1제품높이불일치에러!]";
                        break;
                    case 9:
                        Globals.currentWork.alarm = Alarms.ALARM_RECOGNITION_PALLET_ANGLE_OVER_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1팔레트각도초과에러!]";
                        break;
                    case 10: // case 22랑 중복 확인필요 23.05.16 kdy
                        Globals.currentWork.alarm = Alarms.ALARM_RECOGNITION_DRIVE_DATA_Y_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1주행좌우오차/C1  에    러 !]";
                        break;
                    case 21:
                        Globals.currentWork.alarm = Alarms.ALARM_RECOGNITION_DRIVE_DATA_X_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1주행전후오차/C1  에    러 !]";
                        break;
                    case 22:
                        Globals.currentWork.alarm = Alarms.ALARM_RECOGNITION_DRIVE_DATA_Y_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1주행좌우오차/C1  에    러 !]";
                        break;
                    case 23:
                        Globals.currentWork.alarm = Alarms.ALARM_RECOGNITION_DRIVE_DATA_T_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1주행각도오차/C1  에    러 !]";
                        break;
                    case 24:
                        Globals.currentWork.alarm = Alarms.ALARM_RECOGNITION_SAFETY_DATA_X_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1안착감지전후오차에러!]";
                        break;
                    case 25:
                        Globals.currentWork.alarm = Alarms.ALARM_RECOGNITION_SAFETY_DATA_Y_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1안착감지좌우오차에러!]";
                        break;
                    case 26:
                        Globals.currentWork.alarm = Alarms.ALARM_RECOGNITION_SAFETY_DATA_T_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1안착감지각도오차에러!]";
                        break;
                    case 27:
                        Globals.currentWork.alarm = Alarms.ALARM_RECOGNITION_FRONT_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1전방장애물인식에러!]";
                        break;
                    case 31:
                        Globals.currentWork.alarm = Alarms.ALARM_RECOGNITION_CAN_RECEIVE_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1인 식 통 신 /C1  에    러 !]";
                        break;
                    case 32:
                        Globals.currentWork.alarm = Alarms.ALARM_RECOGNITION_ROS_TOPIC_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1ROS카메라토픽에러!]";
                        break;
                    case 99:
                        Globals.currentWork.alarm = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1소 스 코 드 /C1  에    러 !]";
                        break;
                    default:
                        Globals.currentWork.alarm = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1소 스 코 드 /C1  에    러 !]";
                        break;
                }
            }

            if (Globals.RecognitionSystem.LineAlarm == 2)
            {
                Globals.currentWork.alarm = Alarms.ALARM_RECOGNITION_LINE_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1라 인 인 식 /C1  에    러 !]";
            }
        }
        // BMS Battery 알람
        public void BMSAlarm(Object state)
        {
            #region kdy 23.07.27 변경된 배터리는 해당 fault이제 안올려줌 
            // kdy 23.07.27 변경된 배터리는 해당 fault이제 안올려줌 
            if (Globals.BMSSystem.Fault != 0)
            {
                switch (Globals.BMSSystem.Fault)
                {
                    case 1:
                        Globals.currentWork.alarm = Alarms.ALARM_BMS_COMM_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                        break;
                    case 2:
                        Globals.currentWork.alarm = Alarms.ALARM_BMS_MAIN_RELAY_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                        break;
                    case 3:
                        Globals.currentWork.alarm = Alarms.ALARM_BMS_PRE_CHARGE_RELAY_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                        break;
                    case 4:
                        Globals.currentWork.alarm = Alarms.ALARM_BMS_FUSE_OPEN_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                        break;
                    case 5:
                        Globals.currentWork.alarm = Alarms.ALARM_BMS_SHUTDOWN_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                        break;
                    case 6:
                        Globals.currentWork.alarm = Alarms.ALARM_BMS_MAIN_RELAY_STATUS_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                        break;
                    case 7:
                        Globals.currentWork.alarm = Alarms.ALARM_BMS_PRE_CHARGE_RELAY_STATUS_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                        break;
                    default:
                        break;
                }
            }
            #endregion

            #region 온도
            // 충전중일 때 온도가 높으면 에러
            if (Globals.currentWork.workState == (byte)WorkState.CHARGING && Globals.HMISystem.ChargeEnable && Globals.VehicleSystem.SystemCharge && Globals.BMSSystem.Temperature > 50) // 배터리 온도 제한 35 -> 50도로 변경
            {
                Globals.currentWork.alarm = Alarms.ALARM_BMS_TEMPERATURE_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1배터리온도이상에러!]";
            }
            //else if (Globals.BMSSystem.Temperature > 30) //평균 18도
            //{
            //    Globals.currentWork.alarm = Alarms.ALARM_BMS_TEMPERATURE_ERROR;
            //    Globals.currentWork.alarm_name = "배터리에러";
            //}
            #endregion

            #region 전압
            if(Globals.HMISystem.SystemMode == SystemMode.AUTO) // 23.09.07 kdy 순간적으로 전압량이 튀는 현상이 있어 카운트 추가
            {
                if ((Globals.BMSSystem.Bat_V / 100) >= 90) // 과전압 에러
                {
                    logger.Info(" [Battery_Voltage] " + Globals.BMSSystem.Bat_V + " [BatVCount] " + Globals.BatVCount);
                    Globals.BatVCount++;
                    if(Globals.BatVCount > 3) // Globals.BatVCount는 3초 주기
                    {
                        logger.Info(" [Battery_Voltage] " + Globals.BMSSystem.Bat_V + " [BatVCount] " + Globals.BatVCount);
                        Globals.currentWork.alarm = Alarms.ALARM_BMS_VOLTAGE_HIGH_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1배터리고전압/C1  에    러 !]";
                        Globals.BatVCount = 0;
                    }
                }
                else
                {
                    Globals.BatVCount = 0;
                }

                if ((Globals.BMSSystem.Bat_V / 100) <= 77) // 저전압 에러
                {
                    logger.Info(" [Battery_Voltage] " + Globals.BMSSystem.Bat_V + " [BatVCount] " + Globals.BatVCount);
                    Globals.BatVCount++;
                    if(Globals.BatVCount > 3) // Globals.BatVCount는 3초 주기
                    {
                        logger.Info(" [Battery_Voltage] " + Globals.BMSSystem.Bat_V + " [BatVCount] " + Globals.BatVCount);
                        Globals.currentWork.alarm = Alarms.ALARM_BMS_VOLTAGE_LOW_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1배터리저전압/C1  에    러 !]";
                        Globals.BatVCount = 0;
                    }
                }
                else
                {
                    Globals.BatVCount = 0;
                }
            }
            #endregion

            #region SOC
            if (Globals.BMSSystem.SOC < 15) // 30프로 이하면 에러
            {
                Globals.currentWork.alarm = Alarms.ALARM_BMS_BATTERY_LOW_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1 배터리부족 /C1  에    러 !]";
                //Globals.currentWork.alarm = Alarms.ALARM_BMS_VOLTAGE_LOW_ERROR;
                //Globals.currentWork.alarm_name = "![000/E5151/C1배터리저전압/C1  에    러 !]";
            }
            #endregion

            #region BMS CAN 통신
            if (Globals.BMSSystem.Alarm != 0) // KDY 23.08.09 BMS CAN 통신 에러 추가
            {
                switch (Globals.BMSSystem.Alarm)
                {
                    case 1:
                        Globals.currentWork.alarm = Alarms.ALARM_BMS_COMM_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 배터리통신 /C1  에    러 !]";
                        break;
                    default:
                        break;
                }
            }
            #endregion

            #region 전류
            //충전중일 때 전류가 안올라오면 에러
            if (Globals.HMISystem.ChargeEnable && Globals.VehicleSystem.SystemCharge)
            {
                if (Globals.BMSSystem.Bat_i / 100 < 75) // 정상범주는 80 ~ 150
                {
                    Globals.CurrentTimeCheck++;
                    if (Globals.CurrentTimeCheck > 60) // 3분
                    {
                        Globals.CurrentTimeCheck = 0;
                        Globals.currentWork.alarm = Alarms.ALARM_BMS_CURRENT_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 배터리전류 /C1  에    러 !]";
                        logger.Error("[Current Error] during charge, Current value is wrong ");
                    }
                }
            }
            #endregion
        }
       
        public void BMSWarningErrorCheck(Object state)
        {
            #region OPV_Flag 팩 과전압
            switch (Globals.BMSSystem.OPV_Flag) // 팩 과전압
            {
                case 0: // Normal
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 0;

                    break;
                case 1: // Warnig
                    Globals.BMSSystem.Warning = 1;
                    Globals.BMSSystem.Fault = 0;
                    break;
                case 2: // Cut-off
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 1;
                    Globals.currentWork.alarm = Alarms.ALARM_BMS_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                    break;
                case 3: // Fault
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 1;
                    Globals.currentWork.alarm = Alarms.ALARM_BMS_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                    break;
            }
            #endregion

            #region 팩 저전압
            switch (Globals.BMSSystem.UPV_Flag) // 팩 저전압
            {
                case 0: // Normal
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 0;
                    break;
                case 1: // Warnig
                    Globals.BMSSystem.Warning = 1;
                    Globals.BMSSystem.Fault = 0;
                    break;
                case 2: // Cut-off
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 1;
                    Globals.currentWork.alarm = Alarms.ALARM_BMS_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                    break;
                case 3: // Fault
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 1;
                    Globals.currentWork.alarm = Alarms.ALARM_BMS_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                    break;
            }
            #endregion

            #region 셀 과전압
            switch (Globals.BMSSystem.OCV_Flag) // 셀 과전압
            {
                case 0: // Normal
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 0;
                    break;
                case 1: // Warnig
                    Globals.BMSSystem.Warning = 1;
                    Globals.BMSSystem.Fault = 0;
                    break;
                case 2: // Cut-off
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 1;
                    Globals.currentWork.alarm = Alarms.ALARM_BMS_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                    break;
                case 3: // Fault
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 1;
                    Globals.currentWork.alarm = Alarms.ALARM_BMS_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                    break;
            }
            #endregion

            #region 셀 저전압
            switch (Globals.BMSSystem.UCV_Flag) // 셀 저전압
            {
                case 0: // Normal
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 0;
                    break;
                case 1: // Warnig
                    Globals.BMSSystem.Warning = 1;
                    Globals.BMSSystem.Fault = 0;
                    break;
                case 2: // Cut-off
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 1;
                    Globals.currentWork.alarm = Alarms.ALARM_BMS_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                    break;
                case 3: // Fault
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 1;
                    Globals.currentWork.alarm = Alarms.ALARM_BMS_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                    break;
            }
            #endregion

            #region 과 온도
            switch (Globals.BMSSystem.OT_Flag) // 과 온도
            {
                case 0: // Normal
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 0;
                    break;
                case 1: // Warnig
                    Globals.BMSSystem.Warning = 1;
                    Globals.BMSSystem.Fault = 0;
                    break;
                case 2: // Cut-off
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 1;
                    Globals.currentWork.alarm = Alarms.ALARM_BMS_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                    break;
                case 3: // Fault
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 1;
                    Globals.currentWork.alarm = Alarms.ALARM_BMS_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                    break;
            }
            #endregion

            #region 저 온도
            switch (Globals.BMSSystem.UT_Flag) // 저 온도
            {
                case 0: // Normal
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 0;
                    break;
                case 1: // Warnig
                    Globals.BMSSystem.Warning = 1;
                    Globals.BMSSystem.Fault = 0;
                    break;
                case 2: // Cut-off
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 1;
                    Globals.currentWork.alarm = Alarms.ALARM_BMS_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                    break;
                case 3: // Fault
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 1;
                    Globals.currentWork.alarm = Alarms.ALARM_BMS_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                    break;
            }
            #endregion

            #region 과 셀전압 편차
            switch (Globals.BMSSystem.ODV_Flag) // 과 셀전압 편차
            {
                case 0: // Normal
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 0;
                    break;
                case 1: // Warnig
                    Globals.BMSSystem.Warning = 1;
                    Globals.BMSSystem.Fault = 0;
                    break;
                case 2: // Cut-off
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 1;
                    Globals.currentWork.alarm = Alarms.ALARM_BMS_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                    break;
                case 3: // Fault
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 1;
                    Globals.currentWork.alarm = Alarms.ALARM_BMS_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                    break;
            }
            #endregion

            #region 과 온도 편차
            switch (Globals.BMSSystem.ODT_Flag) // 과 온도 편차
            {
                case 0: // Normal
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 0;
                    break;
                case 1: // Warnig
                    Globals.BMSSystem.Warning = 1;
                    Globals.BMSSystem.Fault = 0;
                    break;
                case 2: // Cut-off
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 1;
                    Globals.currentWork.alarm = Alarms.ALARM_BMS_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                    break;
                case 3: // Fault
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 1;
                    Globals.currentWork.alarm = Alarms.ALARM_BMS_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                    break;
            }
            #endregion

            #region 과 전류
            switch (Globals.BMSSystem.OC_Flag) // 과 전류
            {
                case 0: // Normal
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 0;
                    break;
                case 1: // Warnig
                    Globals.BMSSystem.Warning = 1;
                    Globals.BMSSystem.Fault = 0;
                    break;
                case 2: // Cut-off
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 1;
                    Globals.currentWork.alarm = Alarms.ALARM_BMS_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                    break;
                case 3: // Fault
                    Globals.BMSSystem.Warning = 0;
                    Globals.BMSSystem.Fault = 1;
                    Globals.currentWork.alarm = Alarms.ALARM_BMS_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1 배  터  리 /C1  에    러 !]";
                    break;
            }
            #endregion
        }

        // Navigation Board 알람
        public void NavigationAlarm(Object state)
        {
            ushort alarm = Alarms.NO_ALARM;
            if (Globals.NavigationSystem.Alarm != 0)
            {
                if (Globals.nNaviAlarm_Check != Globals.NavigationSystem.Alarm)
                {
                    Globals.nNaviAlarm_Check = Globals.NavigationSystem.Alarm;
                }

                if(Globals.currentWork.alarm == Alarms.NO_ALARM || Globals.currentWork.alarm > Alarms.WARNING)
                {
                    logger.Info("Globals.NavigationSystem.Alarm" + Globals.NavigationSystem.Alarm);
                    switch (Globals.NavigationSystem.Alarm)
                    {
                        case 1:
                            if(Globals.currentWork.alarm < 200)
                            {
                                break;
                            }
                            else
                            {
                                alarm = Alarms.ALARM_NAVI_BOARD_EMO_ERROR;
                                Globals.currentWork.alarm_name = "![000/E5151/C1보드이상정지/C1  에    러 !]";
                                break;
                            }
                        case 2:
                            alarm = Alarms.ALARM_NAVI_BOARD_BUMPER_STOP_ERROR;
                            Globals.currentWork.alarm_name = "![000/E5151/C1범 퍼 스 탑 /C1  에    러 !]";
                            break;
                        case 3:
                            alarm = Alarms.ALARM_NAVI_BOARD_MOTION_ERROR;
                            Globals.currentWork.alarm_name = "![000/E5151/C1경로설정이상/C1  에    러 !]";
                            break;
                        case 4:
                            alarm = Alarms.ALARM_NAVI_BOARD_MAP_ERROR;
                            Globals.currentWork.alarm_name = "![000/E5151/C1맵경로불일치/C1  에    러 !]";
                            break;
                        case 5:
                            alarm = Alarms.ALARM_NAVI_COMM_ERROR;
                            Globals.currentWork.alarm_name = "![000/E5151/C1보 드 통 신 /C1  에    러 !]";
                            break;
                        case 6:
                            alarm = Alarms.ALARM_NAVI_BOARD_PATH_OVER_ERROR;
                            Globals.currentWork.alarm_name = "![000/E5151/C1경 로 이 탈 /C1  에    러 !]";
                            break;
                        case 7:
                            alarm = Alarms.ALARM_NAVI_BOARD_HEADING_OVER_ERROR;
                            Globals.currentWork.alarm_name = "![000/E5151/C1방향성불일치/C1  에    러 !]";
                            break;
                        case 8:
                            alarm = Alarms.ALARM_NAVI_BOARD_LOCAL_TARGET_ERROR;
                            Globals.currentWork.alarm_name = "![000/E5151/C1 목적지이탈 /C1  에    러 !]";
                            break;
                        case 9:
                            alarm = Alarms.ALARM_NAVI_BOARD_NAV_ERROR;
                            Globals.currentWork.alarm_name = "![000/E5151/C1위 치 통 신 /C1  에    러 !]";
                            break;
                        case 10:
                            alarm = Alarms.ALARM_NAVI_BOARD_STACKING_ERROR;
                            logger.Info("   [Globals.RecognitionSystem.TargetX] " + Globals.RecognitionSystem.TargetX + " [Globals.RecognitionSystem.TargetX] " + Globals.RecognitionSystem.TargetY + " [Globals.RecognitionSystem.TargetTheta] " + Globals.RecognitionSystem.TargetTheta);
                            Globals.currentWork.alarm_name = "![000/E5151/C1 팔레트이재 /C1  에    러 !]";
                            break;
                        case 11:
                            alarm = Alarms.ALARM_NAVI_BOARD_AFL_CAN_ERROR;
                            Globals.currentWork.alarm_name = "![000/E5151/C1두산지게차자체에러!]";
                            break;
                        case 12: // KDY 23.08.02 급발진 방어코드, 속도이상에러
                            alarm = Alarms.ALARM_NAVI_BOARD_SPEED_ERROR;
                            Globals.currentWork.alarm_name = "![000/E5151/C1속도이상동작/C1  에    러 !]";
                            break;
                        case 13:
                            alarm = Alarms.ALARM_NAVI_BOARD_LINE_DETECTION_ERROR;
                            Globals.currentWork.alarm_name = "![000/E5151/C1라 인 인 식 /C1  에    러 !]";
                            break;
                        case 14:
                            alarm = Alarms.ALARM_NAVI_LOADING_FAULT_ERROR;
                            Globals.currentWork.alarm_name = "![000/E5151/C1적 재 불 가 /C1  에    러 !]";
                            break;
                        case 15:
                            alarm = Alarms.ALARM_CHARGE_ERROR;
                            Globals.currentWork.alarm_name = "![000/F0005/E5151/C1충전에러!]";
                            break;
                        case 16:
                            alarm = Alarms.ALARM_GYRO_ERROR;
                            Globals.currentWork.alarm_name = "![000/E5151/C1평 행 센 서 /C1  에    러 !]";
                            break;
                        case 17:
                            alarm = Alarms.ALARM_NAVI_UNNORMALSTEER_ERROR;
                            Globals.currentWork.alarm_name = "![000/F0005/E5151/C1조향에러!]";
                            break;
                        case 18:
                            alarm = Alarms.ALARM_DSN_ERROR;
                            Globals.currentWork.alarm_name = "![000/E5151/C1두 산 자 체 /C1  에    러 !]";
                            break;
                        default:
                            break;
                    }
                    Globals.currentWork.alarm = alarm;
                }
            }
            else
            {
                Globals.nNaviAlarm_Check = 0;
            }
        }

        // Forklift Board 알람
        public void ForkliftAlarm(Object state)
        {
            if (Globals.ForkLiftSystem.LiftSensorAlarm == true)
            {
                Globals.currentWork.alarm = Alarms.ALARM_FORK_LIFT_SENSOR_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1포크리프트센서에러!]";
            }

            if (Globals.ForkLiftSystem.TiltSensorAlarm == true)
            {
                Globals.currentWork.alarm = Alarms.ALARM_FORK_TILT_SENSOR_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1포크틸트센서/C1  에    러 !]";
            }

            if (Globals.ForkLiftSystem.ShiftLeftSensorAlarm == true)
            {
                Globals.currentWork.alarm = Alarms.ALARM_FORK_SHIFT_LEFT_SENSOR_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1왼쪽시프트센서에러!]";
            }

            if (Globals.ForkLiftSystem.ShiftRightSensorAlarm == true)
            {
                Globals.currentWork.alarm = Alarms.ALARM_FORK_SHIFT_RIGHT_SENSOR_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1오른쪽시프트센서에러!]";
            }

            if (Globals.ForkLiftSystem.LiftControlAlarm == true)
            {
                Globals.currentWork.alarm = Alarms.ALARM_FORK_LIFT_CONTROL_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1포크리프트센서에러!]";
            }

            if (Globals.ForkLiftSystem.TiltControlAlarm == true)
            {
                Globals.currentWork.alarm = Alarms.ALARM_FORK_TILT_CONTROL_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1포크틸트센서/C1  에    러 !]";
            }

            if (Globals.ForkLiftSystem.MoverControlAlarm == true)
            {
                Globals.currentWork.alarm = Alarms.ALARM_FORK_MOVER_CONTROL_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1 포크폭간격 /C1  에    러 !]";
            }

            if (Globals.ForkLiftSystem.ShiftControlAlarm == true)
            {
                Globals.currentWork.alarm = Alarms.ALARM_FORK_SHIFT_CONTROL_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1포크좌우동작/C1  에    러 !]";
            }
        }

        // Vehicle Board 알람
        public void VehicleAlarm(Object state)
        {

            if (Globals.VehicleSystem.SystemEMO && Globals.NavigationSystem.Alarm == 0) // && Globals.currentWork.alarm != Alarms.NO_ALARM
            {
                Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_BOARD_EMO_SWITCH_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1비상정지버튼/C1  에    러 !]";
            }

            if (Globals.VehicleSystem.DSNTractionAlarm > 50 && Globals.VehicleSystem.DSNTractionAlarm < 68)
            {
                switch (Globals.VehicleSystem.DSNTractionAlarm)
                {
                    case 51:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_CAN_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1차 량 통 신 /C1  에    러 !]";
                        break;
                    case 52:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_PUMP_PDO_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1지게차구동력/C1  에    러 !]";
                        break;
                    case 53:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_THROTTLE_ON_STARTUP_PDO_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1지게차구동력/C1  에    러 !]";
                        break;
                    case 54:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_SRO_DIRECTION_BEFORE_SEAT_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1지게차구동력/C1  에    러 !]";
                        break;
                    case 55:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_THROTTLE_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1지게차구동력/C1  에    러 !]";
                        break;
                    case 56:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_STEER_POT_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1지게차구동력/C1  에    러 !]";
                        break;
                    case 57:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_LOW_BATTERY_VOLT_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1배터리저전압/C1  에    러 !]";
                        break;
                    case 58:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_CALIBRATION_DISPLAY_SELECTED_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1지게차구동력/C1  에    러 !]";
                        break;
                    case 62:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_DISPLAY_PDO_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1지게차구동력/C1  에    러 !]";
                        break;
                    case 63:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_PDO_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1지게차구동력/C1  에    러 !]";
                        break;
                    case 64:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_SEVERE_STEERING_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1지게차구동력/C1  에    러 !]";
                        break;
                    case 65:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_STEERING_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1지게차구동력/C1  에    러 !]";
                        break;
                    case 66:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_AGV_PDO_TIMEOUT_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1지게차구동력/C1  에    러 !]";
                        break;
                    case 67:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_PDO_TIMEOUT_BMS_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1지게차구동력/C1  에    러 !]";
                        break;
                    default:
                        Globals.currentWork.alarm = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                        break;
                }
            }

            if (Globals.VehicleSystem.DSNPumpAlarm > 50 && Globals.VehicleSystem.DSNPumpAlarm < 68)
            {
                switch (Globals.VehicleSystem.DSNPumpAlarm)
                {
                    case 51:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_PUMP_SIGN_ON_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 지게차펌프 /C1  에    러 !]";
                        break;
                    case 52:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_JOYSTICK_COMM_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 지게차펌프 /C1  에    러 !]";
                        break;
                    case 53:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_JOYSTICK_LIFT_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 지게차펌프 /C1  에    러 !]";
                        break;
                    case 54:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_JOYSTICK_INPUT_BEFORE_KEW_SWITCH_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 지게차펌프 /C1  에    러 !]";
                        break;
                    case 55:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_JOYSTICK_TILT_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 지게차펌프 /C1  에    러 !]";
                        break;
                    case 56:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_JOYSTICK_AUX1_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 지게차펌프 /C1  에    러 !]";
                        break;
                    case 57:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_JOYSTICK_AUX2_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 지게차펌프 /C1  에    러 !]";
                        break;
                    case 58:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_FINGERTIP_ENABLE_CHANGE_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 지게차펌프 /C1  에    러 !]";
                        break;
                    case 59:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_EXM_TILT_BACK_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 지게차펌프 /C1  에    러 !]";
                        break;
                    case 61:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_EXM_LIFT_SOL_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 지게차펌프 /C1  에    러 !]";
                        break;
                    case 62:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_EXM_LOW_SOL_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 지게차펌프 /C1  에    러 !]";
                        break;
                    case 63:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_EXM_AUX1_FWD_SOL_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 지게차펌프 /C1  에    러 !]";
                        break;
                    case 64:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_EXM_TILT_FWD_SOL_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 지게차펌프 /C1  에    러 !]";
                        break;
                    case 65:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_EXM_AUX1_BACK_SOL_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 지게차펌프 /C1  에    러 !]";
                        break;
                    case 66:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_EXM_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 지게차펌프 /C1  에    러 !]";
                        break;
                    case 67:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_EXM_PDO_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1 지게차펌프 /C1  에    러 !]";
                        break;
                    default:
                        //logger.Info("VehicleSystem.DSNTractionAlarm : " + Globals.VehicleSystem.DSNTractionAlarm);
                        //Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_EXM_PDO_ERROR;
                        break;
                }
            }
        }

        public void AddAlarmInfo()
        {
            // 제품 감지 알람
            Globals.alarmTable.Add(Alarms.ALARM_PALLET_LOADED_ERROR, "ALARM_PALLET_LOADED_ERROR|명령은 적재이나,이미 팔레트를 적재하고 있습니다."); // 명령이 적재고 지게차에 팔렛이 적재되어있는 경우
            Globals.alarmTable.Add(Alarms.ALARM_PALLET_LOADED_NOT_ERROR, "ALARM_PALLET_LOADED_NOT_ERROR|명령은 이재이나,적재된 팔레트가 존재하지 않습니다."); // 명령이 이재고 지게차에 팔렛이 없을 경우

            Globals.alarmTable.Add(Alarms.ALARM_PALLET_FRONT_EXIST_ERROR, "ALARM_PALLET_FRONT_EXIST_ERROR|명령은 1단 이재이나,전방에 1단 팔레트가 존재합니다."); // 명령이 1단 이재일 경우 이재 위치에 1단 팔렛이 있을 경우
            Globals.alarmTable.Add(Alarms.ALARM_PALLET_FRONT_NOT_EXIST_ERROR, "ALARM_PALLET_FRONT_NOT_EXIST_ERROR|명령은 2단 이재이나,전방에 팔레트가 존재하지 않습니다."); // 명령이 2단 이재일 경우 팔렛이 1단 이재 위치에 없을 경우

            // MAP 관련
            Globals.alarmTable.Add(Alarms.ALARM_MOVE_1ST_PATH_PLANNING_ERROR, "ALARM_MOVE_1ST_PATH_PLANNING_ERROR|1차 경로를 생성하지 못하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_MOVE_2ND_PATH_PLANNING_ERROR, "ALARM_MOVE_2ND_PATH_PLANNING_ERROR|2차 경로를 생성하지 못하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_MOVE_PATH_SENDING_ERROR, "ALARM_MOVE_PATH_SENDING_ERROR|네비게이션 보드에 경로를 전달하지 못하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_MOVE_NO_PATH_DATA_ERROR, "ALARM_MOVE_NO_PATH_DATA_ERROR|현재위치에서 해당 목적지까지 경로가 존재하지 않습니다.목적지를 다시 확인하십시요.");
            Globals.alarmTable.Add(Alarms.ALARM_MOVE_FRONT_PALLET_ANGLE_OVER_ERROR, "ALARM_MOVE_FRONT_PALLET_ANGLE_OVER_ERROR|전방 팔레트의 인식 범위를 벗어났습니다.팔레트를 정위치로 위치 후 다시 시도하시기 바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_MOVE_COMPLETE_ERROR, "ALARM_MOVE_COMPLETE_ERROR|주행 완료를 수행하지 못하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_MAP_SET_CURRENT_NODE_ERROR, "ALARM_MAP_SET_CURRENT_NODE_ERROR|지게차의 현재 위치를 찾지 못합니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_MAP_SET_CURRENT_NODE_SEND_SERIAL_ERROR, "ALARM_MAP_SET_CURRENT_NODE_SEND_SERIAL_ERROR|지게차의 현재 위치를 네비게이션 보드에 전달하지 못하였습니다.담당자에게 문의바랍니다. ");
            Globals.alarmTable.Add(Alarms.ALARM_MAP_NOT_FOUND_NODE_INFO_ERROR, "ALARM_MAP_NOT_FOUND_NODE_INFO_ERROR|MAP의 노드 정보를 찾을 수 없습니다.MAP 정보를 확인하시기 바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_MAP_TRANSFER_HEADING_ERROR, "ALARM_MAP_TRANSFER_HEADING_ERROR|MAP과 지게차의 방향성이 일치하지 않습니다.MAP 정보를 확인 후 지게차의 방향성을 맞추시길 바랍니다.");

            // 충전 알람
            Globals.alarmTable.Add(Alarms.ALARM_CHARGE_ERROR, "ALARM_CHARGE_ERROR|충전 시 알람이 발생하였습니다.담당자에게 문의바랍니다.");

            // 자이로 알람
            Globals.alarmTable.Add(Alarms.ALARM_GYRO_ERROR, "ALARM_GYRO_ERROR|자이로 센서 알람이 발생하였습니다.담당자에게 문의바랍니다.");

            // ACS 명령 에러
            Globals.alarmTable.Add(Alarms.ALARM_ACS_WRONG_ORDER, "ALARM_ACS_WRONG_ORDER|ACS 명령에 오류가 있습니다. 담당자에게 문의바랍니다.");

            // ACS 연결 에러
            Globals.alarmTable.Add(Alarms.ALARM_ACS_COMM_ERROR, "ALARM_ACS_COMM_ERROR|ACS 통신 연결 에러입니다. 담당자에게 문의바랍니다.");

            // EMO switch
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_BOARD_EMO_SWITCH_ERROR, "ALARM_VEHICLE_BOARD_EMO_SWITCH_ERROR|EMO 버튼을 해제 후 자동 모드로 변경하시기 바랍니다.");

            // 장애물 지속 감지 알람 2분 이상
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_OBSTACLE_STOP_KEEP_ERROR, "[ALARM_NAVI_OBSTACLE_STOP_KEEP_ERROR]|장애물 감지가 3분이상 지속되었습니다. 장애물을 제거 후 운영하시기 바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_OBSTACLE_WT100_STOP_KEEP_ERROR, "[ALARM_NAVI_OBSTACLE_WT100_STOP_KEEP_ERROR]|포크센서 감지가 15초이상 지속되었습니다. 장애물을 제거 후 운영하시기 바랍니다.");

            // Vehicle 관련 알람
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_CAN_ERROR, "ALARM_VEHICLE_CAN_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_PUMP_PDO_ERROR, "ALARM_VEHICLE_PUMP_PDO_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_THROTTLE_ON_STARTUP_PDO_ERROR, "ALARM_VEHICLE_THROTTLE_ON_STARTUP_PDO_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_SRO_DIRECTION_BEFORE_SEAT_ERROR, "ALARM_VEHICLE_SRO_DIRECTION_BEFORE_SEAT_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_THROTTLE_ERROR, "ALARM_VEHICLE_THROTTLE_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_STEER_POT_ERROR, "ALARM_VEHICLE_STEER_POT_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_LOW_BATTERY_VOLT_ERROR, "ALARM_VEHICLE_LOW_BATTERY_VOLT_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_CALIBRATION_DISPLAY_SELECTED_ERROR, "ALARM_VEHICLE_CALIBRATION_DISPLAY_SELECTED_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_DISPLAY_PDO_ERROR, "ALARM_VEHICLE_DISPLAY_PDO_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_PDO_ERROR, "ALARM_VEHICLE_PDO_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_SEVERE_STEERING_ERROR, "ALARM_VEHICLE_SEVERE_STEERING_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_STEERING_ERROR, "ALARM_VEHICLE_STEERING_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_AGV_PDO_TIMEOUT_ERROR, "ALARM_VEHICLE_AGV_PDO_TIMEOUT_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_PDO_TIMEOUT_BMS_ERROR, "ALARM_VEHICLE_PDO_TIMEOUT_BMS_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");

            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_PUMP_SIGN_ON_ERROR, "ALARM_VEHICLE_PUMP_SIGN_ON_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_JOYSTICK_COMM_ERROR, "ALARM_VEHICLE_JOYSTICK_COMM_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_JOYSTICK_LIFT_ERROR, "ALARM_VEHICLE_JOYSTICK_LIFT_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_JOYSTICK_INPUT_BEFORE_KEW_SWITCH_ERROR, "ALARM_VEHICLE_JOYSTICK_INPUT_BEFORE_KEW_SWITCH_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_JOYSTICK_TILT_ERROR, "ALARM_VEHICLE_JOYSTICK_TILT_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_JOYSTICK_AUX1_ERROR, "ALARM_VEHICLE_JOYSTICK_AUX1_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_JOYSTICK_AUX2_ERROR, "ALARM_VEHICLE_JOYSTICK_AUX2_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_FINGERTIP_ENABLE_CHANGE_ERROR, "ALARM_VEHICLE_FINGERTIP_ENABLE_CHANGE_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_EXM_TILT_BACK_ERROR, "ALARM_VEHICLE_EXM_TILT_BACK_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_EXM_LIFT_SOL_ERROR, "ALARM_VEHICLE_EXM_LIFT_SOL_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_EXM_LOW_SOL_ERROR, "ALARM_VEHICLE_EXM_LOW_SOL_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_EXM_AUX1_FWD_SOL_ERROR, "ALARM_VEHICLE_EXM_AUX1_FWD_SOL_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_EXM_TILT_FWD_SOL_ERROR, "ALARM_VEHICLE_EXM_TILT_FWD_SOL_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_EXM_AUX1_BACK_SOL_ERROR, "ALARM_VEHICLE_EXM_AUX1_BACK_SOL_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_EXM_ERROR, "ALARM_VEHICLE_EXM_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_EXM_PDO_ERROR, "ALARM_VEHICLE_EXM_PDO_ERROR|두산지게차 내부 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");

            // Navigation 관련 알람
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_BOARD_STACKING_ERROR, "ALARM_NAVI_BOARD_STACKING_ERROR|스태킹 시 잘못된 데이터로 인해 알람이 발생하였습니다.");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_BOARD_EMO_ERROR, "ALARM_NAVI_BOARD_EMO_ERROR|EMO 스위치가 ON 되었습니다.EMO 스위치 해제후 운영하시기 바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_BOARD_BUMPER_STOP_ERROR, "ALARM_NAVI_BOARD_BUMPER_STOP_ERROR|범퍼 알람이 발생하였습니다.후방 범퍼 상태를 확인 후 HMI 상단의 모드 변환에서 자동 모드로 변환하십시요.");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_BOARD_MOTION_ERROR, "ALARM_NAVI_BOARD_MOTION_ERROR|주행 중 Motion 알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_BOARD_MAP_ERROR, "ALARM_NAVI_BOARD_MAP_ERROR|주행 중 MAP 데이터를 확인할 수 없어 알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_BOARD_VIRTUAL_LINK_ERROR, "ALARM_NAVI_BOARD_VIRTUAL_LINK_ERROR|주행 중 Vitual Link 값을 확인할 수 없어 알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_BOARD_PATH_OVER_ERROR, "ALARM_NAVI_BOARD_PATH_OVER_ERROR|주행 중 경로를 이탈하였습니다.지게차를 수동모드로 전환 후 MAP 노드 위에 지게차를 정차 후 다시 시도하시기 바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_BOARD_HEADING_OVER_ERROR, "ALARM_NAVI_BOARD_HEADING_OVER_ERROR|지게차와 MAP의 방향성이 일치하지 않아 알람이 발생하였습니다.MAP 정보를 확인 후 다시 시도하시기 바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_BOARD_LOCAL_TARGET_ERROR, "ALARM_NAVI_BOARD_LOCAL_TARGET_ERROR|지게차가 주행경로를 벗어나 알람이 발생하였습니다.지게차를 수동모드로 전환 후 MAP 노드 위에 지게차를 정차 후 다시 시도하시기 바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_BOARD_NAV_ERROR, "ALARM_NAVI_BOARD_NAV_ERROR|주행보드에서 NAV데이터를 확인할 수 없습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_LOADING_FAULT_ERROR, "ALARM_NAVI_LOADING_FAULT_ERROR|로딩 실패하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_COMM_ERROR, "ALARM_NAVI_COMM_ERROR|HMI와 제어보드 간 통신 에러입니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_BOARD_SPEED_ERROR, "ALARM_NAVI_BOARD_SPEED_ERROR|지게차 속도 이상상황 발생입니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_UNNORMALSTEER_ERROR, "ALARM_NAVI_UNNORMALSTEER_ERROR|지게차 STEER 알람 발생입니다.담당자에게 문의바랍니다.");

            
            // Navigation 추가 알람
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_BOARD_AFL_CAN_ERROR, "ALARM_NAVI_BOARD_AFL_CAN_ERROR|지게차와 CAN통신 에러입니다. 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_BOARD_LINE_DETECTION_ERROR, "ALARM_NAVI_BOARD_LINE_DETECTION_ERROR|라인 인식 에러입니다. 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_CHANGE_AFL_ID_ERROR, "ALARM_NAVI_CHANGE_AFL_ID_ERROR|호차변환 에러입니다. 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_DSN_ERROR, "ALARM_DSN_ERROR|지게차 자체 알람 발생입니다.담당자에게 문의바랍니다.");


            // Fork 관련
            Globals.alarmTable.Add(Alarms.ALARM_FORK_LIFT_RANGE_OVER_ERROR, "ALARM_FORK_LIFT_RANGE_OVER_ERROR|Lift가 동작 가능한 범위를 벗어나 알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_FORK_TILT_RANGE_OVER_ERROR, "ALARM_FORK_TILT_RANGE_OVER_ERROR|Tilt가 동작 가능한 범위를 벗어나 알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_FORK_REACH_RANGE_OVER_ERROR, "ALARM_FORK_REACH_RANGE_OVER_ERROR|Reach가 동작 가능한 범위를 벗어나 알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_FORK_SIDESHIFT_RANGE_OVER_ERROR, "ALARM_FORK_SIDESHIFT_RANGE_OVER_ERROR|Sideshift가 동작 가능한 범위를 벗어나 알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_FORK_MOVER_RANGE_OVER_ERROR, "ALARM_FORK_MOVER_RANGE_OVER_ERROR|ForkMover가 동작 가능한 범위를 벗어나 알람이 발생하였습니다.담당자에게 문의바랍니다.");

            Globals.alarmTable.Add(Alarms.ALARM_FORK_LIFT_SENSOR_ERROR, "ALARM_FORK_LIFT_SENSOR_ERROR|Lift Wire 센서에 알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_FORK_TILT_SENSOR_ERROR, "ALARM_FORK_TILT_SENSOR_ERROR|Tilt Wire 센서에 알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_FORK_SHIFT_LEFT_SENSOR_ERROR, "ALARM_FORK_SHIFT_LEFT_SENSOR_ERROR|SideShift Left Wire 센서에 알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_FORK_SHIFT_RIGHT_SENSOR_ERROR, "ALARM_FORK_SHIFT_RIGHT_SENSOR_ERROR|SideShift Right Wire 센서에 알람이 발생하였습니다.담당자에게 문의바랍니다.");

            Globals.alarmTable.Add(Alarms.ALARM_FORK_LIFT_CONTROL_ERROR, "ALARM_FORK_LIFT_CONTROL_ERROR|리프트 동작 시 알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_FORK_TILT_CONTROL_ERROR, "ALARM_FORK_TILT_CONTROL_ERROR|틸트 동작 시 알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_FORK_MOVER_CONTROL_ERROR, "ALARM_FORK_MOVER_CONTROL_ERROR|포크 무버 동작 시 알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_FORK_SHIFT_CONTROL_ERROR, "ALARM_FORK_SHIFT_CONTROL_ERROR|사이드쉬프트 동작 시 알람이 발생하였습니다.담당자에게 문의바랍니다.");

            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_LINE_ERROR, "ALARM_RECOGNITION_LINE_ERROR|라인 인식에 실패하였습니다.담당자에게 문의바랍니다.");

            // 인식 관련
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_ERROR, "ALARM_RECOGNITION_ERROR|팔레트 인식에 실패하였습니다. 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_PALLET_BOTTOM_LEFT_ROI_ERROR, "ALARM_RECOGNITION_PALLET_BOTTOM_LEFT_ROI_ERROR|팔레트 왼쪽 초기 ROI를 인식하지 못하였습니다. 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_PALLET_BOTTOM_RIGHT_ROI_ERROR, "ALARM_RECOGNITION_PALLET_BOTTOM_RIGHT_ROI_ERROR|팔레트 오른쪽 초기 ROI를 인식하지 못하였습니다. 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_PALLET_BOTTOM_LEFT_ERROR, "ALARM_RECOGNITION_PALLET_BOTTOM_LEFT_ERROR|팔레트 하단 왼쪽을 인식하지 못하였습니다. 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_PALLET_BOTTOM_RIGHT_ERROR, "ALARM_RECOGNITION_PALLET_BOTTOM_RIGHT_ERROR|팔레트 하단 오른쪽을 인식하지 못하였습니다. 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_PALLET_TOP_LEFT_ERROR, "ALARM_RECOGNITION_PALLET_TOP_LEFT_ERROR|팔레트 상단 왼쪽을 인식하지 못하였습니다. 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_PALLET_TOP_RIGHT_ERROR, "ALARM_RECOGNITION_PALLET_TOP_RIGHT_ERROR|팔레트 상단 오른쪽을 인식하지 못하였습니다. 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_ITEM_HEIGHT_ERROR, "ALARM_RECOGNITION_ITEM_HEIGHT_ERROR|팔레트 제품 높이가 맞지 않습니다. 확인 후 다시 시도하시기 바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_PALLET_ANGLE_OVER_ERROR, "ALARM_RECOGNITION_PALLET_ANGLE_OVER_ERROR|팔레트 Angle Over 알람이 발생하였습니다. 담당자에게 문의바랍니다.");


            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_DRIVE_DATA_X_ERROR, "ALARM_RECOGNITION_DATA_Y_AXIS_ERROR|인식 X축 데이터가 정상범위를 벗어났습니다. 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_DRIVE_DATA_Y_ERROR, "ALARM_RECOGNITION_DATA_Y_AXIS_ERROR|인식 Y축 데이터가 정상범위를 벗어났습니다. 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_DRIVE_DATA_T_ERROR, "ALARM_RECOGNITION_DATA_Y_AXIS_ERROR|인식 Angle 데이터가 정상범위를 벗어났습니다. 담당자에게 문의바랍니다.");

            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_SAFETY_DATA_X_ERROR, "ALARM_RECOGNITION_SAFETY_DATA_X_ERROR|정상 안착감지 중 X축 데이터가 정상범위(10mm)를 벗어났습니다. 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_SAFETY_DATA_Y_ERROR, "ALARM_RECOGNITION_SAFETY_DATA_Y_ERROR|정상 안착감지 중 Y축 데이터가 정상범위(10mm)를 벗어났습니다. 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_SAFETY_DATA_T_ERROR, "ALARM_RECOGNITION_SAFETY_DATA_T_ERROR|정상 안착감지 중 Angle 데이터가 정상범위(1도)를 벗어났습니다. 담당자에게 문의바랍니다.");


            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_ROS_TOPIC_ERROR, "ALARM_RECOGNITION_ROS_TOPIC_ERROR|인식시스템 TOPIC에 알람이 발생하였습니다. 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_SYSTEM_ERROR, "ALARM_RECOGNITION_SYSTEM_ERROR|인식시스템 내부 알람이 발생였습니다. 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_CAN_RECEIVE_ERROR, "ALARM_RECOGNITION_CAN_RECEIVE_ERROR|인식시스템에서 CAN 정보를 받지 못하였습니다. 담당자에게 문의바랍니다.");

            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_FRONT_ERROR, "ALARM_RECOGNITION_FRONT_ERROR|전방에 이미 적재물이 있습니다. 담당자에게 문의바랍니다.");



            // CAN 통신 관련
            Globals.alarmTable.Add(Alarms.ALARM_CAN_CONNECTION_ERROR, "ALARM_CAN_CONNECTION_ERROR|CAN 통신에 이상이 있습니다.담당자에게 문의바랍니다.");

            Globals.alarmTable.Add(Alarms.ALARM_CAN_220_VEHICLE_CONNECTION_ERROR, "ALARM_CAN_220_VEHICLE_CONNECTION_ERROR|CAN 220 통신 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_CAN_221_VEHICLE_CONNECTION_ERROR, "ALARM_CAN_221_VEHICLE_CONNECTION_ERROR|CAN 221 통신 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");

            Globals.alarmTable.Add(Alarms.ALARM_CAN_241_FORKLIFT_CONNECTION_ERROR, "ALARM_CAN_241_FORKLIFT_CONNECTION_ERROR|CAN 241 통신 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_CAN_242_FORKLIFT_CONNECTION_ERROR, "ALARM_CAN_242_FORKLIFT_CONNECTION_ERROR|CAN 242 통신 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");

            Globals.alarmTable.Add(Alarms.ALARM_CAN_260_NAVIGATION_CONNECTION_ERROR, "ALARM_CAN_260_NAVIGATION_CONNECTION_ERROR|CAN 260 통신 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_CAN_261_NAVIGATION_CONNECTION_ERROR, "ALARM_CAN_261_NAVIGATION_CONNECTION_ERROR|CAN 261 통신 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");

            Globals.alarmTable.Add(Alarms.ALARM_CAN_290_RECOGNITION_CONNECTION_ERROR, "ALARM_CAN_290_RECOGNITION_CONNECTION_ERROR|CAN 290 통신 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_CAN_291_RECOGNITION_CONNECTION_ERROR, "ALARM_CAN_291_RECOGNITION_CONNECTION_ERROR|CAN 291 통신 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_CAN_293_RECOGNITION_CONNECTION_ERROR, "ALARM_CAN_293_RECOGNITION_CONNECTION_ERROR|CAN 293 통신 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");

            Globals.alarmTable.Add(Alarms.ALARM_CAN_330_SAFETY_CONNECTION_ERROR, "ALARM_CAN_330_SAFETY_CONNECTION_ERROR|CAN 330 통신 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_CAN_333_GATEWAY_CONNECTION_ERROR, "ALARM_CAN_333_GATEWAY_CONNECTION_ERROR|CAN 333 통신 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_CAN_334_SAFETY_CONNECTION_ERROR, "ALARM_CAN_334_SAFETY_CONNECTION_ERROR|CAN 334 통신 알람입니다. 지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_CAN_340_BMS_CONNECTION_ERROR, "ALARM_CAN_340_BMS_CONNECTION_ERROR|CAN 340 통신 알람입니다.지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_CAN_341_BMS_CONNECTION_ERROR, "ALARM_CAN_341_BMS_CONNECTION_ERROR|CAN 341 통신 알람입니다.지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_CAN_342_BMS_CONNECTION_ERROR, "ALARM_CAN_342_BMS_CONNECTION_ERROR|CAN 342 통신 알람입니다.지게차 전원을 ON/OFF하여 다시 시도하시기 바랍니다. 동일 현상 지속 시 담당자에게 문의바랍니다.");

            // NAV
            Globals.alarmTable.Add(Alarms.ALARM_NAV_350_ERROR, "ALARM_NAV_350_ERROR|상단 라이다 위치센서에서 알수없는 오류가 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_NAV_GYRO_ERROR, "ALARM_NAV_GYRO_ERROR|NAV 자이로 알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_NAV_NO_DATA_ERROR, "ALARM_NAV_NO_DATA_ERROR|NAV MAP 데이터 값에서 오류가 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_NAV_CAN_ERROR, "ALARM_NAV_CAN_ERROR|NAV CAN 통신 데이터 오류가 발생하였습니다.담당자에게 문의바랍니다.");

            //이적재 상태확인
            Globals.alarmTable.Add(Alarms.ALARM_LOADED_STATE_ERROR, "ALARM_LOADED_STATE_ERROR|이적재 상태가 올바른지 확인바랍니다.");

            //TAMS 자체 알람
            Globals.alarmTable.Add(Alarms.ALARM_TAMS_EQUIPMENT_FAULT_ERROR, "ALARM_TAMS_EQUIPMENT_FAULT_ERROR|해당 설비 수동상태 입니다.담당자에게 문의바랍니다.");

            //PAUSE 3분 지속시 알람 23.05.18 KDY
            Globals.alarmTable.Add(Alarms.ALARM_TRANSFER_TRAFFIC_REMOTESWITCH_ERROR, "ALARM_TRANSFER_TRAFFIC_REMOTESWITCH_ERROR|일시정지가 3분 지속되었습니다.담당자에게 문의바랍니다.");

            // Work
            Globals.alarmTable.Add(Alarms.ALARM_WORK_FILE_LIST_ERROR, "ALARM_WORK_FILE_LIST_ERROR|Work 파일이 존재하지 않습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_WORK_FILE_FORMAT_ERROR, "ALARM_WORK_FILE_FORMAT_ERROR|Work 파일 내 잘못된 정보가 있습니다. C\\MOBYUS\\Work 파일을 다시 확인바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_WORK_TIME_OVER_ERROR, "ALARM_WORK_TIME_OVER_ERROR|작업 시간 20분을 초과하였습니다. 담당자에게 문의바랍니다.");

            // BMS
            Globals.alarmTable.Add(Alarms.ALARM_BMS_TEMPERATURE_ERROR, "ALARM_BMS_TEMPERATURE_ERROR|배터리 온도 이상알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_BMS_ERROR, "ALARM_BMS_ERROR|배터리 자체 알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_BMS_CURRENT_ERROR, "ALARM_BMS_CURRENT_ERROR|배터리 전류 이상알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_BMS_SOC_ERROR, "ALARM_BMS_SOC_ERROR|배터리 SOC 이상알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_BMS_BATTERY_LOW_ERROR, "ALARM_BMS_BATTERY_LOW_ERROR|배터리 용량이 부족합니다.즉시 충전하시기 바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_BMS_COMM_ERROR, "ALARM_BMS_COMM_ERROR|BMS 통신(CAN) 이상알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_BMS_MAIN_RELAY_ERROR, "ALARM_BMS_MAIN_RELAY_ERROR|BMS Relay 알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_BMS_PRE_CHARGE_RELAY_ERROR, "ALARM_BMS_PRE_CHARGE_RELAY_ERROR|BMS PRE Charge Relay 알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_BMS_FUSE_OPEN_ERROR, "ALARM_BMS_FUSE_OPEN_ERROR|BMS FUSE Open알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_BMS_SHUTDOWN_ERROR, "ALARM_BMS_SHUTDOWN_ERROR|BMS Shutdown 알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_BMS_MAIN_RELAY_STATUS_ERROR, "ALARM_BMS_MAIN_RELAY_STATUS_ERROR|BMS Main Relay알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_BMS_PRE_CHARGE_RELAY_STATUS_ERROR, "ALARM_BMS_PRE_CHARGE_RELAY_STATUS_ERROR|BMS Charge Relay알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_BMS_VOLTAGE_LOW_ERROR, "ALARM_BMS_VOLTAGE_LOW_ERROR|BMS 저전압 알람이 발생하였습니다.담당자에게 문의바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_BMS_VOLTAGE_HIGH_ERROR, "ALARM_BMS_VOLTAGE_HIGH_ERROR|BMS 고전압 알람이 발생하였습니다.담당자에게 문의바랍니다.");


            // 알수 없는 오류 알람
            Globals.alarmTable.Add(Alarms.ALARM_SYSTEM_UNKNOWN_ERROR, "ALARM_SYSTEM_UNKNOWN_ERROR|알수 없는 오류가 발생하였습니다.C\\MOBYUS\\Log 파일의 오늘 일자를 확인 후 담당자에게 문의바랍니다.");

            // Warning
            Globals.alarmTable.Add(Alarms.ALARM_WORK_WRONG_ORDER_ERROR, "ALARM_WORK_WRONG_ORDER_ERROR|작업리스트 오류가 발생하였습니다.해당 명령의 작업 리스트 파일을 다시 확인바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_PROTOCOL_FORMAT_ERROR, "ALARM_PROTOCOL_FORMAT_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_PALLET_TYPE_INFO_ERROR, "ALARM_PALLET_TYPE_INFO_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_MODE_WRONG_ERROR, "ALARM_MODE_WRONG_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_TCP_CONNECTION_ERROR, "ALARM_TCP_CONNECTION_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_SERIALPORT_CONNECTION_ERROR, "ALARM_SERIALPORT_CONNECTION_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_MQTT_CONNECTION_ERROR, "ALARM_MQTT_CONNECTION_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_WORK_ALREADY_ING_ERROR, "ALARM_WORK_ALREADY_ING_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_RELEASE_CHECK, "ALARM_RELEASE_CHECK|릴리즈 레버 체크 바랍니다.");
            Globals.alarmTable.Add(Alarms.ALARM_RETRY, "ALARM_RETRY|인식 리트라이.");
            Globals.alarmTable.Add(Alarms.ALARM_ACS_RCU_COMM_WARNING, "ALARM_ACS_RCU_COMM_WARNING|RCU 통신 이상 경고.");


        }
    }
}

