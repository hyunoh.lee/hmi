﻿
//using MapDLL;
using EditorLibrary;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobyusTouch.src.Global.Entity.Hyundai
{
    class TGEntity
    {
        public string AmrId;
        public String Cmd;
        public String Result;
        public int ErrorCode;

        public string TMapVersion;
        public List<TempNodeInfo> NodeList = new List<TempNodeInfo>();
        public List<TempLinkInfo> LinkList = new List<TempLinkInfo>();
        public ushort TotalLinkNum;
        public ushort TotalNodeNum;


    }

    public class TempNodeInfo
    {
        public ushort nodenumber;
        public int pointX;
        public int pointY;
        public byte nodetype;
        public byte layernumber;
    }

    public class TempLinkInfo
    {
        public ushort linknumber;
        public ushort startnode;
        public ushort targetnode;
        public byte linktype;
        public byte linkdirection;
        public byte Foption;
        public byte Boption;
        public byte Fvelocity;
        public byte Bvelocity;
        public byte Fsafetych;
        public byte Bsafetych;
        public long linkdistance;
        public ushort curveradius;

        public ushort prelinknumber;
        public ushort nextlinknumber;
        public short curvestartang;
        public short curvesweepang;
        public Point basepoint;

    }
}
