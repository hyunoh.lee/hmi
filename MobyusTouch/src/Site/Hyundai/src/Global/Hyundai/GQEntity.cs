﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobyusTouch.src.Global.Entity.Hyundai
{
    class GQEntity
    {
        public string AmrId;

        public string Cmd;
        public long X;
        public long Y;
        public short T;
        public int State;
        public int ActionID;
        public int Traffic;
        public int AFLTraffic;
        public int Mode;
        public int Auto;
        public int Clamp;
        public int Loaded;
        public int Level;
        public int CurrentNode;
        public int NextNode;
        public int TargetNode;

        public int CurrentLink;
        public int NextLink;

        public int Battery;
        public short Obstacle;
        public int Lift;
        public int Tilt;
        public int SideShift;
        public int ForkMover;

        public int PalletFrontX;
        public int PalletFrontY;
        public int PalletFrontTheta;

        public int PalletStackingX;
        public int PalletStackingY;
        public int PalletStackingTheta;

        public int Speed;
        public int MapRate;

        public int TagID;
        public int ErrorCode;

        public int Retry;
        public int ManualComplete;
        public int ForceComplete;
        public int AFLStart;
    }
}
