﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobyusTouch.src.Global.Entity.Hyundai
{
    class GBEntity
    {
        public string AmrId;
        public String Cmd;

        public int BCellVolt;
        public int BPackVolt;
        public int BPackCurrent;
        public int BChargeVolt;
        public int BTemp;
        public int Battery;
        public int BError;
        public int BWarning;

        public int ErrorCode;
    }
}
