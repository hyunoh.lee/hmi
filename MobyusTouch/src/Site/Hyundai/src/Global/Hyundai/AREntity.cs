﻿
//using MapDLL;
using EditorLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobyusTouch.src.Global.Entity.Hyundai
{
    public class AREntity
    {
        public string AmrId;
        public string Cmd;
        public string Mode;
        public List<TempAstarNodeData> PathList = new List<TempAstarNodeData>();
        public ushort node;
        public int Work;
        public int Level;
        public int PLTType;

        public string Result;
        public int ErrorCode;
    }

    public class TempAstarNodeData
    {
        public ushort LinkNumber;
        public ushort CurrentNodeID;
        public ushort ParentNodeID;
        public byte LinkType;
        public byte LinkDirection;
        public byte LinkOption;
        public byte LayerNumber;
        public byte SafetyChannel;
        public byte MaxVelocity;
        public float VehicleAngle;
        public float LinkAngle;
        public float F_;
        public float G_;
        public float H_;

    }
}
