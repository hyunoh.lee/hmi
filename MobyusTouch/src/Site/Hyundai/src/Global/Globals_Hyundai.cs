﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobyusTouch.src.Global
{
    class Globals_Hyundai
    {
        public const string RESULT_SUCCESS = "S";
        public const string RESULT_FAIL = "F";
        
        public const string PATH_PLANNING_AMR = "A";
        public const string PATH_PLANNING_ACS = "M";

        public const string CHARGE_ENABLE = "C";
        public const string CHARGE_DISABLE = "D";
    }
}
