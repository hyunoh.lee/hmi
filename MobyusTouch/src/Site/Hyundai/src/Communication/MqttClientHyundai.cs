﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using Newtonsoft.Json;
using MobyusTouch.src.Global.Entity;
using MobyusTouch.src.Global;
using System.Threading;
//using MapDLL;
using EditorLibrary;
using MobyusTouch.src.Global.Entity.Hyundai;
using MobyusTouch.src.Main.Sub;
using MobyusTouch.src.Main;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using uPLibrary.Networking.M2Mqtt.Exceptions;
using System.Net.Sockets;
using System.Windows.Media.Media3D;
using System.Windows;
using Point = System.Drawing.Point;
using System.Windows.Threading;

namespace MobyusTouch.src.Communication
{
    public delegate void DialogEMOCloseEvent2();
    public class MqttClientHyundai
    {
        private static Logger logger = LogManager.GetLogger("M2MqttClient");
        public static uPLibrary.Networking.M2Mqtt.MqttClient client;

        public static event DialogEMOCloseEvent2 dlgEMOCloseEvent;
        private static Object locking = new object();
        private static bool threadDoing = false;

        System.Threading.Timer timerRCUCommCheckTimer;
        public static bool Connect()
        {
            bool result = false;

            try
            {
                logger.Info(" MQTT Check");
                if (client == null || client.IsConnected == false)
                {
                    logger.Info(" MQTT Check threadDoing " + threadDoing);
                    if (threadDoing == false)
                    {
                        
                        lock (locking)
                        {
                            logger.Info(" MQTT Check threadDoing " + threadDoing + " client : " + client + " client2 : " + client);
                            if (threadDoing == true || (client != null && client.IsConnected == true))
                            {
                                
                            }
                            else
                            {
                                try
                                {
                                    logger.Info("MQTT Connect Try..");
                                    threadDoing = true;
                                    client = new uPLibrary.Networking.M2Mqtt.MqttClient(Globals.fmsIpAddress);
                                    logger.Info("MQTT Connect Try..1");
                                    client.Connect(Guid.NewGuid().ToString());
                                    logger.Info("MQTT Connect Try..2");
                                    client.MqttMsgPublishReceived += SubscribeMessage;
                                    logger.Info("MQTT Connect Try..3");
                                    client.Subscribe(new string[] { "ACS001>" + Globals.ID }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE}); // kdy 230706기존 레벨MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE
                                    logger.Info("MQTT Connect Try..4");

                                    logger.Info(" [AFL_ID] " + Globals.ID);
                                    threadDoing = false;

                                    if (client.IsConnected == true)
                                    {
                                        logger.Info("MQTT Client Connected.");
                                        Globals.mqtt_connection_count = 0;
                                        result = true;
                                        Publish_Result(new EIEntity()); // 연결되면 초기 위치 전송
                                    }
                                }
                                catch(Exception ex)
                                {
                                    threadDoing = false;
                                    logger.Error(ex.ToString());
                                }
                                
                            }
                        }
                    }
                }    
            }
            catch (Exception ex)
            {
                threadDoing = false;
                logger.Info(ex.ToString());
            }

            return result;
        }

        public static void Disconnect()
        {
            if (IsConnected())
            {
                client.Disconnect();
            }
        }

        public static bool IsConnected()
        {
            return client.IsConnected;
        }


        private static void SubscribeMessage(object sender, MqttMsgPublishEventArgs e)
        {
            try
            {
                string msg = Encoding.UTF8.GetString(e.Message);
                //logger.Info(" [SubscribeMessage]" + msg);

                CmdEntity cmdEntity = JsonConvert.DeserializeObject<CmdEntity>(msg);



                switch (cmdEntity.Cmd)
                {
                    case "EI":
                        Subscribe_EI(msg);
                        //logger.Info("[EI] Subscribe Message : " + msg);
                        break;
                    case "AD":
                        Subscribe_AD(msg);
                        //logger.Info("[AD] Subscribe Message: " + msg);
                        break;
                    case "AR":
                        Subscribe_AR(msg);
                        break;
                    case "AC":
                        Subscribe_AC(msg);
                        //logger.Info("[AC] Subscribe Message: " + msg);
                        break;
                    case "AW":
                        Subscribe_AW(msg);
                        //logger.Info("[AW] Subscribe Message: " + msg);
                        break;
                    case "AP":
                        Subscribe_AP(msg);
                        //logger.Info("[AP] Subscribe Message: " + msg);
                        break;
                    case "AG":
                        //충전 중복명령 체크
                        if (Globals.nACSCorrectCmd == 0)
                        {
                            Globals.strACSCmd = cmdEntity.Cmd;
                            Globals.nACSCorrectCmd++;
                        }
                        else if (Globals.nACSCorrectCmd > 0)
                        {
                            Globals.nACSCorrectCmd++;

                            if (Globals.nACSCorrectCmd > 20 && Globals.strACSCmd == cmdEntity.Cmd)
                            {
                                Globals.nACSCorrectCmd = 0;
                                Globals.strACSCmd = cmdEntity.Cmd;
                                Globals.currentWork.alarm = Alarms.ALARM_ACS_WRONG_ORDER;
                                Globals.currentWork.alarm_name = "![000/E5151/C1주 행 명 령 /C1  에    러 !]";
                            }
                        }
                        Subscribe_AG(msg);
                        //logger.Info("[AG] Subscribe Message: " + msg);
                        break;
                    case "AJ":
                        Subscribe_AJ(msg);
                        //logger.Info("[AJ] Subscribe Message: " + msg);
                        break;
                    case "SL":
                        Subscribe_SL(msg);
                        //logger.Info("[SL] Subscribe Message: " + msg);
                        break;
                    case "SP":
                        Subscribe_SP(msg);
                        //logger.Info("[SP] Subscribe Message: " + msg);
                        break;
                    case "GQ":
                        //rcu 통신 연결 확인
                        Globals.nRCUConnectCheck = 0;
                        if (Globals.bRCUAlarmCheck)
                        {
                            Globals.currentWork.alarm = Alarms.NO_ALARM;
                            Globals.bRCUAlarmCheck = false;
                            Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_RESUME;
                            Globals.HMISystem.ACStoHMI_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_ACS_NONE;
                            Globals.HMISystem.HMItoACS_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_FIRMWARE_NONE;
                            logger.Info("   RCU Connect Success");
                        }

                        if (Astar.FindNodeInfo(Globals.currentWork.curNode).GetNodeType() != Globals.MAP_NODE_CHARGE && Globals.nACSCorrectCmd != 0)
                        {
                            Globals.nACSCorrectCmd = 0;
                        }
                        Subscribe_GQ(msg);
                        //logger.Info("[GQ] Subscribe Message: " + msg);
                        break;
                    case "GB":
                        Subscribe_GB(msg);
                        //logger.Info("[GB] Subscribe Message: " + msg);
                        break;
                    case "GD":
                        Subscribe_GD(msg);
                        //logger.Info("[GD] Subscribe Message: " + msg);
                        break;
                    case "TG":
                        Subscribe_TG(msg);
                        // logger.Info("[TG] Subscribe Message: " + msg);
                        break;
                    case "TS":
                        Subscribe_TS(msg);
                        // logger.Info("[TS] Subscribe Message: " + msg);
                        break;
                    case "GG":
                        Subscribe_GG(msg);
                        //logger.Info("[GG] Subscribe Message: " + msg);
                        break;
                    case "ER":
                        Subscribe_ER(msg);
                        //logger.Info("[ER] Subscribe Message: " + msg);
                        break;
                    default:
                        cmdEntity.Result = Globals_Hyundai.RESULT_FAIL;
                        cmdEntity.ErrorCode = Alarms.ALARM_PROTOCOL_FORMAT_ERROR;
                        Publish_Result(cmdEntity);
                        break;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }

        public static void Publish_Result(Object obj)
        {
            String strEx = "";
            try
            {
                String strValue = JsonConvert.SerializeObject(obj, Formatting.None);
                strEx = strValue;
                //logger.Info("   [Publish] " + strValue);
                //logger.Trace(" [forklift ID] " + Globals.ID);
                client.Publish(Globals.ID + ">ACS001", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);// MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE
            }
            catch (Exception ex)
            {
                //logger.Error("[Publish Error] " + ex.ToString() + " | " + strEx); //kdy 예외 잡아야 함 lhw 1111
            }
        }

        private static void Subscribe_EI(string msg)
        {
            logger.Info("[EI] FORKLIFT POSITION RESULT");
            try
            {
                CmdEntity cmdEntity = JsonConvert.DeserializeObject<CmdEntity>(msg);
                if (Globals_Hyundai.RESULT_SUCCESS.Equals(cmdEntity.Result))
                {
                    Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_RESUME;
                    Globals.HMISystem.ACStoHMI_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_RESUME;
                }
                else
                {
                    Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_STOP;
                    Globals.HMISystem.ACStoHMI_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_STOP;
                    Thread.Sleep(1000);
                    Publish_Result(new EIEntity());
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }


        private static void Subscribe_AR(string msg)
        {
            AREntity arEntity = new AREntity();
            WorkEntity workCommand = new WorkEntity();
            logger.Info("[AR] WORK ORDER ");
            Globals.nForceComplete = 0;
            Globals.nAFL_Start = 0;

            //start버튼 처음 누르고 AR 바로 들어와서 아다리 안맞는 경우 대비
            if (Globals.bAutoMode_AR_Wait)
            {
                Globals.bAutoMode_AR_Wait = false;
                Thread.Sleep(2000);
            }

            if (Globals.HMISystem.SystemMode != SystemMode.AUTO)
            {
                logger.Info("[AR] System Mode is not AUTO");
                arEntity.Result = Globals_Hyundai.RESULT_FAIL;
                arEntity.ErrorCode = Alarms.ALARM_MODE_WRONG_ERROR;
            }

            else if (Globals.TRANSFER_READY != Globals.currentWork.workType)
            {
                logger.Info("[AR] Work Type is not READY");
                arEntity.Result = Globals_Hyundai.RESULT_FAIL;
                arEntity.ErrorCode = Alarms.ALARM_WORK_ALREADY_ING_ERROR;
            }
            else
            {
                try
                {
                    arEntity = JsonConvert.DeserializeObject<AREntity>(msg);

                    //logger.Info("[curNode]" + Globals.currentWork.curNode + "[arEntity.node]" + arEntity.node);
                    //kdy 11.24 retry 일때는 같은 노드여도 패스 생성할수 있게 예외처리

                    if ((Astar.FindNodeInfo(arEntity.node).GetNodeType() == Globals.MAP_NODE_STATION || Astar.FindNodeInfo(arEntity.node).GetNodeType() == Globals.MAP_NODE_EQUIPMENT) && arEntity.Work == Globals.TRANSFER_MOVE)
                    {
                        Globals.currentWork.alarm = Alarms.ALARM_ACS_WRONG_ORDER;
                        Globals.currentWork.alarm_name = "![000/E5151/C1주 행 명 령 /C1  에    러 !]";

                        logger.Info("[AR] ACS Order Error");

                        arEntity.Result = Globals_Hyundai.RESULT_FAIL;
                        arEntity.ErrorCode = workCommand.alarm;

                    }
                    else if (Astar.FindNodeInfo(arEntity.node).GetNodeType() == Globals.MAP_NODE_NORMAL && arEntity.Work != Globals.TRANSFER_MOVE)
                    {
                        Globals.currentWork.alarm = Alarms.ALARM_ACS_WRONG_ORDER;
                        Globals.currentWork.alarm_name = "![000/E5151/C1주 행 명 령 /C1  에    러 !]";

                        logger.Info("[AR] ACS Order Error");

                        arEntity.Result = Globals_Hyundai.RESULT_FAIL;
                        arEntity.ErrorCode = workCommand.alarm;
                    }
                    else
                    {
                        if (Globals.currentWork.curNode != arEntity.node || Globals.bRetry_ACSRetryCheck/*Globals.currentWork.frontRetry == 1*/)
                        {
                            logger.Info("[curNode]" + Globals.currentWork.curNode + "[arEntity.node]" + arEntity.node);

                            workCommand.transferID = arEntity.AmrId;
                            workCommand.workMode = arEntity.Mode;
                            workCommand.workType = arEntity.Work;
                            workCommand.workLevel = arEntity.Level;

                            workCommand.workState = Globals.currentWork.workState; //(byte)Globals.acsWorkState;//WorkState.READY;
                            workCommand.workStateUnit = (byte)WorkStateUnit.READY;

                            workCommand.curLink = Globals.currentWork.curLink;
                            workCommand.curNode = Globals.currentWork.curNode;
                            workCommand.toNode = arEntity.node;
                            workCommand.nextNode = Globals.currentWork.nextNode;
                            List<AstarNodeData> linkList = new List<AstarNodeData>();

                            for (int i = 0; i < arEntity.PathList.Count; i++)
                            {
                                AstarNodeData nodeData = new AstarNodeData();

                                nodeData.SetCurrentNodeID(arEntity.PathList[i].CurrentNodeID);
                                nodeData.SetParentNodeID(arEntity.PathList[i].ParentNodeID);
                                nodeData.SetLinkDirection(arEntity.PathList[i].LinkDirection);
                                nodeData.SetLinkType(arEntity.PathList[i].LinkType);
                                nodeData.SetLinkNumber(arEntity.PathList[i].LinkNumber);
                                nodeData.SetLayerNumber(arEntity.PathList[i].LayerNumber);
                                nodeData.SetSafetyCH(arEntity.PathList[i].SafetyChannel);
                                nodeData.SetLinkMaxVelocity(arEntity.PathList[i].MaxVelocity);
                                nodeData.SetVehicleAng(arEntity.PathList[i].VehicleAngle);
                                nodeData.SetLinkAng(arEntity.PathList[i].LinkAngle);
                                nodeData.SetF_Score(arEntity.PathList[i].F_);
                                nodeData.SetG_Score(arEntity.PathList[i].G_);
                                nodeData.SetH_Score(arEntity.PathList[i].H_);

                                //byte b = arEntity.PathList[i].LinkOption;

                                //if (Astar.FindNodeInfo(arEntity.PathList[i].ParentNodeID).GetNodeType() == Globals.MAP_NODE_STATION)
                                //{
                                //    nodeData.SetLinkOption((byte)(arEntity.PathList[i].LinkOption + 16));
                                //    logger.Info("  Steering Lock [ParentNode] " + arEntity.PathList[i].ParentNodeID + " [CurrentNode] " + arEntity.PathList[i].CurrentNodeID);

                                //}
                                //else
                                nodeData.SetLinkOption(arEntity.PathList[i].LinkOption);

                                linkList.Add(nodeData);
                            }


                            workCommand.realPath = linkList;
                            workCommand.palletType = arEntity.PLTType;
                            if (workCommand.EnqueueWork())
                            {
                                Globals.currentWork = workCommand;

                                Globals.HMISystem.PalletLevel = (byte)workCommand.workLevel;
                                Globals.HMISystem.PalletType = (byte)workCommand.palletType;
                                Globals.HMISystem.WorkType = workCommand.workState;
                                Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_FIRMWARE_NONE;
                                Globals.HMISystem.ACStoHMI_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_ACS_NONE;

                                Thread.Sleep(500);
                                if (Globals_Hyundai.PATH_PLANNING_AMR.Equals(arEntity.Mode))
                                {
                                    List<TempAstarNodeData> tempLinkList = new List<TempAstarNodeData>();
                                    for (int i = 0; i < Globals.currentWork.realPath.Count; i++)
                                    {
                                        TempAstarNodeData nodeData = new TempAstarNodeData();

                                        nodeData.LinkNumber = Globals.currentWork.realPath[i].GetLinkNumber();
                                        nodeData.CurrentNodeID = Globals.currentWork.realPath[i].GetCurrentNodeID();
                                        nodeData.ParentNodeID = Globals.currentWork.realPath[i].GetParentNodeID();
                                        nodeData.LinkType = Globals.currentWork.realPath[i].GetLinkType();
                                        nodeData.LinkDirection = Globals.currentWork.realPath[i].GetLinkDirection();
                                        nodeData.LinkOption = Globals.currentWork.realPath[i].GetLinkOption();
                                        nodeData.LayerNumber = 0x00;
                                        nodeData.SafetyChannel = Globals.currentWork.realPath[i].GetSafetyChannel();
                                        nodeData.MaxVelocity = Globals.currentWork.realPath[i].GetMaxVelocity();
                                        nodeData.VehicleAngle = (float)Globals.currentWork.realPath[i].GetVehicleAng();
                                        nodeData.LinkAngle = (float)Globals.currentWork.realPath[i].GetLinkAng();
                                        nodeData.F_ = 0;
                                        nodeData.G_ = (float)Globals.currentWork.realPath[i].GetG_Score();
                                        nodeData.H_ = 0;

                                        tempLinkList.Add(nodeData);
                                    }
                                    arEntity.PathList = tempLinkList;
                                }
                                arEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
                            }
                            else
                            {
                                logger.Info("[AR] Enqueue Work Error");

                                arEntity.Result = Globals_Hyundai.RESULT_FAIL;
                                arEntity.ErrorCode = workCommand.alarm;
                            }
                        }
                        else
                        {

                            //kdy 11.28 AUTO상태일때 제자리 주행명령은 주행완료로 처리
                            if (arEntity.Work == Globals.TRANSFER_MOVE && /*Globals.nodeData. == Globals.MAP_NODE_CHARGE && */ Globals.HMISystem.SystemMode == SystemMode.AUTO)
                            {
                                logger.Info("  Move Complete when same position order on AUTO mode.");

                                Globals.currentWork.workState = (byte)WorkState.MOVE_COMPLETE;
                                arEntity.Result = Globals_Hyundai.RESULT_SUCCESS;

                            }
                            else
                            {
                                logger.Info("[AR] ACS command when current node and target node are the same ");
                                logger.Info("[AR] [curNode] " + Globals.currentWork.curNode + " [toNode] " + Globals.currentWork.toNode + " [arEntity.node] " + arEntity.node);
                                logger.Info(msg);

                                Globals.currentWork.alarm = Alarms.ALARM_ACS_WRONG_ORDER;
                                Globals.currentWork.alarm_name = "![000/E5151/C1주 행 명 령 /C1  에    러 !]";
                                arEntity.Result = Globals_Hyundai.RESULT_FAIL;
                                arEntity.ErrorCode = workCommand.alarm;
                            }
                        }
                    }


                }
                catch (Exception ex)
                {
                    arEntity.Result = Globals_Hyundai.RESULT_FAIL;
                    arEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                    logger.Error(ex.ToString());
                }
            }
            Publish_Result(arEntity);


        }

        private static void Subscribe_AC(string msg)
        {
            logger.Info("[AC] WORK CANCEL");
            ACEntity acEntity = new ACEntity();
            try
            {
                acEntity = JsonConvert.DeserializeObject<ACEntity>(msg);

                //if( Globals.HMISystem.SystemMode >= SystemMode.MANUAL && Globals.currentWork.workType != Globals.TRANSFER_READY)
                {

                    WorkStep workStep = Globals.workStep;
                    logger.Info("frontRetry : " + Globals.currentWork.frontRetry + " [HMISystem.SystemMode] " + Globals.HMISystem.SystemMode.ToString() + " [bFirstMoveComplete] " + Globals.bRetry_FirstMoveComplete.ToString() +/* " [bsecondpathretry] " + Globals.bRetry_SecondPathRetry.ToString() + " [bliftingretry] " + Globals.bRetry_LiftingRetry.ToString());*/" [PalletFrontEnable] " + Globals.HMISystem.PalletFrontEnable.ToString() + " [PalletFrontExistEnable] " + Globals.HMISystem.PalletFrontExistEnable + " [PalletStackingEnable] " + Globals.HMISystem.PalletStackingEnable + " [PalletStackingSuccessEnable] " + Globals.HMISystem.PalletStackingSuccessEnable + " [PalletHeightEnable] " + Globals.HMISystem.PalletHeightEnable);

                    logger.Info("bRetry_ACSRetryCheck  : " + !Globals.bRetry_ACSRetryCheck + " bRetry_FirstMoveComplete : " + Globals.bRetry_FirstMoveComplete);
                    if (Globals.currentWork.frontRetry == 1)
                    {
                        Globals.bRetry_ACSRetryCheck = true; // 기존 bACSRetryCheck이거랑 구분이 필요함 
                    }

                    Globals.currentWork.alarm = Alarms.NO_ALARM; // kdy02.22 retry 시 알람 한번더 뜨는거 때문에 추가
                    logger.Info("Globals.currentWork.alarm : " + Globals.currentWork.alarm);
                    Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_STOP;
                    Globals.HMISystem.ACStoHMI_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_STOP;
                    //Globals.HMISystem.SystemMode = SystemMode.RESET; //KDY 23.07.18 RETRY할때 기존 알람 RESET해야 다시 안떠서 RETRY 가능
                    Thread.Sleep(1000);
                    Globals.nManualComplete = 0;
                    Globals.nForceComplete = 0;
                    Globals.workStep.Clear();
                    Globals.currentWork.Clear();

                    Globals.HMISystem.LiftEnable = false;
                    Globals.HMISystem.TiltEnable = false;
                    Globals.HMISystem.ReachEnable = false;
                    Globals.HMISystem.SideShiftEnable = false;
                    Globals.HMISystem.MoverEnable = false;
                    Globals.HMISystem.ChargeEnable = false;
                    Globals.nChargingCount = 0;
                    Globals.SOCCount = 0;
                    Globals.CurrentTimeCheck = 0;
                    Globals.HMISystem.ClampEnable = false;
                    Globals.HMISystem.TaggingEnable = false;
                    Globals.HMISystem.PalletStackingEnable = false;
                    Globals.HMISystem.PalletFrontEnable = false;
                    Globals.HMISystem.PalletHeightEnable = false;
                    Globals.HMISystem.PalletStackingSuccessEnable = false;
                    Globals.HMISystem.PalletFrontExistEnable = false;

                    //dlgEMOCloseEvent();

                    /*
                    if (Globals.WORK_FORK_LIFTING >= workStep.step && workStep.step >= Globals.WORK_FORK_FORKMOVER)
                    {
                        switch (workStep.step)
                        {
                            case Globals.WORK_FORK_LIFTING:
                                workStep.value = Globals.beforeLiftValue;
                                break;
                            case Globals.WORK_FORK_TILTING:
                                workStep.value = Globals.beforeTiltValue;
                                break;
                            case Globals.WORK_FORK_REACHING:
                                workStep.value = Globals.beforeReachValue;
                                break;
                            case Globals.WORK_FORK_SIDESHIFT:
                                workStep.value = Globals.beforeSideShiftValue - ((Globals.ForkLiftSystem.ShiftRightPosition - Globals.ForkLiftSystem.ShiftLeftPosition) / 2);
                                break;
                            case Globals.WORK_FORK_FORKMOVER:
                                workStep.value = Globals.beforeForkMoverValue;
                                break;
                            default:
                                break;
                        }
                        workStep.function.BeginInvoke(workStep, null, workStep);
                    }
                    */
                    acEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
                }
                //else
                //{
                //    acEntity.Result = Globals_Hyundai.RESULT_FAIL;
                //    acEntity.ErrorCode = Alarms.ALARM_PROTOCOL_FORMAT_ERROR;
                //}
            }
            catch (Exception ex)
            {
                acEntity.Result = Globals_Hyundai.RESULT_FAIL;
                acEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }

            Publish_Result(acEntity);
            Thread.Sleep(1000);
        }

        private static void Subscribe_AW(string msg)
        {
            logger.Info("[AW] ACTION WORK");
            AWEntity awEntity = new AWEntity();
            try
            {
                awEntity = JsonConvert.DeserializeObject<AWEntity>(msg);

                if (Globals.HMISystem.SystemMode != SystemMode.SEMI_AUTO)
                {
                    awEntity.Result = Globals_Hyundai.RESULT_FAIL;
                    awEntity.ErrorCode = Alarms.ALARM_MODE_WRONG_ERROR;
                }
                else
                {
                    if (awEntity.ActionID > 0 && awEntity.ActionID < 20)
                    {
                        WorkEntity workEntity = new WorkEntity();
                        workEntity.workType = Globals.TRANSFER_LOAD;
                        WorkStep workStep = new WorkStep();
                        switch ((WorkStateUnit)awEntity.ActionID)
                        {
                            case WorkStateUnit.LIFTING:
                                workStep = new WorkStep("WORK_FORK_LIFTING", Globals.WORK_FORK_LIFTING, awEntity.ActionValue, 0, true, ForkControl.Lifting);
                                break;

                            case WorkStateUnit.TILTING:
                                workStep = new WorkStep("WORK_FORK_TILTING", Globals.WORK_FORK_TILTING, awEntity.ActionValue, 0, true, ForkControl.Tilting);
                                break;

                            case WorkStateUnit.SIDESHIFT:
                                workStep = new WorkStep("WORK_FORK_SIDESHIFT", Globals.WORK_FORK_SIDESHIFT, awEntity.ActionValue, 0, true, ForkControl.SideShift);
                                break;

                            case WorkStateUnit.FORKMOVER:
                                workStep = new WorkStep("WORK_FORK_FORKMOVER", Globals.WORK_FORK_FORKMOVER, awEntity.ActionValue, 0, true, ForkControl.ForkMover);
                                break;

                            case WorkStateUnit.PATH_1ST_NODE:
                                workEntity.workMode = Globals.PATH_PLANNING_AMR;
                                workStep = new WorkStep("WORK_MOVE_1ST_PATH_PLANNING", Globals.WORK_MOVE_1ST_PATH_PLANNING, awEntity.ActionValue, 0, true, DriveControl.Move1stPathPlanning);
                                break;

                            case WorkStateUnit.PATH_2ND_STATION:
                                workEntity.workMode = Globals.PATH_PLANNING_AMR;
                                workStep = new WorkStep("WORK_MOVE_2ND_PATH_PLANNING", Globals.WORK_MOVE_2ND_PATH_PLANNING, awEntity.ActionValue, 0, true, DriveControl.Move2ndPathPlanning);
                                break;

                            case WorkStateUnit.PATH_SENDING:
                                workStep = new WorkStep("WORK_MOVE_PATH_SENDING", Globals.WORK_MOVE_PATH_SENDING, 0, 0, true, DriveControl.MovePathSending);
                                break;

                            case WorkStateUnit.PALLET_FRONT:
                                if (awEntity.ActionValue == 1)
                                    workStep = new WorkStep("WORK_PALLET_FRONT_ENABLE", Globals.WORK_PALLET_FRONT_ENABLE, 0, 0, true, CameraControl.PalletFrontEnable);
                                else
                                    workStep = new WorkStep("WORK_PALLET_FRONT_DISABLE", Globals.WORK_PALLET_FRONT_DISABLE, 0, 0, true, CameraControl.PalletFrontDisable);
                                break;

                            case WorkStateUnit.PALLET_STACKING:
                                if (awEntity.ActionValue == 1)
                                    workStep = new WorkStep("WORK_PALLET_STACKING_ENABLE", Globals.WORK_PALLET_STACKING_ENABLE, 0, 0, true, CameraControl.PalletStackingEnable);
                                else
                                    workStep = new WorkStep("WORK_PALLET_STACKING_DISABLE", Globals.WORK_PALLET_STACKING_DISABLE, 0, 0, true, CameraControl.PalletStackingDisable);
                                break;
                            default:
                                awEntity.Result = Globals_Hyundai.RESULT_FAIL;
                                awEntity.ErrorCode = Alarms.ALARM_PROTOCOL_FORMAT_ERROR;
                                break;
                        }

                        workEntity.workQueue.Enqueue(workStep);
                        Globals.currentWork = workEntity;
                        awEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
                    }
                    else
                    {
                        awEntity.Result = Globals_Hyundai.RESULT_FAIL;
                        awEntity.ErrorCode = Alarms.ALARM_PROTOCOL_FORMAT_ERROR;
                    }
                }
            }
            catch (Exception ex)
            {
                awEntity.Result = Globals_Hyundai.RESULT_FAIL;
                awEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }

            Publish_Result(awEntity);
        }

        private static void Subscribe_AP(string msg)
        {
            logger.Info("[AP] TRAFFIC CONTROL ");
            APEntity apEntity = new APEntity();
            try
            {
                apEntity = JsonConvert.DeserializeObject<APEntity>(msg);
                if (SystemTraffic.TRANSFER_TRAFFIC_PAUSE == (SystemTraffic)apEntity.Traffic)
                {
                    Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_PAUSE;
                    Globals.HMISystem.ACStoHMI_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_PAUSE;
                    logger.Info("[SystemTraffic]  " + Globals.HMISystem.SystemTraffic + "  [SystemTrafficPause]  " + Globals.HMISystem.ACStoHMI_TrafficPause);
                    apEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
                }
                else if (SystemTraffic.TRANSFER_TRAFFIC_RESUME == (SystemTraffic)apEntity.Traffic)
                {
                    if(!Globals.RemoteSwichFlag)
                    {
                        Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_RESUME;
                        Globals.HMISystem.ACStoHMI_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_RESUME;
                        logger.Info("[SystemTraffic]  " + Globals.HMISystem.SystemTraffic + "  [SystemTrafficPause]  " + Globals.HMISystem.ACStoHMI_TrafficPause);
                        apEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
                    }
                    else
                    {
                        Globals.HMISystem.ACStoHMI_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_RESUME;
                        logger.Info("[SystemTraffic]  " + Globals.HMISystem.SystemTraffic + "  [SystemTrafficPause]  " + Globals.HMISystem.ACStoHMI_TrafficPause);
                    }
                }
                else if (SystemTraffic.TRANSFER_TRAFFIC_STOP == (SystemTraffic)apEntity.Traffic)
                {
                    Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_STOP;
                    Globals.HMISystem.ACStoHMI_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_STOP;
                    logger.Info("[SystemTraffic]  " + Globals.HMISystem.SystemTraffic + "  [SystemTrafficPause]  " + Globals.HMISystem.ACStoHMI_TrafficPause);
                    apEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
                }
                //11번 설비통신으로 인한 일시정지
                else if (SystemTraffic.TRANSFER_TRAFFIC_EQUIPMENTCOMM_PAUSE == (SystemTraffic)apEntity.Traffic)
                {
                    Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_PAUSE;
                    Globals.HMISystem.ACStoHMI_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_EQUIPMENTCOMM_PAUSE;
                    logger.Info("[SystemTraffic]  " + Globals.HMISystem.SystemTraffic + "  [SystemTrafficPause]  " + Globals.HMISystem.ACStoHMI_TrafficPause);
                    apEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
                }
                //12번 교통제어으로 인한 일시정지
                else if (SystemTraffic.TRANSFER_TRAFFIC_AUTO_PAUSE == (SystemTraffic)apEntity.Traffic)
                {
                    Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_PAUSE;
                    Globals.HMISystem.ACStoHMI_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_AUTO_PAUSE;
                    logger.Info("[SystemTraffic]  " + Globals.HMISystem.SystemTraffic + "  [SystemTrafficPause]  " + Globals.HMISystem.ACStoHMI_TrafficPause);
                    apEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
                }
                //13번 태블릿으로 인한 일시정지
                else if (SystemTraffic.TRANSFER_TRAFFIC_USER_PAUSE == (SystemTraffic)apEntity.Traffic)
                {
                    Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_PAUSE;
                    Globals.HMISystem.ACStoHMI_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_USER_PAUSE;
                    logger.Info("[SystemTraffic]  " + Globals.HMISystem.SystemTraffic + "  [SystemTrafficPause]  " + Globals.HMISystem.ACStoHMI_TrafficPause);
                    apEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
                }
                else
                {
                    apEntity.Result = Globals_Hyundai.RESULT_FAIL;
                    apEntity.ErrorCode = Alarms.ALARM_PROTOCOL_FORMAT_ERROR;
                }
            }

            catch (Exception ex)
            {
                apEntity.Result = Globals_Hyundai.RESULT_FAIL;
                apEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }

            Publish_Result(apEntity);
        }

        private static void Subscribe_AG(string msg)
        {
            logger.Info("[AG] CHARGE CONTROL " + msg);
            AGEntity agEntity = new AGEntity();
            try
            {
                agEntity = JsonConvert.DeserializeObject<AGEntity>(msg);

                if (Globals_Hyundai.CHARGE_ENABLE.Equals(agEntity.Charge))
                {
                    Globals.workStep = new WorkStep("WORK_CHARGE_ENABLE", Globals.WORK_CHARGE_ENABLE, 0, 0, true, ChargeControl.ChargeEnable);
                    Globals.workStep.function.BeginInvoke(Globals.workStep, null, null);
                    Globals.currentWork.workState = (byte)WorkState.CHARGING; // CHARGE
                    agEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
                }
                else if (Globals_Hyundai.CHARGE_DISABLE.Equals(agEntity.Charge))
                {
                    Globals.workStep = new WorkStep("WORK_CHARGE_DISABLE", Globals.WORK_CHARGE_DISABLE, 0, 0, true, ChargeControl.ChargeDisable);
                    Globals.workStep.function.BeginInvoke(Globals.workStep, null, null);
                    Globals.currentWork.workState = (byte)WorkState.READY; // READY
                    agEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
                }
                else
                {
                    agEntity.Result = Globals_Hyundai.RESULT_FAIL;
                    agEntity.ErrorCode = Alarms.ALARM_PROTOCOL_FORMAT_ERROR;
                }
            }
            catch (Exception ex)
            {
                agEntity.Result = Globals_Hyundai.RESULT_FAIL;
                agEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }

            Publish_Result(agEntity);
        }

        private static void Subscribe_AJ(string msg)
        {
            logger.Info("[AJ] MANUAL DRIVING CONTROL ");



            // 추후 진행 예정
        }

        private static void Subscribe_AD(string msg)
        {
            logger.Info("[AD] MODE CHANGE");

            ADEntity adEntity = new ADEntity();
            try
            {
                adEntity = JsonConvert.DeserializeObject<ADEntity>(msg);

                if (SystemMode.RESET <= (SystemMode)adEntity.Mode && SystemMode.AUTO >= (SystemMode)adEntity.Mode)
                {
                    Globals.HMISystem.SystemMode = (SystemMode)adEntity.Mode;
                    logger.Info("adEntity.Mode : " + Globals.HMISystem.SystemMode.ToString());
                    adEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
                }
                else
                {
                    adEntity.Result = Globals_Hyundai.RESULT_FAIL;
                    adEntity.ErrorCode = Alarms.ALARM_PROTOCOL_FORMAT_ERROR;
                }
            }
            catch (Exception ex)
            {
                adEntity.Result = Globals_Hyundai.RESULT_FAIL;
                adEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }

            Publish_Result(adEntity);
        }

        private static void Subscribe_SL(string msg)
        {
            logger.Info("[SL] MANUAL SAFETY SETTING");

            SLEntity slEntity = new SLEntity();
            try
            {
                slEntity = JsonConvert.DeserializeObject<SLEntity>(msg);

                Globals.SafetySystem.LeftInTIM = (byte)slEntity.Level;
                Globals.SafetySystem.RightInTIM = (byte)slEntity.Level;
                Globals.SafetySystem.RearInTIM = (byte)slEntity.Level;

                slEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
            }
            catch (Exception ex)
            {
                slEntity.Result = Globals_Hyundai.RESULT_FAIL;
                slEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }

            Publish_Result(slEntity);

        }

        private static void Subscribe_SP(string msg)
        {
            logger.Info("[SP] MANUAL NETWORK SETTING");
        }

        private static void Subscribe_GQ(string msg)
        {
            //logger.Info("[GQ] FORKLIFT STATUS");
            GQEntity gqEntity = new GQEntity();
            try
            {
                gqEntity = JsonConvert.DeserializeObject<GQEntity>(msg);

                gqEntity.AmrId = Globals.ID; // kdy 11.7 tams요청사항 
                gqEntity.X = Globals.NavigationSystem.NAV_X;
                gqEntity.Y = Globals.NavigationSystem.NAV_Y;
                gqEntity.T = Globals.NavigationSystem.NAV_PHI;
                gqEntity.State = Globals.currentWork.workState;// (byte)Globals.acsWorkState;
                gqEntity.ActionID = Globals.currentWork.workStateUnit;
                gqEntity.Traffic = (int)Globals.HMISystem.ACStoHMI_TrafficPause;
                gqEntity.AFLTraffic = (int)Globals.HMISystem.HMItoACS_TrafficPause;
                gqEntity.Mode = (int)Globals.HMISystem.SystemMode;
                gqEntity.Auto = Globals.VehicleSystem.SystemMode == true ? 1 : 0;
                gqEntity.Clamp = Globals.HMISystem.ClampEnable == true ? 1 : 0;
                gqEntity.Loaded = Globals.loaded == true ? 1 : 0;
                gqEntity.Level = Globals.currentWork.workLevel;
                gqEntity.TargetNode = Globals.currentWork.toNode;
                gqEntity.NextNode = Globals.currentWork.nextNode;
                gqEntity.CurrentNode = Globals.currentWork.curNode;
                gqEntity.CurrentLink = Globals.currentWork.curLink;
                gqEntity.NextLink = Globals.NavigationSystem.NextLink;


                gqEntity.Lift = Globals.ForkLiftSystem.LiftPosition;
                gqEntity.Tilt = Globals.ForkLiftSystem.TiltPosition;
                gqEntity.SideShift = (Globals.ForkLiftSystem.ShiftRightPosition - Globals.ForkLiftSystem.ShiftLeftPosition) / 2;
                gqEntity.ForkMover = Globals.ForkLiftSystem.MoverPosition;

                gqEntity.Battery = Globals.BMSSystem.SOC;

                gqEntity.PalletFrontX = Globals.RecognitionSystem.FrontPalletX;
                gqEntity.PalletFrontY = Globals.RecognitionSystem.FrontPalletY;
                gqEntity.PalletFrontTheta = Globals.RecognitionSystem.FrontPalletTheta;

                gqEntity.PalletStackingX = Globals.RecognitionSystem.TargetX;
                gqEntity.PalletStackingY = Globals.RecognitionSystem.TargetY;
                gqEntity.PalletStackingTheta = Globals.RecognitionSystem.TargetTheta;

                gqEntity.MapRate = Globals.NavigationSystem.SlamScore;
                gqEntity.Speed = (int)(1000 * Globals.VehicleSystem.CurrentSpeed / 900);
                gqEntity.Obstacle = (short)Globals.SafetySystem.NANOSCAN3State_Finally;

                gqEntity.TagID = 0;

                if (Globals.bAlarmSendACS)
                {
                    //Globals.bAlarmSendACS = false;
                    gqEntity.ErrorCode = Globals.currentWork.alarm;
                }
                else if (Globals.currentWork.alarm == Alarms.NO_ALARM)
                    gqEntity.ErrorCode = Alarms.NO_ALARM;


                gqEntity.Retry = Globals.currentWork.frontRetry;
                gqEntity.ManualComplete = Globals.nManualComplete;
                gqEntity.ForceComplete = Globals.nForceComplete;
                gqEntity.AFLStart = Globals.nAFL_Start;

                Globals.GQcount++;
                if (Globals.GQcount > 4)
                {
                    Globals.GQcount = 0;
                    //logger.Trace("   [Publish] AMR_ID: " + gqEntity.AmrId + " loaded: " + gqEntity.Loaded + " curNode:" + gqEntity.CurrentNode + "[NAV_X] : " + Globals.NavigationSystem.NAV_X + " [NAV_Y] :" + Globals.NavigationSystem.NAV_Y + " [NAV_PHI] :" + Globals.NavigationSystem.NAV_PHI);
                }

            }
            catch (Exception ex)
            {
                gqEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }

            Publish_Result(gqEntity);
        }

        private static void Subscribe_GB(string msg)
        {
            //logger.Info("[GB] BMS STATUS");

            GBEntity gbEntity = new GBEntity();
            try
            {
                gbEntity = JsonConvert.DeserializeObject<GBEntity>(msg);

                gbEntity.AmrId = Globals.ID;
                gbEntity.BCellVolt = Globals.BMSSystem.CellVolt;
                gbEntity.BPackVolt = Globals.BMSSystem.Bat_V;
                gbEntity.BPackCurrent = Globals.BMSSystem.Bat_i;
                //gbEntity.BChargeVolt = Globals.BMSSystem.ChargeVolt;
                gbEntity.BTemp = Globals.BMSSystem.Temperature;
                gbEntity.Battery = Globals.BMSSystem.SOC;
                gbEntity.BError = Globals.BMSSystem.Fault;
                gbEntity.BWarning = Globals.BMSSystem.Warning;
            }
            catch (Exception ex)
            {
                gbEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }
            Publish_Result(gbEntity);
        }

        private static void Subscribe_GD(string msg)
        {
            logger.Info("[GD] FORKLIFT INFORMATION");

            GDEntity gdEntity = new GDEntity();
            try
            {
                gdEntity = JsonConvert.DeserializeObject<GDEntity>(msg);

                gdEntity.GMapVersion = "";
                gdEntity.TMapVersion = "";
                gdEntity.FWVersion = "";
            }
            catch (Exception ex)
            {
                gdEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }
            Publish_Result(gdEntity);
        }


        private static void Subscribe_TG(string msg)
        {
            logger.Info("[TG] MAP TOPOLOGY INFORMATION");

            TGEntity tgEntity = new TGEntity();
            try
            {
                tgEntity = JsonConvert.DeserializeObject<TGEntity>(msg);

                //lhw 0930 맵버전
                tgEntity.TMapVersion = "1";//Globals.MapData.GetTopologyMapVirsion().ToString();
                logger.Info("[TMapVersion]" + msg);
                for (int i = 0; i < Globals.nodeData.Count; i++)
                {
                    TempNodeInfo tempNodeInfo = new TempNodeInfo();

                    tempNodeInfo.nodenumber = Globals.nodeData[i].GetNodeNumber();
                    tempNodeInfo.pointX = Globals.nodeData[i].GetNodePoint().X;
                    tempNodeInfo.pointY = Globals.nodeData[i].GetNodePoint().Y;
                    tempNodeInfo.nodetype = Globals.nodeData[i].GetNodeType();
                    tempNodeInfo.layernumber = Globals.nodeData[i].GetLayerNumber();

                    tgEntity.NodeList.Add(tempNodeInfo);
                }

                for (int i = 0; i < Globals.linkData.Count; i++)
                {
                    TempLinkInfo tempLinkInfo = new TempLinkInfo();

                    tempLinkInfo.linknumber = Globals.linkData[i].GetLinkNumber();
                    tempLinkInfo.startnode = Globals.linkData[i].GetStartNodeNumber();
                    tempLinkInfo.targetnode = Globals.linkData[i].GetTargetNodeNumber();
                    tempLinkInfo.linktype = Globals.linkData[i].GetLinkType();
                    tempLinkInfo.linkdirection = Globals.linkData[i].GetLinkDirection();
                    tempLinkInfo.Foption = Globals.linkData[i].GetFoption();
                    tempLinkInfo.Boption = Globals.linkData[i].GetBoption();
                    tempLinkInfo.Fvelocity = Globals.linkData[i].GetFMaxVelocity();
                    tempLinkInfo.Bvelocity = Globals.linkData[i].GetBMaxVelocity();
                    tempLinkInfo.Fsafetych = Globals.linkData[i].GetFrontSafetyCH();
                    tempLinkInfo.Bsafetych = Globals.linkData[i].GetBackSafetyCH();
                    tempLinkInfo.linkdistance = (long)Globals.linkData[i].GetLinkDistance();
                    tempLinkInfo.curveradius = Globals.linkData[i].GetCurveRadius();

                    tempLinkInfo.prelinknumber = Globals.linkData[i].GetPreLinkNumber();
                    tempLinkInfo.nextlinknumber = Globals.linkData[i].GetNextLinkNumber();
                    tempLinkInfo.curvestartang = Globals.linkData[i].GetCurveStartAng();
                    tempLinkInfo.curvesweepang = Globals.linkData[i].GetCurveSweepAng();
                    tempLinkInfo.basepoint = Globals.linkData[i].GetBasePoint();

                    tgEntity.LinkList.Add(tempLinkInfo);
                }

                ushort TotalLinkNum = Globals.MapData.GetTotalLinkNum();
                tgEntity.TotalLinkNum = TotalLinkNum;

                ushort TotalNodeNum = Globals.MapData.GetTotalNodeNum();
                tgEntity.TotalNodeNum = TotalNodeNum;
                tgEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
            }
            catch (Exception ex)
            {
                tgEntity.Result = Globals_Hyundai.RESULT_FAIL;
                tgEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }
            Publish_Result(tgEntity);

            //String strValue = JsonConvert.SerializeObject(tgEntity, Formatting.None);
            //logger.Info("[Publish]" + strValue);

            //client.Publish(Globals.ID + ">ACS001", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
        }


        private static void Subscribe_TS(string msg)
        {
            logger.Info("[TS] MAP TOPOLOGY SET");
            TGEntity tgEntity = new TGEntity();
            try
            {
                tgEntity = JsonConvert.DeserializeObject<TGEntity>(msg);

                SaveData SaveFile = new SaveData();
                List<Point> gridList = new List<Point>();

                List<NodeInfo> nodeList = new List<NodeInfo>();
                for (int i = 0; i < tgEntity.NodeList.Count; i++)
                {
                    NodeInfo nodeInfo = new NodeInfo();

                    nodeInfo.SetNodeNumber(tgEntity.NodeList[i].nodenumber);
                    nodeInfo.SetNodePoint(tgEntity.NodeList[i].pointX, tgEntity.NodeList[i].pointY);
                    nodeInfo.SetNodeType(tgEntity.NodeList[i].nodetype);
                    nodeInfo.SetLayerNumber(tgEntity.NodeList[i].layernumber);
                    nodeInfo.SetOffSet(0);
                    nodeInfo.SetConvDirection(0);
                    nodeInfo.SetQRID(0);

                    nodeList.Add(nodeInfo);
                }

                List<LinkInfo> linkList = new List<LinkInfo>();
                for (int i = 0; i < tgEntity.LinkList.Count; i++)
                {
                    LinkInfo linkInfo = new LinkInfo();

                    linkInfo.SetLinkNumber(tgEntity.LinkList[i].linknumber);
                    linkInfo.SetStartNodeNumber(tgEntity.LinkList[i].startnode);
                    linkInfo.SetTargetNodeNumber(tgEntity.LinkList[i].targetnode);
                    linkInfo.SetLinkType(tgEntity.LinkList[i].linktype);
                    linkInfo.SetLinkDirection(tgEntity.LinkList[i].linkdirection);
                    linkInfo.SetLinkOption(tgEntity.LinkList[i].Foption, tgEntity.LinkList[i].Boption);
                    linkInfo.SetLinkSpeed(tgEntity.LinkList[i].Fvelocity, tgEntity.LinkList[i].Bvelocity);
                    linkInfo.SetSafetyCH(tgEntity.LinkList[i].Fsafetych, tgEntity.LinkList[i].Bsafetych);
                    linkInfo.SetLinkDistance(tgEntity.LinkList[i].linkdistance);
                    linkInfo.SetCurveRadius(tgEntity.LinkList[i].curveradius);

                    linkInfo.SetCurveData(tgEntity.LinkList[i].prelinknumber, tgEntity.LinkList[i].nextlinknumber, tgEntity.LinkList[i].curvestartang, tgEntity.LinkList[i].curvesweepang, tgEntity.LinkList[i].basepoint, tgEntity.LinkList[i].curveradius);
                    linkInfo.SetNextLinkNumber(tgEntity.LinkList[i].linknumber);
                    linkInfo.SetPreLinkNumber(tgEntity.LinkList[i].linknumber);

                    linkList.Add(linkInfo);
                }

                SaveFile.SaveMapData(nodeList, linkList);//, new ZoomInfo(1280, 605));
                SaveFile.SetTotalLinkNum(tgEntity.TotalLinkNum);
                SaveFile.SetTotalNodeNum(tgEntity.TotalNodeNum);
                SaveFile.SetTopologyMapVirsion(tgEntity.TMapVersion);

                BinaryFormatter formatter = new BinaryFormatter();
                Stream stream = File.Open(Globals.MAP_SettingFilePath, FileMode.Create);
                formatter.Serialize(stream, SaveFile);

                Globals.MapData = SaveFile;
                Globals.nodeData = nodeList;
                Globals.linkData = linkList;

                //TouchMain.panelMap.Invalidate();
                //TouchMain.panelMap.Update();

                tgEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
            }
            catch (Exception ex)
            {
                tgEntity.Result = Globals_Hyundai.RESULT_FAIL;
                tgEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }
            Publish_Result(tgEntity);
        }

        private static void Subscribe_GG(string msg)
        {
            logger.Info("[GG] MAP GRID MAP INFORMATION");

            GGEntity ggEntity = new GGEntity();
            try
            {
                try
                {
                    Bitmap Grid = new Bitmap(@"C:\MOBYUS\GridMap.bmp");
                    if (Grid != null)
                    {
                        MemoryStream stream = new MemoryStream();
                        Grid.Save(stream, Grid.RawFormat);
                        byte[] gridData = stream.ToArray();

                        string[] lines = System.IO.File.ReadAllLines(@"C:\Mobyus\GridMap.txt");

                        ushort resolution = 0;
                        long offsetX = 0;
                        long offsetY = 0;

                        foreach (string line in lines)
                        {
                            string[] data = line.Split(':');

                            if (data[0].Equals("resolution"))
                            {
                                float resolutionTemp = 0;
                                resolutionTemp = float.Parse(data[1]);
                                resolution = (ushort)(resolutionTemp * 1000);
                            }
                            else if (data[0].Equals("origin"))
                            {
                                data[1] = data[1].Replace("[", "");
                                data[1] = data[1].Replace("]", "");

                                Point3D origin = Point3D.Parse(data[1]);
                                offsetX = (long)(origin.X * 1000);
                                offsetY = (long)(origin.Y * 1000);
                            }
                        }
                        GridMap gridMap = new GridMap(gridData, resolution, offsetX, offsetY, Grid.Width, Grid.Height);
                        Globals.MapData.SetGridMapData(gridMap);
                    }
                }
                catch (Exception ex)
                {
                    ggEntity.Result = Globals_Hyundai.RESULT_FAIL;
                    ggEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                    logger.Error(ex.ToString());
                }

                ggEntity = JsonConvert.DeserializeObject<GGEntity>(msg);

                ggEntity.GMapVersion = Globals.MapData.GetGridMap().GetGridMapVirsion();
                ggEntity.Resolution = Globals.MapData.GetGridMap().GetMapResolution();
                ggEntity.OffsetX = Globals.MapData.GetGridMap().GetOffSetX();
                ggEntity.OffsetY = Globals.MapData.GetGridMap().GetOffSetY();
                ggEntity.GMapHeight = Globals.MapData.GetGridMap().GetMapHeight();
                ggEntity.GMapWidth = Globals.MapData.GetGridMap().GetMapWidth();
                ggEntity.GMapData = Globals.MapData.GetGridMap().GetMap();

                ggEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
            }
            catch (Exception ex)
            {
                ggEntity.Result = Globals_Hyundai.RESULT_FAIL;
                ggEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }
            Publish_Result(ggEntity);
        }

        private static void Subscribe_ER(string msg)
        {
            logger.Info("[ER] TAMS ERRORCODE");

            EREntity erEntity = new EREntity();
            try
            {
                erEntity = JsonConvert.DeserializeObject<EREntity>(msg);

                switch (erEntity.TAMSErrorCode)
                {
                    case 1:
                        Globals.currentWork.alarm = Alarms.ALARM_TAMS_EQUIPMENT_FAULT_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1설 비 통 신 /C1  에    러 !]";
                        break;
                }
            }
            catch (Exception ex)
            {
                erEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }
            Publish_Result(erEntity);
        }
    }
}