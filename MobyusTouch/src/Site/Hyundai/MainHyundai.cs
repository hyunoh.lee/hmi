﻿using MobyusTouch.src.Communication;
using MobyusTouch.src.Control;
using MobyusTouch.src.Global;
using MobyusTouch.src.Main;
using MobyusTouch.src.View;
using NLog;
using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using Application = System.Windows.Forms.Application;
using MessageBox = System.Windows.Forms.MessageBox;
namespace MobyusTouch.src.Site
{
    public partial class MainHyundai : Form
    {
        private static Logger logger = LogManager.GetLogger("MainHyundai");

        delegate void TimerEventFiredDelegate();

        private PanelAlarm panelAlarm;
        private PanelMap panelMap;
        private PanelSetting panelSetting;

        ElementHost panelElementHost;

        private MainControl mainControl;
        private ExceptionControlHyundai exceptionControl;
        private System.Threading.Timer CheckStatusTimer;
        private System.Threading.Timer communicationTimer;


        public MainHyundai()
        {
            InitializeComponent();

            mainControl = new MainControl();
            exceptionControl = new ExceptionControlHyundai();

            Thread.Sleep(1000);

            panelAlarm = new PanelAlarm();
            panelMap = new PanelMap();

            panelSetting = new PanelSetting();
            panelElementHost = new ElementHost();
            panelElementHost.Child = panelSetting;
            panelElementHost.Dock = DockStyle.Fill;

            panelMap.TopLevel = false;
            panelMap.Visible = true;
            panelAlarm.TopLevel = false;
            panelAlarm.Visible = true;

            this.Controls.Add(panelMain);
            panelMain.Controls.Add(panelMap);

            // this.MainLogo.BackgroundImage = global::MobyusTouch.Properties.Resources.logo;
            buttonMinimize.Image = global::MobyusTouch.Properties.Resources.logo2;

            logger.Info("*****[PROGRAM START]");
            Globals.nAFL_Start = 1;

            InitializeTimers();

            //Globals.currentWork.realPath = Astar.StartAstarAFL(-29000, 81243, 0, 5234);

            //Globals.currentWork.getNextNodePath = Globals.currentWork.realPath.ToList();

            //Globals.currentWork.curNode = 77;
            //for (int i = Globals.currentWork.getNextNodePath.Count - 1; i >= 0; i--)
            //{
            //    //if (Globals.currentWork.realPath[i].GetLinkNumber() == Globals.currentWork.curLink)
            //    if (Globals.currentWork.getNextNodePath[i].GetParentNodeID() == Globals.currentWork.curNode)
            //    {
            //        Globals.currentWork.nextNode = (ushort)Globals.currentWork.getNextNodePath[i].GetCurrentNodeID();

            //        if (Globals.currentWork.nextNode == Globals.currentWork.curNode && i > 0)
            //        {
            //            Globals.currentWork.nextNode = (ushort)Globals.currentWork.getNextNodePath[i - 1].GetCurrentNodeID();
            //        }
            //        Globals.currentWork.getNextNodePath.RemoveAt(i);
            //        break;
            //    }
            //}

        }


        ~MainHyundai()
        {
            mainControl.Close();
            Application.ExitThread();
            Environment.Exit(0);
        }

        private void InitializeTimers()
        {
            // AFL 상태 체크
            /*System.Threading.Timer*/
            CheckStatusTimer = new System.Threading.Timer(CheckStatusTimerCallback);
            CheckStatusTimer.Change(3000, 1000);

            if (!Globals.debug)
            {
                // ACS, CAN, Serial 통신 체크
                /*System.Threading.Timer */
                communicationTimer = new System.Threading.Timer(CheckCommunication);
                communicationTimer.Change(3000, 3000);
                MqttClientHyundai.Connect();
            }
        }

        private void CheckCommunication(Object state)
        {
            if (!CanForklift.IsConnected())
            {
                logger.Info("There is no connection of CAN protocol .");
                CanForklift.Connect();
            }

            if (!SerialPortBoard.IsConnected())
            {
                logger.Info("There is no connection of serial port .");
                SerialPortBoard.Connect();
            }
            if (Globals.IgnoreACS)
            {
                if (MqttClientHyundai.client != null && !MqttClientHyundai.IsConnected())//  && Globals.HMISystem.SystemMode == SystemMode.AUTO)
                {
                    Globals.mqtt_connection_count++;
                    if (Globals.mqtt_connection_count > 20)
                    {
                        Globals.currentWork.alarm = Alarms.ALARM_ACS_COMM_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1관 제 통 신 /C1  에    러 !]";
                    }
                    if (Globals.HMISystem.SystemMode == SystemMode.AUTO)
                    {
                        Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_PAUSE;
                        Globals.HMISystem.ACStoHMI_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_PAUSE;
                    }
                    logger.Info("There is no connection of MQTT Broker.");
                    MqttClientHyundai.Connect();
                }
            }


            if (!TcpClientLED.IsConnected())
            {
                logger.Info("There is no connection of LED TCP Client.");
                TcpClientLED.Disconnect();
                Thread.Sleep(100);
                TcpClientLED.Connect();
            }

        }

        private void CheckStatusTimerCallback(object state)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new TimerEventFiredDelegate(CheckStatus));
            }
        }

        private void CheckStatus()
        {
            CurrentDateTimer();
            SystemModeStatus();
            UpdateTransferStatus();
            CheckTransferStatus();
            SendTAMSObstacle();

            //if (Globals.bStartChargeFlag)
            //    ChargeControl.ChargingNode(); // 1227 충전소노드 확인하여 충전신호보내기 lhw 위치 변경

        }

        private void CurrentDateTimer()
        {
            labelCurrentDate.Text = DateTime.Now.ToString("yyyy/MM/dd \n  H:mm:ss");
        }

        private void buttonZoomUp_Click(object sender, EventArgs e)
        {
            panelMap.Zoom_data.IncreaseRatio();
        }

        private void buttonZoomDown_Click(object sender, EventArgs e)
        {
            panelMap.Zoom_data.DecreaseRatio();
        }

        private void buttonMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            logger.Info("*****[PROGRAM CLOSE]");
            CheckStatusTimer.Dispose();
            if (!Globals.debug)
                communicationTimer.Dispose();

            mainControl.Close();
            Application.ExitThread();
            Environment.Exit(0);
        }

        private void buttonAlarm_Click(object sender, EventArgs e)
        {
            panelMain.Controls.Remove(panelMap);
            panelMain.Controls.Remove(panelElementHost);
            panelMain.Controls.Add(panelAlarm);
        }

        private void buttonMap_Click(object sender, EventArgs e)
        {
            panelMain.Controls.Remove(panelAlarm);
            panelMain.Controls.Remove(panelElementHost);
            panelMain.Controls.Add(panelMap);
        }

        private void buttonSetting_Click(object sender, EventArgs e)
        {
            panelMain.Controls.Remove(panelAlarm);
            panelMain.Controls.Remove(panelMap);
            panelMain.Controls.Add(panelElementHost);
        }

        private void buttonModeSelect_Click(object sender, EventArgs e)
        {
            if (Globals.HMISystem.SystemMode == SystemMode.EMO)
            {
                Globals.HMISystem.SystemMode = SystemMode.RESET;

                if (Globals.VehicleSystem.SystemEMO)
                {
                    MessageBox.Show("EMO 스위치를 해제 후 다시 RESET 해주세요.");
                    return;
                }
                buttonModeSelect.Text = "RESET";
                MessageBox.Show("EMO 모드가 해제되었습니다.");
            }

            DialogMode dialogMode = new DialogMode(buttonModeSelect.Text);
            if (dialogMode.ShowDialog() == true)
            {
                buttonModeSelect.Text = dialogMode.Result;
                Globals.HMISystem.SystemMode = dialogMode.ModeIndex;
            }
        }

        private void buttonWorkOrder_Click(object sender, EventArgs e)
        {
            if (Globals.HMISystem.SystemMode == SystemMode.SEMI_AUTO)
            {
                DialogOrderWork dialogOrderWork = new DialogOrderWork();
                if (dialogOrderWork.ShowDialog() == true)
                {
                }
                dialogOrderWork.TimerStop();
            }
            else
            {
                MessageBox.Show("\"SEMI AUTO MODE\" 시 작동합니다.");
            }
        }

        private void buttonUnitOrder_Click(object sender, EventArgs e)
        {
            if (Globals.HMISystem.SystemMode == SystemMode.MANUAL)
            {
                DialogOrderUnit dialogOrderUnit = new DialogOrderUnit();
                if (dialogOrderUnit.ShowDialog() == true)
                {
                }
                dialogOrderUnit.TimerStop();
            }
            else
            {
                MessageBox.Show("\"MANUAL MODE\" 시 작동합니다.");
            }
        }

        private void BatteryBtn_Click(object sender, EventArgs e)
        {
            DialogBMS dialogBMS = new DialogBMS();
            if (dialogBMS.ShowDialog() == true)
            {
            }
            dialogBMS.TimerStop();
        }

        bool dialogEMOFlag = false;
        private void SystemModeStatus()
        {
            if (Globals.HMISystem.SystemMode == SystemMode.OFFLINE)
            {
                this.buttonModeSelect.Text = "OFFLINE";
                dialogEMOFlag = false;
            }
            else if (Globals.HMISystem.SystemMode == SystemMode.EMO)
            {
                this.buttonModeSelect.Text = "EMO";

                if (!dialogEMOFlag && Globals.currentWork.alarm != Alarms.NO_ALARM)
                {
                    dialogEMOFlag = true;
                    if (dialogEMOFlag)
                    {
                        DialogEMO dialogEMO = new DialogEMO(Globals.currentWork);
                        Globals.bDlgEMOShow = true;
                        dialogEMO.ShowDialog();
                    }

                }
            }
            else if (Globals.HMISystem.SystemMode == SystemMode.RESET)
            {
                this.buttonModeSelect.Text = "RESET";
                dialogEMOFlag = false;
            }
            else if (Globals.HMISystem.SystemMode == SystemMode.MANUAL)
            {
                this.buttonModeSelect.Text = "MANUAL";
                dialogEMOFlag = false;

            }
            else if (Globals.HMISystem.SystemMode == SystemMode.SEMI_AUTO)
            {
                this.buttonModeSelect.Text = "SEMIAUTO";
                dialogEMOFlag = false;
            }
            else if (Globals.HMISystem.SystemMode == SystemMode.AUTO)
            {
                this.buttonModeSelect.Text = "AUTO";
                dialogEMOFlag = false;
            }
            else
            {
                this.buttonModeSelect.Text = "ERROR";
                dialogEMOFlag = false;
            }
        }

        private void CheckTransferStatus()
        {
            if (Globals.currentWork.alarm > 0 && Globals.currentWork.alarm < 200)
            {
                if (buttonAlarm.BackColor == Color.FromArgb(62, 120, 138))
                {
                    buttonAlarm.BackColor = Color.DarkRed;
                }
                else
                {
                    buttonAlarm.BackColor = Color.FromArgb(62, 120, 138);
                }
            }
            else
            {
                buttonAlarm.BackColor = Color.FromArgb(62, 120, 138);
            }

            if (Globals.VehicleSystem.DSNAlarm == true)
            {
                this.StatusAlarm.BackColor = Color.DarkRed;
                this.StatusAlarm.Text = "DS-CAN";
            }
            else if (Globals.NavigationSystem.NAVState != 0)
            {
                this.StatusAlarm.BackColor = Color.DarkRed;
                this.StatusAlarm.Text = "NIU-NAV";
            }
            else if (Globals.NavigationSystem.Alarm != 0)
            {
                this.StatusAlarm.BackColor = Color.DarkRed;
                this.StatusAlarm.Text = "NIU";
            }
            else if (Globals.RecognitionSystem.Alarm != 0)
            {
                this.StatusAlarm.BackColor = Color.DarkRed;
                this.StatusAlarm.Text = "NANO";
            }
            else
            {
                this.StatusAlarm.Text = "OK";
                this.StatusAlarm.BackColor = Color.Green;
            }

            if (mainControl != null && MqttClientHyundai.client != null && MqttClientHyundai.IsConnected())
            {
                this.StatusFMS.BackColor = Color.Green;
            }
            else
            {
                if (this.StatusFMS.BackColor == Color.DarkRed)
                {
                    this.StatusFMS.BackColor = Color.Orange;
                }
                else
                {
                    this.StatusFMS.BackColor = Color.DarkRed;
                }
                this.StatusAlarm.BackColor = Color.DarkRed;
                this.StatusAlarm.Text = "FMS";
            }

            if (mainControl != null && CanForklift.isConnected)
            {
                this.StatusCAN.BackColor = Color.Green;
            }
            else
            {
                if (this.StatusCAN.BackColor == Color.DarkRed)
                {
                    this.StatusCAN.BackColor = Color.Orange;
                }
                else
                {
                    this.StatusCAN.BackColor = Color.DarkRed;
                }
                this.StatusAlarm.BackColor = Color.DarkRed;
                this.StatusAlarm.Text = "CAN";
            }

            if (mainControl != null && SerialPortBoard.IsConnected())
            {
                this.StatusSerial.BackColor = Color.Green;
            }
            else
            {
                if (this.StatusSerial.BackColor == Color.DarkRed)
                {
                    this.StatusSerial.BackColor = Color.Orange;
                }
                else
                {
                    this.StatusSerial.BackColor = Color.DarkRed;
                }
                this.StatusAlarm.BackColor = Color.DarkRed;
                this.StatusAlarm.Text = "SERIAL";
            }


            if (mainControl != null && Globals.currentWork.alarm != Alarms.ALARM_CAN_220_VEHICLE_CONNECTION_ERROR && CanForklift.isConnected)
            {
                this.StatusVehicle.BackColor = Color.Green;
            }
            else
            {
                if (this.StatusVehicle.BackColor == Color.DarkRed)
                {
                    this.StatusVehicle.BackColor = Color.Orange;
                }
                else
                {
                    this.StatusVehicle.BackColor = Color.DarkRed;
                }
            }

            if (mainControl != null && Globals.currentWork.alarm != Alarms.ALARM_CAN_241_FORKLIFT_CONNECTION_ERROR && Globals.currentWork.alarm != Alarms.ALARM_CAN_242_FORKLIFT_CONNECTION_ERROR && CanForklift.isConnected)
            {
                this.StatusForkLift.BackColor = Color.Green;
            }
            else
            {
                if (this.StatusForkLift.BackColor == Color.DarkRed)
                {
                    this.StatusForkLift.BackColor = Color.Orange;
                }
                else
                {
                    this.StatusForkLift.BackColor = Color.DarkRed;
                }
            }

            if (mainControl != null && Globals.currentWork.alarm != Alarms.ALARM_CAN_260_NAVIGATION_CONNECTION_ERROR && Globals.currentWork.alarm != Alarms.ALARM_CAN_261_NAVIGATION_CONNECTION_ERROR && CanForklift.isConnected)
            {
                this.StatusNavi.BackColor = Color.Green;
            }
            else
            {
                if (this.StatusNavi.BackColor == Color.DarkRed)
                {
                    this.StatusNavi.BackColor = Color.Orange;
                }
                else
                {
                    this.StatusNavi.BackColor = Color.DarkRed;
                }
            }

            if (mainControl != null && Globals.currentWork.alarm != Alarms.ALARM_CAN_290_RECOGNITION_CONNECTION_ERROR && CanForklift.isConnected)
            {
                this.StatusRecog.BackColor = Color.Green;
            }
            else
            {
                if (this.StatusRecog.BackColor == Color.DarkRed)
                {
                    this.StatusRecog.BackColor = Color.Orange;
                }
                else
                {
                    this.StatusRecog.BackColor = Color.DarkRed;
                }
            }

            if (mainControl != null /*&& Globals.currentWork.alarm != Alarms.ALARM_CAN_333_GATEWAY_CONNECTION_ERROR*/ && CanForklift.isConnected)
            {
                this.StatusSafety.BackColor = Color.Green;
            }
            else
            {
                if (this.StatusSafety.BackColor == Color.DarkRed)
                {
                    this.StatusSafety.BackColor = Color.Orange;
                }
                else
                {
                    this.StatusSafety.BackColor = Color.DarkRed;
                }
            }

            if (mainControl != null /*&& (Globals.currentWork.alarm != Alarms.ALARM_CAN_340_BMS_CONNECTION_ERROR && Globals.currentWork.alarm != Alarms.ALARM_CAN_341_BMS_CONNECTION_ERROR)*/ && CanForklift.isConnected)
            {
                this.StatusBMS.BackColor = Color.Green;
            }
            else
            {
                if (this.StatusBMS.BackColor == Color.DarkRed)
                {
                    this.StatusBMS.BackColor = Color.Orange;
                }
                else
                {
                    this.StatusBMS.BackColor = Color.DarkRed;
                }
            }
        }

        private void SendTAMSObstacle() // 22.11.28 kdy nano스캔, 초음파, wt100L, 공간감지 묶어서 한번에 보내기
        {
            int visionaryT = Globals.SafetySystem.VisioneryT == true ? 256 : 0;
            int ultrasonic = Globals.NavigationSystem.bUltraSonic == true ? 512 : 0;
            int WT100 = Globals.NavigationSystem.bWT100 == true ? 1024 : 0;
            Globals.SafetySystem.NANOSCAN3State_Finally = Globals.SafetySystem.NANOSCAN3State + visionaryT + ultrasonic + WT100;
        }
        private void UpdateTransferStatus()
        {
            //work
            if (Globals.currentWork.workState == (byte)WorkState.READY)
            {
                this.WorkBtn.Text = "READY";
            }
            else if (Globals.currentWork.workState == (byte)WorkState.MOVING)
            {
                this.WorkBtn.Text = "MOVING";
            }
            else if (Globals.currentWork.workState == (byte)WorkState.LOADING && Globals.currentWork.workType == Globals.TRANSFER_LOAD)
            {
                this.WorkBtn.Text = "LOADING";
            }
            else if (Globals.currentWork.workState == (byte)WorkState.UNLOADING && Globals.currentWork.workType == Globals.TRANSFER_UNLOAD)
            {
                this.WorkBtn.Text = "UNLOADING";
            }
            else if (Globals.currentWork.workState == (byte)WorkState.MOVE_COMPLETE)
            {
                this.WorkBtn.Text = "MOVE COMPLETE";
            }
            else if (Globals.currentWork.workState == (byte)WorkState.LOAD_COMPLETE)
            {
                this.WorkBtn.Text = "LOAD COMPLETE";
            }
            else if (Globals.currentWork.workState == (byte)WorkState.UNLOAD_COMPLETE)
            {
                this.WorkBtn.Text = "UNLOAD COMPLETE";
            }
            else if (Globals.currentWork.workState == (byte)WorkState.CHARGING && Globals.VehicleSystem.SystemCharge)
            {
                this.WorkBtn.Text = "CHARGING";
            }

            //workUnit
            if (Globals.currentWork.workStateUnit == (byte)WorkStateUnit.READY)
            {
                this.WorkUnitBtn.Text = "READY";
            }
            else if (Globals.currentWork.workStateUnit == (byte)WorkStateUnit.LIFTING)
            {
                this.WorkUnitBtn.Text = "LIFTING";
            }
            else if (Globals.currentWork.workStateUnit == (byte)WorkStateUnit.TILTING)
            {
                this.WorkUnitBtn.Text = "TILTING";
            }
            else if (Globals.currentWork.workStateUnit == (byte)WorkStateUnit.SIDESHIFT)
            {
                this.WorkUnitBtn.Text = "SIDESHIFT";
            }
            else if (Globals.currentWork.workStateUnit == (byte)WorkStateUnit.FORKMOVER)
            {
                this.WorkUnitBtn.Text = "FORKMOVER";
            }
            else if (Globals.currentWork.workStateUnit == (byte)WorkStateUnit.PATH_1ST_NODE)
            {
                this.WorkUnitBtn.Text = "1ST DRIVE";
            }
            else if (Globals.currentWork.workStateUnit == (byte)WorkStateUnit.PATH_2ND_STATION)
            {
                this.WorkUnitBtn.Text = "2ND DRIVE";
            }
            else if (Globals.currentWork.workStateUnit == (byte)WorkStateUnit.PATH_SENDING)
            {
                this.WorkUnitBtn.Text = "PATH SENDING";
            }
            else if (Globals.currentWork.workStateUnit == (byte)WorkStateUnit.PALLET_FRONT)
            {
                this.WorkUnitBtn.Text = "PALLET FRONT";
            }
            else if (Globals.currentWork.workStateUnit == (byte)WorkStateUnit.PALLET_STACKING)
            {
                this.WorkUnitBtn.Text = "PALLET STACKING";
            }
            else if (Globals.currentWork.workStateUnit == (byte)WorkStateUnit.TAGGING)
            {
                this.WorkUnitBtn.Text = "TAGGING";
            }

            if (Globals.currentWork.workState == 7)
            {
                this.WorkBtn.Text = "TRAFFIC";

                if (this.WorkBtn.BackColor == Color.FromArgb(41, 44, 51))
                {
                    this.WorkBtn.BackColor = Color.Orange;
                }
                else
                {
                    this.WorkBtn.BackColor = Color.FromArgb(41, 44, 51);
                }
            }
            else
            {
                this.WorkBtn.BackColor = Color.FromArgb(41, 44, 51);
            }

            string traffic = "READY";
            if (Globals.HMISystem.SystemTraffic == SystemTraffic.TRANSFER_TRAFFIC_FIRMWARE_NONE)
            {
                traffic = "READY";
            }
            else if (Globals.HMISystem.SystemTraffic == SystemTraffic.TRANSFER_TRAFFIC_PAUSE)
            {
                traffic = "PAUSE";
            }
            else if (Globals.HMISystem.SystemTraffic == SystemTraffic.TRANSFER_TRAFFIC_RESUME)
            {
                traffic = "RESUME";
            }
            else if (Globals.HMISystem.SystemTraffic == SystemTraffic.TRANSFER_TRAFFIC_STOP)
            {
                traffic = "STOP";
            }

            this.TrafficBtn.Text = traffic.ToString();

            this.SteerBtn.Text = Globals.VehicleSystem.CurrentAngle.ToString() + " deg";
            //this.SpeedBtn.Text = (Globals.VehicleSystem.CurrentSpeed * 0.10472 * 3.6).ToString() + " m/s";
            this.SpeedBtn.Text = (Globals.VehicleSystem.CurrentSpeed / 250).ToString() + " km/h";

            this.LiftBtn.Text = Globals.ForkLiftSystem.LiftPosition.ToString() + " mm";
            this.TiltBtn.Text = Globals.ForkLiftSystem.TiltPosition.ToString() + " deg";
            this.ForkCenterBtn.Text = ((Globals.ForkLiftSystem.ShiftRightPosition - Globals.ForkLiftSystem.ShiftLeftPosition) / 2).ToString() + " mm";
            this.ForkWidthBtn.Text = Globals.ForkLiftSystem.MoverPosition.ToString() + " mm";
            //this.ReachBtn.Text = Globals.ForkLiftSystem.ReachPosition.ToString() + " mm";
            this.ChargeBtn.Text = Globals.HMISystem.ChargeEnable == true && Globals.VehicleSystem.SystemCharge == true ? "ON" : "OFF";
            //this.ObstacleBtn.Text = Globals.NavigationSystem.VStateBeamStopArea == true ? "STOP" : Globals.NavigationSystem.VStateBeamLowArea == true ? "LOW" : "OFF";
            this.BatteryBtn.Text = Globals.BMSSystem.SOC.ToString() + "%";
            this.LoadedBtn.Text = Globals.loaded == true ? "ON" : "OFF";

            //23.09.05 kdy 안전센서 위치 표시

            if (Globals.NavigationSystem.VStateBeamStopArea || Globals.SafetySystem.VisioneryT || Globals.NavigationSystem.bUltraSonic || Globals.NavigationSystem.bWT100) //Globals.currentWork.workState != (byte)WorkState.READY && 
            {
                String Front = "";
                String Left = "";
                String Right = "";
                String Tail = "";
                String VisionaryT = "";
                String UltraSonic = "";
                String WT100 = "";
                if (Globals.SafetySystem.FrontNS3State) //  == Globals.WORK_SAFETY_FRONTNS3_STOP
                {
                    Front = "F";
                }
                else
                {
                    Front = " ";
                }
                if (Globals.SafetySystem.LeftNS3State) //  == Globals.WORK_SAFETY_LEFTNS3_STOP
                {
                    Left = "L";
                }
                else
                {
                    Left = " ";
                }
                if (Globals.SafetySystem.RightNS3State) //  == Globals.WORK_SAFETY_RIGHTNS3_STOP
                {
                    Right = "R";
                }
                else
                {
                    Right = " ";
                }
                if (Globals.SafetySystem.TailNS3State) //  == Globals.WORK_SAFETY_TAILNS3_STOP
                {
                    Tail = "T";
                }
                else
                {
                    Tail = " ";
                }
                if (Globals.SafetySystem.VisioneryT)
                {
                    VisionaryT = "V";
                }
                else
                {
                    VisionaryT = " ";
                }
                if (Globals.NavigationSystem.bUltraSonic)
                {
                    UltraSonic = "U";
                }
                else
                {
                    UltraSonic = " ";
                }
                if (Globals.NavigationSystem.bWT100)
                {
                    WT100 = "W";
                }
                else
                {
                    WT100 = " ";
                }
                this.ObstacleBtn.Text = Front + Left + Right + Tail + "\n" + VisionaryT + UltraSonic + WT100;
            }
            else
            {
                this.ObstacleBtn.Text = "OFF";
            }
            // kdy 1118 retry UI표시
            if (Globals.currentWork.frontRetry == 1)
            {
                this.RetryBtn.Text = "Retrying";
            }
            else
            {
                this.RetryBtn.Text = "-";
            }

            //차량 호차 표시
            switch (Globals.ID)
            {
                case "AMR001":
                    this.buttonIDSelect.Text = "1 호 차";
                    break;
                case "AMR002":
                    this.buttonIDSelect.Text = "2 호 차";
                    break;
                case "AMR003":
                    this.buttonIDSelect.Text = "3 호 차";
                    break;
            }
        }

        private void buttonForceComplete_Click(object sender, EventArgs e)
        {
            if (MqttClientHyundai.client != null && MqttClientHyundai.IsConnected())
            {
                //Globals.nForceComplete = 1;
                MessageBox.Show("강제 완료 신호 송신 완료");
                logger.Info("   Force Complete Button Click");

                Globals.currentWork.alarm = Alarms.NO_ALARM;

                //kdy 23.06.15 강제버튼 클릭시 안착감지 체크
                if (Globals.SafetySystem.LeftPx || Globals.SafetySystem.RightPx)
                {
                    Globals.loaded = true;
                }
                else
                {
                    Globals.loaded = false;
                }

                Thread.Sleep(1000);
            }
            else if (MqttClientHyundai.client != null && !MqttClientHyundai.IsConnected())
                MessageBox.Show("Mqtt 통신 연결 확인");
            else if (Globals.HMISystem.SystemMode == SystemMode.AUTO || Globals.HMISystem.SystemMode == SystemMode.SEMI_AUTO)
                MessageBox.Show("매뉴얼 모드 확인");

        }

        private void buttonIDSelect_Click(object sender, EventArgs e)
        {
            DialogID dialogID = new DialogID();

            if (dialogID.ShowDialog() == true)
            {

            }
            buttonIDSelect.Text = "-";
        }
    }
}
