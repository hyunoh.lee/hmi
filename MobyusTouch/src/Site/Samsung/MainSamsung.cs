﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using MobyusTouch.src.Communication;
using MobyusTouch.src.Control;
using MobyusTouch.src.Global;
using MobyusTouch.src.Main;
using MobyusTouch.src.Main.Sub;
using MobyusTouch.src.Utility;
using MobyusTouch.src.View;
using MobyusTouch.Src.Utility;
using NLog;
using Peak.Can.Basic;
using Application = System.Windows.Forms.Application;
using MessageBox = System.Windows.Forms.MessageBox;

using TPCANHandle = System.UInt16;
using TPCANTimestampFD = System.UInt64;

namespace MobyusTouch.src
{
    public partial class MainSamsung : Form
    {
        private static Logger logger = LogManager.GetLogger("TouchMain");

        delegate void TimerEventFiredDelegate();

        private PanelAlarm panelAlarm;
        private PanelMap panelMap;
        private PanelSetting panelSetting;

        ElementHost panelElementHost;

        private MainControl mainControl;
        private ExceptionControlSamsung exceptionControl;

        public MainSamsung()
        {
            InitializeComponent();
            InitializeTimers();

            mainControl = new MainControl();
            exceptionControl = new ExceptionControlSamsung();

            Thread.Sleep(1000);

            panelAlarm = new PanelAlarm();
            panelMap = new PanelMap();

            panelSetting = new PanelSetting();
            panelElementHost = new ElementHost();
            panelElementHost.Child = panelSetting;
            panelElementHost.Dock = DockStyle.Fill;

            panelMap.TopLevel = false;
            panelMap.Visible = true;
            //panelAlarm.TopLevel = false;
            //panelAlarm.Visible = true;

            this.Controls.Add(panelMain);
            panelMain.Controls.Add(panelMap);

            NLog.LogManager.Configuration = LogManager.Configuration;
        }

        ~MainSamsung()
        {
            mainControl.Close();
            Application.ExitThread();
            Environment.Exit(0);
        }

        private void InitializeTimers()
        {
            System.Threading.Timer CheckStatusTimer = new System.Threading.Timer(CheckStatusTimerCallback);
            CheckStatusTimer.Change(0, 1000);


            // ACS, CAN, Serial 통신 체크
            System.Threading.Timer communicationTimer = new System.Threading.Timer(CheckCommunication);
            communicationTimer.Change(0, 3000);
        }

        private void CheckCommunication(Object state)
        {
            
            if (!MqttClientSamsung.IsConnected())
            {
                if (Globals.HMISystem.SystemMode == SystemMode.AUTO)
                {
                    Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_PAUSE;
                }
                logger.Info("There is no connection of MQTT Broker.");
                MqttClientSamsung.Connect();
            }
            
            if (!CanForklift.IsConnected())
            {
                logger.Info("There is no connection of CAN protocol .");
                CanForklift.Connect();
            }

            if (!SerialPortBoard.IsConnected())
            {
                logger.Info("There is no connection of serial port .");
                SerialPortBoard.Connect();
            }
        }

        private void CheckStatusTimerCallback(object state)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new TimerEventFiredDelegate(CheckStatusTimer));
            }
        }

        private void CurrentDateTimer()
        {
            labelCurrentDate.Text = DateTime.Now.ToString("yyyy/MM/dd \n  H:mm:ss");
        }

        private void CheckStatusTimer()
        {
            GetNavAndIPalletData();
            CurrentDateTimer();
            SystemModeStatus();
            UpdateTransferStatus();
            CheckTransferStatus();
        }

        private void GetNavAndIPalletData()
        {
            CurrentNodeBtn.Text = Globals.currentWork.curNode.ToString();
            TargetNodeBtn.Text = Globals.currentWork.toNode.ToString();
            NextNodeBtn.Text = Globals.currentWork.nextNode.ToString();

            NavXBtn.Text = Globals.NavigationSystem.NAV_X.ToString();
            NavYBtn.Text = Globals.NavigationSystem.NAV_Y.ToString();
            NavPHIBtn.Text = Globals.NavigationSystem.NAV_PHI.ToString();

            StackingXBtn.Text = Globals.RecognitionSystem.TargetX.ToString();
            StackingYBtn.Text = Globals.RecognitionSystem.TargetY.ToString();
            StackingThetaBtn.Text = Globals.RecognitionSystem.TargetTheta.ToString();

            PalletXBtn.Text = Globals.RecognitionSystem.FrontPalletX.ToString();
            PalletYBtn.Text = Globals.RecognitionSystem.FrontPalletY.ToString();
            PalletThetaBtn.Text = Globals.RecognitionSystem.FrontPalletTheta.ToString();

        }

        private void buttonZoomUp_Click(object sender, EventArgs e)
        {
            panelMap.Zoom_data.IncreaseRatio();
        }

        private void buttonZoomDown_Click(object sender, EventArgs e)
        {
            panelMap.Zoom_data.DecreaseRatio();
        }

        private void buttonMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            mainControl.Close();
            Application.ExitThread();
            Environment.Exit(0);
        }

        private void buttonAlarm_Click(object sender, EventArgs e)
        {
            panelMain.Controls.Remove(panelMap);
            panelMain.Controls.Remove(panelElementHost);
            //panelMain.Controls.Add(panelAlarm);
        }

        private void buttonMap_Click(object sender, EventArgs e)
        {
            //panelMain.Controls.Remove(panelAlarm);
            panelMain.Controls.Remove(panelElementHost);
            panelMain.Controls.Add(panelMap);
        }

        private void buttonSetting_Click(object sender, EventArgs e)
        {
            //panelMain.Controls.Remove(panelAlarm);
            panelMain.Controls.Remove(panelMap);
            panelMain.Controls.Add(panelElementHost);
        }

        private void buttonModeSelect_Click(object sender, EventArgs e)
        {
            if (Globals.HMISystem.SystemMode == SystemMode.EMO)
            {
                Globals.HMISystem.SystemMode = SystemMode.RESET;

                if (Globals.VehicleSystem.SystemEMO)
                {
                    MessageBox.Show("EMO 스위치를 해제 후 다시 RESET 해주세요.");
                    return;
                }
                buttonModeSelect.Text = "RESET";
                MessageBox.Show("EMO 모드가 해제되었습니다.");
            }

            DialogMode dialogMode = new DialogMode(buttonModeSelect.Text);
            if (dialogMode.ShowDialog() == true)
            {
                buttonModeSelect.Text = dialogMode.Result;
                Globals.HMISystem.SystemMode = dialogMode.ModeIndex;
            }
        }

        private void buttonWorkOrder_Click(object sender, EventArgs e)
        {
            if (Globals.HMISystem.SystemMode == SystemMode.SEMI_AUTO)
            {
                DialogOrderWork dialogOrderWork = new DialogOrderWork();
                if (dialogOrderWork.ShowDialog() == true)
                {
                }
                dialogOrderWork.TimerStop();
            }
            else
            {
                MessageBox.Show("\"SEMI AUTO MODE\" 시 작동합니다.");
            }
        }

        private void buttonUnitOrder_Click(object sender, EventArgs e)
        {
            if (Globals.HMISystem.SystemMode == SystemMode.MANUAL)
            {
                DialogOrderUnit dialogOrderUnit = new DialogOrderUnit();
                if (dialogOrderUnit.ShowDialog() == true)
                {
                }
                dialogOrderUnit.TimerStop();
            }
            else
            {
                MessageBox.Show("\"MANUAL MODE\" 시 작동합니다.");
            }
        }

        private void BatteryBtn_Click(object sender, EventArgs e)
        {
            DialogBMS dialogBMS = new DialogBMS();
            if (dialogBMS.ShowDialog() == true)
            {
            }
            dialogBMS.TimerStop();
        }

        bool dialogEMOFlag = false;
        private void SystemModeStatus()
        {
            if (Globals.HMISystem.SystemMode == SystemMode.OFFLINE)
            {
                this.buttonModeSelect.Text = "OFFLINE";
                dialogEMOFlag = false;
            }
            else if (Globals.HMISystem.SystemMode == SystemMode.EMO)
            {
                this.buttonModeSelect.Text = "EMO";

                if (!dialogEMOFlag)
                {
                    dialogEMOFlag = true;
                    if (dialogEMOFlag)
                    {
                        DialogEMO dialogEMO = new DialogEMO(Globals.currentWork);
                        dialogEMO.ShowDialog();
                    }

                }
            }
            else if (Globals.HMISystem.SystemMode == SystemMode.RESET)
            {
                this.buttonModeSelect.Text = "RESET";
                dialogEMOFlag = false;
            }
            else if (Globals.HMISystem.SystemMode == SystemMode.MANUAL)
            {
                this.buttonModeSelect.Text = "MANUAL MODE";
                dialogEMOFlag = false;
            }
            else if (Globals.HMISystem.SystemMode == SystemMode.SEMI_AUTO)
            {
                this.buttonModeSelect.Text = "SEMI AUTO MODE";
                dialogEMOFlag = false;
            }
            else if (Globals.HMISystem.SystemMode == SystemMode.AUTO)
            {
                this.buttonModeSelect.Text = "AUTO MODE";
                dialogEMOFlag = false;
            }
            else
            {
                this.buttonModeSelect.Text = "ERROR";
                dialogEMOFlag = false;
            }
        }

        private void CheckTransferStatus()
        {
            /*
            if (Globals.currentWork.alarm > 0)
            {
                if (buttonAlarm.BackColor == Color.FromArgb(62, 120, 138))
                {
                    buttonAlarm.BackColor = Color.DarkRed;
                }
                else
                {
                    buttonAlarm.BackColor = Color.FromArgb(62, 120, 138);
                }
            }
            else
            {
                buttonAlarm.BackColor = Color.FromArgb(62, 120, 138);
            }
            */

            if (Globals.VehicleSystem.DSNAlarm == true)
            {
                this.StatusAlarm.BackColor = Color.DarkRed;
                this.StatusAlarm.Text = "VIU";
            }
            else if (Globals.NavigationSystem.NAVState != 0)
            {
                this.StatusAlarm.BackColor = Color.DarkRed;
                this.StatusAlarm.Text = "NIU-NAV";
            }
            else if (Globals.NavigationSystem.Alarm != 0)
            {
                this.StatusAlarm.BackColor = Color.DarkRed;
                this.StatusAlarm.Text = "NIU";
            }
            else if (Globals.RecognitionSystem.Alarm != 0)
            {
                this.StatusAlarm.BackColor = Color.DarkRed;
                this.StatusAlarm.Text = "NANO";
            }
            else
            {
                this.StatusAlarm.Text = "OK";
                this.StatusAlarm.BackColor = Color.Green;
            }

            if (mainControl != null && MqttClientSamsung.IsConnected())
            {
                this.StatusFMS.BackColor = Color.Green;
            }
            else
            {
                if (this.StatusFMS.BackColor == Color.DarkRed)
                {
                    this.StatusFMS.BackColor = Color.Orange;
                }
                else
                {
                    this.StatusFMS.BackColor = Color.DarkRed;
                }
                this.StatusAlarm.BackColor = Color.DarkRed;
                this.StatusAlarm.Text = "FMS";
            }

            if (mainControl != null && CanForklift.isConnected)
            {
                this.StatusCAN.BackColor = Color.Green;
            }
            else
            {
                if (this.StatusCAN.BackColor == Color.DarkRed)
                {
                    this.StatusCAN.BackColor = Color.Orange;
                }
                else
                {
                    this.StatusCAN.BackColor = Color.DarkRed;
                }
                this.StatusAlarm.BackColor = Color.DarkRed;
                this.StatusAlarm.Text = "CAN";
            }

            if (mainControl != null && SerialPortBoard.IsConnected())
            {
                this.StatusSerial.BackColor = Color.Green;
            }
            else
            {
                if (this.StatusSerial.BackColor == Color.DarkRed)
                {
                    this.StatusSerial.BackColor = Color.Orange;
                }
                else
                {
                    this.StatusSerial.BackColor = Color.DarkRed;
                }
                this.StatusAlarm.BackColor = Color.DarkRed;
                this.StatusAlarm.Text = "SERIAL";
            }
        }

        private void UpdateTransferStatus()
        {
            //work
            if (Globals.currentWork.workState == (byte)WorkState.READY)
            {
                this.WorkBtn.Text = "READY";
            }
            else if (Globals.currentWork.workState == (byte)WorkState.MOVING)
            {
                this.WorkBtn.Text = "MOVING";
            }
            else if (Globals.currentWork.workState == (byte)WorkState.LOADING && Globals.currentWork.workType == Globals.TRANSFER_LOAD)
            {
                this.WorkBtn.Text = "LOADING";
            }
            else if (Globals.currentWork.workState == (byte)WorkState.UNLOADING && Globals.currentWork.workType == Globals.TRANSFER_UNLOAD)
            {
                this.WorkBtn.Text = "UNLOADING";
            }
            else if (Globals.currentWork.workState == (byte)WorkState.MOVE_COMPLETE)
            {
                this.WorkBtn.Text = "MOVE COMPLATE";
            }
            else if (Globals.currentWork.workState == (byte)WorkState.LOAD_COMPLETE)
            {
                this.WorkBtn.Text = "LOAD COMPLATE";
            }
            else if (Globals.currentWork.workState == (byte)WorkState.UNLOAD_COMPLETE)
            {
                this.WorkBtn.Text = "UNLOAD COMPLATE";
            }
            else if (Globals.currentWork.workState == (byte)WorkState.CHARGING)
            {
                this.WorkBtn.Text = "CHARGING";
            }
            /*
            //workUnit
            if (Globals.currentWork.workStateUnit == (byte)WorkStateUnit.READY)
            {
                this.WorkUnitBtn.Text = "READY";
            }
            else if (Globals.currentWork.workStateUnit == (byte)WorkStateUnit.LIFTING)
            {
                this.WorkUnitBtn.Text = "LIFTING";
            }
            else if (Globals.currentWork.workStateUnit == (byte)WorkStateUnit.TILTING)
            {
                this.WorkUnitBtn.Text = "TILTING";
            }
            else if (Globals.currentWork.workStateUnit == (byte)WorkStateUnit.SIDESHIFT)
            {
                this.WorkUnitBtn.Text = "SIDESHIFT";
            }
            else if (Globals.currentWork.workStateUnit == (byte)WorkStateUnit.FORKMOVER)
            {
                this.WorkUnitBtn.Text = "FORKMOVER";
            }
            else if (Globals.currentWork.workStateUnit == (byte)WorkStateUnit.PATH_1ST_NODE)
            {
                this.WorkUnitBtn.Text = "1ST DRIVE";
            }
            else if (Globals.currentWork.workStateUnit == (byte)WorkStateUnit.PATH_2ND_STATION)
            {
                this.WorkUnitBtn.Text = "2ND DRIVE";
            }
            else if (Globals.currentWork.workStateUnit == (byte)WorkStateUnit.PATH_SENDING)
            {
                this.WorkUnitBtn.Text = "PATH SENDING";
            }
            else if (Globals.currentWork.workStateUnit == (byte)WorkStateUnit.PALLET_FRONT)
            {
                this.WorkUnitBtn.Text = "PALLET FRONT";
            }
            else if (Globals.currentWork.workStateUnit == (byte)WorkStateUnit.PALLET_STACKING)
            {
                this.WorkUnitBtn.Text = "PALLET STACKING";
            }
            else if (Globals.currentWork.workStateUnit == (byte)WorkStateUnit.TAGGING)
            {
                this.WorkUnitBtn.Text = "TAGGING";
            }

            if (Globals.currentWork.workState == 7)
            {
                this.WorkBtn.Text = "TRAFFIC";

                if (this.WorkBtn.BackColor == Color.FromArgb(41, 44, 51))
                {
                    this.WorkBtn.BackColor = Color.Orange;
                }
                else
                {
                    this.WorkBtn.BackColor = Color.FromArgb(41, 44, 51);
                }
            }
            else
            {
                this.WorkBtn.BackColor = Color.FromArgb(41, 44, 51);
            }
            */

            string traffic = "READY";
            if (Globals.HMISystem.SystemTraffic == SystemTraffic.TRANSFER_TRAFFIC_FIRMWARE_NONE)
            {
                traffic = "READY";
            }
            else if (Globals.HMISystem.SystemTraffic == SystemTraffic.TRANSFER_TRAFFIC_PAUSE)
            {
                traffic = "PAUSE";
            }
            else if (Globals.HMISystem.SystemTraffic == SystemTraffic.TRANSFER_TRAFFIC_RESUME)
            {
                traffic = "RESUME";
            }
            else if (Globals.HMISystem.SystemTraffic == SystemTraffic.TRANSFER_TRAFFIC_STOP)
            {
                traffic = "STOP";
            }

            this.TrafficBtn.Text = traffic.ToString();

            this.SteerBtn.Text = Globals.VehicleSystem.CurrentAngle.ToString() + " deg";
            this.SpeedBtn.Text = Globals.VehicleSystem.CurrentSpeed.ToString() + " rpm";

            this.LiftBtn.Text = Globals.ForkLiftSystem.LiftPosition.ToString() + " mm";
            this.TiltBtn.Text = Globals.ForkLiftSystem.TiltPosition.ToString() + " deg";
            //this.ForkCenterBtn.Text = ((Globals.ForkLiftSystem.ShiftRightPosition - Globals.ForkLiftSystem.ShiftLeftPosition) / 2).ToString() + " mm";
            //this.ForkWidthBtn.Text = Globals.ForkLiftSystem.MoverPosition.ToString() + " mm";
            //this.ReachBtn.Text = Globals.ForkLiftSystem.ReachPosition.ToString() + " mm";
            //this.ChargeBtn.Text = Globals.rx310Data.SystemCharge == true ? "ON" : "OFF";
            this.ObstacleBtn.Text = Globals.NavigationSystem.VStateBeamStopArea == true ? "STOP" : Globals.NavigationSystem.VStateBeamLowArea == true ? "LOW" : "OFF";
            this.BatteryBtn.Text = Globals.BMSSystem.Battery.ToString() + "%";
            //this.LoadedBtn.Text = Globals.loaded == true ? "ON" : "OFF";
        }
    }
}
