﻿using MobyusTouch.src.Communication;
using MobyusTouch.src.Global;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MobyusTouch.src.Control
{
    [Serializable()]
    class ExceptionControlSamsung : System.Exception
    {
        private static Logger logger = LogManager.GetLogger("ExceptionControl");

        private static int ObstacleCnt = 0;

        private static int CAN_220_CNT = 0;

        private static int CAN_241_CNT = 0;
        private static int CAN_242_CNT = 0;

        private static int CAN_260_CNT = 0;
        private static int CAN_261_CNT = 0;

        private static int CAN_290_CNT = 0;
        //private static int CAN_291_CNT = 0;
        //private static int CAN_292_CNT = 0;

        //private static int CAN_340_CNT = 0;
        //private static int CAN_341_CNT = 0;

        System.Threading.Timer bmsTimer;
        System.Threading.Timer canTimer;
        System.Threading.Timer obstacleStopTimer;
        System.Threading.Timer cameraPalletTimer;
        System.Threading.Timer navTimer;
        System.Threading.Timer navigationTimer;
        System.Threading.Timer forkliftTimer;
        System.Threading.Timer vehicleTimer;

        public ExceptionControlSamsung()
        {
            AddAlarmInfo();

            TimerStart();
        }

        ~ExceptionControlSamsung()
        {
            TimerStop();
        }

        public static bool IsInterruptedAlarm()
        {
            bool interrupted = false;
            if (Globals.currentWork.alarm > 0 && Globals.currentWork.alarm < Alarms.WARNING && Globals.HMISystem.SystemMode > SystemMode.RESET) // 알람코드 200이상은 Warning
            {
                interrupted = true;
                Globals.HMISystem.SystemMode = SystemMode.EMO;
            }
            else if (Globals.currentWork.alarm > Alarms.NO_ALARM)
            {
                interrupted = false;
            }
            return interrupted;
        }

        public void TimerStart()
        {
            if (!Globals.debug)
            {

                // CAN 통신 알람
                canTimer = new System.Threading.Timer(CANAlarm);
                canTimer.Change(0, 2000); // 10초

                // 장애물 지속 알람
                obstacleStopTimer = new System.Threading.Timer(ObstacleStopAlarm);
                obstacleStopTimer.Change(0, 5000); // 2분 지속

                // 인식 시스템(Nano) 팔렛 알람
                cameraPalletTimer = new System.Threading.Timer(CameraPalletAlarm);
                cameraPalletTimer.Change(0, 2000);

                // Navigation Board 알람
                navigationTimer = new System.Threading.Timer(NavigationAlarm);
                navigationTimer.Change(0, 2000);

                // Forklift Board 알람
                forkliftTimer = new System.Threading.Timer(ForkliftAlarm);
                forkliftTimer.Change(0, 2000);

                // Vehicle Board 알람
                vehicleTimer = new System.Threading.Timer(VehicleAlarm);
                vehicleTimer.Change(0, 2000);

                // NAV 알람
                navTimer = new System.Threading.Timer(NAVAlarm);
                navTimer.Change(0, 5000);

                // BMS 알람
                bmsTimer = new System.Threading.Timer(BMSAlarm);
                bmsTimer.Change(0, 3000);

            }
        }

        public void TimerStop()
        {
            canTimer.Dispose();
            obstacleStopTimer.Dispose();
            cameraPalletTimer.Dispose();
            navigationTimer.Dispose();
            navTimer.Dispose();
        }


        ////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////
        /// Alarm

        // CAN ALARM
        public void CANAlarm(Object state)
        {
            if (Globals.CAN_220_FLAG == false)
            {
                CAN_220_CNT++;
            }
            else
            {
                CAN_220_CNT = 0;
            }
            if (CAN_220_CNT >= 5)
            {
                CAN_220_CNT = 0;
                Globals.currentWork.alarm = Alarms.ALARM_CAN_220_VEHICLE_CONNECTION_ERROR;
            }

            if (Globals.CAN_260_FLAG == false)
            {
                CAN_260_CNT++;
            }
            else
            {
                CAN_260_CNT = 0;
            }
            if (CAN_260_CNT >= 5)
            {
                CAN_260_CNT = 0;
                Globals.currentWork.alarm = Alarms.ALARM_CAN_260_NAVIGATION_CONNECTION_ERROR;
            }

            if (Globals.CAN_261_FLAG == false)
            {
                CAN_261_CNT++;
            }
            else
            {
                CAN_261_CNT = 0;
            }
            if (CAN_261_CNT >= 5)
            {
                CAN_261_CNT = 0;
                Globals.currentWork.alarm = Alarms.ALARM_CAN_261_NAVIGATION_CONNECTION_ERROR;
            }

            if (Globals.CAN_241_FLAG == false)
            {
                CAN_241_CNT++;
            }
            else
            {
                CAN_241_CNT = 0;
            }
            if (CAN_241_CNT >= 5)
            {
                CAN_241_CNT = 0;
                Globals.currentWork.alarm = Alarms.ALARM_CAN_241_FORKLIFT_CONNECTION_ERROR;
            }

            if (Globals.CAN_242_FLAG == false)
            {
                CAN_242_CNT++;
            }
            else
            {
                CAN_242_CNT = 0;
            }
            if (CAN_242_CNT >= 5)
            {
                CAN_242_CNT = 0;
                Globals.currentWork.alarm = Alarms.ALARM_CAN_242_FORKLIFT_CONNECTION_ERROR;
            }

            if (Globals.HMISystem.PalletStackingEnable)
            {
                if (Globals.CAN_290_FLAG == false)
                {
                    CAN_290_CNT++;
                }
                else
                {
                    CAN_290_CNT = 0;
                }
            }
            if (CAN_290_CNT >= 5)
            {
                CAN_290_CNT = 0;
                Globals.currentWork.alarm = Alarms.ALARM_CAN_290_RECOGNITION_CONNECTION_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1인 식 통 신 /C1  에러290 !]";
            }
        }

        // NAV 알람
        public void NAVAlarm(Object state)
        {
            if (Globals.NavigationSystem.NAVState != 0)
            {
                switch (Globals.NavigationSystem.NAVState)
                {

                    case 1:
                        Globals.currentWork.alarm = Alarms.ALARM_NAV_350_ERROR;
                        break;
                    case 2:
                        Globals.currentWork.alarm = Alarms.ALARM_NAV_GYRO_ERROR;
                        break;
                    case 3:
                        Globals.currentWork.alarm = Alarms.ALARM_NAV_NO_DATA_ERROR;
                        break;
                    case 4:
                        Globals.currentWork.alarm = Alarms.ALARM_NAV_CAN_ERROR;
                        break;
                    default:
                        break;

                }
            }
        }

        // 장애물 지속 알람
        public void ObstacleStopAlarm(Object state)
        {
            if (Globals.NavigationSystem.VStateBeamStopArea == true && Globals.currentWork.workType != Globals.TRANSFER_CHARGE && Globals.currentWork.workType != Globals.TRANSFER_READY)
            {
                ObstacleCnt++;
                if (ObstacleCnt >= 24)
                {
                    Globals.currentWork.alarm = Alarms.ALARM_NAVI_OBSTACLE_STOP_KEEP_ERROR;
                }
            }
            else
            {
                ObstacleCnt = 0;
            }
        }

        // 인식 시스템(Nano) 팔렛 알람
        public void CameraPalletAlarm(Object state)
        {
            if (Globals.RecognitionSystem.Alarm != 0)
            {
                switch (Globals.RecognitionSystem.Alarm)
                {
                    default:
                        Globals.currentWork.alarm = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                        break;
                }
            }
        }
        // BMS Battery 알람
        public void BMSAlarm(Object state)
        {
            if (Globals.BMSSystem.Fault != 0)
            {
                switch (Globals.BMSSystem.Fault)
                {
                    case 1:
                        Globals.currentWork.alarm = Alarms.ALARM_BMS_COMM_ERROR;
                        break;
                    case 2:
                        Globals.currentWork.alarm = Alarms.ALARM_BMS_MAIN_RELAY_ERROR;
                        break;
                    case 3:
                        Globals.currentWork.alarm = Alarms.ALARM_BMS_PRE_CHARGE_RELAY_ERROR;
                        break;
                    case 4:
                        Globals.currentWork.alarm = Alarms.ALARM_BMS_FUSE_OPEN_ERROR;
                        break;
                    case 5:
                        Globals.currentWork.alarm = Alarms.ALARM_BMS_SHUTDOWN_ERROR;
                        break;
                    case 6:
                        Globals.currentWork.alarm = Alarms.ALARM_BMS_MAIN_RELAY_STATUS_ERROR;
                        break;
                    case 7:
                        Globals.currentWork.alarm = Alarms.ALARM_BMS_PRE_CHARGE_RELAY_STATUS_ERROR;
                        break;
                    default:
                        break;
                }
            }
        }

        // Navigation Board 알람
        public void NavigationAlarm(Object state)
        {
            ushort alarm = Alarms.NO_ALARM;
            if (Globals.NavigationSystem.Alarm != 0)
            {
                switch (Globals.NavigationSystem.Alarm)
                {
                    case 1:
                        alarm = Alarms.ALARM_NAVI_BOARD_EMO_ERROR;
                        break;
                    case 2:
                        alarm = Alarms.ALARM_NAVI_BOARD_BUMPER_STOP_ERROR;
                        break;
                    case 3:
                        alarm = Alarms.ALARM_NAVI_BOARD_MOTION_ERROR;
                        break;
                    case 4:
                        alarm = Alarms.ALARM_NAVI_BOARD_MAP_ERROR;
                        break;
                    case 5:
                        alarm = Alarms.ALARM_NAVI_BOARD_VIRTUAL_LINK_ERROR;
                        break;
                    case 6:
                        alarm = Alarms.ALARM_NAVI_BOARD_PATH_OVER_ERROR;
                        break;
                    case 7:
                        alarm = Alarms.ALARM_NAVI_BOARD_HEADING_OVER_ERROR;
                        break;
                    case 8:
                        alarm = Alarms.ALARM_NAVI_BOARD_LOCAL_TARGET_ERROR;
                        break;
                    case 9:
                        alarm = Alarms.ALARM_NAVI_BOARD_NAV_ERROR;
                        break;
                    default:
                        break;

                }
                Globals.currentWork.alarm = alarm;
            }
        }

        // Forklift Board 알람
        public void ForkliftAlarm(Object state)
        {
            if (Globals.ForkLiftSystem.LiftSensorAlarm == true)
            {
                Globals.currentWork.alarm = Alarms.ALARM_FORK_LIFT_SENSOR_ERROR;
            }

            if (Globals.ForkLiftSystem.TiltSensorAlarm == true)
            {
                Globals.currentWork.alarm = Alarms.ALARM_FORK_TILT_SENSOR_ERROR;
            }

            if (Globals.ForkLiftSystem.ShiftLeftSensorAlarm == true)
            {
                Globals.currentWork.alarm = Alarms.ALARM_FORK_SHIFT_LEFT_SENSOR_ERROR;
            }

            if (Globals.ForkLiftSystem.ShiftRightSensorAlarm == true)
            {
                Globals.currentWork.alarm = Alarms.ALARM_FORK_SHIFT_RIGHT_SENSOR_ERROR;
            }

            if (Globals.ForkLiftSystem.LiftControlAlarm == true)
            {
                Globals.currentWork.alarm = Alarms.ALARM_FORK_LIFT_CONTROL_ERROR;
            }

            if (Globals.ForkLiftSystem.TiltControlAlarm == true)
            {
                Globals.currentWork.alarm = Alarms.ALARM_FORK_TILT_CONTROL_ERROR;
            }

            if (Globals.ForkLiftSystem.MoverControlAlarm == true)
            {
                Globals.currentWork.alarm = Alarms.ALARM_FORK_MOVER_CONTROL_ERROR;
            }

            if (Globals.ForkLiftSystem.ShiftControlAlarm == true)
            {
                Globals.currentWork.alarm = Alarms.ALARM_FORK_SHIFT_CONTROL_ERROR;
            }
        }

        // Vehicle Board 알람
        public void VehicleAlarm(Object state)
        {

            if (Globals.VehicleSystem.SystemEMO)
            {
                Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_BOARD_EMO_SWITCH_ERROR;
            }
            /*
            if (Globals.VehicleSystem.DSNTractionAlarm > 50 && Globals.VehicleSystem.DSNTractionAlarm < 68)
            {
                switch(Globals.VehicleSystem.DSNTractionAlarm)
                {
                    case 51:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_CAN_ERROR;
                        break;
                    case 52:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_PUMP_PDO_ERROR;
                        break;
                    case 53:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_THROTTLE_ON_STARTUP_PDO_ERROR;
                        break;
                    case 54:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_SRO_DIRECTION_BEFORE_SEAT_ERROR;
                        break;
                    case 55:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_THROTTLE_ERROR;
                        break;
                    case 56:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_STEER_POT_ERROR;
                        break;
                    case 57:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_LOW_BATTERY_VOLT_ERROR;
                        break;
                    case 58:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_CALIBRATION_DISPLAY_SELECTED_ERROR;
                        break;
                    case 62:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_DISPLAY_PDO_ERROR;
                        break;
                    case 63:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_PDO_ERROR;
                        break;
                    case 64:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_SEVERE_STEERING_ERROR;
                        break;
                    case 65:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_STEERING_ERROR;
                        break;
                    case 66:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_AGV_PDO_TIMEOUT_ERROR;
                        break;
                    case 67:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_PDO_TIMEOUT_BMS_ERROR;
                        break;
                    default:
                        Globals.currentWork.alarm = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                        break;
                }

            }

            if (Globals.VehicleSystem.DSNPumpAlarm > 50 && Globals.VehicleSystem.DSNPumpAlarm < 68)
            {
                switch (Globals.VehicleSystem.DSNTractionAlarm)
                {
                    case 51:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_PUMP_SIGN_ON_ERROR;
                        break;
                    case 52:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_JOYSTICK_COMM_ERROR;
                        break;
                    case 53:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_JOYSTICK_LIFT_ERROR;
                        break;
                    case 54:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_JOYSTICK_INPUT_BEFORE_KEW_SWITCH_ERROR;
                        break;
                    case 55:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_JOYSTICK_TILT_ERROR;
                        break;
                    case 56:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_JOYSTICK_AUX1_ERROR;
                        break;
                    case 57:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_JOYSTICK_AUX2_ERROR;
                        break;
                    case 58:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_FINGERTIP_ENABLE_CHANGE_ERROR;
                        break;
                    case 59:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_EXM_TILT_BACK_ERROR;
                        break;
                    case 61:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_EXM_LIFT_SOL_ERROR;
                        break;
                    case 62:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_EXM_LOW_SOL_ERROR;
                        break;
                    case 63:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_EXM_AUX1_FWD_SOL_ERROR;
                        break;
                    case 64:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_EXM_TILT_FWD_SOL_ERROR;
                        break;
                    case 65:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_EXM_AUX1_BACK_SOL_ERROR;
                        break;
                    case 66:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_EXM_ERROR;
                        break;
                    case 67:
                        Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_EXM_AUX1_BACK_SOL_ERROR;
                        break;
                    default:
                        logger.Info("VehicleSystem.DSNTractionAlarm : " + Globals.VehicleSystem.DSNTractionAlarm);
                        //Globals.currentWork.alarm = Alarms.ALARM_VEHICLE_EXM_PDO_ERROR;
                        break;
                }

            }
             */
        }

        public void AddAlarmInfo()
        {
            // 제품 감지 알람
            Globals.alarmTable.Add(Alarms.ALARM_PALLET_LOADED_ERROR, "ALARM_PALLET_LOADED_ERROR"); // 명령이 적재고 지게차에 팔렛이 적재되어있는 경우
            Globals.alarmTable.Add(Alarms.ALARM_PALLET_LOADED_NOT_ERROR, "ALARM_PALLET_LOADED_NOT_ERROR "); // 명령이 이재고 지게차에 팔렛이 없을 경우

            Globals.alarmTable.Add(Alarms.ALARM_PALLET_FRONT_EXIST_ERROR, "ALARM_PALLET_FRONT_EXIST_ERROR "); // 명령이 1단 이재일 경우 이재 위치에 1단 팔렛이 있을 경우
            Globals.alarmTable.Add(Alarms.ALARM_PALLET_FRONT_NOT_EXIST_ERROR, "ALARM_PALLET_FRONT_NOT_EXIST_ERROR "); // 명령이 2단 이재일 경우 팔렛이 1단 이재 위치에 없을 경우

            // MAP 관련
            Globals.alarmTable.Add(Alarms.ALARM_MOVE_1ST_PATH_PLANNING_ERROR, "ALARM_MOVE_1ST_PATH_PLANNING_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_MOVE_2ND_PATH_PLANNING_ERROR, "ALARM_MOVE_2ND_PATH_PLANNING_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_MOVE_PATH_SENDING_ERROR, "ALARM_MOVE_PATH_SENDING_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_MOVE_NO_PATH_DATA_ERROR, "ALARM_MOVE_NO_PATH_DATA_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_MOVE_FRONT_PALLET_ANGLE_OVER_ERROR, "ALARM_MOVE_FRONT_PALLET_ANGLE_OVER_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_MOVE_COMPLETE_ERROR, "ALARM_MOVE_COMPLETE_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_MAP_SET_CURRENT_NODE_ERROR, "ALARM_MAP_SET_CURRENT_NODE_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_MAP_SET_CURRENT_NODE_SEND_SERIAL_ERROR, "ALARM_MAP_SET_CURRENT_NODE_SEND_SERIAL_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_MAP_NOT_FOUND_NODE_INFO_ERROR, "ALARM_MAP_NOT_FOUND_NODE_INFO_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_MAP_TRANSFER_HEADING_ERROR, "ALARM_MAP_TRANSFER_HEADING_ERROR ");

            // 충전 알람
            Globals.alarmTable.Add(Alarms.ALARM_CHARGE_ERROR, "ALARM_CHARGE_ERROR ");

            // EMO switch
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_BOARD_EMO_SWITCH_ERROR, "ALARM_VEHICLE_BOARD_EMO_SWITCH_ERROR ");

            // 장애물 지속 감지 알람 2분 이상
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_OBSTACLE_STOP_KEEP_ERROR, "[ALARM_NAVI_OBSTACLE_STOP_KEEP_ERROR] Obastacle Detection Overtime 2minutes");

            // Vehicle 관련 알람
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_CAN_ERROR, "ALARM_VEHICLE_CAN_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_PUMP_PDO_ERROR, "ALARM_VEHICLE_PUMP_PDO_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_THROTTLE_ON_STARTUP_PDO_ERROR, "ALARM_VEHICLE_THROTTLE_ON_STARTUP_PDO_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_SRO_DIRECTION_BEFORE_SEAT_ERROR, "ALARM_VEHICLE_SRO_DIRECTION_BEFORE_SEAT_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_THROTTLE_ERROR, "ALARM_VEHICLE_THROTTLE_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_STEER_POT_ERROR, "ALARM_VEHICLE_STEER_POT_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_LOW_BATTERY_VOLT_ERROR, "ALARM_VEHICLE_LOW_BATTERY_VOLT_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_CALIBRATION_DISPLAY_SELECTED_ERROR, "ALARM_VEHICLE_CALIBRATION_DISPLAY_SELECTED_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_DISPLAY_PDO_ERROR, "ALARM_VEHICLE_DISPLAY_PDO_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_PDO_ERROR, "ALARM_VEHICLE_PDO_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_SEVERE_STEERING_ERROR, "ALARM_VEHICLE_SEVERE_STEERING_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_STEERING_ERROR, "ALARM_VEHICLE_STEERING_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_AGV_PDO_TIMEOUT_ERROR, "ALARM_VEHICLE_AGV_PDO_TIMEOUT_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_PDO_TIMEOUT_BMS_ERROR, "ALARM_VEHICLE_PDO_TIMEOUT_BMS_ERROR ");

            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_PUMP_SIGN_ON_ERROR, "ALARM_VEHICLE_PUMP_SIGN_ON_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_JOYSTICK_COMM_ERROR, "ALARM_VEHICLE_JOYSTICK_COMM_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_JOYSTICK_LIFT_ERROR, "ALARM_VEHICLE_JOYSTICK_LIFT_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_JOYSTICK_INPUT_BEFORE_KEW_SWITCH_ERROR, "ALARM_VEHICLE_JOYSTICK_INPUT_BEFORE_KEW_SWITCH_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_JOYSTICK_TILT_ERROR, "ALARM_VEHICLE_JOYSTICK_TILT_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_JOYSTICK_AUX1_ERROR, "ALARM_VEHICLE_JOYSTICK_AUX1_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_JOYSTICK_AUX2_ERROR, "ALARM_VEHICLE_JOYSTICK_AUX2_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_FINGERTIP_ENABLE_CHANGE_ERROR, "ALARM_VEHICLE_FINGERTIP_ENABLE_CHANGE_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_EXM_TILT_BACK_ERROR, "ALARM_VEHICLE_EXM_TILT_BACK_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_EXM_LIFT_SOL_ERROR, "ALARM_VEHICLE_EXM_LIFT_SOL_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_EXM_LOW_SOL_ERROR, "ALARM_VEHICLE_EXM_LOW_SOL_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_EXM_AUX1_FWD_SOL_ERROR, "ALARM_VEHICLE_EXM_AUX1_FWD_SOL_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_EXM_TILT_FWD_SOL_ERROR, "ALARM_VEHICLE_EXM_TILT_FWD_SOL_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_EXM_AUX1_BACK_SOL_ERROR, "ALARM_VEHICLE_EXM_AUX1_BACK_SOL_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_EXM_ERROR, "ALARM_VEHICLE_EXM_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_VEHICLE_EXM_PDO_ERROR, "ALARM_VEHICLE_EXM_PDO_ERROR ");

            // Navigation 관련 알람
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_BOARD_EMO_ERROR, "ALARM_NAVI_BOARD_EMO_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_BOARD_BUMPER_STOP_ERROR, "ALARM_NAVI_BOARD_BUMPER_STOP_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_BOARD_MOTION_ERROR, "ALARM_NAVI_BOARD_MOTION_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_BOARD_MAP_ERROR, "ALARM_NAVI_BOARD_MAP_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_BOARD_VIRTUAL_LINK_ERROR, "ALARM_NAVI_BOARD_VIRTUAL_LINK_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_BOARD_PATH_OVER_ERROR, "ALARM_NAVI_BOARD_PATH_OVER_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_BOARD_HEADING_OVER_ERROR, "ALARM_NAVI_BOARD_HEADING_OVER_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_BOARD_LOCAL_TARGET_ERROR, "ALARM_NAVI_BOARD_LOCAL_TARGET_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_NAVI_BOARD_NAV_ERROR, "ALARM_NAVI_BOARD_NAV_ERROR ");

            // Fork 관련
            Globals.alarmTable.Add(Alarms.ALARM_FORK_LIFT_RANGE_OVER_ERROR, "ALARM_FORK_LIFT_RANGE_OVER_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_FORK_TILT_RANGE_OVER_ERROR, "ALARM_FORK_TILT_RANGE_OVER_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_FORK_REACH_RANGE_OVER_ERROR, "ALARM_FORK_REACH_RANGE_OVER_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_FORK_SIDESHIFT_RANGE_OVER_ERROR, "ALARM_FORK_SIDESHIFT_RANGE_OVER_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_FORK_MOVER_RANGE_OVER_ERROR, "ALARM_FORK_MOVER_RANGE_OVER_ERROR ");

            Globals.alarmTable.Add(Alarms.ALARM_FORK_LIFT_SENSOR_ERROR, "ALARM_FORK_LIFT_SENSOR_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_FORK_TILT_SENSOR_ERROR, "ALARM_FORK_TILT_SENSOR_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_FORK_SHIFT_LEFT_SENSOR_ERROR, "ALARM_FORK_SHIFT_LEFT_SENSOR_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_FORK_SHIFT_RIGHT_SENSOR_ERROR, "ALARM_FORK_SHIFT_RIGHT_SENSOR_ERROR ");

            Globals.alarmTable.Add(Alarms.ALARM_FORK_LIFT_CONTROL_ERROR, "ALARM_FORK_LIFT_CONTROL_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_FORK_TILT_CONTROL_ERROR, "ALARM_FORK_TILT_CONTROL_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_FORK_MOVER_CONTROL_ERROR, "ALARM_FORK_MOVER_CONTROL_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_FORK_SHIFT_CONTROL_ERROR, "ALARM_FORK_SHIFT_CONTROL_ERROR ");

            // 인식 관련
            /*
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_ERROR, "ALARM_RECOGNITION_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_MATCHING_ERROR, "ALARM_RECOGNITION_MATCHING_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_RANGE_ERROR, "ALARM_RECOGNITION_RANGE_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_ANGLE_ERROR, "ALARM_RECOGNITION_ANGLE_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_DATA_Y_ERROR, "ALARM_RECOGNITION_DATA_Y_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_STACKING_DROP_ERROR, "ALARM_RECOGNITION_STACKING_DROP_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_CALLBACK_ERROR, "ALARM_RECOGNITION_CALLBACK_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_CAN_ERROR, "ALARM_RECOGNITION_CAN_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_CAMERA_LEFT_ERROR, "ALARM_RECOGNITION_CAMERA_LEFT_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_RECOGNITION_CAMERA_RIGHT_ERROR, "ALARM_RECOGNITION_CAMERA_RIGHT_ERROR ");
            */

            Globals.alarmTable.Add(Alarms.ALARM_MOVE_FRONT_PALLET_RETRY_ERROR, "ALARM_MOVE_FRONT_PALLET_RETRY_ERROR ");

            // CAN 통신 관련
            Globals.alarmTable.Add(Alarms.ALARM_CAN_CONNECTION_ERROR, "ALARM_CAN_CONNECTION_ERROR ");

            Globals.alarmTable.Add(Alarms.ALARM_CAN_220_VEHICLE_CONNECTION_ERROR, "ALARM_CAN_220_VEHICLE_CONNECTION_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_CAN_221_VEHICLE_CONNECTION_ERROR, "ALARM_CAN_221_VEHICLE_CONNECTION_ERROR ");

            Globals.alarmTable.Add(Alarms.ALARM_CAN_241_FORKLIFT_CONNECTION_ERROR, "ALARM_CAN_241_FORKLIFT_CONNECTION_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_CAN_242_FORKLIFT_CONNECTION_ERROR, "ALARM_CAN_242_FORKLIFT_CONNECTION_ERROR ");

            Globals.alarmTable.Add(Alarms.ALARM_CAN_260_NAVIGATION_CONNECTION_ERROR, "ALARM_CAN_260_NAVIGATION_CONNECTION_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_CAN_261_NAVIGATION_CONNECTION_ERROR, "ALARM_CAN_261_NAVIGATION_CONNECTION_ERROR ");

            Globals.alarmTable.Add(Alarms.ALARM_CAN_290_RECOGNITION_CONNECTION_ERROR, "ALARM_CAN_290_RECOGNITION_CONNECTION_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_CAN_291_RECOGNITION_CONNECTION_ERROR, "ALARM_CAN_291_RECOGNITION_CONNECTION_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_CAN_293_RECOGNITION_CONNECTION_ERROR, "ALARM_CAN_293_RECOGNITION_CONNECTION_ERROR ");

            // NAV
            Globals.alarmTable.Add(Alarms.ALARM_NAV_350_ERROR, "ALARM_NAV_245_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_NAV_GYRO_ERROR, "ALARM_NAV_GYRO_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_NAV_NO_DATA_ERROR, "ALARM_NAV_NO_DATA_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_NAV_CAN_ERROR, "ALARM_NAV_CAN_ERROR ");

            // BMS
            Globals.alarmTable.Add(Alarms.ALARM_BMS_BATTERY_LOW_ERROR, "ALARM_BMS_BATTERY_LOW_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_BMS_COMM_ERROR, "ALARM_BMS_COMM_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_BMS_MAIN_RELAY_ERROR, "ALARM_BMS_MAIN_RELAY_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_BMS_PRE_CHARGE_RELAY_ERROR, "ALARM_BMS_PRE_CHARGE_RELAY_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_BMS_FUSE_OPEN_ERROR, "ALARM_BMS_FUSE_OPEN_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_BMS_SHUTDOWN_ERROR, "ALARM_BMS_SHUTDOWN_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_BMS_MAIN_RELAY_STATUS_ERROR, "ALARM_BMS_MAIN_RELAY_STATUS_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_BMS_PRE_CHARGE_RELAY_STATUS_ERROR, "ALARM_BMS_PRE_CHARGE_RELAY_STATUS_ERROR ");

            // 알수 없는 오류 알람
            Globals.alarmTable.Add(Alarms.ALARM_SYSTEM_UNKNOWN_ERROR, "ALARM_SYSTEM_UNKNOWN_ERROR ");

            // Warning
            Globals.alarmTable.Add(Alarms.ALARM_WORK_WRONG_ORDER_ERROR, "ALARM_WORK_WRONG_ORDER_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_PROTOCOL_FORMAT_ERROR, "ALARM_PROTOCOL_FORMAT_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_PALLET_TYPE_INFO_ERROR, "ALARM_PALLET_TYPE_INFO_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_MODE_WRONG_ERROR, "ALARM_MODE_WRONG_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_TCP_CONNECTION_ERROR, "ALARM_TCP_CONNECTION_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_SERIALPORT_CONNECTION_ERROR, "ALARM_SERIALPORT_CONNECTION_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_MQTT_CONNECTION_ERROR, "ALARM_MQTT_CONNECTION_ERROR ");
            Globals.alarmTable.Add(Alarms.ALARM_WORK_ALREADY_ING_ERROR, "ALARM_WORK_ALREADY_ING_ERROR ");

        }
    }
}

