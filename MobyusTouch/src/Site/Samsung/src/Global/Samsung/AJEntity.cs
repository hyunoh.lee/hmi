﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobyusTouch.src.Site.Mobyus.src.Global.Entity.Samsung
{
    class AJEntity
    {
        public string AmrId;
        public String Cmd;
        public String Result;
        public int ErrorCode;

        public int LinearVelocity;
        public int AngularVelocity;
    }
}
