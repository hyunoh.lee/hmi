﻿
//using MapDLL;
using EditorLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobyusTouch.src.Global.Entity.Samsung
{
    public class AREntity
    {
        public string AmrId;
        public string Cmd;
        public string Mode;
        public List<TempAstarNodeData> PathList = new List<TempAstarNodeData>();
        public ushort node;
        public int Work;
        public int Level;
        public int PLTType;

        public string Result;
        public int ErrorCode;
    }

    public class TempAstarNodeData
    {
        public ushort linknumber;
        public ushort currentnode;
        public ushort parentnode;
        public byte linktype;
        public byte linkdirection;
        public byte linkoption;
        public byte layernumber;
        public byte safetych;
        public byte maxvelocity;
        public float vehicleang;
        public float linkang;
        public float F_;
        public float G_;
        public float H_;

    }
}
