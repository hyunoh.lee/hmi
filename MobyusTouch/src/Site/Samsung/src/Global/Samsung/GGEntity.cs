﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobyusTouch.src.Global.Entity.Samsung
{
    class GGEntity
    {
        public string AmrId;
        public String Cmd;

        public String GMapVersion;
        public ushort Resolution;

        public long OffsetX;
        public long OffsetY;

        public float GMapHeight;
        public float GMapWidth;
        public byte[] GMapData;

        public String Result;
        public int ErrorCode;
    }
}
