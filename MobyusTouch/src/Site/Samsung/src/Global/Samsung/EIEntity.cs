﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobyusTouch.src.Global.Entity.Samsung
{
    public class EIEntity
    {
        public EIEntity()
        {
            this.Cmd = "EI";
            this.X = Globals.NavigationSystem.NAV_X;
            this.Y = Globals.NavigationSystem.NAV_Y;
            this.H = Globals.NavigationSystem.NAV_PHI;
        }

        public String Result;
        public String Cmd;
        public long X;
        public long Y;
        public long H;
    }
}
