﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using Newtonsoft.Json;
using MobyusTouch.src.Global.Entity;
using MobyusTouch.src.Global;
using System.Threading;
//using MapDLL;
using EditorLibrary;
using MobyusTouch.src.Global.Entity.Hyundai;
using MobyusTouch.src.Main.Sub;
using MobyusTouch.src.Main;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using uPLibrary.Networking.M2Mqtt.Exceptions;
using System.Net.Sockets;
using System.Windows.Media.Media3D;
using MobyusTouch.src.Site.Mobyus.src.Global.Entity.Hyundai;

namespace MobyusTouch.src.Communication
{
    public class MqttClientSamsung
    {
        private static Logger logger = LogManager.GetLogger("M2MqttClient");

        public static uPLibrary.Networking.M2Mqtt.MqttClient client = new uPLibrary.Networking.M2Mqtt.MqttClient("");

        public static int PalletType = 0;
        public static bool Connect()
        {
            bool result = false;

            try
            {
                if (!IsConnected())
                {
                    client = new uPLibrary.Networking.M2Mqtt.MqttClient(Globals.fmsIpAddress);
                    client.Connect(Guid.NewGuid().ToString());
                    client.MqttMsgPublishReceived += SubscribeMessage;
                    client.Subscribe(new string[] { "ACS001>AMR001" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
                }

                if (IsConnected())
                {
                    logger.Info("MQTT Client Connected.");
                    result = true;
                    Publish_Result(new EIEntity()); // 연결되면 초기 위치 전송
                }
            }
            catch (MqttConnectionException ex)
            {

            }
            catch (Exception ex)
            {
                logger.Info(ex.ToString());
            }

            return result;
        }

        public static void Disconnect()
        {
            if (IsConnected())
            {
                client.Disconnect();
            }
        }

        public static bool IsConnected()
        {
            return client.IsConnected;
        }

        private static void SubscribeMessage(object sender, MqttMsgPublishEventArgs e)
        {
            try
            {
                string msg = Encoding.UTF8.GetString(e.Message);
                //logger.Info("[MODE]" + Globals.HMISystem.SystemMode + " [SubscribeMessage]" + msg);

                CmdEntity cmdEntity = JsonConvert.DeserializeObject<CmdEntity>(msg);
                switch (cmdEntity.Cmd)
                {
                    case "EI":
                        Subscribe_EI(msg);
                        break;
                    case "AD":
                        Subscribe_AD(msg);
                        break;
                    case "AR":
                        Subscribe_AR(msg);
                        break;
                    case "AC":
                        Subscribe_AC(msg);
                        break;
                    case "AW":
                        Subscribe_AW(msg);
                        break;
                    case "AP":
                        Subscribe_AP(msg);
                        break;
                    case "AG":
                        Subscribe_AG(msg);
                        break;
                    case "AJ":
                        Subscribe_AJ(msg);
                        break;
                    case "SL":
                        Subscribe_SL(msg);
                        break;
                    case "SP":
                        Subscribe_SP(msg);
                        break;
                    case "GQ":
                        Subscribe_GQ(msg);
                        break;
                    case "GB":
                        Subscribe_GB(msg);
                        break;
                    case "GD":
                        Subscribe_GD(msg);
                        break;
                    case "TG":
                        Subscribe_TG(msg);
                        break;
                    case "TS":
                        Subscribe_TS(msg);
                        break;
                    case "GG":
                        Subscribe_GG(msg);
                        break;
                    default:
                        cmdEntity.Result = Globals_Hyundai.RESULT_FAIL;
                        cmdEntity.ErrorCode = Alarms.ALARM_PROTOCOL_FORMAT_ERROR;
                        Publish_Result(cmdEntity);
                        break;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }

        public static void Publish_Result(Object obj)
        {
            String strValue = JsonConvert.SerializeObject(obj, Formatting.None);
            //logger.Info("[Publish]" + str Value);

            client.Publish("AMR001>ACS001", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
        }

        private static void Subscribe_EI(string msg)
        {
            logger.Info("[EI] FORKLIFT POSITION RESULT");
            try
            {
                CmdEntity cmdEntity = JsonConvert.DeserializeObject<CmdEntity>(msg);
                if (Globals_Hyundai.RESULT_SUCCESS.Equals(cmdEntity.Result))
                {
                    Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_RESUME;
                }
                else
                {
                    Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_STOP;
                    Thread.Sleep(1000);
                    Publish_Result(new EIEntity());
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }

        //lhw
        //private static void Subscribe_AR(string msg)
        //{
        //    logger.Info("[AR] WORK ORDER");

        //    AREntity arEntity = new AREntity();
        //    WorkEntity workCommand = new WorkEntity();
        //    if (Globals.HMISystem.SystemMode != SystemMode.AUTO)
        //    {
        //        arEntity.Result = Globals_Hyundai.RESULT_FAIL;
        //        arEntity.ErrorCode = Alarms.ALARM_MODE_WRONG_ERROR;
        //    }
        //    else if (Globals.TRANSFER_READY != Globals.currentWork.workType)
        //    {
        //        arEntity.Result = Globals_Hyundai.RESULT_FAIL;
        //        arEntity.ErrorCode = Alarms.ALARM_WORK_ALREADY_ING_ERROR;
        //    }
        //    else
        //    {
        //        try
        //        {
        //            arEntity = JsonConvert.DeserializeObject<AREntity>(msg);

        //            workCommand = new WorkEntity();
        //            workCommand.transferID = arEntity.AmrId;

        //            workCommand.workMode = arEntity.Mode;
        //            workCommand.workType = arEntity.Work;
        //            workCommand.workLevel = arEntity.Level;
        //            workCommand.workState = (byte)WorkState.READY;
        //            workCommand.workStateUnit = (byte)WorkStateUnit.READY;

        //            workCommand.curLink = Globals.currentWork.curLink;
        //            workCommand.curNode = Globals.currentWork.curNode;
        //            workCommand.toNode = arEntity.node;
        //            workCommand.nextNode = Globals.currentWork.nextNode;

        //            List<AstarNodeData> linkList = new List<AstarNodeData>();
        //            for (int i = 0; i < arEntity.PathList.Count; i++)
        //            {
        //                AstarNodeData nodeData = new AstarNodeData(
        //                arEntity.PathList[i].linknumber,
        //                arEntity.PathList[i].currentnode,
        //                arEntity.PathList[i].parentnode,
        //                arEntity.PathList[i].linktype,
        //                arEntity.PathList[i].linkdirection,
        //                arEntity.PathList[i].linkoption,
        //                arEntity.PathList[i].layernumber,
        //                arEntity.PathList[i].safetych,
        //                arEntity.PathList[i].maxvelocity,
        //                arEntity.PathList[i].vehicleang,
        //                arEntity.PathList[i].linkang,
        //                arEntity.PathList[i].F_,
        //                arEntity.PathList[i].G_,
        //                arEntity.PathList[i].H_);

        //                linkList.Add(nodeData);
        //            }
        //            workCommand.realPath = linkList;
        //            workCommand.palletType = arEntity.PLTType;

        //            if (workCommand.EnqueueWork())
        //            {
        //                Globals.currentWork = workCommand;
        //                Thread.Sleep(500);
        //                if (Globals_Hyundai.PATH_PLANNING_AMR.Equals(arEntity.Mode))
        //                {
        //                    List<TempAstarNodeData> tempLinkList = new List<TempAstarNodeData>();
        //                    for (int i = 0; i < Globals.currentWork.realPath.Count; i++)
        //                    {
        //                        TempAstarNodeData nodeData = new TempAstarNodeData();

        //                        nodeData.linknumber = Globals.currentWork.realPath[i].GetLinkNumber();
        //                        nodeData.currentnode = Globals.currentWork.realPath[i].GetCurrentNodeID();
        //                        nodeData.parentnode = Globals.currentWork.realPath[i].GetParentNodeID();
        //                        nodeData.linktype = Globals.currentWork.realPath[i].GetLinkType();
        //                        nodeData.linkdirection = Globals.currentWork.realPath[i].GetLinkDirection();
        //                        nodeData.linkoption = Globals.currentWork.realPath[i].GetLinkOption();
        //                        nodeData.layernumber = 0x00;
        //                        nodeData.safetych = Globals.currentWork.realPath[i].GetSafetyChannel();
        //                        nodeData.maxvelocity = Globals.currentWork.realPath[i].GetMaxVelocity();
        //                        nodeData.vehicleang = Globals.currentWork.realPath[i].GetVehicleAng();
        //                        nodeData.linkang = Globals.currentWork.realPath[i].GetLinkAng();
        //                        nodeData.F_ = 0;
        //                        nodeData.G_ = Globals.currentWork.realPath[i].GetScord_G();
        //                        nodeData.H_ = 0;

        //                        tempLinkList.Add(nodeData);
        //                    }
        //                    arEntity.PathList = tempLinkList;
        //                }
        //                arEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
        //            }
        //            else
        //            {
        //                arEntity.Result = Globals_Hyundai.RESULT_FAIL;
        //                arEntity.ErrorCode = workCommand.alarm;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            arEntity.Result = Globals_Hyundai.RESULT_FAIL;
        //            arEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
        //            logger.Error(ex.ToString());
        //        }
        //    }
        //    Publish_Result(arEntity);
        //}


        private static void Subscribe_AR(string msg)
        {
            logger.Info("[AR] WORK ORDER");

            AREntity arEntity = new AREntity();
            WorkEntity workCommand = new WorkEntity();
            if (Globals.HMISystem.SystemMode != SystemMode.AUTO)
            {
                arEntity.Result = Globals_Hyundai.RESULT_FAIL;
                arEntity.ErrorCode = Alarms.ALARM_MODE_WRONG_ERROR;
            }
            else if (Globals.TRANSFER_READY != Globals.currentWork.workType)
            {
                arEntity.Result = Globals_Hyundai.RESULT_FAIL;
                arEntity.ErrorCode = Alarms.ALARM_WORK_ALREADY_ING_ERROR;
            }
            else
            {
                try
                {
                    arEntity = JsonConvert.DeserializeObject<AREntity>(msg);

                    workCommand = new WorkEntity();
                    workCommand.transferID = arEntity.AmrId;

                    workCommand.workMode = arEntity.Mode;
                    workCommand.workType = arEntity.Work;
                    workCommand.workLevel = arEntity.Level;
                    workCommand.workState = (byte)WorkState.READY;
                    workCommand.workStateUnit = (byte)WorkStateUnit.READY;

                    workCommand.curLink = Globals.currentWork.curLink;
                    workCommand.curNode = Globals.currentWork.curNode;
                    workCommand.toNode = arEntity.node;
                    workCommand.nextNode = Globals.currentWork.nextNode;

                    List<AstarNodeData> linkList = new List<AstarNodeData>();
                    for (int i = 0; i < arEntity.PathList.Count; i++)
                    {
                        AstarNodeData nodeData = new AstarNodeData();

                        nodeData.SetLinkAng(arEntity.PathList[i].LinkNumber);
                        nodeData.SetCurrentNodeID(arEntity.PathList[i].CurrentNodeID);
                        nodeData.SetParentNodeID(arEntity.PathList[i].ParentNodeID);
                        nodeData.SetLinkType(arEntity.PathList[i].LinkType);
                        nodeData.SetLinkDirection(arEntity.PathList[i].LinkDirection);
                        nodeData.SetLinkOption(arEntity.PathList[i].LinkOption);
                        nodeData.SetLayerNumber(arEntity.PathList[i].LayerNumber);
                        nodeData.SetSafetyCH(arEntity.PathList[i].SafetyChannel);
                        nodeData.SetLinkMaxVelocity(arEntity.PathList[i].MaxVelocity);
                        nodeData.SetVehicleAng(arEntity.PathList[i].VehicleAngle);
                        nodeData.SetLinkAng(arEntity.PathList[i].LinkAngle);
                        nodeData.SetF_Score(arEntity.PathList[i].F_);
                        nodeData.SetG_Score(arEntity.PathList[i].G_);
                        nodeData.SetH_Score(arEntity.PathList[i].H_);


                        linkList.Add(nodeData);
                    }
                    workCommand.realPath = linkList;
                    workCommand.palletType = PalletType;

                    if (workCommand.EnqueueWork())
                    {
                        Globals.currentWork = workCommand;
                        Thread.Sleep(500);
                        if (Globals_Hyundai.PATH_PLANNING_AMR.Equals(arEntity.Mode))
                        {
                            List<TempAstarNodeData> tempLinkList = new List<TempAstarNodeData>();
                            for (int i = 0; i < Globals.currentWork.realPath.Count; i++)
                            {
                                TempAstarNodeData nodeData = new TempAstarNodeData();

                                nodeData.LinkNumber = Globals.currentWork.realPath[i].GetLinkNumber();
                                nodeData.CurrentNodeID = Globals.currentWork.realPath[i].GetCurrentNodeID();
                                nodeData.ParentNodeID = Globals.currentWork.realPath[i].GetParentNodeID();
                                nodeData.LinkType = Globals.currentWork.realPath[i].GetLinkType();
                                nodeData.LinkDirection = Globals.currentWork.realPath[i].GetLinkDirection();
                                nodeData.LinkOption = Globals.currentWork.realPath[i].GetLinkOption();
                                nodeData.LayerNumber = 0x00;
                                nodeData.SafetyChannel = Globals.currentWork.realPath[i].GetSafetyChannel();
                                nodeData.MaxVelocity = Globals.currentWork.realPath[i].GetMaxVelocity();
                                nodeData.VehicleAngle = (float)Globals.currentWork.realPath[i].GetVehicleAng();
                                nodeData.LinkAngle = (float)Globals.currentWork.realPath[i].GetLinkAng();
                                nodeData.F_ = 0;
                                nodeData.G_ = (float)Globals.currentWork.realPath[i].GetG_Score();
                                nodeData.H_ = 0;

                                tempLinkList.Add(nodeData);
                            }
                            arEntity.PathList = tempLinkList;
                        }
                        arEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
                    }
                    else
                    {
                        arEntity.Result = Globals_Hyundai.RESULT_FAIL;
                        arEntity.ErrorCode = workCommand.alarm;
                    }
                }
                catch (Exception ex)
                {
                    arEntity.Result = Globals_Hyundai.RESULT_FAIL;
                    arEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                    logger.Error(ex.ToString());
                }
            }
            Publish_Result(arEntity);
        }

        private static void Subscribe_AC(string msg)
        {
            logger.Info("[AC] WORK CANCEL");
            ACEntity acEntity = new ACEntity();
            try
            {
                acEntity = JsonConvert.DeserializeObject<ACEntity>(msg);

                if (Globals.HMISystem.SystemMode >= SystemMode.MANUAL)
                {
                    WorkStep workStep = Globals.workStep;

                    Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_STOP;
                    Thread.Sleep(200);
                    Globals.workStep.Clear();
                    Globals.currentWork.Clear();

                    if (Globals.WORK_FORK_LIFTING >= workStep.step && workStep.step >= Globals.WORK_FORK_FORKMOVER)
                    {
                        switch (workStep.step)
                        {
                            case Globals.WORK_FORK_LIFTING:
                                workStep.value = Globals.beforeLiftValue;
                                break;
                            case Globals.WORK_FORK_TILTING:
                                workStep.value = Globals.beforeTiltValue;
                                break;
                            case Globals.WORK_FORK_REACHING:
                                workStep.value = Globals.beforeReachValue;
                                break;
                            case Globals.WORK_FORK_SIDESHIFT:
                                workStep.value = Globals.beforeSideShiftValue - ((Globals.ForkLiftSystem.ShiftRightPosition - Globals.ForkLiftSystem.ShiftLeftPosition) / 2);
                                break;
                            case Globals.WORK_FORK_FORKMOVER:
                                workStep.value = Globals.beforeForkMoverValue;
                                break;
                            default:
                                break;
                        }
                        workStep.function.BeginInvoke(workStep, null, workStep);
                    }
                    acEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
                }
                else
                {
                    acEntity.Result = Globals_Hyundai.RESULT_FAIL;
                    acEntity.ErrorCode = Alarms.ALARM_PROTOCOL_FORMAT_ERROR;
                }
            }
            catch (Exception ex)
            {
                acEntity.Result = Globals_Hyundai.RESULT_FAIL;
                acEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }

            Publish_Result(acEntity);
        }

        private static void Subscribe_AW(string msg)
        {
            logger.Info("[AW] ACTION WORK");
            AWEntity awEntity = new AWEntity();
            try
            {
                awEntity = JsonConvert.DeserializeObject<AWEntity>(msg);

                if (Globals.HMISystem.SystemMode != SystemMode.SEMI_AUTO)
                {
                    awEntity.Result = Globals_Hyundai.RESULT_FAIL;
                    awEntity.ErrorCode = Alarms.ALARM_MODE_WRONG_ERROR;
                }
                else
                {
                    if (awEntity.ActionID > 0 && awEntity.ActionID < 20)
                    {
                        switch ((WorkStateUnit)awEntity.ActionID)
                        {
                            case WorkStateUnit.LIFTING:
                                Globals.workStep = new WorkStep("WORK_FORK_LIFTING", Globals.WORK_FORK_LIFTING, awEntity.ActionValue, 0, true, ForkControl.Lifting);
                                break;

                            case WorkStateUnit.TILTING:
                                Globals.workStep = new WorkStep("WORK_FORK_TILTING", Globals.WORK_FORK_TILTING, awEntity.ActionValue, 0, true, ForkControl.Tilting);
                                break;

                            case WorkStateUnit.SIDESHIFT:
                                Globals.workStep = new WorkStep("WORK_FORK_SIDESHIFT", Globals.WORK_FORK_SIDESHIFT, awEntity.ActionValue, 0, true, ForkControl.SideShift);
                                break;

                            case WorkStateUnit.FORKMOVER:
                                Globals.workStep = new WorkStep("WORK_FORK_FORKMOVER", Globals.WORK_FORK_FORKMOVER, awEntity.ActionValue, 0, true, ForkControl.ForkMover);
                                break;

                            case WorkStateUnit.PATH_1ST_NODE:
                                Globals.currentWork.workMode = Globals.PATH_PLANNING_AMR;
                                Globals.workStep = new WorkStep("WORK_MOVE_1ST_PATH_PLANNING", Globals.WORK_MOVE_1ST_PATH_PLANNING, awEntity.ActionValue, 0, true, DriveControl.Move1stPathPlanning);
                                break;

                            case WorkStateUnit.PATH_2ND_STATION:
                                Globals.currentWork.workMode = Globals.PATH_PLANNING_AMR;
                                Globals.workStep = new WorkStep("WORK_MOVE_2ND_PATH_PLANNING", Globals.WORK_MOVE_2ND_PATH_PLANNING, awEntity.ActionValue, 0, true, DriveControl.Move2ndPathPlanning);
                                break;

                            case WorkStateUnit.PATH_SENDING:
                                Globals.workStep = new WorkStep("WORK_MOVE_PATH_SENDING", Globals.WORK_MOVE_PATH_SENDING, 0, 0, true, DriveControl.MovePathSending);
                                break;


                            case WorkStateUnit.PALLET_FRONT:
                                if (awEntity.ActionValue == 1)
                                {
                                    Globals.workStep = new WorkStep("WORK_PALLET_FRONT_ENABLE", Globals.WORK_PALLET_FRONT_ENABLE, 0, 0, true, CameraControl.PalletFrontEnable);
                                }
                                else
                                {
                                    Globals.workStep = new WorkStep("WORK_PALLET_FRONT_DISABLE", Globals.WORK_PALLET_FRONT_DISABLE, 0, 0, true, CameraControl.PalletFrontDisable);
                                }
                                break;

                            case WorkStateUnit.PALLET_STACKING:
                                if (awEntity.ActionValue == 1)
                                {
                                    Globals.workStep = new WorkStep("WORK_PALLET_STACKING_ENABLE", Globals.WORK_PALLET_STACKING_ENABLE, 0, 0, true, CameraControl.PalletStackingEnable);
                                }
                                else
                                {
                                    Globals.workStep = new WorkStep("WORK_PALLET_STACKING_DISABLE", Globals.WORK_PALLET_STACKING_DISABLE, 0, 0, true, CameraControl.PalletStackingDisable);
                                }
                                break;
                            default:
                                awEntity.Result = Globals_Hyundai.RESULT_FAIL;
                                awEntity.ErrorCode = Alarms.ALARM_PROTOCOL_FORMAT_ERROR;
                                break;
                        }

                        Globals.workStep.function.BeginInvoke(Globals.workStep, null, null);
                        awEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
                    }
                    else
                    {
                        awEntity.Result = Globals_Hyundai.RESULT_FAIL;
                        awEntity.ErrorCode = Alarms.ALARM_PROTOCOL_FORMAT_ERROR;
                    }
                }
            }
            catch (Exception ex)
            {
                awEntity.Result = Globals_Hyundai.RESULT_FAIL;
                awEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }

            Publish_Result(awEntity);
        }

        private static void Subscribe_AP(string msg)
        {
            logger.Info("[AP] TRAFFIC CONTROL ");
            APEntity apEntity = new APEntity();
            try
            {
                apEntity = JsonConvert.DeserializeObject<APEntity>(msg);

                if (SystemTraffic.TRANSFER_TRAFFIC_PAUSE == (SystemTraffic)apEntity.Traffic)
                {
                    Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_PAUSE;
                    apEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
                }
                else if (SystemTraffic.TRANSFER_TRAFFIC_RESUME == (SystemTraffic)apEntity.Traffic)
                {
                    Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_RESUME;
                    apEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
                }
                else if (SystemTraffic.TRANSFER_TRAFFIC_STOP == (SystemTraffic)apEntity.Traffic)
                {
                    Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_STOP;
                    apEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
                }
                else
                {
                    apEntity.Result = Globals_Hyundai.RESULT_FAIL;
                    apEntity.ErrorCode = Alarms.ALARM_PROTOCOL_FORMAT_ERROR;
                }
            }
            catch (Exception ex)
            {
                apEntity.Result = Globals_Hyundai.RESULT_FAIL;
                apEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }

            Publish_Result(apEntity);
        }

        private static void Subscribe_AG(string msg)
        {
            logger.Info("[AG] CHARGE CONTROL " + msg);
            AGEntity agEntity = new AGEntity();
            try
            {
                agEntity = JsonConvert.DeserializeObject<AGEntity>(msg);

                if (Globals_Hyundai.CHARGE_ENABLE.Equals(agEntity.Charge))
                {
                    Globals.workStep = new WorkStep("WORK_CHARGE_ENABLE", Globals.WORK_CHARGE_ENABLE, 0, 0, true, ChargeControl.ChargeEnable);
                    Globals.workStep.function.BeginInvoke(Globals.workStep, null, null);
                    Globals.currentWork.workState = (byte)WorkState.CHARGING; // CHARGE
                    agEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
                }
                else if (Globals_Hyundai.CHARGE_DISABLE.Equals(agEntity.Charge))
                {
                    Globals.workStep = new WorkStep("WORK_CHARGE_DISABLE", Globals.WORK_CHARGE_DISABLE, 0, 0, true, ChargeControl.ChargeDisable);
                    Globals.workStep.function.BeginInvoke(Globals.workStep, null, null);
                    Globals.currentWork.workState = (byte)WorkState.READY; // READY
                    agEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
                }
                else
                {
                    agEntity.Result = Globals_Hyundai.RESULT_FAIL;
                    agEntity.ErrorCode = Alarms.ALARM_PROTOCOL_FORMAT_ERROR;
                }
            }
            catch (Exception ex)
            {
                agEntity.Result = Globals_Hyundai.RESULT_FAIL;
                agEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }

            Publish_Result(agEntity);
        }

        private static void Subscribe_AJ(string msg)
        {
            logger.Info("[AJ] MANUAL DRIVING CONTROL ");

            AJEntity ajEntity = new AJEntity();
            try
            {
                ajEntity = JsonConvert.DeserializeObject<AJEntity>(msg);
                PalletType = ajEntity.LinearVelocity;
                Console.WriteLine("pallet Type : " + PalletType);
                ajEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
            }
            catch (Exception ex)
            {
                ajEntity.Result = Globals_Hyundai.RESULT_FAIL;
                ajEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }

        }

        private static void Subscribe_AD(string msg)
        {
            logger.Info("[AD] MODE CHANGE");

            ADEntity adEntity = new ADEntity();
            try
            {
                adEntity = JsonConvert.DeserializeObject<ADEntity>(msg);
                logger.Info("   [adEntity.Mode] " + adEntity.Mode);

                if (SystemMode.RESET <= (SystemMode)adEntity.Mode && SystemMode.AUTO >= (SystemMode)adEntity.Mode)
                {
                    Globals.HMISystem.SystemMode = (SystemMode)adEntity.Mode;
                    adEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
                }
                else
                {
                    adEntity.Result = Globals_Hyundai.RESULT_FAIL;
                    adEntity.ErrorCode = Alarms.ALARM_PROTOCOL_FORMAT_ERROR;
                }
            }
            catch (Exception ex)
            {
                adEntity.Result = Globals_Hyundai.RESULT_FAIL;
                adEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }

            Publish_Result(adEntity);
        }

        private static void Subscribe_SL(string msg)
        {
            logger.Info("[SL] MANUAL SAFETY SETTING");

            SLEntity slEntity = new SLEntity();
            try
            {
                slEntity = JsonConvert.DeserializeObject<SLEntity>(msg);

                Globals.SafetySystem.LeftInTIM = (byte)slEntity.Level;
                Globals.SafetySystem.RightInTIM = (byte)slEntity.Level;
                Globals.SafetySystem.RearInTIM = (byte)slEntity.Level;

                slEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
            }
            catch (Exception ex)
            {
                slEntity.Result = Globals_Hyundai.RESULT_FAIL;
                slEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }

            Publish_Result(slEntity);

        }

        private static void Subscribe_SP(string msg)
        {
            logger.Info("[SP] MANUAL NETWORK SETTING");
        }

        private static void Subscribe_GQ(string msg)
        {
            //logger.Info("[GQ] FORKLIFT STATUS");
            GQEntity gqEntity = new GQEntity();
            try
            {
                gqEntity = JsonConvert.DeserializeObject<GQEntity>(msg);

                gqEntity.X = Globals.NavigationSystem.NAV_X;
                gqEntity.Y = Globals.NavigationSystem.NAV_Y;
                gqEntity.T = Globals.NavigationSystem.NAV_PHI;

                gqEntity.State = Globals.currentWork.workState;
                gqEntity.ActionID = Globals.currentWork.workStateUnit;
                gqEntity.Traffic = (int)Globals.HMISystem.SystemTraffic;
                gqEntity.Mode = (int)Globals.HMISystem.SystemMode;
                gqEntity.Auto = Globals.VehicleSystem.SystemMode == true ? 1 : 0;

                gqEntity.Clamp = Globals.HMISystem.ClampEnable == true ? 1 : 0;
                gqEntity.Loaded = Globals.loaded == true ? 1 : 0;

                gqEntity.Level = Globals.currentWork.workLevel;
                gqEntity.CurrentNode = Globals.currentWork.curNode;
                gqEntity.NextNode = Globals.currentWork.nextNode;
                gqEntity.TargetNode = Globals.currentWork.toNode;

                //gqEntity.Obstacle =
                gqEntity.Lift = Globals.ForkLiftSystem.LiftPosition;
                gqEntity.Tilt = Globals.ForkLiftSystem.TiltPosition;
                gqEntity.SideShift = (Globals.ForkLiftSystem.ShiftRightPosition - Globals.ForkLiftSystem.ShiftLeftPosition) / 2;
                gqEntity.ForkMover = Globals.ForkLiftSystem.MoverPosition;

                gqEntity.Battery = Globals.BMSSystem.Battery;

                gqEntity.PalletFrontX = Globals.RecognitionSystem.FrontPalletX;
                gqEntity.PalletFrontY = Globals.RecognitionSystem.FrontPalletY;
                gqEntity.PalletFrontTheta = Globals.RecognitionSystem.FrontPalletTheta;

                gqEntity.PalletStackingX = Globals.RecognitionSystem.TargetX;
                gqEntity.PalletStackingY = Globals.RecognitionSystem.TargetY;
                gqEntity.PalletStackingTheta = Globals.RecognitionSystem.TargetTheta;

                gqEntity.MapRate = Globals.NavigationSystem.SlamScore;
                gqEntity.Speed = (int)(1000 * Globals.VehicleSystem.CurrentSpeed / 900);

                gqEntity.TagID = 0;
                gqEntity.ErrorCode = Globals.currentWork.alarm;

            }
            catch (Exception ex)
            {
                gqEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }

            Publish_Result(gqEntity);
        }

        private static void Subscribe_GB(string msg)
        {
            //logger.Info("[GB] BMS STATUS");

            GBEntity gbEntity = new GBEntity();
            try
            {
                gbEntity = JsonConvert.DeserializeObject<GBEntity>(msg);

                gbEntity.BCellVolt = Globals.BMSSystem.CellVolt;
                gbEntity.BPackVolt = Globals.BMSSystem.PackVolt;
                gbEntity.BPackCurrent = Globals.BMSSystem.PackCurrent;
                gbEntity.BChargeVolt = Globals.BMSSystem.ChargeVolt;
                gbEntity.BTemp = Globals.BMSSystem.Temperature;
                gbEntity.Battery = Globals.BMSSystem.Battery;
                gbEntity.BError = Globals.BMSSystem.Fault;
                gbEntity.BWarning = Globals.BMSSystem.Warning;
            }
            catch (Exception ex)
            {
                gbEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }
            Publish_Result(gbEntity);
        }

        private static void Subscribe_GD(string msg)
        {
            logger.Info("[GD] FORKLIFT INFORMATION");

            GDEntity gdEntity = new GDEntity();
            try
            {
                gdEntity = JsonConvert.DeserializeObject<GDEntity>(msg);

                gdEntity.GMapVersion = "";
                gdEntity.TMapVersion = "";
                gdEntity.FWVersion = "";
            }
            catch (Exception ex)
            {
                gdEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }
            Publish_Result(gdEntity);
        }
        //lhw
        //private static void Subscribe_TG(string msg)
        //{
        //    logger.Info("[TG] MAP TOPOLOGY INFORMATION");

        //    TGEntity tgEntity = new TGEntity();
        //    try
        //    {
        //        tgEntity = JsonConvert.DeserializeObject<TGEntity>(msg);

        //        tgEntity.TMapVersion = Globals.MapData.GetTopologyMapVirsion().ToString();

        //        for (int i = 0; i < Globals.nodeData.Count; i++)
        //        {
        //            TempNodeInfo tempNodeInfo = new TempNodeInfo();

        //            tempNodeInfo.nodenumber = Globals.nodeData[i].GetNodeNumber();
        //            tempNodeInfo.pointX = Globals.nodeData[i].GetNodePoint().X;
        //            tempNodeInfo.pointY = Globals.nodeData[i].GetNodePoint().Y;
        //            tempNodeInfo.nodetype = Globals.nodeData[i].GetNodeType();
        //            tempNodeInfo.layernumber = Globals.nodeData[i].GetLayerNumber();

        //            tgEntity.NodeList.Add(tempNodeInfo);
        //        }

        //        for (int i = 0; i < Globals.linkData.Count; i++)
        //        {
        //            TempLinkInfo tempLinkInfo = new TempLinkInfo();

        //            tempLinkInfo.linknumber = Globals.linkData[i].GetLinkNumber();
        //            tempLinkInfo.startnode = Globals.linkData[i].GetStartNode();
        //            tempLinkInfo.targetnode = Globals.linkData[i].GetTargetNode();
        //            tempLinkInfo.linktype = Globals.linkData[i].GetLinkType();
        //            tempLinkInfo.linkdirection = Globals.linkData[i].GetLinkDirection();
        //            tempLinkInfo.Foption = Globals.linkData[i].GetLinkFrontOption();
        //            tempLinkInfo.Boption = Globals.linkData[i].GetLinkBackOption();
        //            tempLinkInfo.Fvelocity = Globals.linkData[i].GetFrontMaxVelocity();
        //            tempLinkInfo.Bvelocity = Globals.linkData[i].GetBackMaxVelocity();
        //            tempLinkInfo.Fsafetych = Globals.linkData[i].GetFSafetyCH();
        //            tempLinkInfo.Bsafetych = Globals.linkData[i].GetBSafetyCH();
        //            tempLinkInfo.linkdistance = Globals.linkData[i].GetLinkDistance();
        //            tempLinkInfo.curveradius = Globals.linkData[i].GetCurveRadius();

        //            tempLinkInfo.prelinknumber = Globals.linkData[i].GetPreLinkNumber();
        //            tempLinkInfo.nextlinknumber = Globals.linkData[i].GetNextLinkNumber();
        //            tempLinkInfo.curvestartang = Globals.linkData[i].GetStartAngle();
        //            tempLinkInfo.curvesweepang = Globals.linkData[i].GetSweepAngle();
        //            tempLinkInfo.basepoint = Globals.linkData[i].GetBasePoint();

        //            tgEntity.LinkList.Add(tempLinkInfo);
        //        }

        //        ushort TotalLinkNum = 0;
        //        Globals.MapData.GetTotalLinkNumber(ref TotalLinkNum);
        //        tgEntity.TotalLinkNum = TotalLinkNum;

        //        ushort TotalNodeNum = 0;
        //        Globals.MapData.GetTotalNodeNumber(ref TotalNodeNum);
        //        tgEntity.TotalNodeNum = TotalNodeNum;
        //        tgEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
        //    }
        //    catch (Exception ex)
        //    {
        //        tgEntity.Result = Globals_Hyundai.RESULT_FAIL;
        //        tgEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
        //        logger.Error(ex.ToString());
        //    }
        //    //Publish_Result(tgEntity);

        //    String strValue = JsonConvert.SerializeObject(tgEntity, Formatting.None);
        //    logger.Info("[Publish]" + strValue);

        //    client.Publish("AMR001>ACS001", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
        //}

        private static void Subscribe_TG(string msg)
        {
            logger.Info("[TG] MAP TOPOLOGY INFORMATION");

            TGEntity tgEntity = new TGEntity();
            try
            {
                tgEntity = JsonConvert.DeserializeObject<TGEntity>(msg);

                tgEntity.TMapVersion = Globals.MapData.GetTopologyMapVirsion().ToString();

                for (int i = 0; i < Globals.nodeData.Count; i++)
                {
                    TempNodeInfo tempNodeInfo = new TempNodeInfo();

                    tempNodeInfo.nodenumber = Globals.nodeData[i].GetNodeNumber();
                    tempNodeInfo.pointX = Globals.nodeData[i].GetNodePoint().X;
                    tempNodeInfo.pointY = Globals.nodeData[i].GetNodePoint().Y;
                    tempNodeInfo.nodetype = Globals.nodeData[i].GetNodeType();
                    tempNodeInfo.layernumber = Globals.nodeData[i].GetLayerNumber();

                    tgEntity.NodeList.Add(tempNodeInfo);
                }

                for (int i = 0; i < Globals.linkData.Count; i++)
                {
                    TempLinkInfo tempLinkInfo = new TempLinkInfo();

                    tempLinkInfo.linknumber = Globals.linkData[i].GetLinkNumber();
                    tempLinkInfo.startnode = Globals.linkData[i].GetStartNodeNumber();
                    tempLinkInfo.targetnode = Globals.linkData[i].GetTargetNodeNumber();
                    tempLinkInfo.linktype = Globals.linkData[i].GetLinkType();
                    tempLinkInfo.linkdirection = Globals.linkData[i].GetLinkDirection();
                    tempLinkInfo.Foption = Globals.linkData[i].GetFoption();
                    tempLinkInfo.Boption = Globals.linkData[i].GetBoption();
                    tempLinkInfo.Fvelocity = Globals.linkData[i].GetFMaxVelocity();
                    tempLinkInfo.Bvelocity = Globals.linkData[i].GetBMaxVelocity();
                    tempLinkInfo.Fsafetych = Globals.linkData[i].GetFrontSafetyCH();
                    tempLinkInfo.Bsafetych = Globals.linkData[i].GetBackSafetyCH();
                    tempLinkInfo.linkdistance = (long)Globals.linkData[i].GetLinkDistance();
                    tempLinkInfo.curveradius = Globals.linkData[i].GetCurveRadius();

                    tempLinkInfo.prelinknumber = Globals.linkData[i].GetPreLinkNumber();
                    tempLinkInfo.nextlinknumber = Globals.linkData[i].GetNextLinkNumber();
                    tempLinkInfo.curvestartang = Globals.linkData[i].GetCurveStartAng();
                    tempLinkInfo.curvesweepang = Globals.linkData[i].GetCurveSweepAng();
                    tempLinkInfo.basepoint = Globals.linkData[i].GetBasePoint();

                    tgEntity.LinkList.Add(tempLinkInfo);
                }

                ushort TotalLinkNum = Globals.MapData.GetTotalLinkNum();
                tgEntity.TotalLinkNum = TotalLinkNum;

                ushort TotalNodeNum = Globals.MapData.GetTotalNodeNum();
                tgEntity.TotalNodeNum = TotalNodeNum;
                tgEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
            }
            catch (Exception ex)
            {
                tgEntity.Result = Globals_Hyundai.RESULT_FAIL;
                tgEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }
            //Publish_Result(tgEntity);

            String strValue = JsonConvert.SerializeObject(tgEntity, Formatting.None);
            logger.Info("[Publish]" + strValue);

            client.Publish("AMR001>ACS001", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
        }

        //lhw
        //private static void Subscribe_TS(string msg)
        //{
        //    logger.Info("[TS] MAP TOPOLOGY SET");
        //    TGEntity tgEntity = new TGEntity();
        //    try
        //    {
        //        tgEntity = JsonConvert.DeserializeObject<TGEntity>(msg);

        //        Savedata SaveFile = new Savedata();
        //        List<Point> gridList = new List<Point>();

        //        List<NodeInfo> nodeList = new List<NodeInfo>();
        //        for (int i = 0; i < tgEntity.NodeList.Count; i++)
        //        {
        //            NodeInfo nodeInfo = new NodeInfo(tgEntity.NodeList[i].nodenumber, tgEntity.NodeList[i].pointX, tgEntity.NodeList[i].pointY, tgEntity.NodeList[i].nodetype, tgEntity.NodeList[i].layernumber);

        //            nodeList.Add(nodeInfo);
        //        }

        //        List<LinkInfo> linkList = new List<LinkInfo>();
        //        for (int i = 0; i < tgEntity.LinkList.Count; i++)
        //        {
        //            LinkInfo linkInfo = new LinkInfo(tgEntity.LinkList[i].linknumber,
        //                tgEntity.LinkList[i].startnode,
        //                tgEntity.LinkList[i].targetnode,
        //                tgEntity.LinkList[i].linktype,
        //                tgEntity.LinkList[i].linkdirection,
        //                tgEntity.LinkList[i].Foption,
        //                tgEntity.LinkList[i].Boption,
        //                tgEntity.LinkList[i].Fvelocity,
        //                tgEntity.LinkList[i].Bvelocity,
        //                tgEntity.LinkList[i].Fsafetych,
        //                tgEntity.LinkList[i].Bsafetych,
        //                tgEntity.LinkList[i].linkdistance,
        //                tgEntity.LinkList[i].curveradius);

        //            linkInfo.SetCurveData(tgEntity.LinkList[i].prelinknumber, tgEntity.LinkList[i].nextlinknumber, tgEntity.LinkList[i].curvestartang, tgEntity.LinkList[i].curvesweepang, tgEntity.LinkList[i].basepoint, tgEntity.LinkList[i].curveradius);
        //            linkInfo.SetCurveRadius(tgEntity.LinkList[i].curveradius);
        //            linkInfo.SetLinkType(tgEntity.LinkList[i].linktype);
        //            linkInfo.SetNextLinkNumber(tgEntity.LinkList[i].linknumber);
        //            linkInfo.SetPreLinkNumber(tgEntity.LinkList[i].linknumber);
        //            linkInfo.SetStartNode(tgEntity.LinkList[i].startnode);
        //            linkInfo.SetTargetNode(tgEntity.LinkList[i].targetnode);

        //            linkList.Add(linkInfo);
        //        }

        //        SaveFile.SaveData(nodeList, linkList, new ZoomInfo(1280, 605));
        //        SaveFile.SetTotalLinkNumber(tgEntity.TotalLinkNum);
        //        SaveFile.SetTotalNodeNumber(tgEntity.TotalNodeNum);
        //        SaveFile.SetTopologyMapVirsion(tgEntity.TMapVersion);

        //        BinaryFormatter formatter = new BinaryFormatter();
        //        Stream stream = File.Open(Globals.MAP_SettingFilePath, FileMode.Create);
        //        formatter.Serialize(stream, SaveFile);

        //        Globals.MapData = SaveFile;
        //        Globals.nodeData = nodeList;
        //        Globals.linkData = linkList;

        //        //TouchMain.panelMap.Invalidate();
        //        //TouchMain.panelMap.Update();

        //        tgEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
        //    }
        //    catch (Exception ex)
        //    {
        //        tgEntity.Result = Globals_Hyundai.RESULT_FAIL;
        //        tgEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
        //        logger.Error(ex.ToString());
        //    }
        //    Publish_Result(tgEntity);
        //}


        private static void Subscribe_TS(string msg)
        {
            logger.Info("[TS] MAP TOPOLOGY SET");
            TGEntity tgEntity = new TGEntity();
            try
            {
                tgEntity = JsonConvert.DeserializeObject<TGEntity>(msg);

                SaveData SaveFile = new SaveData();
                List<Point> gridList = new List<Point>();

                List<NodeInfo> nodeList = new List<NodeInfo>();
                for (int i = 0; i < tgEntity.NodeList.Count; i++)
                {
                    NodeInfo nodeInfo = new NodeInfo();

                    nodeInfo.SetNodeNumber(tgEntity.NodeList[i].nodenumber);
                    nodeInfo.SetNodePoint(tgEntity.NodeList[i].pointX, tgEntity.NodeList[i].pointY);
                    nodeInfo.SetNodeType(tgEntity.NodeList[i].nodetype);
                    nodeInfo.SetLayerNumber(tgEntity.NodeList[i].layernumber);
                    nodeInfo.SetOffSet(0);
                    nodeInfo.SetConvDirection(0);
                    nodeInfo.SetQRID(0);
                    nodeList.Add(nodeInfo);
                }

                List<LinkInfo> linkList = new List<LinkInfo>();
                for (int i = 0; i < tgEntity.LinkList.Count; i++)
                {
                    LinkInfo linkInfo = new LinkInfo();

                    linkInfo.SetLinkNumber(tgEntity.LinkList[i].linknumber);
                    linkInfo.SetStartNodeNumber(tgEntity.LinkList[i].startnode);
                    linkInfo.SetTargetNodeNumber(tgEntity.LinkList[i].targetnode);
                    linkInfo.SetLinkType(tgEntity.LinkList[i].linktype);
                    linkInfo.SetLinkDirection(tgEntity.LinkList[i].linkdirection);
                    linkInfo.SetLinkOption(tgEntity.LinkList[i].Foption, tgEntity.LinkList[i].Boption);
                    linkInfo.SetLinkSpeed(tgEntity.LinkList[i].Fvelocity, tgEntity.LinkList[i].Bvelocity);
                    linkInfo.SetSafetyCH(tgEntity.LinkList[i].Fsafetych, tgEntity.LinkList[i].Bsafetych);
                    linkInfo.SetLinkDistance(tgEntity.LinkList[i].linkdistance);
                    linkInfo.SetCurveRadius(tgEntity.LinkList[i].curveradius);


                    linkInfo.SetCurveData(tgEntity.LinkList[i].prelinknumber, tgEntity.LinkList[i].nextlinknumber, tgEntity.LinkList[i].curvestartang, tgEntity.LinkList[i].curvesweepang, tgEntity.LinkList[i].basepoint, tgEntity.LinkList[i].curveradius);
                    linkInfo.SetNextLinkNumber(tgEntity.LinkList[i].linknumber);
                    linkInfo.SetPreLinkNumber(tgEntity.LinkList[i].linknumber);

                    linkList.Add(linkInfo);
                }

                SaveFile.SaveMapData(nodeList, linkList);//, new ZoomInfo(1280, 605));
                SaveFile.SetTotalLinkNum(tgEntity.TotalLinkNum);
                SaveFile.SetTotalNodeNum(tgEntity.TotalNodeNum);
                SaveFile.SetTopologyMapVirsion(tgEntity.TMapVersion);

                BinaryFormatter formatter = new BinaryFormatter();
                Stream stream = File.Open(Globals.MAP_SettingFilePath, FileMode.Create);
                formatter.Serialize(stream, SaveFile);

                Globals.MapData = SaveFile;
                Globals.nodeData = nodeList;
                Globals.linkData = linkList;

                //TouchMain.panelMap.Invalidate();
                //TouchMain.panelMap.Update();

                tgEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
            }
            catch (Exception ex)
            {
                tgEntity.Result = Globals_Hyundai.RESULT_FAIL;
                tgEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }
            Publish_Result(tgEntity);
        }

        private static void Subscribe_GG(string msg)
        {
            logger.Info("[GG] MAP GRID MAP INFORMATION");

            GGEntity ggEntity = new GGEntity();
            try
            {
                try
                {
                    Bitmap Grid = new Bitmap(@"C:\MOBYUS\GridMap.bmp");
                    if (Grid != null)
                    {
                        MemoryStream stream = new MemoryStream();
                        Grid.Save(stream, Grid.RawFormat);
                        byte[] gridData = stream.ToArray();

                        string[] lines = System.IO.File.ReadAllLines(@"C:\Mobyus\GridMap.txt");

                        ushort resolution = 0;
                        long offsetX = 0;
                        long offsetY = 0;

                        foreach (string line in lines)
                        {
                            string[] data = line.Split(':');

                            if (data[0].Equals("resolution"))
                            {
                                float resolutionTemp = 0;
                                resolutionTemp = float.Parse(data[1]);
                                resolution = (ushort)(resolutionTemp * 1000);
                            }
                            else if (data[0].Equals("origin"))
                            {
                                data[1] = data[1].Replace("[", "");
                                data[1] = data[1].Replace("]", "");

                                Point3D origin = Point3D.Parse(data[1]);
                                offsetX = (long)(origin.X * 1000);
                                offsetY = (long)(origin.Y * 1000);
                            }
                        }
                        GridMap gridMap = new GridMap(gridData, resolution, offsetX, offsetY, Grid.Width, Grid.Height);
                        Globals.MapData.SetGridMapData(gridMap);
                    }
                }
                catch (Exception ex)
                {
                    ggEntity.Result = Globals_Hyundai.RESULT_FAIL;
                    ggEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                    logger.Error(ex.ToString());
                }

                ggEntity = JsonConvert.DeserializeObject<GGEntity>(msg);

                ggEntity.GMapVersion = Globals.MapData.GetGridMap().GetGridMapVirsion();
                ggEntity.Resolution = Globals.MapData.GetGridMap().GetMapResolution();
                ggEntity.OffsetX = Globals.MapData.GetGridMap().GetOffSetX();
                ggEntity.OffsetY = Globals.MapData.GetGridMap().GetOffSetY();
                ggEntity.GMapHeight = Globals.MapData.GetGridMap().GetMapHeight();
                ggEntity.GMapWidth = Globals.MapData.GetGridMap().GetMapWidth();
                ggEntity.GMapData = Globals.MapData.GetGridMap().GetMap();

                ggEntity.Result = Globals_Hyundai.RESULT_SUCCESS;
            }
            catch (Exception ex)
            {
                ggEntity.Result = Globals_Hyundai.RESULT_FAIL;
                ggEntity.ErrorCode = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                logger.Error(ex.ToString());
            }
            Publish_Result(ggEntity);
        }
    }
}