﻿namespace MobyusTouch.src
{
    partial class MainSamsung
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.StatusAlarm = new System.Windows.Forms.Button();
            this.StatusCAN = new System.Windows.Forms.Button();
            this.StatusSerial = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.buttonMinimize = new System.Windows.Forms.Button();
            this.buttonSetting = new System.Windows.Forms.Button();
            this.StatusFMS = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.buttonMap = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panelMain = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.ObstacleBtn = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.TrafficBtn = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.ReachBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.ChargeBtn = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.WorkBtn = new System.Windows.Forms.Button();
            this.GearBtn = new System.Windows.Forms.Button();
            this.BatteryBtn = new System.Windows.Forms.Button();
            this.BrakeBtn = new System.Windows.Forms.Button();
            this.TiltBtn = new System.Windows.Forms.Button();
            this.LiftBtn = new System.Windows.Forms.Button();
            this.SpeedBtn = new System.Windows.Forms.Button();
            this.SteerBtn = new System.Windows.Forms.Button();
            this.lableTopName = new System.Windows.Forms.Label();
            this.labelCurrentDate = new System.Windows.Forms.Label();
            this.touchMaintop = new System.Windows.Forms.Panel();
            this.buttonWorkOrder = new System.Windows.Forms.Button();
            this.buttonUnitOrder = new System.Windows.Forms.Button();
            this.buttonModeSelect = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.NextNodeBtn = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.TargetNodeBtn = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.CurrentNodeBtn = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.NavPHIBtn = new System.Windows.Forms.Button();
            this.NavYBtn = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.NavXBtn = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.PalletThetaBtn = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.PalletYBtn = new System.Windows.Forms.Button();
            this.PalletXBtn = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.StackingThetaBtn = new System.Windows.Forms.Button();
            this.StackingYBtn = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.StackingXBtn = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.touchMaintop.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.StatusAlarm);
            this.panel1.Controls.Add(this.StatusCAN);
            this.panel1.Controls.Add(this.StatusSerial);
            this.panel1.Controls.Add(this.buttonExit);
            this.panel1.Controls.Add(this.buttonMinimize);
            this.panel1.Controls.Add(this.buttonSetting);
            this.panel1.Controls.Add(this.StatusFMS);
            this.panel1.Controls.Add(this.button28);
            this.panel1.Controls.Add(this.buttonMap);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(106, 600);
            this.panel1.TabIndex = 0;
            // 
            // StatusAlarm
            // 
            this.StatusAlarm.BackColor = System.Drawing.Color.DarkRed;
            this.StatusAlarm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StatusAlarm.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusAlarm.ForeColor = System.Drawing.Color.White;
            this.StatusAlarm.Location = new System.Drawing.Point(3, 342);
            this.StatusAlarm.Name = "StatusAlarm";
            this.StatusAlarm.Size = new System.Drawing.Size(98, 39);
            this.StatusAlarm.TabIndex = 7;
            this.StatusAlarm.Text = "ALARM";
            this.StatusAlarm.UseVisualStyleBackColor = false;
            // 
            // StatusCAN
            // 
            this.StatusCAN.BackColor = System.Drawing.Color.DarkRed;
            this.StatusCAN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StatusCAN.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusCAN.ForeColor = System.Drawing.Color.White;
            this.StatusCAN.Location = new System.Drawing.Point(3, 456);
            this.StatusCAN.Name = "StatusCAN";
            this.StatusCAN.Size = new System.Drawing.Size(98, 39);
            this.StatusCAN.TabIndex = 6;
            this.StatusCAN.Text = "CAN";
            this.StatusCAN.UseVisualStyleBackColor = false;
            // 
            // StatusSerial
            // 
            this.StatusSerial.BackColor = System.Drawing.Color.DarkRed;
            this.StatusSerial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StatusSerial.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusSerial.ForeColor = System.Drawing.Color.White;
            this.StatusSerial.Location = new System.Drawing.Point(3, 418);
            this.StatusSerial.Name = "StatusSerial";
            this.StatusSerial.Size = new System.Drawing.Size(98, 39);
            this.StatusSerial.TabIndex = 5;
            this.StatusSerial.Text = "SERIAL";
            this.StatusSerial.UseVisualStyleBackColor = false;
            // 
            // buttonExit
            // 
            this.buttonExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExit.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExit.ForeColor = System.Drawing.Color.White;
            this.buttonExit.Location = new System.Drawing.Point(3, 551);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(98, 46);
            this.buttonExit.TabIndex = 4;
            this.buttonExit.Text = "종료";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // buttonMinimize
            // 
            this.buttonMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMinimize.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMinimize.ForeColor = System.Drawing.Color.White;
            this.buttonMinimize.Location = new System.Drawing.Point(3, 499);
            this.buttonMinimize.Name = "buttonMinimize";
            this.buttonMinimize.Size = new System.Drawing.Size(98, 48);
            this.buttonMinimize.TabIndex = 4;
            this.buttonMinimize.Text = "최소화";
            this.buttonMinimize.UseVisualStyleBackColor = true;
            this.buttonMinimize.Click += new System.EventHandler(this.buttonMinimize_Click);
            // 
            // buttonSetting
            // 
            this.buttonSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.buttonSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetting.Font = new System.Drawing.Font("Leelawadee UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSetting.ForeColor = System.Drawing.Color.Black;
            this.buttonSetting.Image = global::MobyusTouch.Properties.Resources.settings;
            this.buttonSetting.Location = new System.Drawing.Point(3, 141);
            this.buttonSetting.Name = "buttonSetting";
            this.buttonSetting.Size = new System.Drawing.Size(98, 85);
            this.buttonSetting.TabIndex = 1;
            this.buttonSetting.Text = "Setting";
            this.buttonSetting.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonSetting.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonSetting.UseVisualStyleBackColor = false;
            this.buttonSetting.Click += new System.EventHandler(this.buttonSetting_Click);
            // 
            // StatusFMS
            // 
            this.StatusFMS.BackColor = System.Drawing.Color.DarkRed;
            this.StatusFMS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StatusFMS.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusFMS.ForeColor = System.Drawing.Color.White;
            this.StatusFMS.Location = new System.Drawing.Point(3, 380);
            this.StatusFMS.Name = "StatusFMS";
            this.StatusFMS.Size = new System.Drawing.Size(98, 39);
            this.StatusFMS.TabIndex = 4;
            this.StatusFMS.Text = "FMS";
            this.StatusFMS.UseVisualStyleBackColor = false;
            // 
            // button28
            // 
            this.button28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button28.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.button28.ForeColor = System.Drawing.Color.White;
            this.button28.Location = new System.Drawing.Point(3, 315);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(98, 28);
            this.button28.TabIndex = 4;
            this.button28.Text = "STATUS";
            this.button28.UseVisualStyleBackColor = true;
            // 
            // buttonMap
            // 
            this.buttonMap.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.buttonMap.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMap.Font = new System.Drawing.Font("Leelawadee UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMap.ForeColor = System.Drawing.Color.Black;
            this.buttonMap.Image = global::MobyusTouch.Properties.Resources.map1;
            this.buttonMap.Location = new System.Drawing.Point(3, 52);
            this.buttonMap.Name = "buttonMap";
            this.buttonMap.Size = new System.Drawing.Size(98, 85);
            this.buttonMap.TabIndex = 1;
            this.buttonMap.Text = "Map";
            this.buttonMap.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonMap.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonMap.UseVisualStyleBackColor = false;
            this.buttonMap.Click += new System.EventHandler(this.buttonMap_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(104, 47);
            this.panel3.TabIndex = 0;
            // 
            // panelMain
            // 
            this.panelMain.BackColor = System.Drawing.Color.LightGray;
            this.panelMain.Location = new System.Drawing.Point(106, 48);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(918, 471);
            this.panelMain.TabIndex = 8;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.button5);
            this.panel2.Controls.Add(this.ObstacleBtn);
            this.panel2.Controls.Add(this.button4);
            this.panel2.Controls.Add(this.TrafficBtn);
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.ReachBtn);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.ChargeBtn);
            this.panel2.Controls.Add(this.button27);
            this.panel2.Controls.Add(this.button11);
            this.panel2.Controls.Add(this.button18);
            this.panel2.Controls.Add(this.button30);
            this.panel2.Controls.Add(this.button22);
            this.panel2.Controls.Add(this.button20);
            this.panel2.Controls.Add(this.button6);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.button9);
            this.panel2.Controls.Add(this.WorkBtn);
            this.panel2.Controls.Add(this.GearBtn);
            this.panel2.Controls.Add(this.BatteryBtn);
            this.panel2.Controls.Add(this.BrakeBtn);
            this.panel2.Controls.Add(this.TiltBtn);
            this.panel2.Controls.Add(this.LiftBtn);
            this.panel2.Controls.Add(this.SpeedBtn);
            this.panel2.Controls.Add(this.SteerBtn);
            this.panel2.Location = new System.Drawing.Point(106, 520);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(918, 80);
            this.panel2.TabIndex = 1;
            // 
            // button5
            // 
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Century Gothic", 6.5F, System.Drawing.FontStyle.Bold);
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(698, 4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(72, 28);
            this.button5.TabIndex = 11;
            this.button5.Text = "OBSTACLE";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // ObstacleBtn
            // 
            this.ObstacleBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ObstacleBtn.Font = new System.Drawing.Font("Century Gothic", 7.5F);
            this.ObstacleBtn.ForeColor = System.Drawing.Color.White;
            this.ObstacleBtn.Location = new System.Drawing.Point(698, 31);
            this.ObstacleBtn.Name = "ObstacleBtn";
            this.ObstacleBtn.Size = new System.Drawing.Size(72, 46);
            this.ObstacleBtn.TabIndex = 12;
            this.ObstacleBtn.Text = "OFF";
            this.ObstacleBtn.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Century Gothic", 7.5F, System.Drawing.FontStyle.Bold);
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(130, 4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(72, 28);
            this.button4.TabIndex = 9;
            this.button4.Text = "TRAFFIC";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // TrafficBtn
            // 
            this.TrafficBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TrafficBtn.Font = new System.Drawing.Font("Century Gothic", 7.5F);
            this.TrafficBtn.ForeColor = System.Drawing.Color.White;
            this.TrafficBtn.Location = new System.Drawing.Point(130, 31);
            this.TrafficBtn.Name = "TrafficBtn";
            this.TrafficBtn.Size = new System.Drawing.Size(72, 46);
            this.TrafficBtn.TabIndex = 10;
            this.TrafficBtn.Text = "-";
            this.TrafficBtn.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Century Gothic", 7.5F, System.Drawing.FontStyle.Bold);
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(556, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(72, 28);
            this.button3.TabIndex = 7;
            this.button3.Text = "REACH";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // ReachBtn
            // 
            this.ReachBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ReachBtn.Font = new System.Drawing.Font("Century Gothic", 7.5F);
            this.ReachBtn.ForeColor = System.Drawing.Color.White;
            this.ReachBtn.Location = new System.Drawing.Point(556, 31);
            this.ReachBtn.Name = "ReachBtn";
            this.ReachBtn.Size = new System.Drawing.Size(72, 46);
            this.ReachBtn.TabIndex = 8;
            this.ReachBtn.Text = "OFF";
            this.ReachBtn.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 7.5F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(769, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(72, 28);
            this.button1.TabIndex = 5;
            this.button1.Text = "CHARGE";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // ChargeBtn
            // 
            this.ChargeBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ChargeBtn.Font = new System.Drawing.Font("Century Gothic", 7.5F);
            this.ChargeBtn.ForeColor = System.Drawing.Color.White;
            this.ChargeBtn.Location = new System.Drawing.Point(769, 31);
            this.ChargeBtn.Name = "ChargeBtn";
            this.ChargeBtn.Size = new System.Drawing.Size(72, 46);
            this.ChargeBtn.TabIndex = 6;
            this.ChargeBtn.Text = "OFF";
            this.ChargeBtn.UseVisualStyleBackColor = true;
            // 
            // button27
            // 
            this.button27.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button27.Font = new System.Drawing.Font("Century Gothic", 7.5F, System.Drawing.FontStyle.Bold);
            this.button27.ForeColor = System.Drawing.Color.White;
            this.button27.Location = new System.Drawing.Point(59, 4);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(72, 28);
            this.button27.TabIndex = 4;
            this.button27.Text = "WORK";
            this.button27.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Century Gothic", 7.5F, System.Drawing.FontStyle.Bold);
            this.button11.ForeColor = System.Drawing.Color.White;
            this.button11.Location = new System.Drawing.Point(201, 4);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(72, 28);
            this.button11.TabIndex = 4;
            this.button11.Text = "GEAR";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // button18
            // 
            this.button18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button18.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.button18.ForeColor = System.Drawing.Color.White;
            this.button18.Location = new System.Drawing.Point(0, 4);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(60, 73);
            this.button18.TabIndex = 4;
            this.button18.Text = "AFL";
            this.button18.UseVisualStyleBackColor = true;
            // 
            // button30
            // 
            this.button30.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button30.Font = new System.Drawing.Font("Century Gothic", 7.5F, System.Drawing.FontStyle.Bold);
            this.button30.ForeColor = System.Drawing.Color.White;
            this.button30.Location = new System.Drawing.Point(840, 4);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(72, 28);
            this.button30.TabIndex = 4;
            this.button30.Text = "BATTERY";
            this.button30.UseVisualStyleBackColor = true;
            // 
            // button22
            // 
            this.button22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button22.Font = new System.Drawing.Font("Century Gothic", 7.5F, System.Drawing.FontStyle.Bold);
            this.button22.ForeColor = System.Drawing.Color.White;
            this.button22.Location = new System.Drawing.Point(627, 4);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(72, 28);
            this.button22.TabIndex = 4;
            this.button22.Text = "BRAKE";
            this.button22.UseVisualStyleBackColor = true;
            // 
            // button20
            // 
            this.button20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button20.Font = new System.Drawing.Font("Century Gothic", 7.5F, System.Drawing.FontStyle.Bold);
            this.button20.ForeColor = System.Drawing.Color.White;
            this.button20.Location = new System.Drawing.Point(485, 4);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(72, 28);
            this.button20.TabIndex = 4;
            this.button20.Text = "TILT";
            this.button20.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Century Gothic", 7.5F, System.Drawing.FontStyle.Bold);
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(414, 4);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(72, 28);
            this.button6.TabIndex = 4;
            this.button6.Text = "LIFT";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Century Gothic", 7.5F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(343, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(72, 28);
            this.button2.TabIndex = 4;
            this.button2.Text = "SPEED";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Century Gothic", 7.5F, System.Drawing.FontStyle.Bold);
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.Location = new System.Drawing.Point(272, 4);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(72, 28);
            this.button9.TabIndex = 4;
            this.button9.Text = "STEER";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // WorkBtn
            // 
            this.WorkBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.WorkBtn.Font = new System.Drawing.Font("Century Gothic", 7F);
            this.WorkBtn.ForeColor = System.Drawing.Color.White;
            this.WorkBtn.Location = new System.Drawing.Point(59, 31);
            this.WorkBtn.Name = "WorkBtn";
            this.WorkBtn.Size = new System.Drawing.Size(72, 46);
            this.WorkBtn.TabIndex = 4;
            this.WorkBtn.Text = "-";
            this.WorkBtn.UseVisualStyleBackColor = true;
            // 
            // GearBtn
            // 
            this.GearBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GearBtn.Font = new System.Drawing.Font("Century Gothic", 7.5F);
            this.GearBtn.ForeColor = System.Drawing.Color.White;
            this.GearBtn.Location = new System.Drawing.Point(201, 31);
            this.GearBtn.Name = "GearBtn";
            this.GearBtn.Size = new System.Drawing.Size(72, 46);
            this.GearBtn.TabIndex = 4;
            this.GearBtn.Text = "N";
            this.GearBtn.UseVisualStyleBackColor = true;
            // 
            // BatteryBtn
            // 
            this.BatteryBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BatteryBtn.Font = new System.Drawing.Font("Century Gothic", 7.5F);
            this.BatteryBtn.ForeColor = System.Drawing.Color.White;
            this.BatteryBtn.Location = new System.Drawing.Point(840, 31);
            this.BatteryBtn.Name = "BatteryBtn";
            this.BatteryBtn.Size = new System.Drawing.Size(72, 46);
            this.BatteryBtn.TabIndex = 4;
            this.BatteryBtn.Text = "0 %";
            this.BatteryBtn.UseVisualStyleBackColor = true;
            // 
            // BrakeBtn
            // 
            this.BrakeBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BrakeBtn.Font = new System.Drawing.Font("Century Gothic", 7.5F);
            this.BrakeBtn.ForeColor = System.Drawing.Color.White;
            this.BrakeBtn.Location = new System.Drawing.Point(627, 31);
            this.BrakeBtn.Name = "BrakeBtn";
            this.BrakeBtn.Size = new System.Drawing.Size(72, 46);
            this.BrakeBtn.TabIndex = 4;
            this.BrakeBtn.Text = "OFF";
            this.BrakeBtn.UseVisualStyleBackColor = true;
            // 
            // TiltBtn
            // 
            this.TiltBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TiltBtn.Font = new System.Drawing.Font("Century Gothic", 7.5F);
            this.TiltBtn.ForeColor = System.Drawing.Color.White;
            this.TiltBtn.Location = new System.Drawing.Point(485, 31);
            this.TiltBtn.Name = "TiltBtn";
            this.TiltBtn.Size = new System.Drawing.Size(72, 46);
            this.TiltBtn.TabIndex = 4;
            this.TiltBtn.Text = "0 deg";
            this.TiltBtn.UseVisualStyleBackColor = true;
            // 
            // LiftBtn
            // 
            this.LiftBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LiftBtn.Font = new System.Drawing.Font("Century Gothic", 7.5F);
            this.LiftBtn.ForeColor = System.Drawing.Color.White;
            this.LiftBtn.Location = new System.Drawing.Point(414, 31);
            this.LiftBtn.Name = "LiftBtn";
            this.LiftBtn.Size = new System.Drawing.Size(72, 46);
            this.LiftBtn.TabIndex = 4;
            this.LiftBtn.Text = "0 mm";
            this.LiftBtn.UseVisualStyleBackColor = true;
            // 
            // SpeedBtn
            // 
            this.SpeedBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SpeedBtn.Font = new System.Drawing.Font("Century Gothic", 7.5F);
            this.SpeedBtn.ForeColor = System.Drawing.Color.White;
            this.SpeedBtn.Location = new System.Drawing.Point(343, 31);
            this.SpeedBtn.Name = "SpeedBtn";
            this.SpeedBtn.Size = new System.Drawing.Size(72, 46);
            this.SpeedBtn.TabIndex = 4;
            this.SpeedBtn.Text = "0 mm/s";
            this.SpeedBtn.UseVisualStyleBackColor = true;
            // 
            // SteerBtn
            // 
            this.SteerBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SteerBtn.Font = new System.Drawing.Font("Century Gothic", 7.5F);
            this.SteerBtn.ForeColor = System.Drawing.Color.White;
            this.SteerBtn.Location = new System.Drawing.Point(272, 31);
            this.SteerBtn.Name = "SteerBtn";
            this.SteerBtn.Size = new System.Drawing.Size(72, 46);
            this.SteerBtn.TabIndex = 4;
            this.SteerBtn.Text = "0 deg";
            this.SteerBtn.UseVisualStyleBackColor = true;
            // 
            // lableTopName
            // 
            this.lableTopName.AutoSize = true;
            this.lableTopName.Font = new System.Drawing.Font("Century Gothic", 18F);
            this.lableTopName.ForeColor = System.Drawing.Color.White;
            this.lableTopName.Location = new System.Drawing.Point(16, 7);
            this.lableTopName.Name = "lableTopName";
            this.lableTopName.Size = new System.Drawing.Size(216, 30);
            this.lableTopName.TabIndex = 2;
            this.lableTopName.Text = "TRANSFER System";
            // 
            // labelCurrentDate
            // 
            this.labelCurrentDate.AutoSize = true;
            this.labelCurrentDate.Location = new System.Drawing.Point(819, 2);
            this.labelCurrentDate.Name = "labelCurrentDate";
            this.labelCurrentDate.Size = new System.Drawing.Size(96, 42);
            this.labelCurrentDate.TabIndex = 2;
            this.labelCurrentDate.Text = "2019-06-28 \n  10:40:20";
            // 
            // touchMaintop
            // 
            this.touchMaintop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.touchMaintop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.touchMaintop.Controls.Add(this.lableTopName);
            this.touchMaintop.Controls.Add(this.labelCurrentDate);
            this.touchMaintop.Controls.Add(this.buttonWorkOrder);
            this.touchMaintop.Controls.Add(this.buttonUnitOrder);
            this.touchMaintop.Controls.Add(this.buttonModeSelect);
            this.touchMaintop.ForeColor = System.Drawing.Color.White;
            this.touchMaintop.Location = new System.Drawing.Point(106, 0);
            this.touchMaintop.Name = "touchMaintop";
            this.touchMaintop.Size = new System.Drawing.Size(918, 47);
            this.touchMaintop.TabIndex = 6;
            // 
            // buttonWorkOrder
            // 
            this.buttonWorkOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonWorkOrder.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold);
            this.buttonWorkOrder.ForeColor = System.Drawing.Color.White;
            this.buttonWorkOrder.Location = new System.Drawing.Point(527, 5);
            this.buttonWorkOrder.Name = "buttonWorkOrder";
            this.buttonWorkOrder.Size = new System.Drawing.Size(125, 36);
            this.buttonWorkOrder.TabIndex = 4;
            this.buttonWorkOrder.Text = "작업 명령";
            this.buttonWorkOrder.UseVisualStyleBackColor = true;
            this.buttonWorkOrder.Click += new System.EventHandler(this.buttonWorkOrder_Click);
            // 
            // buttonUnitOrder
            // 
            this.buttonUnitOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUnitOrder.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold);
            this.buttonUnitOrder.ForeColor = System.Drawing.Color.White;
            this.buttonUnitOrder.Location = new System.Drawing.Point(667, 5);
            this.buttonUnitOrder.Name = "buttonUnitOrder";
            this.buttonUnitOrder.Size = new System.Drawing.Size(125, 36);
            this.buttonUnitOrder.TabIndex = 4;
            this.buttonUnitOrder.Text = "단위 명령";
            this.buttonUnitOrder.UseVisualStyleBackColor = true;
            this.buttonUnitOrder.Click += new System.EventHandler(this.buttonUnitOrder_Click);
            // 
            // buttonModeSelect
            // 
            this.buttonModeSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonModeSelect.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold);
            this.buttonModeSelect.ForeColor = System.Drawing.Color.White;
            this.buttonModeSelect.Location = new System.Drawing.Point(290, 5);
            this.buttonModeSelect.Name = "buttonModeSelect";
            this.buttonModeSelect.Size = new System.Drawing.Size(222, 36);
            this.buttonModeSelect.TabIndex = 4;
            this.buttonModeSelect.Text = "OFFLINE";
            this.buttonModeSelect.UseVisualStyleBackColor = true;
            this.buttonModeSelect.Click += new System.EventHandler(this.buttonModeSelect_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Location = new System.Drawing.Point(107, 446);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(60, 73);
            this.button7.TabIndex = 48;
            this.button7.Text = "MAP";
            this.button7.UseVisualStyleBackColor = false;
            // 
            // NextNodeBtn
            // 
            this.NextNodeBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.NextNodeBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NextNodeBtn.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.NextNodeBtn.ForeColor = System.Drawing.Color.White;
            this.NextNodeBtn.Location = new System.Drawing.Point(308, 473);
            this.NextNodeBtn.Name = "NextNodeBtn";
            this.NextNodeBtn.Size = new System.Drawing.Size(72, 46);
            this.NextNodeBtn.TabIndex = 46;
            this.NextNodeBtn.Text = "-";
            this.NextNodeBtn.UseVisualStyleBackColor = false;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button13.Cursor = System.Windows.Forms.Cursors.Default;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button13.ForeColor = System.Drawing.Color.White;
            this.button13.Location = new System.Drawing.Point(379, 446);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(72, 28);
            this.button13.TabIndex = 36;
            this.button13.Text = "NAV X";
            this.button13.UseVisualStyleBackColor = false;
            // 
            // TargetNodeBtn
            // 
            this.TargetNodeBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.TargetNodeBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TargetNodeBtn.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.TargetNodeBtn.ForeColor = System.Drawing.Color.White;
            this.TargetNodeBtn.Location = new System.Drawing.Point(237, 473);
            this.TargetNodeBtn.Name = "TargetNodeBtn";
            this.TargetNodeBtn.Size = new System.Drawing.Size(72, 46);
            this.TargetNodeBtn.TabIndex = 47;
            this.TargetNodeBtn.Text = "-";
            this.TargetNodeBtn.UseVisualStyleBackColor = false;
            // 
            // button40
            // 
            this.button40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button40.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button40.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button40.ForeColor = System.Drawing.Color.White;
            this.button40.Location = new System.Drawing.Point(450, 446);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(72, 28);
            this.button40.TabIndex = 34;
            this.button40.Text = "NAV Y";
            this.button40.UseVisualStyleBackColor = false;
            // 
            // button42
            // 
            this.button42.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button42.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button42.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button42.ForeColor = System.Drawing.Color.White;
            this.button42.Location = new System.Drawing.Point(521, 446);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(72, 28);
            this.button42.TabIndex = 32;
            this.button42.Text = "NAV θ";
            this.button42.UseVisualStyleBackColor = false;
            // 
            // CurrentNodeBtn
            // 
            this.CurrentNodeBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.CurrentNodeBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CurrentNodeBtn.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.CurrentNodeBtn.ForeColor = System.Drawing.Color.White;
            this.CurrentNodeBtn.Location = new System.Drawing.Point(166, 473);
            this.CurrentNodeBtn.Name = "CurrentNodeBtn";
            this.CurrentNodeBtn.Size = new System.Drawing.Size(72, 46);
            this.CurrentNodeBtn.TabIndex = 50;
            this.CurrentNodeBtn.Text = "-";
            this.CurrentNodeBtn.UseVisualStyleBackColor = false;
            // 
            // button25
            // 
            this.button25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button25.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button25.ForeColor = System.Drawing.Color.White;
            this.button25.Location = new System.Drawing.Point(308, 446);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(72, 28);
            this.button25.TabIndex = 43;
            this.button25.Text = "NEXT";
            this.button25.UseVisualStyleBackColor = false;
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button17.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button17.ForeColor = System.Drawing.Color.White;
            this.button17.Location = new System.Drawing.Point(237, 446);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(72, 28);
            this.button17.TabIndex = 44;
            this.button17.Text = "TARGET";
            this.button17.UseVisualStyleBackColor = false;
            // 
            // NavPHIBtn
            // 
            this.NavPHIBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.NavPHIBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NavPHIBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.NavPHIBtn.ForeColor = System.Drawing.Color.White;
            this.NavPHIBtn.Location = new System.Drawing.Point(521, 473);
            this.NavPHIBtn.Name = "NavPHIBtn";
            this.NavPHIBtn.Size = new System.Drawing.Size(72, 46);
            this.NavPHIBtn.TabIndex = 31;
            this.NavPHIBtn.Text = "-";
            this.NavPHIBtn.UseVisualStyleBackColor = false;
            // 
            // NavYBtn
            // 
            this.NavYBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.NavYBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NavYBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.NavYBtn.ForeColor = System.Drawing.Color.White;
            this.NavYBtn.Location = new System.Drawing.Point(450, 473);
            this.NavYBtn.Name = "NavYBtn";
            this.NavYBtn.Size = new System.Drawing.Size(72, 46);
            this.NavYBtn.TabIndex = 33;
            this.NavYBtn.Text = "-";
            this.NavYBtn.UseVisualStyleBackColor = false;
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button15.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button15.ForeColor = System.Drawing.Color.White;
            this.button15.Location = new System.Drawing.Point(166, 446);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(72, 28);
            this.button15.TabIndex = 45;
            this.button15.Text = "CURRENT";
            this.button15.UseVisualStyleBackColor = false;
            // 
            // NavXBtn
            // 
            this.NavXBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.NavXBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NavXBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.NavXBtn.ForeColor = System.Drawing.Color.White;
            this.NavXBtn.Location = new System.Drawing.Point(379, 473);
            this.NavXBtn.Name = "NavXBtn";
            this.NavXBtn.Size = new System.Drawing.Size(72, 46);
            this.NavXBtn.TabIndex = 35;
            this.NavXBtn.Text = "-";
            this.NavXBtn.UseVisualStyleBackColor = false;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold);
            this.button10.ForeColor = System.Drawing.Color.White;
            this.button10.Location = new System.Drawing.Point(947, 446);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(72, 28);
            this.button10.TabIndex = 57;
            this.button10.Text = "PALLET θ";
            this.button10.UseVisualStyleBackColor = false;
            // 
            // PalletThetaBtn
            // 
            this.PalletThetaBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.PalletThetaBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PalletThetaBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.PalletThetaBtn.ForeColor = System.Drawing.Color.White;
            this.PalletThetaBtn.Location = new System.Drawing.Point(947, 473);
            this.PalletThetaBtn.Name = "PalletThetaBtn";
            this.PalletThetaBtn.Size = new System.Drawing.Size(72, 46);
            this.PalletThetaBtn.TabIndex = 56;
            this.PalletThetaBtn.Text = "-";
            this.PalletThetaBtn.UseVisualStyleBackColor = false;
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold);
            this.button12.ForeColor = System.Drawing.Color.White;
            this.button12.Location = new System.Drawing.Point(876, 446);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(72, 28);
            this.button12.TabIndex = 53;
            this.button12.Text = "PALLET Y";
            this.button12.UseVisualStyleBackColor = false;
            // 
            // PalletYBtn
            // 
            this.PalletYBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.PalletYBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PalletYBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.PalletYBtn.ForeColor = System.Drawing.Color.White;
            this.PalletYBtn.Location = new System.Drawing.Point(876, 473);
            this.PalletYBtn.Name = "PalletYBtn";
            this.PalletYBtn.Size = new System.Drawing.Size(72, 46);
            this.PalletYBtn.TabIndex = 52;
            this.PalletYBtn.Text = "-";
            this.PalletYBtn.UseVisualStyleBackColor = false;
            // 
            // PalletXBtn
            // 
            this.PalletXBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.PalletXBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PalletXBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.PalletXBtn.ForeColor = System.Drawing.Color.White;
            this.PalletXBtn.Location = new System.Drawing.Point(805, 473);
            this.PalletXBtn.Name = "PalletXBtn";
            this.PalletXBtn.Size = new System.Drawing.Size(72, 46);
            this.PalletXBtn.TabIndex = 54;
            this.PalletXBtn.Text = "-";
            this.PalletXBtn.UseVisualStyleBackColor = false;
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold);
            this.button14.ForeColor = System.Drawing.Color.White;
            this.button14.Location = new System.Drawing.Point(805, 446);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(72, 28);
            this.button14.TabIndex = 55;
            this.button14.Text = "PALLET X";
            this.button14.UseVisualStyleBackColor = false;
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button16.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold);
            this.button16.ForeColor = System.Drawing.Color.White;
            this.button16.Location = new System.Drawing.Point(592, 446);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(72, 28);
            this.button16.TabIndex = 42;
            this.button16.Text = "STACKING X";
            this.button16.UseVisualStyleBackColor = false;
            // 
            // StackingThetaBtn
            // 
            this.StackingThetaBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.StackingThetaBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StackingThetaBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.StackingThetaBtn.ForeColor = System.Drawing.Color.White;
            this.StackingThetaBtn.Location = new System.Drawing.Point(734, 473);
            this.StackingThetaBtn.Name = "StackingThetaBtn";
            this.StackingThetaBtn.Size = new System.Drawing.Size(72, 46);
            this.StackingThetaBtn.TabIndex = 37;
            this.StackingThetaBtn.Text = "-";
            this.StackingThetaBtn.UseVisualStyleBackColor = false;
            // 
            // StackingYBtn
            // 
            this.StackingYBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.StackingYBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StackingYBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.StackingYBtn.ForeColor = System.Drawing.Color.White;
            this.StackingYBtn.Location = new System.Drawing.Point(663, 473);
            this.StackingYBtn.Name = "StackingYBtn";
            this.StackingYBtn.Size = new System.Drawing.Size(72, 46);
            this.StackingYBtn.TabIndex = 39;
            this.StackingYBtn.Text = "-";
            this.StackingYBtn.UseVisualStyleBackColor = false;
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button19.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold);
            this.button19.ForeColor = System.Drawing.Color.White;
            this.button19.Location = new System.Drawing.Point(663, 446);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(72, 28);
            this.button19.TabIndex = 40;
            this.button19.Text = "STACKING Y";
            this.button19.UseVisualStyleBackColor = false;
            // 
            // StackingXBtn
            // 
            this.StackingXBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.StackingXBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StackingXBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.StackingXBtn.ForeColor = System.Drawing.Color.White;
            this.StackingXBtn.Location = new System.Drawing.Point(592, 473);
            this.StackingXBtn.Name = "StackingXBtn";
            this.StackingXBtn.Size = new System.Drawing.Size(72, 46);
            this.StackingXBtn.TabIndex = 41;
            this.StackingXBtn.Text = "-";
            this.StackingXBtn.UseVisualStyleBackColor = false;
            // 
            // button21
            // 
            this.button21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.button21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button21.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold);
            this.button21.ForeColor = System.Drawing.Color.White;
            this.button21.Location = new System.Drawing.Point(734, 446);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(72, 28);
            this.button21.TabIndex = 38;
            this.button21.Text = "STACKING θ";
            this.button21.UseVisualStyleBackColor = false;
            // 
            // MainSamsung
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1024, 600);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.NextNodeBtn);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.TargetNodeBtn);
            this.Controls.Add(this.button40);
            this.Controls.Add(this.button42);
            this.Controls.Add(this.CurrentNodeBtn);
            this.Controls.Add(this.button25);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.NavPHIBtn);
            this.Controls.Add(this.NavYBtn);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.NavXBtn);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.PalletThetaBtn);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.PalletYBtn);
            this.Controls.Add(this.PalletXBtn);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.StackingThetaBtn);
            this.Controls.Add(this.StackingYBtn);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.StackingXBtn);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.touchMaintop);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainSamsung";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.touchMaintop.ResumeLayout(false);
            this.touchMaintop.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lableTopName;
        private System.Windows.Forms.Button buttonMap;
        private System.Windows.Forms.Label labelCurrentDate;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonMinimize;
        private System.Windows.Forms.Panel touchMaintop;
        private System.Windows.Forms.Button buttonSetting;
        private System.Windows.Forms.Button SteerBtn;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button GearBtn;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button WorkBtn;
        private System.Windows.Forms.Button StatusFMS;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.Button buttonModeSelect;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button LiftBtn;
        private System.Windows.Forms.Button SpeedBtn;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button TiltBtn;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button BrakeBtn;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button BatteryBtn;
        private System.Windows.Forms.Button buttonUnitOrder;
        private System.Windows.Forms.Button buttonWorkOrder;
        private System.Windows.Forms.Button StatusSerial;
        private System.Windows.Forms.Button StatusCAN;
        private System.Windows.Forms.Button StatusAlarm;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button ChargeBtn;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button ReachBtn;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button TrafficBtn;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button ObstacleBtn;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button NextNodeBtn;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button TargetNodeBtn;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button CurrentNodeBtn;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button NavPHIBtn;
        private System.Windows.Forms.Button NavYBtn;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button NavXBtn;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button PalletThetaBtn;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button PalletYBtn;
        private System.Windows.Forms.Button PalletXBtn;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button StackingThetaBtn;
        private System.Windows.Forms.Button StackingYBtn;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button StackingXBtn;
        private System.Windows.Forms.Button button21;
    }
}

