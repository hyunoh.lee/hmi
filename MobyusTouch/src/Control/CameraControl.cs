﻿using MobyusTouch.src.Global;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MobyusTouch.src.Main.Sub
{
    public static class CameraControl
    {
        private static Logger logger = LogManager.GetLogger("CameraControl");

        public static bool PalletStackingEnable(WorkStep workStep)
        {
            logger.Info(">>>>[START] PalletStackingEnable");

            bool result = false;
            Globals.HMISystem.PalletStackingEnable = true;

            if (Globals.HMISystem.PalletStackingEnable)
            {
                logger.Info("[PalletStackingEnable] " + Globals.HMISystem.PalletStackingEnable);
                result = true;
            }

            Thread.Sleep(500);
            return result;
        }

        public static bool PalletStackingDisable(WorkStep workStep)
        {
            logger.Info(">>>>[START] PalletStackingDisable");

            bool result = false;
            Globals.HMISystem.PalletStackingEnable = false;

            if (!Globals.HMISystem.PalletStackingEnable)
            {
                logger.Info("[PalletStackingDisable] " + Globals.HMISystem.PalletStackingEnable);
                result = true;
            }

            Thread.Sleep(200);
            return result;
        }

        public static bool PalletFrontEnable(WorkStep workStep)
        {
            logger.Info(">>>>[START] PalletFrontEnable");

            bool result = false;

            if (Globals.currentWork.curNode != Globals.currentWork.toNode)
            {
                Globals.HMISystem.PalletFrontEnable = true;

                if (Globals.HMISystem.PalletFrontEnable)
                {
                    logger.Info("[PalletFrontEnable] " + Globals.HMISystem.PalletFrontEnable);
                    result = true;
                }
            }
            else
            {
                logger.Info("[PalletFrontEnable] Current and Target Node Same.." + Globals.HMISystem.PalletFrontEnable);
                result = true;
            }
            
            Thread.Sleep(500);
            return result;
        }

        public static bool PalletFrontDisable(WorkStep workStep)
        {
            logger.Info(">>>>[START] PalletFrontDisable");

            bool result = false;
            Globals.HMISystem.PalletFrontEnable = false;

            if (!Globals.HMISystem.PalletFrontEnable)
            {
                logger.Info("[PalletFrontDisable] " + Globals.HMISystem.PalletFrontEnable);

                result = true;
            }

            Thread.Sleep(200);
            return result;
        }

        public static bool PalletHeightEnable(WorkStep workStep)
        {
            logger.Info(">>>>[START] PalletHeightEnable");

            bool result = false;

            if (Globals.currentWork.curNode != Globals.currentWork.toNode)
            {
                Globals.HMISystem.PalletHeightEnable = true;

                if (Globals.HMISystem.PalletHeightEnable)
                {
                    logger.Info("[PalletHeightEnable] " + Globals.HMISystem.PalletHeightEnable);
                    result = true;
                }
                Thread.Sleep(1500);
            }
            else
            {
                logger.Info("[PalletHeightEnable] Current and Target Node Same.." + Globals.HMISystem.PalletHeightEnable);
                result = false;
            }

            return result;
        }

        public static bool PalletHeightDisable(WorkStep workStep)
        {
            logger.Info(">>>>[START] PalletHeightDisable");
            Thread.Sleep(200);

            bool result = false;
            Globals.HMISystem.PalletHeightEnable = false;

            if (!Globals.HMISystem.PalletHeightEnable)
            {
                logger.Info("[PalletHeightDisable] " + Globals.HMISystem.PalletHeightEnable);
                result = true;
            }

            return result;
        }

        public static bool PalletStackingSuccessEnable(WorkStep workStep)
        {
            logger.Info(">>>>[START] PalletStackingSuccessEnable");

            bool result = false;

            if (Globals.currentWork.curNode == Globals.currentWork.toNode)
            {
                Globals.HMISystem.PalletStackingSuccessEnable = true;

                if (Globals.HMISystem.PalletStackingSuccessEnable)
                {
                    logger.Info("[PalletStackingSuccessEnable] " + Globals.HMISystem.PalletStackingSuccessEnable);
                    result = true;
                }
                Thread.Sleep(1000);
            }
            else
            {
                logger.Info("[PalletStackingSuccessEnable] Current and Target Node Not Same.." + Globals.HMISystem.PalletStackingSuccessEnable);
                result = false;
            }

            return result;
        }

        public static bool PalletStackingSuccessDisable(WorkStep workStep)
        {
            logger.Info(">>>>[START] PalletStackingSuccessDisable");

            bool result = false;
            Globals.HMISystem.PalletStackingSuccessEnable = false;

            if (!Globals.HMISystem.PalletStackingSuccessEnable)
            {
                logger.Info("[PalletStackingSuccessDisable] " + Globals.HMISystem.PalletStackingSuccessEnable);
                result = true;
            }

            Thread.Sleep(200);
            return result;
        }

        public static bool PalletFrontExistEnable(WorkStep workStep)
        {
            logger.Info(">>>>[START] PalletFrontExistEnable");

            bool result = false;

            Globals.HMISystem.PalletFrontExistEnable = true;

            if (Globals.HMISystem.PalletFrontExistEnable)
            {
                logger.Info("[PalletFrontExistEnable] " + Globals.HMISystem.PalletFrontExistEnable);
                result = true;
            }
            Thread.Sleep(500);

            return result;
        }

        public static bool PalletFrontExistDisable(WorkStep workStep)
        {
            logger.Info(">>>>[START] PalletFrontExistDisable");

            bool result = false;
            Globals.HMISystem.PalletFrontExistEnable = false;

            if (!Globals.HMISystem.PalletFrontExistEnable)
            {
                logger.Info("[PalletFrontExistDisable] " + Globals.HMISystem.PalletFrontExistEnable);
                result = true;
            }

            Thread.Sleep(200);
            return result;
        }
    }
}
