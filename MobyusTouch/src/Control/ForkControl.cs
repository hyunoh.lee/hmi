﻿using MobyusTouch.src.Control;
using MobyusTouch.src.Control.Sub;
using MobyusTouch.src.Global;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MobyusTouch.src.Main.Sub
{
    public static class ForkControl
    {
        private static Logger logger = LogManager.GetLogger("ForkControl");

        public static bool MoveFromCharger()
        {
            logger.Info(">>>>[START]" + " MoveFromCharger " + " [curNode]" + Globals.currentWork.curNode);

            bool bLiftResult = false;
            bool bTiltResult = false;
            bool result = false;

            Globals.beforeLiftValue = Globals.ForkLiftSystem.LiftPosition;

            CheckControl.IsMinLimitOverOnAlarm("Lifting", 270, Globals.FORK_LIFT_MIN_LIMIT, Alarms.ALARM_FORK_LIFT_RANGE_OVER_ERROR);
            CheckControl.IsMaxLimitOverOnAlarm("Lifting", 270, Globals.FORK_LIFT_MAX_LIMIT, Alarms.ALARM_FORK_LIFT_RANGE_OVER_ERROR);

            while (Globals.currentWork.alarm == Alarms.NO_ALARM || Globals.currentWork.alarm >= Alarms.WARNING)
            {

                Globals.HMISystem.LiftingValue = (ushort)270;
                Globals.HMISystem.LiftEnable = true;


                if (TrafficControl.IsInterruptedTraffic())
                {
                    break;
                }

                if (Globals.ForkLiftSystem.LiftState == true
                    && (Globals.ForkLiftSystem.LiftPosition + 5 >= Globals.HMISystem.LiftingValue && Globals.ForkLiftSystem.LiftPosition - 5 <= Globals.HMISystem.LiftingValue))
                {
                    Globals.HMISystem.LiftEnable = false;
                    Globals.HMISystem.LiftingValue = 0;
                    bLiftResult = true;
                    break;
                }

                Thread.Sleep(50);
            }


            Globals.beforeTiltValue = Globals.ForkLiftSystem.TiltPosition;

            CheckControl.IsMinLimitOverOnAlarm("Tilting", 30, Globals.FORK_TILT_MIN_LIMIT, Alarms.ALARM_FORK_TILT_RANGE_OVER_ERROR);
            CheckControl.IsMaxLimitOverOnAlarm("Tilting", 30, Globals.FORK_TILT_MAX_LIMIT, Alarms.ALARM_FORK_TILT_RANGE_OVER_ERROR);

            while (Globals.currentWork.alarm == Alarms.NO_ALARM || Globals.currentWork.alarm >= Alarms.WARNING)
            {
                Globals.HMISystem.TiltingValue = (sbyte)30;
                Globals.HMISystem.TiltEnable = true;


                if (TrafficControl.IsInterruptedTraffic())
                {
                    break;
                }

                if (Globals.ForkLiftSystem.TiltState == true
                    && (Globals.ForkLiftSystem.TiltPosition + 3 >= Globals.HMISystem.TiltingValue && Globals.ForkLiftSystem.TiltPosition - 3 <= Globals.HMISystem.TiltingValue))
                {
                    Globals.HMISystem.TiltEnable = false;
                    Globals.HMISystem.TiltingValue = 0;
                    bTiltResult = true;
                    break;
                }

                Thread.Sleep(50);
            }

            if (bLiftResult && bTiltResult)
                result = true;

            return result;
        }

        public static bool Lifting(WorkStep workStep)
        {
            logger.Info(">>>>[START]" + workStep.name + " [ThreadID]" + Thread.CurrentThread.ManagedThreadId + " [Step]" + workStep.step + " [Current_Value]" + Globals.ForkLiftSystem.LiftPosition.ToString() + " [Target_Value]" + workStep.value + " [Sleep]" + workStep.sleep + " [curNode]" + Globals.currentWork.curNode);


            if (workStep.sleep > 0)
            {
                Thread.Sleep(workStep.sleep);
            }

            Thread.Sleep(500);
            bool result = false;
            Globals.beforeLiftValue = Globals.ForkLiftSystem.LiftPosition;

            CheckControl.IsMinLimitOverOnAlarm("Lifting", workStep.value, Globals.FORK_LIFT_MIN_LIMIT, Alarms.ALARM_FORK_LIFT_RANGE_OVER_ERROR);
            CheckControl.IsMaxLimitOverOnAlarm("Lifting", workStep.value, Globals.FORK_LIFT_MAX_LIMIT, Alarms.ALARM_FORK_LIFT_RANGE_OVER_ERROR);


            if (Globals.bRetry_SecondPathRetry)
            {
                if (Globals.currentWork.palletType != 0)
                {
                    Globals.bRetry_LiftingRetry = false;
                    //logger.Info("  [Globals.bLiftingRetry] " + Globals.bRetry_LiftingRetry);
                }
                else if (Globals.currentWork.palletType == 0 && Globals.nRetry_HeadTypeLiftingRetry > 0)
                {
                    Globals.bRetry_LiftingRetry = false;
                    //logger.Info("  [Globals.bLiftingRetry] " + Globals.bRetry_LiftingRetry + " [Globals.currentWork.palletType] " + Globals.currentWork.palletType + " [Globals.nHeadTypeLiftingRetry] " + Globals.nRetry_HeadTypeLiftingRetry);
                }


                Globals.nRetry_HeadTypeLiftingRetry++;



            }
            while (Globals.currentWork.alarm == Alarms.NO_ALARM || Globals.currentWork.alarm >= Alarms.WARNING)
            {
                Globals.HMISystem.LiftingValue = (ushort)workStep.value;
                Globals.HMISystem.LiftEnable = true;

                Globals.nWhileLiftWorkCount++;

                if (Globals.nWhileLiftWorkCount % (20 * 5) == 0)
                {
                    if (Globals.beforeLiftValue_ != Globals.ForkLiftSystem.LiftPosition || Globals.HMISystem.SystemTraffic == SystemTraffic.TRANSFER_TRAFFIC_PAUSE || Globals.bObstacle_Puase)
                        Globals.nWhileLiftWorkCount = 0;
                    Globals.beforeLiftValue_ = Globals.ForkLiftSystem.LiftPosition;
                }

                if (Globals.nWhileLiftWorkCount > 20 * 30)
                {
                    Globals.nWhileLiftWorkCount = 0;
                    logger.Info("  Working Lift [Enable] " + Globals.HMISystem.LiftEnable + " [LiftPosition] " + Globals.ForkLiftSystem.LiftPosition + " [State] " + Globals.ForkLiftSystem.LiftState);

                    if (Globals.beforeLiftValue_ == Globals.ForkLiftSystem.LiftPosition)
                    {
                        Globals.currentWork.alarm = Alarms.ALARM_FORK_LIFT_CONTROL_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1포크리프트동작에러!]";
                    }
                }

                #region 목적지가 53번 노드일때 리프트동작에러 뜨게끔 함 23.10.12 kdy
                //목적지가 53번 노드일때 리프트동작에러 뜨게끔 함 23.10.12 kdy
                if (Globals.currentWork.toNode == 53)
                {
                    ushort tmp = (ushort)workStep.value;
                    Globals.HMISystem.LiftingValue = (ushort)(workStep.value + 60);

                    if (Globals.beforeLiftValue <= Globals.HMISystem.LiftingValue) //리프트 상승중
                    {
                        if (Globals.ForkLiftSystem.LiftPosition > tmp + 50)
                        {
                            Globals.currentWork.alarm = Alarms.ALARM_FORK_LIFT_CONTROL_ERROR;
                            Globals.currentWork.alarm_name = "![000/E5151/C1리프트상승범위에러!]";
                        }
                    }
                }
                #endregion

                // kdy 23.08.22 리프트 목표값보다 50mm 더 내려가거나 올라갔을 경우 에러 발생
                if (Globals.beforeLiftValue >= Globals.HMISystem.LiftingValue) // 리프트 하강중
                {
                    if (Globals.ForkLiftSystem.LiftPosition < Globals.HMISystem.LiftingValue - 50)
                    {
                        Globals.currentWork.alarm = Alarms.ALARM_FORK_LIFT_CONTROL_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1리프트하강범위에러!]";
                    }
                }
                else if(Globals.beforeLiftValue <= Globals.HMISystem.LiftingValue) //리프트 상승중
                {
                    if(Globals.ForkLiftSystem.LiftPosition > Globals.HMISystem.LiftingValue + 50)
                    {
                        Globals.currentWork.alarm = Alarms.ALARM_FORK_LIFT_CONTROL_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1리프트상승범위에러!]";
                    }
                }

                if (Globals.currentWork.workQueue == null)
                {
                    Globals.HMISystem.LiftEnable = false;
                    Globals.HMISystem.LiftingValue = 0;
                    break;
                }

                if (TrafficControl.IsInterruptedTraffic())
                {
                    break;
                }

                if (Globals.ForkLiftSystem.LiftState == true
                    && (Globals.ForkLiftSystem.LiftPosition + 5 >= Globals.HMISystem.LiftingValue && Globals.ForkLiftSystem.LiftPosition - 5 <= Globals.HMISystem.LiftingValue))
                {
                    Globals.HMISystem.LiftEnable = false;
                    Globals.HMISystem.LiftingValue = 0;
                    result = true;
                    break;
                }

                Thread.Sleep(50);
            }

            return result;
        }

        public static bool Tilting(WorkStep workStep)
        {
            logger.Info(">>>>[START]" + workStep.name + " [ThreadID]" + Thread.CurrentThread.ManagedThreadId + " [Step]" + workStep.step + " [Current_Value]" + Globals.ForkLiftSystem.TiltPosition.ToString() + " [Target_Value]" + workStep.value + " [Sleep]" + workStep.sleep + " [curNode]" + Globals.currentWork.curNode);
            if (workStep.sleep > 0)
            {
                Thread.Sleep(workStep.sleep);
            }

            Thread.Sleep(500);
            bool result = false;
            Globals.beforeTiltValue = Globals.ForkLiftSystem.TiltPosition;

            CheckControl.IsMinLimitOverOnAlarm("Tilting", workStep.value, Globals.FORK_TILT_MIN_LIMIT, Alarms.ALARM_FORK_TILT_RANGE_OVER_ERROR);
            CheckControl.IsMaxLimitOverOnAlarm("Tilting", workStep.value, Globals.FORK_TILT_MAX_LIMIT, Alarms.ALARM_FORK_TILT_RANGE_OVER_ERROR);

            while (Globals.currentWork.alarm == Alarms.NO_ALARM || Globals.currentWork.alarm >= Alarms.WARNING)
            {
                Globals.HMISystem.TiltingValue = (sbyte)workStep.value;
                Globals.HMISystem.TiltEnable = true;

                Globals.nWhileTiltWorkCount++;

                if (Globals.nWhileTiltWorkCount % (20 * 5) == 0)
                {
                    if (Globals.beforeTiltValue_ != Globals.ForkLiftSystem.TiltPosition || Globals.HMISystem.SystemTraffic == SystemTraffic.TRANSFER_TRAFFIC_PAUSE || Globals.bObstacle_Puase)
                        Globals.nWhileTiltWorkCount = 0;
                    Globals.beforeTiltValue_ = Globals.ForkLiftSystem.TiltPosition;
                }

                if (Globals.nWhileTiltWorkCount > 20 * 30)
                {
                    Globals.nWhileTiltWorkCount = 0;
                    logger.Info("   Working Tilt [Enable] " + Globals.HMISystem.TiltEnable + " [TiltPosition] " + Globals.ForkLiftSystem.TiltPosition + " [State] " + Globals.ForkLiftSystem.TiltState);

                    if (Globals.beforeTiltValue_ == Globals.ForkLiftSystem.TiltPosition)
                    {
                        Globals.currentWork.alarm = Alarms.ALARM_FORK_TILT_CONTROL_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1포크틸트센서/C1  에    러 !]";
                    }
                }

                // kdy 23.08.22 틸트 목표값보다 10 더 내려가거나 올라갔을 경우 에러 발생
                if (Globals.beforeTiltValue >= Globals.HMISystem.TiltingValue) // 리프트 하강중
                {
                    if (Globals.ForkLiftSystem.TiltPosition < Globals.HMISystem.TiltingValue - 10)
                    {
                        Globals.currentWork.alarm = Alarms.ALARM_FORK_TILT_CONTROL_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1틸트하강범위에러!]";
                    }
                }
                else if (Globals.beforeTiltValue <= Globals.HMISystem.TiltingValue) //리프트 상승중
                {
                    if (Globals.ForkLiftSystem.TiltPosition > Globals.HMISystem.TiltingValue + 10)
                    {
                        Globals.currentWork.alarm = Alarms.ALARM_FORK_TILT_CONTROL_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1틸트상승범위에러!]";
                    }
                }

                if (Globals.currentWork.workQueue == null)
                {
                    Globals.HMISystem.TiltEnable = false;
                    Globals.HMISystem.TiltingValue = 0;
                    break;
                }

                if (TrafficControl.IsInterruptedTraffic())
                {
                    break;
                }

                if (Globals.ForkLiftSystem.TiltState == true
                    && (Globals.ForkLiftSystem.TiltPosition + 4 >= Globals.HMISystem.TiltingValue && Globals.ForkLiftSystem.TiltPosition - 4 <= Globals.HMISystem.TiltingValue))
                {
                    Globals.HMISystem.TiltEnable = false;
                    Globals.HMISystem.TiltingValue = 0;
                    result = true;
                    break;
                }

                Thread.Sleep(50);
            }
            return result;
        }

        public static bool Reaching(WorkStep workStep)
        {
            logger.Info(">>>>[START]" + workStep.name + " [ThreadID]" + Thread.CurrentThread.ManagedThreadId + " [Step]" + workStep.step + " [Current_Value]" + Globals.ForkLiftSystem.ReachPosition.ToString() + " [Target_Value]" + workStep.value + " [curNode]" + Globals.currentWork.curNode);

            Thread.Sleep(500);
            bool result = false;
            Globals.beforeReachValue = Globals.ForkLiftSystem.ReachPosition;

            CheckControl.IsMinLimitOverOnAlarm("Reaching", workStep.value, Globals.FORK_REACH_MIN_LIMIT, Alarms.ALARM_FORK_REACH_RANGE_OVER_ERROR);
            CheckControl.IsMaxLimitOverOnAlarm("Reaching", workStep.value, Globals.FORK_REACH_MAX_LIMIT, Alarms.ALARM_FORK_REACH_RANGE_OVER_ERROR);

            while (Globals.currentWork.alarm == Alarms.NO_ALARM || Globals.currentWork.alarm >= Alarms.WARNING)
            {
                Globals.HMISystem.ReachingValue = (ushort)workStep.value;
                Globals.HMISystem.ReachEnable = true;


                if (Globals.currentWork.workQueue == null)
                {
                    Globals.HMISystem.ReachEnable = false;
                    Globals.HMISystem.ReachingValue = 0;
                    break;
                }

                if (TrafficControl.IsInterruptedTraffic())
                {
                    break;
                }

                if (Globals.ForkLiftSystem.ReachState == true)
                {
                    Globals.HMISystem.ReachEnable = false;
                    Globals.HMISystem.ReachingValue = 0;
                    result = true;
                    break;
                }

                Thread.Sleep(50);
            }

            return result;
        }

        public static bool SideShift(WorkStep workStep)
        {
            logger.Info(">>>>[START]" + workStep.name + " [ThreadID]" + Thread.CurrentThread.ManagedThreadId + " [Step]" + workStep.step + " [Current_Value]" + ((Globals.ForkLiftSystem.ShiftRightPosition - Globals.ForkLiftSystem.ShiftLeftPosition) / 2).ToString() + " [Target_Value]" + workStep.value + " [curNode]" + Globals.currentWork.curNode);

            bool result = false;
            int forkShiftLeftLimit = 0;
            int forkShiftRightLimit = 0;

            Globals.beforeSideShiftValue = (Globals.ForkLiftSystem.ShiftRightPosition - Globals.ForkLiftSystem.ShiftLeftPosition) / 2;

            if (Globals.HMISystem.PalletFrontEnable == true)
            {
                Globals.HMISystem.SideShiftValue = Globals.RecognitionSystem.TargetY;

                logger.Info("  SideShifting [FRONT] " + Globals.HMISystem.SideShiftValue);

                if ((short)workStep.value != 0)
                {
                    Globals.HMISystem.SideShiftValue += (short)workStep.value;
                    logger.Info("  SideShifting [FRONT] [USER OFFSET] " + workStep.value + " [VALAUE] " + Globals.HMISystem.SideShiftValue);
                }
            }
            else if (Globals.HMISystem.PalletStackingEnable == true)
            {
                Globals.HMISystem.SideShiftValue = Globals.RecognitionSystem.TargetY;
                logger.Info("  SideShifting [STACKING] " + Globals.HMISystem.SideShiftValue);

                if ((short)workStep.value != 0)
                {
                    Globals.HMISystem.SideShiftValue += (short)workStep.value;
                    logger.Info("  SideShifting [STACKING] [USER OFFSET] " + workStep.value + " [VALAUE] " + Globals.HMISystem.SideShiftValue);
                }
            }
            else
            {
                if ((short)workStep.value == 0)
                {
                    Globals.HMISystem.SideShiftValue = (short)(Globals.beforeSideShiftValue * -1);
                    logger.Info("  SideShifting [CENTER] " + Globals.HMISystem.SideShiftValue);
                }
                else
                {
                    Globals.HMISystem.SideShiftValue = (short)workStep.value;
                    logger.Info("  SideShifting [USER] " + Globals.HMISystem.SideShiftValue);
                }
            }
            
            //KDY side shift limit 구문추가
            if (Globals.ForkLiftSystem.ShiftLeftPosition <= Globals.FORK_SHIFT_LIMIT - Globals.ForkLiftSystem.ShiftRightPosition)
            {
                forkShiftLeftLimit = Globals.ForkLiftSystem.ShiftLeftPosition;
                forkShiftRightLimit = Globals.ForkLiftSystem.ShiftRightPosition;
            }
            else
            {
                forkShiftLeftLimit = Globals.FORK_SHIFT_LIMIT - Globals.ForkLiftSystem.ShiftRightPosition;
                forkShiftRightLimit = Globals.FORK_SHIFT_LIMIT - Globals.ForkLiftSystem.ShiftLeftPosition;
            }
            ////
            CheckControl.IsLeftLimitOverOnAlarm("SideShifting Left", Globals.HMISystem.SideShiftValue, forkShiftLeftLimit, Alarms.ALARM_FORK_SIDESHIFT_RANGE_OVER_ERROR);
            CheckControl.IsRightLimitOverOnAlarm("SideShifting Right", Globals.HMISystem.SideShiftValue, forkShiftRightLimit, Alarms.ALARM_FORK_SIDESHIFT_RANGE_OVER_ERROR);

            while (Globals.currentWork.alarm == Alarms.NO_ALARM || Globals.currentWork.alarm >= Alarms.WARNING)
            {
                Globals.HMISystem.SideShiftEnable = true;

                //Globals.nWhileSideShiftWorkCount++;

                //if (Globals.nWhileSideShiftWorkCount % 20 == 0)
                //{
                //    if (Globals.beforeSideShiftValue_ != Globals.HMISystem.SideShiftValue)
                //        Globals.nWhileSideShiftWorkCount = 0;
                //    Globals.beforeSideShiftValue_ = Globals.HMISystem.SideShiftValue;
                //}

                //if (Globals.nWhileSideShiftWorkCount > 20 * 180)
                //{
                //    Globals.nWhileSideShiftWorkCount = 0;
                //    logger.Info("   Working SideShift [Enable] " + Globals.HMISystem.SideShiftEnable + " [Value] " + Globals.HMISystem.SideShiftValue + " [State] " + Globals.ForkLiftSystem.SideShiftState);

                //    if (Globals.beforeSideShiftValue_ == Globals.HMISystem.SideShiftValue)
                //    {
                //        Globals.currentWork.alarm = Alarms.ALARM_FORK_SHIFT_CONTROL_ERROR;
                //        Globals.currentWork.alarm_name = "시프트동작";
                //    }
                //}

                if (Globals.currentWork.workQueue == null)
                {
                    Globals.HMISystem.SideShiftEnable = false;
                    Globals.HMISystem.SideShiftValue = 0;
                    break;
                }

                if (TrafficControl.IsInterruptedTraffic())
                {
                    break;
                }

                if (Globals.ForkLiftSystem.SideShiftState == true)
                {
                    Globals.HMISystem.SideShiftEnable = false;
                    Globals.HMISystem.SideShiftValue = 0;
                    result = true;
                    break;
                }
                Thread.Sleep(50);
            }
            return result;
        }

        public static bool ForkMover(WorkStep workStep)
        {
            logger.Info(">>>>[START]" + workStep.name + " [ThreadID]" + Thread.CurrentThread.ManagedThreadId + " [Step]" + (ushort)workStep.step + " [Current_Value]" + Globals.ForkLiftSystem.MoverPosition.ToString() + " [Target_Value]" + (ushort)workStep.value + " [curNode]" + Globals.currentWork.curNode);
            bool result = false;
            Globals.beforeForkMoverValue = Globals.ForkLiftSystem.MoverPosition;


            CheckControl.IsMinLimitOverOnAlarm("ForkMover", workStep.value, Globals.FORK_MOVER_MIN_LIMIT, Alarms.ALARM_FORK_MOVER_RANGE_OVER_ERROR);
            CheckControl.IsMaxLimitOverOnAlarm("ForkMover", workStep.value, Globals.FORK_MOVER_MAX_LIMIT, Alarms.ALARM_FORK_MOVER_RANGE_OVER_ERROR);

            //시간 제한 타이머
            //DateTime StartTime = DateTime.Now;
            while (Globals.currentWork.alarm == Alarms.NO_ALARM || Globals.currentWork.alarm >= Alarms.WARNING)
            {
                Globals.HMISystem.MoverValue = (ushort)workStep.value;
                Globals.HMISystem.MoverEnable = true;


                //Globals.nWhileForkMoverWorkCount++;

                //if (Globals.nWhileForkMoverWorkCount % 20 == 0)
                //{
                //    if(Globals.beforeForkMoverValue_ != Globals.HMISystem.MoverValue)
                //        Globals.nWhileForkMoverWorkCount = 0;

                //    Globals.beforeForkMoverValue_ = Globals.HMISystem.MoverValue;
                //}

                //if (Globals.nWhileForkMoverWorkCount > 20 * 180)
                //{
                //    Globals.nWhileForkMoverWorkCount = 0;
                //    logger.Info("   Working ForkMover [Enable] " + Globals.HMISystem.MoverEnable + " [Value] " + Globals.HMISystem.MoverValue + " [State] " + Globals.ForkLiftSystem.MoverState);

                //    if (Globals.beforeForkMoverValue_ == Globals.HMISystem.MoverValue)
                //    {
                //        Globals.currentWork.alarm = Alarms.ALARM_FORK_MOVER_CONTROL_ERROR;
                //        Globals.currentWork.alarm_name = "무버동작";
                //    }
                //}

                if (Globals.currentWork.workQueue == null)
                {
                    Globals.HMISystem.MoverEnable = false;
                    Globals.HMISystem.MoverValue = 0;
                    break;
                }

                if (TrafficControl.IsInterruptedTraffic())
                {
                    break;
                }

                if (Globals.ForkLiftSystem.MoverState == true)
                {
                    Globals.HMISystem.MoverEnable = false;
                    Globals.HMISystem.MoverValue = 0;
                    result = true;
                    break;
                }
                /*
                DateTime EndTime = DateTime.Now;
                long elapsedTicks = EndTime.Ticks - StartTime.Ticks;
                TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);
                if (elapsedSpan.TotalSeconds >= 20)
                {
                    result = true;
                    Globals.HMISystem.MoverEnable = false;
                    Thread.Sleep(100);
                    Globals.HMISystem.MoverValue = 0;
                    break;
                }
                */
                Thread.Sleep(50);
            }
            return result;
        }
    }
}
