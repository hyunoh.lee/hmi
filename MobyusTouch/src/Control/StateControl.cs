﻿//using MapDLL;
using EditorLibrary;
using MobyusTouch.src.Communication;
using MobyusTouch.src.Global;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MobyusTouch.src.Main.Sub;

namespace MobyusTouch.src.Control.Sub
{
    public static class StateControl
    {
        private static Logger logger = LogManager.GetLogger("StateControl");
        private static bool drivingNode = false;
        private static String previousLED = "";

        public static void UpdateLEDState()
        {
            if ((Globals.NavigationSystem.VStateBeamStopArea || Globals.NavigationSystem.bWT100 || Globals.NavigationSystem.bUltraSonic || Globals.SafetySystem.VisioneryT) && Globals.VehicleSystem.CurrentSpeed <= 0)
            {
                Globals.bObstacle_Puase = true;
            }
            else
            {
                Globals.bObstacle_Puase = false;
            }


            //색상 C0:무색, C1:빨강, C2:녹색, C3:노랑, C4:파랑, C5:자주색, C6:하늘색, C7:흰색
            String currentLED = "";
            if (Globals.HMISystem.SystemMode == SystemMode.OFFLINE)
            {
                currentLED = "![000/F0005/E5151/C7수동모드!]";
            }
            else if (Globals.CrevisEMO.FrontLeft || Globals.CrevisEMO.FrontRight || Globals.CrevisEMO.RearLeft || Globals.CrevisEMO.RearRight)
            {
                String str = "";

                if (Globals.CrevisEMO.FrontLeft)
                {
                    str += "FL ";
                }
                else
                {
                    str += " ";
                }
                if (Globals.CrevisEMO.FrontRight)
                {
                    str += "FR ";
                }
                else
                {
                    str += " ";
                }
                if (Globals.CrevisEMO.RearLeft)
                {
                    str += "RL ";
                }
                else
                {
                    str += " ";
                }
                if (Globals.CrevisEMO.RearRight)
                {
                    str += "RR ";
                }
                else
                {
                    str += " ";
                }

                currentLED = "![000/E5151/C1이상상황발생/C1" + str + "!]";
            }


            else if (Globals.HMISystem.SystemMode == SystemMode.EMO && !Globals.bRetry_LiftingRetry) // 작업자 개입 필요한 상황
            {
                currentLED = "![000/E5151/C1이상상황발생/C1수동조치필요!]";
            }
            else if (Globals.HMISystem.SystemMode == SystemMode.EMO)
            {
                //logger.Info("@@@@@ alarm_name : " + Globals.currentWork.alarm_name);
                currentLED = Globals.currentWork.alarm_name;
            }
            else if (Globals.HMISystem.SystemMode == SystemMode.RESET)
            {
                currentLED = "![000/F0005/E5151/C1리   셋!]";
            }

            //14번 관제(TAMS) 통신 에러 GQ가 끊겼을 경우
            else if (Globals.HMISystem.HMItoACS_TrafficPause == SystemTraffic.TRANSFER_TRAFFIC_RCU_PAUSE)
            {
                Globals.bObstacle_Puase = true;
                currentLED = "![000/E5151/C1  일시정지  /C1서버통신정지!]";
            }
            //15번 유인지게차 일시정지 버튼 눌렸을 경우
            else if (Globals.HMISystem.HMItoACS_TrafficPause == SystemTraffic.TRANSFER_TRAFFIC_REMOTESWITCH_PAUSE)
            {
                Globals.bObstacle_Puase = true;
                currentLED = "![000/E5151/C1  일시정지  /C1 유인지게차 !]";
            }
            //13번 태블릿으로 인한 일시정지
            else if (Globals.HMISystem.ACStoHMI_TrafficPause == SystemTraffic.TRANSFER_TRAFFIC_USER_PAUSE)
            {
                Globals.bObstacle_Puase = true;
                currentLED = "![000/E5151/C1  일시정지  /C1 태블릿정지 !]";
            }
            //12번 교통제어로 인한 일시정지
            else if (Globals.HMISystem.ACStoHMI_TrafficPause == SystemTraffic.TRANSFER_TRAFFIC_AUTO_PAUSE)
            {
                Globals.bObstacle_Puase = true;
                currentLED = "![000/E5151/C1  일시정지  /C1교통제어정지!]";
            }
            //11번 설비통신으로 인한 일시정지
            else if (Globals.HMISystem.ACStoHMI_TrafficPause == SystemTraffic.TRANSFER_TRAFFIC_EQUIPMENTCOMM_PAUSE)
            {
                Globals.bObstacle_Puase = true;
                currentLED = "![000/E5151/C1  일시정지  /C1 설비통신중 !]";
            }
            else if (Globals.currentWork.workState != (byte)WorkState.READY && Globals.HMISystem.ACStoHMI_TrafficPause == SystemTraffic.TRANSFER_TRAFFIC_PAUSE /*&& Globals.bDisplayPauseCheck*/)
            {
                Globals.bObstacle_Puase = true;
                currentLED = "![000/F0005/E5151/C1일시정지!]";
            }
            else if (Globals.currentWork.workState != (byte)WorkState.READY && Globals.HMISystem.ACStoHMI_TrafficPause == SystemTraffic.TRANSFER_TRAFFIC_STOP /*&& Globals.bDisplayPauseCheck*/)
            {
                currentLED = "![000/F0005/E5151/C1일시정지!]";
            }
            else if (Globals.currentWork.workState != (byte)WorkState.READY && Globals.NavigationSystem.bStopButton /*&& Globals.bDisplayPauseCheck*/)
            {
                currentLED = "![000/F0005/E5151/C1일시정지!]";
            }
            else if (Globals.NavigationSystem.VStateBeamStopArea == true) //Globals.currentWork.workState != (byte)WorkState.READY && 
            {
                String Front = "";
                String Left = "";
                String Right = "";
                String Tail = "";

                if (Globals.SafetySystem.FrontNS3State) //  == Globals.WORK_SAFETY_FRONTNS3_STOP
                {
                    Front = "전";
                }
                else
                {
                    Front = " ";
                }
                if (Globals.SafetySystem.LeftNS3State) //  == Globals.WORK_SAFETY_LEFTNS3_STOP
                {
                    Left = "좌";
                }
                else
                {
                    Left = " ";
                }
                if (Globals.SafetySystem.RightNS3State) //  == Globals.WORK_SAFETY_RIGHTNS3_STOP
                {
                    Right = "우";
                }
                else
                {
                    Right = " ";
                }
                if (Globals.SafetySystem.TailNS3State) //  == Globals.WORK_SAFETY_TAILNS3_STOP
                {
                    Tail = "후";
                }
                else
                {
                    Tail = " ";
                }
                currentLED = "![000/E5151/C1 장애물정지 /C3 " + Front + " " + Left + " " + Right + " " + Tail + "!]";
                //else
                //{
                //    currentLED = "![000/E4949/C1 장애물정지 /C3 안전  센서 !]";
                //}
            }
            //else if (Globals.currentWork.workState == (byte)WorkState.MOVING && Globals.NavigationSystem.VStateBeamLowArea)
            //{
            //    String Front = "";
            //    String Left = "";
            //    String Right = "";
            //    String Tail = "";
            //    if(Globals.SafetySystem.FrontNS3Slow)
            //    {
            //        Front = "전";
            //    }
            //    else
            //    {
            //        Front = " ";
            //    }
            //    if (Globals.SafetySystem.LeftNS3State)
            //    {
            //        Front = "좌";
            //    }
            //    else
            //    {
            //        Front = " ";
            //    }
            //    if (Globals.SafetySystem.RightNS3State)
            //    {
            //        Front = "우";
            //    }
            //    else
            //    {
            //        Front = " ";
            //    }
            //    if (Globals.SafetySystem.TailNS3State)
            //    {
            //        Front = "후";
            //    }
            //    else
            //    {
            //        Front = " ";
            //    }
            //    currentLED = "![000/E5151/C3 장애물감속 /C3 " + Front + " " + Left + " " + Right + " " + Tail + "!]";
            //}
            else if (Globals.currentWork.workState != (byte)WorkState.READY && Globals.NavigationSystem.bWT100)// (Globals.SafetySystem.RightWT100 == true || Globals.SafetySystem.LeftWT100 == true))
            {
                currentLED = "![000/E5151/C1 장애물정지 /C3 포크  센서 !]";
            }

            else if (Globals.currentWork.workState != (byte)WorkState.READY && Globals.NavigationSystem.bUltraSonic == true)
            {
                currentLED = "![000/E5151/C1 장애물정지 /C3 초음파감지 !]";
            }
            else if (Globals.currentWork.workState != (byte)WorkState.READY && Globals.VehicleSystem.CurrentSpeed <= 0 && Globals.SafetySystem.VisioneryT == true) //Globals.RecognitionSystem.AreaState == (byte)ObstacleAreaState.STOP
            {
                currentLED = "![000/E5151/C1 장애물정지 /C3 공간  감지 !]";
            }
            else if (Globals.NavigationSystem.LineOption_Rear == true || Globals.NavigationSystem.LineOption_Front == true)
            {
                if (Globals.HMISystem.SystemMode == SystemMode.AUTO)
                {
                    currentLED = "![000/X0000/Y0206/E5151/C2라인인식중!]";
                }
                if (Globals.HMISystem.SystemMode == SystemMode.SEMI_AUTO)
                {
                    currentLED = "![000/X0000/Y0206/E5151/C3라인인식중!]";
                }
            }
            else if (Globals.VehicleSystem.NxUseable == true)
            {
                currentLED = "![000/X0000/Y0206/E5151/C3 위치확인중 !]";
            }
            else if (Globals.currentWork.workState == (byte)WorkState.READY)
            {
                if (Globals.HMISystem.SystemMode == SystemMode.AUTO)
                {
                    currentLED = "![000/F0005/E5151/C2대 기 중!]";
                }
                if (Globals.HMISystem.SystemMode == SystemMode.SEMI_AUTO)
                {
                    currentLED = "![000/F0005/E5151/C3대 기 중!]";
                }
            }
            else if (Globals.currentWork.workState == (byte)WorkState.LOAD_COMPLETE)
            {
                if (Globals.HMISystem.SystemMode == SystemMode.AUTO)
                {
                    currentLED = "![000/F0005/E5151/C2적재완료!]";
                }
                if (Globals.HMISystem.SystemMode == SystemMode.SEMI_AUTO)
                {
                    currentLED = "![000/F0005/E5151/C3적재완료!]";
                }
            }
            else if (Globals.currentWork.workState == (byte)WorkState.UNLOAD_COMPLETE)
            {
                if (Globals.HMISystem.SystemMode == SystemMode.AUTO)
                {
                    currentLED = "![000/F0005/E5151/C2이재완료!]";
                }
                if (Globals.HMISystem.SystemMode == SystemMode.SEMI_AUTO)
                {
                    currentLED = "![000/F0005/E5151/C3이재완료!]";
                }
            }
            else if (Globals.currentWork.workState == (byte)WorkState.UNLOADING)
            {
                if (Globals.HMISystem.SystemMode == SystemMode.AUTO)
                {
                    currentLED = "![000/F0005/E5151/C2이 재 중!]";
                }
                if (Globals.HMISystem.SystemMode == SystemMode.SEMI_AUTO)
                {
                    currentLED = "![000/F0005/E5151/C3이 재 중!]";
                }
            }
            else if (Globals.currentWork.workState == (byte)WorkState.LOADING)
            {
                if (Globals.HMISystem.SystemMode == SystemMode.AUTO)
                {
                    currentLED = "![000/F0005/E5151/C2적 재 중!]";
                }
                if (Globals.HMISystem.SystemMode == SystemMode.SEMI_AUTO)
                {
                    currentLED = "![000/F0005/E5151/C3적 재 중!]";
                }
            }
            else if (Globals.currentWork.workState == (byte)WorkState.MOVE_COMPLETE)
            {

                if (Globals.HMISystem.SystemMode == SystemMode.AUTO)
                {
                    currentLED = "![000/F0005/E5151/C2주행완료!]";
                }
                if (Globals.HMISystem.SystemMode == SystemMode.SEMI_AUTO)
                {
                    currentLED = "![000/F0005/E5151/C3주행완료!]";
                }
            }
            else if (Astar.FindNodeInfo(Globals.currentWork.toNode).GetNodeType() == Globals.MAP_NODE_CHARGE)////////////
            {
                if (Globals.HMISystem.SystemMode == SystemMode.AUTO)
                {
                    currentLED = "![000/X0000/Y0206/E5151/C2충전소이동중!]";
                }
                if (Globals.HMISystem.SystemMode == SystemMode.SEMI_AUTO)
                {
                    currentLED = "![000/X0000/Y0206/E5151/C3충전소이동중!]";
                }
            }
            else if (Globals.currentWork.workState == (byte)WorkState.MOVING)
            {
                if (Globals.HMISystem.SystemMode == SystemMode.AUTO)
                {
                    switch (Globals.currentWork.toNode)
                    {
                        ////안산 적재장 KDY 23.08.31
                        //case 36:
                        //    currentLED = "![000/X0000/Y0206/E5151/C2적재장 A!]";
                        //    break;
                        //case 29:
                        //    currentLED = "![000/X0000/Y0206/E5151/C2적재장 B!]";
                        //    break;
                        //--------------------------------헤드--------------------------------
                        // 헤드배출
                        case 37: //헤드배출기 이전노드
                        case 183: //헤드배출기 이전노드
                        case 184: //헤드배출기 이전노드
                        case ushort i when (4001 <= i && i <= 4003):
                            currentLED = "![000/X0000/Y0206/E5151/C2헤드배출기!]";
                            break;
                        // 헤드투입
                        case 44: //헤드투입기 이전노드
                        case 603: //헤드공파레트 이전노드
                        case 700: //헤드투입기 이전노드
                        case 705: //헤드투입기 이전노드
                        case 710: //헤드투입기 이전노드
                        case ushort i when (4004 <= i && i <= 4008):
                            currentLED = "![000/X0000/Y0206/E5151/C2헤드투입기!]";
                            break;
                        // 헤드간지배출
                        case 647: //헤드간지배출 이전노드
                        case 4009:
                        case ushort i when (4011 <= i && i <= 4012):
                            currentLED = "![000/X0000/Y0206/E5151/C2헤드간지배출!]";
                            break;
                        // 헤드간지투입
                        case 436: //헤드간지투입 이전노드
                        case 4010:
                            currentLED = "![000/X0000/Y0206/E5151/C2헤드간지투입!]";
                            break;
                        // 헤드적재장A
                        case ushort i when (5001 <= i && i <= 5043):
                            currentLED = "![000/X0000/Y0206/E5151/C2헤드적재장A!]";
                            break;
                        // 헤드적재장B
                        case ushort i when (5101 <= i && i <= 5128):
                            currentLED = "![000/X0000/Y0206/E5151/C2헤드적재장B!]";
                            break;
                        //--------------------------------크랑크--------------------------------
                        // 크랑크배출
                        case 447: //크랑크배출기 이전노드
                        case 449: //크랑크배출기 이전노드
                        case 451: //크랑크배출기 이전노드
                        case ushort i when (4100 <= i && i <= 4102):
                            currentLED = "![000/X0000/Y0206/E5151/C2크랑크배출기!]";
                            break;
                        // 크랑크투입
                        case 557: //크랑크투입기 이전노드
                        case 580: //크랑크투입기 이전노드
                        case 581: //크랑크투입기 이전노드
                        case 582: //크랑크투입기 이전노드
                        case ushort i when (4103 <= i && i <= 4106):
                            currentLED = "![000/X0000/Y0206/E5151/C2크랑크투입기!]";
                            break;
                        // 크랑크적재장
                        case ushort i when (5201 <= i && i <= 5274):
                            currentLED = "![000/X0000/Y0206/E5151/C2크랑크적재장!]";
                            break;
                        //--------------------------------블록--------------------------------
                        // 블록배출
                        case 390: //블록배출기 이전노드
                        case 396: //블록배출기 이전노드
                        case 404: //블록배출기 이전노드
                        case ushort i when (4200 <= i && i <= 4202):
                            currentLED = "![000/X0000/Y0206/E5151/C2블록배출기!]";
                            break;
                        // 블록투입
                        case 231: //블록투입기 이전노드
                        case 232: //블록투입기 이전노드
                        case 233: //블록투입기 이전노드
                        case 450: //블록투입기 이전노드
                        case ushort i when (4203 <= i && i <= 4206):
                            currentLED = "![000/X0000/Y0206/E5151/C2블록투입기!]";
                            break;
                        // 블록적재장A
                        case 618:
                        case ushort i when (5301 <= i && i <= 5336):
                            currentLED = "![000/X0000/Y0206/E5151/C2블록적재장!]";
                            break;
                        // 블록적재장B
                        case ushort i when (5341 <= i && i <= 5356):
                            currentLED = "![000/X0000/Y0206/E5151/C2블록적재장!]";
                            break;


                        default:
                            currentLED = "![000/F0005/E5151/C2주 행 중!]";
                            break;
                    }

                }
                if (Globals.HMISystem.SystemMode == SystemMode.SEMI_AUTO)
                {
                    currentLED = "![000/F0005/E5151/C3주 행 중!]";
                }
            }
            
            else if (/*Globals.currentWork.workState == (byte)WorkState.CHARGING &&*/ Globals.BMSSystem.SOC >= 85)//== 100)
            {
                currentLED = "![000/F0005/E5151/C2충전완료!]";
            }
            else if (Globals.currentWork.workState == (byte)WorkState.CHARGING && Globals.HMISystem.ChargeEnable == true && Globals.VehicleSystem.SystemCharge)
            {
                if (Globals.HMISystem.SystemMode == SystemMode.AUTO)
                {
                    currentLED = "![000/F0005/E5151/C2충 전 중!]";
                }
                if (Globals.HMISystem.SystemMode == SystemMode.SEMI_AUTO)
                {
                    currentLED = "![000/F0005/E5151/C3충 전 중!]";
                }
            }

            //else if ((Globals.HMISystem.PalletFrontEnable == true || Globals.HMISystem.PalletStackingEnable == true)
            //    && Globals.VehicleSystem.CurrentSpeed == 0)
            //{
            //    currentLED = "![000/F0005/S8000/E5151/C4인 식 중!]";
            //}
            if (!"".Equals(currentLED) && !previousLED.Equals(currentLED) && TcpClientLED.IsConnected())
            {
                previousLED = currentLED;
                TcpClientLED.SendText(currentLED);
            }
        }

        public static void UpdateWorkStateUnit()
        {
            // Work State Unit
            if (Globals.workStep.step == Globals.WORK_FORK_LIFTING)
            {
                Globals.currentWork.workStateUnit = (byte)WorkStateUnit.LIFTING;
            }
            else if (Globals.workStep.step == Globals.WORK_FORK_TILTING)
            {
                Globals.currentWork.workStateUnit = (byte)WorkStateUnit.TILTING;
            }
            else if (Globals.workStep.step == Globals.WORK_FORK_SIDESHIFT)
            {
                Globals.currentWork.workStateUnit = (byte)WorkStateUnit.SIDESHIFT;
            }
            else if (Globals.workStep.step == Globals.WORK_FORK_FORKMOVER)
            {
                Globals.currentWork.workStateUnit = (byte)WorkStateUnit.FORKMOVER;
            }
            else if (Globals.workStep.step == Globals.WORK_MOVE_1ST_PATH_PLANNING)
            {
                Globals.currentWork.workStateUnit = (byte)WorkStateUnit.PATH_1ST_NODE;
                drivingNode = true;
            }
            else if (Globals.workStep.step == Globals.WORK_MOVE_2ND_PATH_PLANNING)
            {
                Globals.currentWork.workStateUnit = (byte)WorkStateUnit.PATH_2ND_STATION;
                drivingNode = false;
            }
            else if (Globals.workStep.step == Globals.WORK_MOVE_PATH_SENDING)
            {
                Globals.currentWork.workStateUnit = (byte)WorkStateUnit.PATH_SENDING;
            }
            else if (Globals.workStep.step == Globals.WORK_PALLET_FRONT_ENABLE)
            {
                Globals.currentWork.workStateUnit = (byte)WorkStateUnit.PALLET_FRONT;
            }
            else if (Globals.workStep.step == Globals.WORK_PALLET_STACKING_ENABLE)
            {
                Globals.currentWork.workStateUnit = (byte)WorkStateUnit.PALLET_STACKING;
            }
            else if (Globals.workStep.step == Globals.WORK_CHECK_EXIST_PALLET_FRONT || Globals.workStep.step == Globals.WORK_CHECK_EXIST_PALLET_LOADED)
            {
                Globals.currentWork.workStateUnit = (byte)WorkStateUnit.CHECKING;
            }

            if (Globals.NavigationSystem.ModeState == (ushort)MovingState.AUTO_STATE_MOVING_NORMAL || Globals.NavigationSystem.ModeState == (ushort)MovingState.SEMI_AUTO_STATE_MOVING_NORMAL)
            {
                if (drivingNode)
                {
                    Globals.currentWork.workStateUnit = (byte)WorkStateUnit.PATH_1ST_NODE;
                }
                else
                {
                    Globals.currentWork.workStateUnit = (byte)WorkStateUnit.PATH_2ND_STATION;
                }
            }
        }

        //public static void UpdateManualLoaded()
        //{
        //    if (Globals.SafetySystem.LeftPx || Globals.SafetySystem.RightPx)
        //    {
        //        Globals.loaded = true;
        //    }
        //    else
        //    {
        //        Globals.loaded = false;
        //    }
        //}
        public static void GetChargingState()
        {
            if (Globals.currentWork.workState == (byte)WorkState.CHARGING)// kdy 23.05.05 85퍼가 되기전에 충전기에서 충전신호를 끊는거 같아 84로 테스트
            {
                if (Globals.BMSSystem.SOC >= 81)
                {
                    Globals.HMISystem.ChargeEnable = false;
                    Globals.nChargingCount = 0;
                    Globals.SOCCount = 0;
                    Globals.currentWork.workState = (byte)WorkState.READY;
                    Thread.Sleep(1000);

                    logger.Info("[SUCCESS] Chargeing Complete [SystemCharge] " + Globals.VehicleSystem.SystemCharge + " [ChargeEnable] " + Globals.HMISystem.ChargeEnable + " [workState] " + Globals.currentWork.workState + " [SOC] " + Globals.BMSSystem.SOC);
                    if (Globals.VehicleSystem.SystemCharge == false)
                    {
                        logger.Info("[DISCONNECT] ChargeDisable [SystemCharge] " + Globals.VehicleSystem.SystemCharge + " [ChargeEnable] " + Globals.HMISystem.ChargeEnable + " [workState] " + Globals.currentWork.workState + " [SOC] " + Globals.BMSSystem.SOC);
                    }
                }
                else
                {
                    if (Globals.nChargingCount == 0)
                    {
                        Globals.nFirstSOC = Globals.BMSSystem.SOC;
                    }

                    Globals.nChargingCount++;
                    if (Globals.nChargingCount >= 6000) // 10분
                    {
                        Globals.nLastSOC = Globals.BMSSystem.SOC;
                        Globals.nChargingCount = 0;

                        if (Globals.nFirstSOC >= Globals.nLastSOC)
                        {
                            Globals.currentWork.alarm = Alarms.ALARM_CHARGE_ERROR;
                            Globals.currentWork.alarm_name = "![000/F0005/E5151/C1충전에러!]";
                            logger.Error("[CHARGING FAILED] Charger Error during 30 minutues ");
                        }
                    }
                }
            }

            //kdy 23.08.16 평상시에 soc 변화량이 없으면 에러(급발진 이후 추가 방어코드)

            if (Globals.HMISystem.ChargeEnable && Globals.VehicleSystem.SystemCharge)
            {
                Globals.ChargeCheckFlag = true;
            }
            if(Globals.SOCCount == 0)
            {
                Globals.nFirstSOC2 = Globals.BMSSystem.SOC;
                Globals.ChargeCheckFlag = false;
            }
            Globals.SOCCount++;
            if (Globals.SOCCount >= 144000 && !Globals.ChargeCheckFlag) // kdy 23.10.04 1시간으로 해본 결과, 변화량이 없었던적이 있어 1시간 반으로 늘림
            {
                Globals.nLastSOC2 = Globals.BMSSystem.SOC;
                Globals.SOCCount = 0;
                if(Globals.nFirstSOC2 == Globals.nLastSOC2)
                {
                    Globals.currentWork.alarm = Alarms.ALARM_BMS_SOC_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1배터리변화량/C1  에    러 !]";
                    logger.Error("[SOC Error] during 4 hour, SOC didn't change ");
                }
            }

        }

        public static void UpdateWorkState()
        {

            if (Globals.bDisplaySecondPathWork)
            {
                if (Globals.currentWork.workType == Globals.TRANSFER_LOAD)
                {
                    Globals.currentWork.workState = (byte)WorkState.LOADING; // 적재중
                }
                else if (Globals.currentWork.workType == Globals.TRANSFER_UNLOAD)
                {
                    Globals.currentWork.workState = (byte)WorkState.UNLOADING; // 이재중
                }
            }
            else
            {
                Globals.currentWork.workState = (byte)WorkState.MOVING; // 주행중

                if (Globals.nCorrectMoving == 0)
                {
                    Globals.uTmpCurNode = Globals.currentWork.curNode;
                    Globals.nCorrectMoving++;
                }
                else if (Globals.nCorrectMoving > 0)
                {
                    Globals.nCorrectMoving++;

                    if (Globals.HMISystem.ACStoHMI_TrafficPause == SystemTraffic.TRANSFER_TRAFFIC_EQUIPMENTCOMM_PAUSE ||
                        Globals.HMISystem.ACStoHMI_TrafficPause == SystemTraffic.TRANSFER_TRAFFIC_AUTO_PAUSE ||
                        Globals.HMISystem.ACStoHMI_TrafficPause == SystemTraffic.TRANSFER_TRAFFIC_USER_PAUSE ||
                        Globals.HMISystem.HMItoACS_TrafficPause == SystemTraffic.TRANSFER_TRAFFIC_RCU_PAUSE ||
                        Globals.HMISystem.HMItoACS_TrafficPause == SystemTraffic.TRANSFER_TRAFFIC_REMOTESWITCH_PAUSE)
                    {
                        Globals.nCorrectMoving = 0;
                    }

                    //23.05.16kdy 18000(30분)에서 2400(4분)으로 수정
                    if (Globals.nCorrectMoving > 2400 &&
                    Globals.uTmpCurNode == Globals.currentWork.curNode &&
                    Globals.HMISystem.ACStoHMI_TrafficPause != SystemTraffic.TRANSFER_TRAFFIC_EQUIPMENTCOMM_PAUSE &&
                    Globals.HMISystem.ACStoHMI_TrafficPause != SystemTraffic.TRANSFER_TRAFFIC_AUTO_PAUSE &&
                    Globals.HMISystem.ACStoHMI_TrafficPause != SystemTraffic.TRANSFER_TRAFFIC_USER_PAUSE &&
                    Globals.HMISystem.HMItoACS_TrafficPause != SystemTraffic.TRANSFER_TRAFFIC_RCU_PAUSE &&
                    Globals.HMISystem.HMItoACS_TrafficPause != SystemTraffic.TRANSFER_TRAFFIC_REMOTESWITCH_PAUSE)
                    //Globals.HMISystem.SystemTraffic == SystemTraffic.TRANSFER_TRAFFIC_RESUME)
                    {
                        Globals.nCorrectMoving = 0;
                        Globals.currentWork.alarm = Alarms.ALARM_MOVE_COMPLETE_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1목적지미도착/C1  에    러 !]";
                        logger.Info(" No moving for 4minute ");
                    }

                }

            }

            // 작업이 완료되면
            if (Globals.currentWork.workQueue.Count == 0)
            {
                Globals.nCorrectMoving = 0;

                if (Globals.currentWork.workType == Globals.TRANSFER_MOVE) // 완료된 작업이 이동이면 
                {
                    Globals.currentWork.workState = (byte)WorkState.MOVE_COMPLETE; // 도착
                    Globals.bACSOrderDlgShow = false;
                    Globals.bRetry_ACSRetryCheck = false;
                    Globals.bDisplaySecondPathWork = false;
                    //Globals.bDisplayPauseCheck = true;

                }
                else if (Globals.currentWork.workType == Globals.TRANSFER_LOAD) // 완료된 작업이 적재이면 
                {
                    Globals.bRetry_ACSRetryCheck = false;
                    Globals.bDisplaySecondPathWork = false;
                    //Globals.bDisplayPauseCheck = true;
                    Globals.loaded = true;
                    logger.Info(" Loaded On" + " [Loaded] " + Globals.loaded + " [LeftPx] " + Globals.SafetySystem.LeftPx + " [RightPx] " + Globals.SafetySystem.RightPx);
                    Globals.currentWork.workState = (byte)WorkState.LOAD_COMPLETE; // 적재 완료
                    Globals.bACSOrderDlgShow = false;
                }
                else if (Globals.currentWork.workType == Globals.TRANSFER_UNLOAD) // 완료된 작업이 이재이면 
                {
                    Globals.bRetry_ACSRetryCheck = false;
                    Globals.bDisplaySecondPathWork = false;
                    Globals.loaded = false;
                    logger.Info(" Loaded Off" + " [Loaded] " + Globals.loaded + " [LeftPx] " + Globals.SafetySystem.LeftPx + " [RightPx] " + Globals.SafetySystem.RightPx);
                    Globals.currentWork.workState = (byte)WorkState.UNLOAD_COMPLETE; // 이재 완료
                    Globals.bACSOrderDlgShow = false;
                    //Globals.bDisplayPauseCheck = true;
                }
                
                UpdateLEDState();

                //무브컴플리트면 목표노드를 현재노드로 강제
                /*
                if (Globals.currentWork.curNode != Globals.currentWork.toNode)
                {
                    Globals.toTmpNode = Globals.currentWork.toNode;
                    logger.Info(" Current and Target node are different when finish");
                    Globals.currentWork.alarm = Alarms.ALARM_MAP_SET_CURRENT_NODE_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1현 재 위 치 /C1  에    러 !]";
                }
                */

                Globals.toTmpNode = 0;

                Globals.HMISystem = new HMISystem(Globals.HMISystem.SystemMode, SystemTraffic.TRANSFER_TRAFFIC_ACS_NONE);


                if (Globals.HMISystem.SystemMode == SystemMode.AUTO)
                    Globals.currentWork.Clear_exceptstate(); // kdy 상태 대기로 안하고 완료로 대기
                else
                    Globals.currentWork.Clear();

                Globals.workStep.Clear();

                logger.Info("****[FINISH] [WORK]" + Globals.currentWork.ToString() + " Globals.loaded: " + Globals.loaded + "\n");
            }
        }

        public static void FirmWareLog()
        {
            Globals.FWCount++;
            if (Globals.FWCount >= 5)
            {
                Globals.FWCount = 0;
                logger.Debug(" [Mode] " + Globals.NavigationSystem.ModeState + " [VehicleState] " + Globals.NavigationSystem.VehicleState + " [Alarm] " + Globals.NavigationSystem.Alarm +
                             " [CurrentNode] " + Globals.FirmwareLog.CurrentNode + " [NextNode] " + Globals.FirmwareLog.NextNode + " [SavedCurrentNode] " + Globals.FirmwareLog.SavedCurrentNode +
                             " [CurrentLink] " + Globals.NavigationSystem.CurrentLink + " [NextLink] " + Globals.NavigationSystem.NextLink +
                             " [NAV_X] " + Globals.NavigationSystem.NAV_X + " [NAV_y] " + Globals.NavigationSystem.NAV_Y + " [NAV_PHI] " + Globals.NavigationSystem.NAV_PHI +
                             " [DS_State] " + Globals.FirmwareLog.DSState + " [Rho] " + Globals.FirmwareLog.Rho + 
                             " [DL] " + Globals.FirmwareLog.DL +
                             " [TargetSteer] " + Globals.FirmwareLog.TargetSteer + " [CurrentSteer] " + Globals.FirmwareLog.CurrentSteer + " [SteerOutPut] " + Globals.FirmwareLog.SteerOutPut +
                             " [TargetSpeed] " + Globals.FirmwareLog.TargetSpeed + " [CurrentSpeed] " + Globals.FirmwareLog.CurrentSpeed + " [SpeedOutPut] " + Globals.FirmwareLog.SpeedOutPut +
                             " [LeftRPM] " + Globals.FirmwareLog.LeftRPM + " [RightRPM] " + Globals.FirmwareLog.RightRPM +
                             " [LocaltargetCount] " + Globals.FirmwareLog.LocaltargetCount + " [DiffAngle] " + Globals.FirmwareLog.DiffAngle +
                             " [NavError] " + Globals.FirmwareLog.NavError + " [NavWarning] " + Globals.FirmwareLog.NavWarning +
                             " [AFLID] " + Globals.FirmwareLog.AFLID + " [HornWarningEMO] " + Globals.FirmwareLog.HornWarningEMO +
                             " [TargetRPM] " + Globals.FirmwareLog.TargetRPM + " [LeftRPMDev] " + Globals.FirmwareLog.LeftRPMDev +
                             " [RightRPMDev] " + Globals.FirmwareLog.RightRPMDev + " [AccCount] " + Globals.FirmwareLog.AccCount +
                             " [DecCount] " + Globals.FirmwareLog.DecCount + " [SpdCount] " + Globals.FirmwareLog.SpdCount +
                             " [UnnormalSpeedCheck] " + Globals.FirmwareLog.UnnormalSpeedCheck + " [SteerDev] " + Globals.FirmwareLog.SteerDev +
                             " [SteerDevData] " + Globals.FirmwareLog.SteerDevData + " [SteerCheckCount] " + Globals.FirmwareLog.SteerCheckCount +
                             " [UnnormalSteerCheck] " + Globals.FirmwareLog.UnnormalSteerCheck);
                logger.Trace(" [LeftRPM] " + Globals.FirmwareLog.LeftRPM + " [RightRPM] " + Globals.FirmwareLog.RightRPM +
                             " [TargetRPM] " + Globals.FirmwareLog.TargetRPM + " [LeftRPMDev] " + Globals.FirmwareLog.LeftRPMDev +
                             " [RightRPMDev] " + Globals.FirmwareLog.RightRPMDev + " [AccCount] " + Globals.FirmwareLog.AccCount +
                             " [DecCount] " + Globals.FirmwareLog.DecCount + " [SpdCount] " + Globals.FirmwareLog.SpdCount + " [UnnormalSpeedCheck] " + Globals.FirmwareLog.UnnormalSpeedCheck +
                             " [TargetSteer] " + Globals.FirmwareLog.TargetSteer + " [CurrentSteer] " + Globals.FirmwareLog.CurrentSteer +
                             " [SteerDev] " + Globals.FirmwareLog.SteerDev + " [SteerDevData] " + Globals.FirmwareLog.SteerDevData + " [SteerCheckCount] " + Globals.FirmwareLog.SteerCheckCount +
                             " [UnnormalSteerCheck] " + Globals.FirmwareLog.UnnormalSteerCheck);

                if (Astar.FindNodeInfo(Globals.currentWork.curNode).GetNodeType() == Globals.MAP_NODE_CHARGE)
                {
                    logger.Info("[BATTERY][BMS_CHARGING] " + Globals.BMSSystem.Charging + " [BMS_CHARGE_COMPLETE] " + Globals.BMSSystem.Charge_Complete + " [SystemCharge] " + Globals.VehicleSystem.SystemCharge + " [ChargeEnable] " + Globals.HMISystem.ChargeEnable +
                        "[BATTERY][CELL_VOLT] " + Globals.BMSSystem.CellVolt +
                        " [TEMPERATURE] " + Globals.BMSSystem.Temperature +
                        " [V] " + Globals.BMSSystem.Bat_V / 100 +
                        " [I] " + Globals.BMSSystem.Bat_i / 100 +
                        " [SOC] " + Globals.BMSSystem.SOC +
                        " [SOH] " + Globals.BMSSystem.SOH);
                }
            }
            if (Globals.HMISystem.PalletStackingSuccessEnable || Globals.HMISystem.PalletFrontEnable)
            {
                logger.Info("Stacking [X] " + Globals.RecognitionSystem.TargetX + " [Y] " + Globals.RecognitionSystem.TargetY + " [Theta] " + Globals.RecognitionSystem.TargetTheta);
            }
        }
    }
}
