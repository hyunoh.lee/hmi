﻿using MobyusTouch.src.Communication;
using MobyusTouch.src.Control.Sub;
using MobyusTouch.src.Global;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MobyusTouch.src.Main.Sub
{
    public static class TrafficControl
    {
        private static Logger logger = LogManager.GetLogger("TrafficControl");

        public static bool IsInterruptedTraffic()
        {
            bool interrupt = false;

            if (Globals.HMISystem.SystemTraffic == SystemTraffic.TRANSFER_TRAFFIC_STOP)
            {
                logger.Info("[Traffic] [ThreadID]" + Thread.CurrentThread.ManagedThreadId + " [STOP]" + Globals.HMISystem.SystemTraffic);
                byte preWorkState = Globals.currentWork.workState;
                Globals.currentWork.workState = (byte)WorkState.TRAFFIC; // 트래픽

                while (true)
                {
                    Globals.HMISystem.LiftEnable = false;
                    Globals.HMISystem.TiltEnable = false;
                    Globals.HMISystem.ReachEnable = false;
                    Globals.HMISystem.SideShiftEnable = false;
                    Globals.HMISystem.MoverEnable = false;
                    Globals.HMISystem.PalletStackingEnable = false;
                    Globals.HMISystem.PalletFrontEnable = false;
                    Globals.HMISystem.PalletHeightEnable = false;
                    Globals.HMISystem.PalletStackingSuccessEnable = false;
                    Globals.HMISystem.PalletFrontExistEnable = false;

                    if (Globals.HMISystem.SystemTraffic == SystemTraffic.TRANSFER_TRAFFIC_RESUME)
                    {
                        logger.Info("[Traffic] [ThreadID]" + Thread.CurrentThread.ManagedThreadId + " [RESUME]" + Globals.HMISystem.SystemTraffic);
                        Globals.currentWork.workState = preWorkState;
                        break;
                    }

                    if (Globals.workStep.step == 0) // 작업 취소 시 - Globals.workStep.Clear() 하여 작업 취소 
                    {
                        logger.Info("[Traffic] [workStep] 0");
                        interrupt = true;
                        break;
                    }

                    if (ModeControl.IsChangedSystemMode())
                    {
                        interrupt = true;
                        break;
                    }

                    Thread.Sleep(100);
                }
            }

            return interrupt;
        }

        public static void IsInterruptedTrafficRemote()
        {
            #region 기존 알고리즘
            //if (Globals.SafetySystem.RemoteSwith)
            //{
            //    Globals.TrafficPauseCount++;
            //    Globals.RemoteSwichCount++;
            //    if (Globals.RemoteSwichCount > 10)
            //    {
            //        Globals.RemoteSwichCount = 0;
            //        logger.Info("[REMOTESWICH] " + Globals.SafetySystem.RemoteSwith);
            //        Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_PAUSE; // 제어한테 PAUSE 내리기
            //        Globals.HMISystem.HMItoACS_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_REMOTESWITCH_PAUSE; // 지게차 자체 PAUSE 상태 보내기
            //    }

            //    //일시정지 3분 지속시 알람 추가 23.05.18 kdy
            //    if(Globals.TrafficPauseCount >= 1800)
            //    {
            //        Globals.TrafficPauseCount = 0;
            //        Globals.currentWork.alarm = Alarms.ALARM_TRANSFER_TRAFFIC_REMOTESWITCH_ERROR;
            //        Globals.currentWork.alarm_name = "![000/E5151/C1일 시 정 지 /C1  에    러 !]";
            //    }
            //}
            //else
            //{
            //    Globals.TrafficPauseCount = 0;
            //    switch (Globals.HMISystem.ACStoHMI_TrafficPause)
            //    {
            //        case SystemTraffic.TRANSFER_TRAFFIC_EQUIPMENTCOMM_PAUSE:
            //        case SystemTraffic.TRANSFER_TRAFFIC_AUTO_PAUSE:
            //        case SystemTraffic.TRANSFER_TRAFFIC_USER_PAUSE:
            //            break;
            //        default:
            //            if(Globals.HMISystem.HMItoACS_TrafficPause == SystemTraffic.TRANSFER_TRAFFIC_RCU_PAUSE)
            //            {
            //                break;
            //            }
            //            else
            //            { 
            //                Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_RESUME; // 평상시에 RESUME 상태, TRAFFIC_NONE 보내는 거 
            //                Globals.HMISystem.HMItoACS_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_FIRMWARE_NONE; // 평상시에 지게차 자체 PAUSE 안보내는거 
            //                break;
            //            }
            //    }
            //}
            #endregion

            if (Globals.SafetySystem.RemoteSwith)
            {
                Globals.TrafficPauseCount++;
                Globals.RemoteSwichCount++;
                if (Globals.RemoteSwichCount > 10) // 1초동안 누르면 일시정지
                {
                    logger.Info("[REMOTESWICH ON]");
                    Globals.RemoteSwichFlag = true;
                    Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_PAUSE; // 제어한테 PAUSE 내리기
                    Globals.HMISystem.HMItoACS_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_REMOTESWITCH_PAUSE; // 지게차 자체 PAUSE 상태 보내기
                }
                else
                {
                    switch (Globals.HMISystem.ACStoHMI_TrafficPause)
                    {
                        case SystemTraffic.TRANSFER_TRAFFIC_EQUIPMENTCOMM_PAUSE:
                        case SystemTraffic.TRANSFER_TRAFFIC_AUTO_PAUSE:
                        case SystemTraffic.TRANSFER_TRAFFIC_USER_PAUSE:
                            break;
                        default:
                            if (Globals.HMISystem.HMItoACS_TrafficPause == SystemTraffic.TRANSFER_TRAFFIC_RCU_PAUSE)
                            {
                                break;
                            }
                            else
                            {
                                Globals.RemoteSwichFlag = false;
                                logger.Info("[REMOTESWICH OFF]");
                                Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_RESUME; // 평상시에 RESUME 상태, TRAFFIC_NONE 보내는 거 
                                Globals.HMISystem.HMItoACS_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_FIRMWARE_NONE; // 평상시에 지게차 자체 PAUSE 안보내는거 
                                break;
                            }
                    }
                }
            }
            else
            {
                Globals.RemoteSwichCount = 0;
            }


            //3분 지속시 알람
            if (Globals.RemoteSwichFlag)
            {
                Globals.TrafficPauseCount++;
            }

            if (Globals.TrafficPauseCount >= 1800) // 3분지속시 에러 발생
            {
                Globals.TrafficPauseCount = 0;
                Globals.RemoteSwichFlag = false;
                Globals.currentWork.alarm = Alarms.ALARM_TRANSFER_TRAFFIC_REMOTESWITCH_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1일 시 정 지 /C1  에    러 !]";
            }
        }
    }
}
