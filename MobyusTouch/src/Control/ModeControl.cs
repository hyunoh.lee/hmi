﻿using MobyusTouch.src.Global;
using MobyusTouch.src;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MobyusTouch;
using System.Threading;
using MessageBox = System.Windows.Forms.MessageBox;

namespace MobyusTouch.src.Control.Sub
{
    public delegate void DialogEMOCloseEvent();

    public static class ModeControl
    {
        private static Logger logger = LogManager.GetLogger("ModeControl");

        public static event DialogEMOCloseEvent dlgEMOCloseEvent;

        public static bool IsChangedSystemMode()
        {
            bool isChanged = false;
            if (!Globals.debug)
            {
                if (Globals.VehicleSystem.SystemMode)
                {
                    if (Globals.HMISystem.SystemMode == SystemMode.OFFLINE)
                    {
                        isChanged = true;
                        Globals.bRetry_ACSRetryCheck = false;
                        Globals.HMISystem.ChargeEnable = false;
                        Globals.nChargingCount = 0;
                        Globals.SOCCount = 0;

                        if (Globals.HMISystem.SystemMode != SystemMode.SEMI_AUTO)
                            Globals.bNodeLinkCheck = true;
                        //   Globals.HMISystem.SystemMode = SystemMode.MANUAL;
                        Globals.HMISystem.SystemMode = SystemMode.SEMI_AUTO;
                    }
                }
                else
                {
                    isChanged = true;

                    if (Globals.HMISystem.SystemMode != SystemMode.OFFLINE)
                        Globals.bNodeLinkCheck = true;

                    if (Globals.HMISystem.ChargeEnable)
                    {
                        Globals.HMISystem.ChargeEnable = false;
                        Globals.nChargingCount = 0;
                        Globals.SOCCount = 0;
                    }



                    Globals.HMISystem.SystemMode = SystemMode.OFFLINE;

                    try
                    {
                        if (Globals.b1stPathPlanContinue)
                        {
                            Globals.b1stPathPlanContinue = false;
                            logger.Info("   Init 1st Continue Astar Flag");// [NAV_X] " + Globals.NavigationSystem.NAV_X + " [NAV_Y] " + Globals.NavigationSystem.NAV_Y);

                        }

                        if (Globals.b2ndPathPlanContinue)
                        {
                            Globals.b2ndPathPlanContinue = false;
                            logger.Info("   Init 2nd Continue Astar Flag");// [NAV_X] " + Globals.NavigationSystem.NAV_X + " [NAV_Y] " + Globals.NavigationSystem.NAV_Y);
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Error("   Init Flag Error [NAV_X] " + Globals.NavigationSystem.NAV_X + " [NAV_Y] " + Globals.NavigationSystem.NAV_Y);
                        logger.Error("   " + e.ToString());
                        Globals.currentWork.alarm = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1알 수 없 는 /C1  에    러 !]";
                    }
                }

            }

            if (Globals.preSystemMode != Globals.HMISystem.SystemMode)
            {
                logger.Info("[SYSTEM MODE] is Changed From:" + Globals.preSystemMode + "  To:" + Globals.HMISystem.SystemMode + " Alarm:" + Globals.currentWork.alarm + " [NAV_X] : " + Globals.NavigationSystem.NAV_X + " [NAV_Y] :" + Globals.NavigationSystem.NAV_Y + " [NAV_PHI] :" + Globals.NavigationSystem.NAV_PHI);
                //logger.Info("[CURNODE] " + Globals.currentWork.curNode + " [CURLINK] " + Globals.currentWork.curLink);

                isChanged = true;
                Globals.tempSystemMode = Globals.preSystemMode;
                Globals.preSystemMode = Globals.HMISystem.SystemMode;



                if (Globals.HMISystem.SystemMode >= SystemMode.RESET && Globals.HMISystem.SystemMode != SystemMode.AUTO)
                {
                    logger.Info("[RESET] ALL DATA");

                    Globals.HMISystem = new HMISystem(Globals.HMISystem.SystemMode, SystemTraffic.TRANSFER_TRAFFIC_ACS_NONE);
                    Globals.currentWork.Clear();
                    Globals.workStep.Clear();
                    //Globals.tmpPath.Clear();
                }
            }
            return isChanged;
        }

        public static bool IsInterruptedAlarm()
        {
            bool interrupted = false;
            if (Globals.currentWork.alarm > 0 && Globals.currentWork.alarm < Alarms.WARNING && Globals.HMISystem.SystemMode > SystemMode.RESET) // 알람코드 200이상은 Warning
            {
                interrupted = true;

                logger.Info("frontRetry : " + Globals.currentWork.frontRetry +
                    " [ACSRetryCheck] " + !Globals.bRetry_ACSRetryCheck +
                    " [HMISystem.SystemMode] " + Globals.HMISystem.SystemMode.ToString() +
                    " [bFirstMoveComplete] " + Globals.bRetry_FirstMoveComplete.ToString() +
                    " [PalletFrontEnable] " + Globals.HMISystem.PalletFrontEnable.ToString() +
                    " [PalletFrontExistEnable] " + Globals.HMISystem.PalletFrontExistEnable +
                    " [PalletStackingEnable] " + Globals.HMISystem.PalletStackingEnable +
                    " [PalletStackingSuccessEnable] " + Globals.HMISystem.PalletStackingSuccessEnable +
                    " [PalletHeightEnable] " + Globals.HMISystem.PalletHeightEnable);

                //kdy 23.12.27 retry 해제
                if (!Globals.bRetry_ACSRetryCheck && Globals.bRetry_FirstMoveComplete &&
                    (Globals.HMISystem.PalletFrontEnable || Globals.HMISystem.PalletFrontExistEnable || Globals.HMISystem.PalletStackingEnable || Globals.HMISystem.PalletStackingSuccessEnable || Globals.HMISystem.PalletHeightEnable)
                    &&/* Globals.bRetry_LiftingRetry && Globals.bRetry_SecondPathRetry && */Globals.HMISystem.SystemMode == SystemMode.AUTO && !Globals.VehicleSystem.SystemEMO)
                {
                    Globals.currentWork.alarm = Alarms.ALARM_RETRY;
                    Globals.currentWork.alarm_name = "![000/E5151/C1리 트 라 이 !]";
                    Globals.currentWork.frontRetry = 1;
                    logger.Info("[[[RETRY]]]] [Front Retry] 1 [Alarm] " + Globals.currentWork.alarm);
                    Thread.Sleep(1000);
                }
                else
                {
                    if (Globals.bRetry_FirstMoveComplete && 
                        (Globals.HMISystem.PalletFrontEnable || Globals.HMISystem.PalletFrontExistEnable || Globals.HMISystem.PalletStackingEnable ||  Globals.HMISystem.PalletStackingSuccessEnable  || Globals.HMISystem.PalletHeightEnable)
                        && /*Globals.bRetry_LiftingRetry && Globals.bRetry_SecondPathRetry && */Globals.HMISystem.SystemMode == SystemMode.AUTO && Globals.VehicleSystem.SystemEMO)
                    {
                        Globals.bAlarmMemory = true;
                    }


                    Globals.HMISystem = new HMISystem(SystemMode.EMO);
                    logger.Error("[[[ALARM]]]" + Globals.currentWork.alarm + "[METHOD]" + (new System.Diagnostics.StackTrace()).GetFrame(1).GetMethod().Name + "[MSG]" + Globals.alarmTable[Globals.currentWork.alarm] + "[WORK]" + Globals.currentWork.ToString());

                    logger.Info("[BATTERY][CELL_VOLT] " + Globals.BMSSystem.CellVolt +
                                " [TEMPERATURE] " + Globals.BMSSystem.Temperature +
                                " [V] " + Globals.BMSSystem.Bat_V / 100 +
                                " [I] " + Globals.BMSSystem.Bat_i / 100 +
                                " [SOC] " + Globals.BMSSystem.SOC +
                                " [SOH] " + Globals.BMSSystem.SOH);
                    //logger.Info(" [tmpNode]" + Globals.tmpPath[Globals.tmpPath.Count - 1].GetParentNodeID() + " [tmpPath Link]" + Globals.tmpPath[Globals.tmpPath.Count - 1].GetLinkNumber() + " [WORK]" + Globals.currentWork.workType + " [PALLET_TYPE]" + Globals.currentWork.palletType + "[NAV_X]" + Globals.NavigationSystem.NAV_X + " [NAV_Y]" + Globals.NavigationSystem.NAV_Y + " [NAV_PHI]:" + Globals.NavigationSystem.NAV_PHI);
                }
                Globals.bAlarmSendACS = true;
            }
            else if (Globals.currentWork.alarm > Alarms.WARNING)
            {
                Globals.bAlarmSendACS = true;
            }

            if (Globals.currentWork.alarm == Alarms.NO_ALARM)
            {
                Globals.bAlarmSendACS = false;
            }
            //else if (Globals.currentWork.alarm > Alarms.NO_ALARM)
            //{
            //    interrupted = false;
            //}
            return interrupted;
        }

        public static bool IsReleaseCheck()
        {
            bool interrupted = false;
            if (Globals.HMISystem.SystemMode == SystemMode.AUTO || Globals.HMISystem.SystemMode == SystemMode.SEMI_AUTO)
            {
                if (Globals.ForkLiftSystem.ReleaseCheck)
                    Globals.currentWork.alarm = Alarms.ALARM_RELEASE_CHECK;
            }

            return interrupted;
        }

        //1129 kdy 버튼 누를시 모드 변경 추가
        public static void PushButton()
        {

            if (Globals.NavigationSystem.bResetButton) // 알람삭제
            {
                Globals.bResetPushWait = false;
                if (!Globals.bResetPushWait)
                {
                    Globals.bResetPushWait = true;
                    logger.Info("[Button Push] RESET");
                    Globals.currentWork.alarm = Alarms.NO_ALARM;
                    Globals.HMISystem = new HMISystem(Globals.HMISystem.SystemMode, SystemTraffic.TRANSFER_TRAFFIC_STOP);
                    //Globals.HMISystem = new HMISystem(SystemMode.RESET);

                    Thread.Sleep(200);
                    Globals.workStep.Clear();
                    Globals.currentWork.Clear();
                    // Globals.tmpPath.Clear();

                    Globals.HMISystem.LiftEnable = false;
                    Globals.HMISystem.TiltEnable = false;
                    Globals.HMISystem.ReachEnable = false;
                    Globals.HMISystem.SideShiftEnable = false;
                    Globals.HMISystem.MoverEnable = false;
                    Globals.HMISystem.ChargeEnable = false;
                    Globals.nChargingCount = 0;
                    Globals.SOCCount = 0;
                    Globals.CurrentTimeCheck = 0;
                    Globals.HMISystem.ClampEnable = false;
                    Globals.HMISystem.TaggingEnable = false;
                    Globals.HMISystem.PalletStackingEnable = false;
                    Globals.HMISystem.PalletFrontEnable = false;
                    Globals.HMISystem.PalletHeightEnable = false;
                    Globals.HMISystem.PalletStackingSuccessEnable = false;
                    Globals.HMISystem.PalletFrontExistEnable = false;

                    if (Globals.bDlgEMOShow)
                    {
                        dlgEMOCloseEvent();
                    }
                    //Globals.HMISystem = new HMISystem(SystemMode.SEMI_AUTO);
                    Globals.HMISystem = new HMISystem(SystemMode.RESET);


                    //Globals.RemoteSwichFlag = false;
                    Thread.Sleep(1000);
                    Globals.bResetPushWait = false;
                }
            }

            if (Globals.NavigationSystem.bStopButton) // 작업삭제, 모드는 semi-auto
            {
                Globals.bStopPushWait = false;
                if (!Globals.bStopPushWait)
                {
                    Globals.bStopPushWait = true;
                    logger.Info("[Button Push] STOP");
                    Globals.HMISystem = new HMISystem(Globals.HMISystem.SystemMode, SystemTraffic.TRANSFER_TRAFFIC_STOP);
                    Thread.Sleep(200);
                    Globals.workStep.Clear();
                    Globals.currentWork.Clear();
                    // Globals.tmpPath.Clear();

                    Globals.HMISystem.LiftEnable = false;
                    Globals.HMISystem.TiltEnable = false;
                    Globals.HMISystem.ReachEnable = false;
                    Globals.HMISystem.SideShiftEnable = false;
                    Globals.HMISystem.MoverEnable = false;
                    Globals.HMISystem.ChargeEnable = false;
                    Globals.nChargingCount = 0;
                    Globals.SOCCount = 0;
                    Globals.CurrentTimeCheck = 0;
                    Globals.HMISystem.ClampEnable = false;
                    Globals.HMISystem.TaggingEnable = false;
                    Globals.HMISystem.PalletStackingEnable = false;
                    Globals.HMISystem.PalletFrontEnable = false;
                    Globals.HMISystem.PalletHeightEnable = false;
                    Globals.HMISystem.PalletStackingSuccessEnable = false;
                    Globals.HMISystem.PalletFrontExistEnable = false;

                    Globals.HMISystem = new HMISystem(SystemMode.SEMI_AUTO);
                    //MessageBox.Show("현재 작업을 삭제하였습니다.");
                    Thread.Sleep(1000);
                    Globals.bStopPushWait = false;
                }
            }
            if (Globals.NavigationSystem.bStartButton) // 모드 auto
            {
                Globals.bStartPushWait = false;
                if (!Globals.bStartPushWait)
                {
                    Globals.bStartPushWait = true;
                    Globals.bAutoMode_AR_Wait = true;

                    logger.Info("[Button Push] Start");

                    //if (Globals.SafetySystem.LeftPx || Globals.SafetySystem.RightPx)
                    //{
                    //    Globals.loaded = true;
                    //}
                    //else
                    //{
                    //    Globals.loaded = false;
                    //}

                    if (Globals.bAlarmMemory /*&& Globals.bFirstMoveComplete && Globals.bLiftingRetry && Globals.bSecondPathRetry*/)
                    {
                        Globals.bAlarmMemory = false;

                        Globals.currentWork.alarm = Alarms.ALARM_RETRY;
                        Globals.currentWork.alarm_name = "![000/E5151/C1리 트 라 이 !]";
                        Globals.currentWork.frontRetry = 1;
                        logger.Info("   [[[EMO RETRY]]] Front Retry 1");

                    }
                    else if (Globals.HMISystem.SystemMode != SystemMode.AUTO)
                        Globals.HMISystem = new HMISystem(SystemMode.AUTO);

                    Thread.Sleep(1000);
                    Globals.bStartPushWait = false;
                }
            }
        }
    }
}
