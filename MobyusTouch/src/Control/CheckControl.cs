﻿using MobyusTouch.src.Global;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MobyusTouch.src.Main.Sub
{
    public static class CheckControl
    {
        private static Logger logger = LogManager.GetLogger("CheckControl");

        public static bool IsLoadedPallet(WorkStep workStep)
        {
            //logger.Info("[CHECK]IsLoadedPallet");
            bool result = false;

            //if (Globals.currentWork.workType == Globals.TRANSFER_LOAD && (Globals.SafetySystem.LeftPx || Globals.SafetySystem.RightPx)) // Globals.SafetySystem.PressureSensor >= 750
            //{
            //    Globals.currentWork.alarm = Alarms.ALARM_PALLET_LOADED_ERROR;
            //    Globals.currentWork.alarm_name = "적재불가능";
            //    result = false;  
            //}
            //else if (Globals.currentWork.workType == Globals.TRANSFER_UNLOAD && !Globals.SafetySystem.LeftPx && !Globals.SafetySystem.RightPx)
            //{
            //    Globals.currentWork.alarm = Alarms.ALARM_PALLET_LOADED_NOT_ERROR;
            //    Globals.currentWork.alarm_name = "이재불가능";
            //    result = false;
            //}
            //else
            //{
            //    result = true;
            //}
            result = true; // 위 주석으로 인한 추가사항, 위 주석 풀면 삭제할것 23.01.09 김도연
            logger.Info("[IsLoadedPallet] WorkType: " + Globals.currentWork.workType + " Left PX Sensor :" + Globals.SafetySystem.LeftPx + " //   Left PX Sensor :" + Globals.SafetySystem.RightPx);
            return result;
        }

        public static bool IsExistFrontPallet(WorkStep workStep)
        {
            logger.Info("[CHECK]IsExistFrontPallet");
            bool result = false;

            if (Globals.currentWork.workType == Globals.TRANSFER_LOAD && Globals.RecognitionSystem.FrontPalletResult == false)
            {
                Globals.currentWork.alarm = Alarms.ALARM_PALLET_FRONT_NOT_EXIST_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1전방팔렛없음/C1  에    러 !]";
                result = false;
            }
            else if (Globals.currentWork.workType == Globals.TRANSFER_UNLOAD && Globals.RecognitionSystem.FrontPalletResult == true)
            {
                Globals.currentWork.alarm = Alarms.ALARM_PALLET_FRONT_EXIST_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1전방팔렛있음/C1  에    러 !]";
                result = false;
            }
            else
            {
                result = true;
            }

            logger.Info("[IsExistFrontPallet] " + Globals.RecognitionSystem.FrontPalletResult);
            return result;
        }

        public static bool IsNotExistFrontObstacle(WorkStep workStep)
        {
            logger.Info("[CHECK]IsNotExistFrontObstacle");
            bool result = false;

            if (Globals.SafetySystem.LeftWT100 == true || Globals.SafetySystem.RightWT100 == true)
            {
                Globals.currentWork.alarm = Alarms.ALARM_FORK_EXIST_OBASTACLE_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1 진입장애물 /C1  에    러 !]";
                result = false;
            }
            else
            {
                result = true;
            }

            logger.Info("[IsNotExistFrontObstacle] " + result);
            return result;
        }

        public static void IsMinLimitOverOnAlarm(string name, int target_value, int min_limit, int alarm)
        {
            if (min_limit > target_value)
            {
                logger.Info(name + " [MIN LIMIT]" + min_limit + " [VALUE]" + target_value);
                Globals.currentWork.alarm = alarm;

                if (alarm == Alarms.ALARM_FORK_LIFT_RANGE_OVER_ERROR)
                    Globals.currentWork.alarm_name = "![000/E5151/C1포크리프트범위오차에러!]";
                else if (alarm == Alarms.ALARM_FORK_TILT_RANGE_OVER_ERROR)
                    Globals.currentWork.alarm_name = "![000/E5151/C1포크틸트범위오차에러!]";
                else if (alarm == Alarms.ALARM_FORK_SIDESHIFT_RANGE_OVER_ERROR)
                    Globals.currentWork.alarm_name = "![000/E5151/C1포크좌우이동범위오차에러!]";
                else if (alarm == Alarms.ALARM_FORK_MOVER_RANGE_OVER_ERROR)
                    Globals.currentWork.alarm_name = "![000/E5151/C1포크좌우동작범위오차에러!]";
            }
        }

        public static void IsMaxLimitOverOnAlarm(string name, int target_value, int max_limit, int alarm)
        {
            if (max_limit < target_value)
            {
                logger.Info(name + " [MAX LIMIT]" + max_limit + " [VALUE]" + target_value);
                Globals.currentWork.alarm = alarm;

                if (alarm == Alarms.ALARM_FORK_LIFT_RANGE_OVER_ERROR)
                    Globals.currentWork.alarm_name = "![000/E5151/C1포크리프트범위오차에러!]";
                else if (alarm == Alarms.ALARM_FORK_TILT_RANGE_OVER_ERROR)
                    Globals.currentWork.alarm_name = "![000/E5151/C1포크틸트범위오차에러!]";
                else if (alarm == Alarms.ALARM_FORK_SIDESHIFT_RANGE_OVER_ERROR)
                    Globals.currentWork.alarm_name = "![000/E5151/C1포크좌우이동범위오차에러!]";
                else if (alarm == Alarms.ALARM_FORK_MOVER_RANGE_OVER_ERROR)
                    Globals.currentWork.alarm_name = "![000/E5151/C1포크좌우동작범위오차에러!]";
            }
        }

        public static void IsLeftLimitOverOnAlarm(string name, int target_value, int left_limit, int alarm)
        {
            if (left_limit < target_value)
            {
                logger.Info(name + " [MIN LIMIT]" + left_limit + " [VALUE]" + target_value);
                Globals.currentWork.alarm = alarm;

                Globals.currentWork.alarm_name = "![000/E5151/C1포크좌우이동범위오차에러!]";
            }
        }

        public static void IsRightLimitOverOnAlarm(string name, int target_value, int right_limit, int alarm)
        {
            if ((target_value <= 0) && (right_limit < Math.Abs(target_value)))
            {
                logger.Info(name + " [MAX LIMIT]" + right_limit + " [VALUE]" + target_value);
                Globals.currentWork.alarm = alarm;


                Globals.currentWork.alarm_name = "![000/E5151/C1포크좌우이동범위오차에러!]";
            }
        }
    }
}
