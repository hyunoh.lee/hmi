﻿using MobyusTouch.src.Global;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MobyusTouch.src.Control.Sub
{
    public static class SafetyControl
    {
        private static Logger logger = LogManager.GetLogger("SafetyControl");

        public static bool ClampEnable(WorkStep workStep)
        {
            logger.Info("ClampEnable");
            bool result = true;
            Globals.HMISystem.ClampEnable = true;
            Thread.Sleep(500);

            logger.Info("[ClampEnable] " + Globals.HMISystem.ClampEnable);
            return result;
        }

        public static bool ClampDisable(WorkStep workStep)
        {
            logger.Info("ClampDisable");
            bool result = true;
            Globals.HMISystem.ClampEnable = false;
            Thread.Sleep(500);

            logger.Info("[ClampDisable] " + Globals.HMISystem.ClampEnable);
            return result;
        }

        
    }
}
