﻿using MobyusTouch.src.Control;
using MobyusTouch.src.Control.Sub;
using MobyusTouch.src.Global;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MobyusTouch.src.Main.Sub
{
    public static class ChargeControl
    {
        private static Logger logger = LogManager.GetLogger("ChargeControl");

        public static bool ChargeEnable(WorkStep workStep)
        {
            logger.Info(">>>>[START] ChargeEnable [SOC] " + Globals.BMSSystem.SOC);
            bool result = false;
            Globals.HMISystem.ChargeEnable = true;
            while (Globals.currentWork.alarm == Alarms.NO_ALARM || Globals.currentWork.alarm >= Alarms.WARNING)
            {
                if (ModeControl.IsChangedSystemMode())
                {
                    logger.Info("[FAILED] IsChangedSystemMode");
                    break;
                }

                if (TrafficControl.IsInterruptedTraffic())
                {
                    logger.Info("[FAILED] IsInterruptedTraffic");
                    break;
                }

                if (Globals.VehicleSystem.SystemCharge == true)
                {
                    result = true;
                    logger.Info("[SUCCESS] [SystemCharge] " + Globals.VehicleSystem.SystemCharge +
                                " [ChargeEnable] " + Globals.HMISystem.ChargeEnable +
                                " [workState] " + Globals.currentWork.workState);
                    logger.Info("[BATTERY][CELL_VOLT] " + Globals.BMSSystem.CellVolt +
                                " [TEMPERATURE] " + Globals.BMSSystem.Temperature +
                                " [V] " + Globals.BMSSystem.Bat_V / 100 +
                                " [I] " + Globals.BMSSystem.Bat_i / 100 +
                                " [SOC] " + Globals.BMSSystem.SOC +
                                " [SOH] " + Globals.BMSSystem.SOH);
                    break;
                }

                Thread.Sleep(100);
            }
            return result;
        }

        public static bool ChargeDisable(WorkStep workStep)
        {
            logger.Info(">>>>[START] ChargeDisable [SOC] " + Globals.BMSSystem.SOC);
            bool result = false;

            Globals.HMISystem.ChargeEnable = false;
            while (Globals.currentWork.alarm == Alarms.NO_ALARM || Globals.currentWork.alarm >= Alarms.WARNING)
            {
                if (ModeControl.IsChangedSystemMode())
                {
                    break;
                }

                if (TrafficControl.IsInterruptedTraffic())
                {
                    break;
                }
                
                if (Globals.VehicleSystem.SystemCharge == false)
                {
                    result = true;
                    logger.Info("[SUCCESS] [SystemCharge] " + Globals.VehicleSystem.SystemCharge + 
                                " [ChargeEnable] " + Globals.HMISystem.ChargeEnable + 
                                " [workState] " + Globals.currentWork.workState);
                    logger.Info("[BATTERY][CELL_VOLT] " + Globals.BMSSystem.CellVolt +
                                " [TEMPERATURE] " + Globals.BMSSystem.Temperature +
                                " [V] " + Globals.BMSSystem.Bat_V / 100 +
                                " [I] " + Globals.BMSSystem.Bat_i / 100 +
                                " [SOC] " + Globals.BMSSystem.SOC +
                                " [SOH] " + Globals.BMSSystem.SOH);
                    break;
                }
                
                Thread.Sleep(100);
            }
            return result;
        }

        public static void ChargingNode()
        {
            try
            {
                for(int i = 0; i < Globals.nodeData.Count; i++)
                {
                    if (Globals.nodeData[i].GetNodeNumber() == Convert.ToUInt16(Globals.currentWork.curNode) && 
                        Globals.nodeData[i].GetNodeType() == Globals.MAP_NODE_CHARGE && Globals.VehicleSystem.CurrentSpeed == 0)
                    {
                        Globals.HMISystem.ChargeEnable = true;
                        Globals.currentWork.workState = (byte)WorkState.CHARGING; // CHARGE
                        logger.Info("[SUCCESS] ChargeEnable " + Globals.VehicleSystem.SystemCharge);
                       
                    }
                }

                Globals.bStartChargeFlag = false;
            }
            catch(Exception ex)
            {
                logger.Info("[Charging] " + ex.ToString());
            }
        }
    }
}
