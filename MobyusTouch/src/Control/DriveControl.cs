﻿
using MobyusTouch.src.Communication;
using MobyusTouch.src.Control;
using MobyusTouch.src.Control.Sub;
using MobyusTouch.src.Global;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;

//using MapDLL;
using EditorLibrary;

namespace MobyusTouch.src.Main.Sub
{
    public static class DriveControl
    {
        private static Logger logger = LogManager.GetLogger("MoveControl");

        public static bool Move1stPathPlanning(WorkStep workStep)
        {
            logger.Info(">>>>[START]" + workStep.name + " [ThreadID]" + Thread.CurrentThread.ManagedThreadId + " [Step]" + (ushort)workStep.step + " [Value]" + (ushort)workStep.value + " [curNode]" + Globals.currentWork.curNode);
            bool result = false;
            try
            {
                ushort endNode = workStep.value == 0 ? Globals.currentWork.toNode : (ushort)workStep.value; // ACS에서 명령보낼때는 workStep.value가 0, toNode에 목적지를 줘서 tonode를 따르고, semi로 보낼땐 목적지를 hmi화면에서 누르는 번호로 하기때문에 이때는 workStep.value값을 따름
                Globals.currentWork.toNode = endNode;

                if (Globals.currentWork.curNode <= 0)
                {
                    logger.Info(" [1stPathPlanning]  Current Node Error [curNode] " + Globals.currentWork.curNode);
                    Globals.currentWork.alarm = Alarms.ALARM_MAP_SET_CURRENT_NODE_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1현 재 위 치 /C1  에    러 !]";
                }

                else
                {
                    if (endNode == Globals.currentWork.curNode && !Globals.bRetry_ACSRetryCheck)
                    {
                        logger.Info("  Current Node is End Node [curNode] " + Globals.currentWork.curNode + " [endNode] " + endNode);
                        result = true;
                    }
                    else
                    {
                        Globals.bRetry_FirstPathRetry = true;
                        Globals.bFirstPathComplete = true;

                        Globals.nAFL_Start = 0;

                        if (Globals.currentWork.workMode == Globals.PATH_PLANNING_AMR)
                        {

                            if (Globals.b1stPathPlanContinue)
                            {
                                Globals.currentWork.realPath.Clear();

                                if (Globals.bNoRocFromFirmware && Globals.curTmpNode != 0 && Globals.curTmpLink != 0)
                                {
                                    logger.Info("  Continue path planning using tmp [endNode]" + endNode + " [tmpNode]" + Globals.curTmpNode + " [tmppLink]" + Globals.curTmpLink + " [WORK]" + Globals.currentWork.workType + " [PALLET_TYPE]" + Globals.currentWork.palletType + "[NAV_X] " + Globals.NavigationSystem.NAV_X + " [NAV_Y] " + Globals.NavigationSystem.NAV_Y + " [NAV_PHI] " + Globals.NavigationSystem.NAV_PHI);
                                    Globals.currentWork.realPath = Astar.ConTinueAstarAFL(Globals.curTmpNode, Globals.curTmpLink, Globals.NavigationSystem.NAV_PHI, endNode);
                                }
                                else
                                {
                                    logger.Info("  Continue path planning using firmware [endNode]" + endNode + " [curNode]" + Globals.currentWork.curNode + " [curLink]" + Globals.currentWork.curLink + " [WORK]" + Globals.currentWork.workType + " [PALLET_TYPE]" + Globals.currentWork.palletType + "[NAV_X] " + Globals.NavigationSystem.NAV_X + " [NAV_Y] " + Globals.NavigationSystem.NAV_Y + " [NAV_PHI] " + Globals.NavigationSystem.NAV_PHI);
                                    Globals.currentWork.realPath = Astar.ConTinueAstarAFL(Globals.currentWork.curNode, Globals.currentWork.curLink, Globals.NavigationSystem.NAV_PHI, endNode);

                                }
                            }
                            else
                            {
                                Globals.currentWork.realPath.Clear();
                                logger.Info("  Path Planning after First Start [endNode]" + endNode + " [curNode]" + Globals.currentWork.curNode.ToString() + " [curLink]" + Globals.currentWork.curLink + " [WORK]" + Globals.currentWork.workType + " [PALLET_TYPE]" + Globals.currentWork.palletType + "[NAV_X] " + Globals.NavigationSystem.NAV_X + " [NAV_Y] " + Globals.NavigationSystem.NAV_Y + " [NAV_PHI] " + Globals.NavigationSystem.NAV_PHI);
                                Globals.currentWork.realPath = Astar.StartAstarAFL(Globals.NavigationSystem.NAV_X, Globals.NavigationSystem.NAV_Y, Globals.NavigationSystem.NAV_PHI, endNode);
                                Globals.b1stPathPlanContinue = true;
                            }

                            // logger.Info("  StartAstar Complete");

                        }
                        else
                        {
                            logger.Info("   ACS makes a Path [workMode] " + Globals.currentWork.workMode + " [PathCount] " + Globals.currentWork.realPath.Count + " [endNode]" + endNode + " [curNode]" + Globals.currentWork.curNode.ToString() + " [curLink]" + Globals.currentWork.curLink + " [WORK]" + Globals.currentWork.workType + " [PALLET_TYPE]" + Globals.currentWork.palletType + "[NAV_X]" + Globals.NavigationSystem.NAV_X + " [NAV_Y]" + Globals.NavigationSystem.NAV_Y + " [NAV_PHI]:" + Globals.NavigationSystem.NAV_PHI);
                        }


                        if (Globals.currentWork.realPath.Count > 0)
                        {
                            if (Globals.currentWork.realPath.Count > 1)
                            {
                                Astar.VerifyPath(Globals.NavigationSystem.NAV_X, Globals.NavigationSystem.NAV_Y, ref Globals.currentWork.realPath);
                                logger.Info("   Verify Path [NAV_X] " + Globals.NavigationSystem.NAV_X + " [NAV_Y] " + Globals.NavigationSystem.NAV_Y + " [PathCount] " + Globals.currentWork.realPath.Count);
                            }


                            if (Astar.FindNodeInfo(endNode).GetNodeType() == Globals.MAP_NODE_STATION || Astar.FindNodeInfo(endNode).GetNodeType() == Globals.MAP_NODE_EQUIPMENT) // Target Node가 station 이면 전 노드까지 1차 주행
                            {
                                Thread.Sleep(30);
                                //lhw 0425 목적 스테이션 바로 앞 노드에서 acs 명령 받을 시 패스클리어하고 세컨드패스플래닝에서 다시
                                if (Globals.currentWork.realPath.Count == 1)
                                {
                                    Globals.currentWork.realPath.Clear();
                                    logger.Info("  First path clear for Second");
                                }
                                //무브컴플리트용 퍼스트패스플래닝 마지막 노드 저장
                                else if (Globals.currentWork.realPath.Count > 1)
                                {
                                    logger.Info("  Remove [Last Node] " + Astar.FindNodeInfo(endNode).GetNodeNumber() + " [Cnt] " + Globals.currentWork.realPath.Count);
                                    Globals.currentWork.realPath.RemoveAt(0);
                                    Globals.nFirstPath_LastNode = (int)Globals.currentWork.realPath[0].GetCurrentNodeID();
                                }
                                else
                                {
                                    Globals.nFirstPath_LastNode = (int)Globals.currentWork.realPath[0].GetCurrentNodeID();
                                }
                                Thread.Sleep(30);

                                logger.Info("   First Path Planning [realPath.Count] " + Globals.currentWork.realPath.Count + "[TARGET] " + Globals.nFirstPath_LastNode + " when target is station or equipment");

                            }
                            else
                            {
                                Globals.nFirstPath_LastNode = (int)Globals.currentWork.toNode;
                                logger.Info("   First Path Planning [realPath.Count] " + Globals.currentWork.realPath.Count + " [TARGET] " + Globals.nFirstPath_LastNode + " when target is node");

                            }
                            result = true;
                        }
                        else
                        {
                            logger.Error("  There is no path data " + "[MODE]" + Globals.currentWork.workMode + " [CNT] " + Globals.currentWork.realPath.Count + " [NAV_X]" + Globals.NavigationSystem.NAV_X + " [NAV_Y]" + Globals.NavigationSystem.NAV_Y + " [NAV_PHI]:" + Globals.NavigationSystem.NAV_PHI);
                            Globals.currentWork.alarm = Alarms.ALARM_MOVE_NO_PATH_DATA_ERROR;
                            Globals.currentWork.alarm_name = "![000/E5151/C1경로데이터없음에러!]";
                        }
                    }
                }
                if (result)
                {

                    //23.02.28 kdy 충전소 일때 강제로 lift, tilt 기본값으로 올리기 // KDY 23.08.17 해당 함수 위치 MQTT.AR에서 옮김
                    if (Astar.FindNodeInfo(Globals.currentWork.curNode).GetNodeType() == Globals.MAP_NODE_CHARGE)
                    {
                        while (!ForkControl.MoveFromCharger())
                        {
                            if (ForkControl.MoveFromCharger())
                            {
                                logger.Info("<<<<[END]MoveFormCharger [RESULT]TRUE [ALARM]" + Globals.currentWork.alarm + " [MODE]" + Globals.HMISystem.SystemMode
                                                      + " [ThreadID]" + Thread.CurrentThread.ManagedThreadId + " [AFL_STATE]" + Globals.currentWork.workState + " [NAVI_STATE]" + Globals.NavigationSystem.ModeState
                                                     + " [STATE]" + Globals.currentWork.workState + " [STATE_UNIT]" + Globals.currentWork.workStateUnit
                                                     + " [CUR_NODE]" + Globals.currentWork.curNode + " [NEXT_NODE]" + Globals.currentWork.nextNode + " [TO_NODE]" + Globals.currentWork.toNode
                                                     + " [NAV_X]" + Globals.NavigationSystem.NAV_X + " [NAV_Y]" + Globals.NavigationSystem.NAV_Y + " [NAV_PHI]:" + Globals.NavigationSystem.NAV_PHI);
                            }

                        }
                    }

                    string logStr = "";
                    if (Globals.currentWork.realPath.Count > 0)
                    {
                        for (int i = Globals.currentWork.realPath.Count - 1; i >= 0; i--)
                        {
                            logStr += "->" + Globals.currentWork.realPath[i].GetParentNodeID() + "/" + Globals.currentWork.realPath[i].GetLinkNumber() + "/" + Globals.currentWork.realPath[i].GetLinkType() + "/" + Globals.currentWork.realPath[i].GetLinkOption() + "/" + Globals.currentWork.realPath[i].GetCurrentNodeID();
                        }
                        logger.Info("  [Mode] " + Globals.currentWork.workMode + " [Node] " + logStr);


                        byte b = Globals.currentWork.realPath[Globals.currentWork.realPath.Count - 1].GetLinkOption();

                        if (Astar.FindNodeInfo(Globals.currentWork.realPath[Globals.currentWork.realPath.Count - 1].GetParentNodeID()).GetNodeType() == Globals.MAP_NODE_STATION ||
                            Astar.FindNodeInfo(Globals.currentWork.realPath[Globals.currentWork.realPath.Count - 1].GetParentNodeID()).GetNodeType() == Globals.MAP_NODE_EQUIPMENT)
                        {
                            Globals.currentWork.realPath[Globals.currentWork.realPath.Count - 1].SetLinkOption((byte)(b + 16));
                            logger.Info("  Steering Lock [ParentNode] " + Globals.currentWork.realPath[Globals.currentWork.realPath.Count - 1].GetParentNodeID() +
                                        " [CurrentNode] " + Globals.currentWork.realPath[Globals.currentWork.realPath.Count - 1].GetCurrentNodeID());
                        }

                        Globals.currentWork.getNextNodePath.Clear();
                        Globals.currentWork.getNextNodePath = Globals.currentWork.realPath.ToList();

                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                Globals.currentWork.alarm = Alarms.ALARM_MOVE_1ST_PATH_PLANNING_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C11차경로생성실패에러!]";
            }
            return result;
        }

        public static bool Move2ndPathPlanning(WorkStep workStep)
        {
            logger.Info(">>>>[START]" + workStep.name + " [ThreadID]" + Thread.CurrentThread.ManagedThreadId + " [Step]" + (ushort)workStep.step + " [Value]" + (ushort)workStep.value + " [curNode]" + Globals.currentWork.curNode);

            bool result = false;
            try
            {
                ushort endNode = workStep.value == 0 ? Globals.currentWork.toNode : (ushort)workStep.value;
                Globals.currentWork.toNode = endNode;

                if (Globals.currentWork.curNode <= 0)
                {
                    logger.Info(" [2ndPathPlanning]  Current Node Error [curNode] " + Globals.currentWork.curNode);
                    Globals.currentWork.alarm = Alarms.ALARM_MAP_SET_CURRENT_NODE_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1현 재 위 치 /C1  에    러 !]";
                }

                else if (Astar.FindNodeInfo(endNode).GetNodeType() != Globals.MAP_NODE_STATION && Astar.FindNodeInfo(endNode).GetNodeType() != Globals.MAP_NODE_EQUIPMENT)
                {
                    logger.Error("  End node is not Station or Equipment node  [endNode] " + endNode + " [X] " + Globals.NavigationSystem.NAV_X.ToString() + " [Y} " + Globals.NavigationSystem.NAV_Y.ToString() + " [T] " + Globals.NavigationSystem.NAV_PHI.ToString());

                    Globals.currentWork.alarm = Alarms.ALARM_MOVE_2ND_PATH_PLANNING_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C12차경로생성실패에러!]";
                }
                else
                {
                    if (endNode == Globals.currentWork.curNode)
                    {
                        logger.Info("  Current Node is  End Node [curNode] " + Globals.currentWork.curNode + " [endNode] " + endNode);
                        result = true;
                    }
                    else
                    {
                        //if (Globals.currentWork.workMode == Globals.PATH_PLANNING_AMR)
                        {
                            Globals.bRetry_SecondPathRetry = true;
                            Globals.bSecondPathComplete = true;
                            Globals.bFirstPathComplete = false;
                            Globals.bDisplaySecondPathWork = true;
                            //logger.Info(" [[[RETRY]]] Globals.bSecondPathRetry : " + Globals.bRetry_SecondPathRetry);

                            if (Globals.b2ndPathPlanContinue)
                            {
                                Globals.currentWork.realPath.Clear();

                                if (Globals.bNoRocFromFirmware && Globals.curTmpNode != 0 && Globals.curTmpLink != 0)
                                {
                                    logger.Info("  Continue path planning using tmp [endNode]" + endNode + " [tmpNode]" + Globals.curTmpNode + " [tmppLink]" + Globals.curTmpLink + " [WORK]" + Globals.currentWork.workType + " [PALLET_TYPE]" + Globals.currentWork.palletType + "[NAV_X]" + Globals.NavigationSystem.NAV_X + " [NAV_Y]" + Globals.NavigationSystem.NAV_Y + " [NAV_PHI]:" + Globals.NavigationSystem.NAV_PHI);
                                    Globals.currentWork.realPath = Astar.ConTinueAstarAFL(Globals.curTmpNode, Globals.curTmpLink, Globals.NavigationSystem.NAV_PHI, endNode);
                                }
                                else
                                {
                                    logger.Info("  Continue path planning using firmware [endNode]" + endNode + " [curNode]" + Globals.currentWork.curNode + " [curLink]" + Globals.currentWork.curLink + " [WORK]" + Globals.currentWork.workType + " [PALLET_TYPE]" + Globals.currentWork.palletType + "[NAV_X]" + Globals.NavigationSystem.NAV_X + " [NAV_Y]" + Globals.NavigationSystem.NAV_Y + " [NAV_PHI]:" + Globals.NavigationSystem.NAV_PHI);
                                    Globals.currentWork.realPath = Astar.ConTinueAstarAFL(Globals.currentWork.curNode, Globals.currentWork.curLink, Globals.NavigationSystem.NAV_PHI, endNode);
                                }

                            }
                            else
                            {
                                Globals.currentWork.realPath.Clear();
                                logger.Info("  Path Planning after First Start [endNode]" + endNode + " [curNode]" + Globals.currentWork.curNode.ToString() + " [curLink]" + Globals.currentWork.curLink + " [WORK]" + Globals.currentWork.workType + " [PALLET_TYPE]" + Globals.currentWork.palletType + "[NAV_X]" + Globals.NavigationSystem.NAV_X + " [NAV_Y]" + Globals.NavigationSystem.NAV_Y + " [NAV_PHI]:" + Globals.NavigationSystem.NAV_PHI);
                                Globals.currentWork.realPath = Astar.StartAstarAFL(Globals.NavigationSystem.NAV_X, Globals.NavigationSystem.NAV_Y, Globals.NavigationSystem.NAV_PHI, endNode);

                                Globals.b2ndPathPlanContinue = true;
                            }


                            // logger.Info("  StartAstar Complete");

                            if (Globals.currentWork.realPath.Count > 0)
                            {
                                Globals.currentWork.realPath.RemoveRange(1, Globals.currentWork.realPath.Count - 1);
                                logger.Info("   [Path Count] " + Globals.currentWork.realPath.Count);
                            }
                            Globals.currentWork.realPath[0].SetLinkType((byte)Globals.currentWork.workLastLinkType);
                        }
                        //else
                        //{
                        //    logger.Info("   Globals.currentWork.workMode : " + Globals.currentWork.workMode);
                        //}

                        if (Globals.currentWork.realPath.Count <= 2 && Globals.currentWork.realPath.Count > 0)
                        {
                            result = true;
                        }
                        else
                        {
                            logger.Error("  Path Planning of 2nd is path count(0 < cnt <= 2) over error. [CNT]" + Globals.currentWork.realPath.Count + " [MODE]" + Globals.currentWork.workMode);
                            Globals.currentWork.alarm = Alarms.ALARM_MOVE_NO_PATH_DATA_ERROR;
                            Globals.currentWork.alarm_name = "![000/E5151/C1경로데이터없음에러!]";
                        }
                    }

                    if (result)
                    {
                        string logStr = "";
                        if (Globals.currentWork.realPath.Count > 0)
                        {
                            for (int i = Globals.currentWork.realPath.Count - 1; i >= 0; i--)
                            {
                                logStr += "->" + Globals.currentWork.realPath[i].GetCurrentNodeID() + "/" + Globals.currentWork.realPath[i].GetLinkType();
                            }
                            logger.Info("  [Mode] " + Globals.currentWork.workMode + " [Node] " + logStr);


                            byte b = Globals.currentWork.realPath[Globals.currentWork.realPath.Count - 1].GetLinkOption();

                            if (Astar.FindNodeInfo(Globals.currentWork.realPath[Globals.currentWork.realPath.Count - 1].GetParentNodeID()).GetNodeType() == Globals.MAP_NODE_STATION || Astar.FindNodeInfo(Globals.currentWork.realPath[Globals.currentWork.realPath.Count - 1].GetParentNodeID()).GetNodeType() == Globals.MAP_NODE_EQUIPMENT)
                            {
                                Globals.currentWork.realPath[Globals.currentWork.realPath.Count - 1].SetLinkOption((byte)(b + 16));
                                logger.Info("  Steering Lock [ParentNode] " + Globals.currentWork.realPath[Globals.currentWork.realPath.Count - 1].GetParentNodeID() + " [CurrentNode] " + Globals.currentWork.realPath[Globals.currentWork.realPath.Count - 1].GetCurrentNodeID());
                            }
                            if (Astar.FindNodeInfo(Globals.currentWork.realPath[Globals.currentWork.realPath.Count - 1].GetCurrentNodeID()).GetNodeType() == Globals.MAP_NODE_EQUIPMENT && Globals.currentWork.workType == Globals.TRANSFER_LOAD)
                            {
                                Globals.currentWork.realPath[Globals.currentWork.realPath.Count - 1].SetLinkOption((byte)(b + 32));
                                logger.Info("  Equipment Node Option [ParentNode] " + Globals.currentWork.realPath[Globals.currentWork.realPath.Count - 1].GetParentNodeID() + " [CurrentNode] " + Globals.currentWork.realPath[Globals.currentWork.realPath.Count - 1].GetCurrentNodeID());
                            }


                            Globals.currentWork.getNextNodePath.Clear();
                            Globals.currentWork.getNextNodePath = Globals.currentWork.realPath.ToList();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                Globals.currentWork.alarm = Alarms.ALARM_MOVE_2ND_PATH_PLANNING_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C12차경로생성실패에러!]";
            }
            return result;
        }

        public static bool MovePathPlanning(WorkStep workStep)
        {
            logger.Info(">>>>[START]" + workStep.name + " [ThreadID]" + Thread.CurrentThread.ManagedThreadId + " [Step]" + (ushort)workStep.step + " [Value]" + (ushort)workStep.value + " [curNode]" + Globals.currentWork.curNode);
            bool result = false;
            try
            {
                ushort endNode = workStep.value == 0 ? Globals.currentWork.toNode : (ushort)workStep.value;
                Globals.currentWork.toNode = endNode;

                if (Globals.currentWork.curNode <= 0)
                {
                    logger.Info(" [MovePathPlanning]  Current Node Error [curNode] " + Globals.currentWork.curNode);
                    Globals.currentWork.alarm = Alarms.ALARM_MAP_SET_CURRENT_NODE_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1현 재 위 치 /C1  에    러 !]";
                }

                else
                {
                    if (endNode == Globals.currentWork.curNode)
                    {
                        logger.Info("  Current Node is  End Node [curNode] " + Globals.currentWork.curNode + " [endNode] " + endNode);
                        result = true;
                    }
                    else
                    {
                        if (Globals.currentWork.workMode == Globals.PATH_PLANNING_AMR)
                        {
                            Globals.currentWork.realPath.Clear();
                            logger.Info("  [endNode]" + endNode + " [curNode]" + Globals.currentWork.curNode.ToString() + " [WORK]" + Globals.currentWork.workType + " [PALLET_TYPE]" + Globals.currentWork.palletType + "[NAV_X]" + Globals.NavigationSystem.NAV_X + " [NAV_Y]" + Globals.NavigationSystem.NAV_Y + " [NAV_PHI]" + Globals.NavigationSystem.NAV_PHI + " [CNT]" + Globals.currentWork.realPath.Count);
                            Globals.currentWork.realPath = Astar.StartAstarAFL(Globals.NavigationSystem.NAV_X, Globals.NavigationSystem.NAV_Y, Globals.NavigationSystem.NAV_PHI, endNode);
                        }

                        if (Globals.currentWork.realPath.Count > 0)
                        {
                            result = true;
                        }
                        else
                        {
                            logger.Error("  There is no path data " + "[MODE]" + Globals.currentWork.workMode + " [CNT] " + Globals.currentWork.realPath.Count + " [NAV_X]" + Globals.NavigationSystem.NAV_X + " [NAV_Y]" + Globals.NavigationSystem.NAV_Y + " [NAV_PHI]:" + Globals.NavigationSystem.NAV_PHI);
                            Globals.currentWork.alarm = Alarms.ALARM_MOVE_NO_PATH_DATA_ERROR;
                            Globals.currentWork.alarm_name = "![000/E5151/C1경로데이터없음에러!]";
                        }
                    }
                }
                if (result)
                {
                    string logStr = "";
                    if (Globals.currentWork.realPath.Count > 0)
                    {
                        for (int i = Globals.currentWork.realPath.Count - 1; i >= 0; i--)
                        {
                            logStr += "->" + Globals.currentWork.realPath[i].GetCurrentNodeID() + "/" + Globals.currentWork.realPath[i].GetLinkType() + "/" + Globals.currentWork.realPath[i].GetLinkOption();
                        }
                        logger.Info("  [Mode] " + Globals.currentWork.workMode + " [Node] " + logStr);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                Globals.currentWork.alarm = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
            }
            return result;
        }

        public static bool MovePathSending(WorkStep workStep)
        {
            logger.Info(">>>>[START]" + workStep.name + " [ThreadID]" + Thread.CurrentThread.ManagedThreadId + " [Step]" + (ushort)workStep.step + " [Value]" + (ushort)workStep.value + " [curNode]" + Globals.currentWork.curNode);
            bool result = false;


            if (Globals.currentWork.realPath.Count > 0) /*&& Globals.currentWork.toNode != Globals.currentWork.curNode)*/
            {
                int tryCnt = 0;
                bool boardResult = false;

                Globals.nLastNodeID = Globals.currentWork.realPath[0].GetCurrentNodeID();
                List<PathDataAFL> serialPathData = SerialPortBoard.CreatePathSerial(Globals.currentWork.realPath);

                if (serialPathData.Count > 1)
                {
                    while (tryCnt < 3)
                    {
                        if (Globals.currentWork.workQueue == null)
                        {
                            break;
                        }

                        if (TrafficControl.IsInterruptedTraffic())
                        {
                            break;
                        }

                        try
                        {
                            tryCnt++;
                            boardResult = SerialPortBoard.SendPathSerial(serialPathData);
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex.ToString());
                        }

                        if (boardResult)
                        {
                            string logStr = "";
                            if (Globals.currentWork.realPath.Count > 0)
                            {
                                for (int i = Globals.currentWork.realPath.Count - 1; i >= 0; i--)
                                {
                                    logStr += "->" + Globals.currentWork.realPath[i].GetCurrentNodeID() + "/" + Globals.currentWork.realPath[i].GetLinkType();
                                }
                                logger.Info("   [Node]" + logStr);
                            }
                            Globals.moveComplete = false;
                            result = true;
                            break;
                        }
                        logger.Info("  Try: " + tryCnt);
                        Thread.Sleep(200);
                    }
                    if (tryCnt >= 3)
                    {
                        logger.Info("  ERROR [Send Try] " + tryCnt + " [X] " + Globals.NavigationSystem.NAV_X.ToString() + " [Y] " + Globals.NavigationSystem.NAV_Y.ToString() + " [T] " + Globals.NavigationSystem.NAV_PHI.ToString());
                        Globals.currentWork.alarm = Alarms.ALARM_MOVE_PATH_SENDING_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1경 로 생 성 /C1  에    러 !]";
                    }
                }
                else
                {
                    logger.Info("  No Send Data. [Serial Path Count] " + serialPathData.Count);
                    Globals.moveComplete = true;
                    result = true;
                }
            }
            else
            {
                logger.Info("  No Send Data. [Path Count] " + Globals.currentWork.realPath.Count);
                Globals.moveComplete = true;
                result = true;
            }
            return result;
        }

        public static bool MoveFromStation(WorkStep workStep)
        {
            logger.Info(">>>>[START]" + workStep.name + " [ThreadID]" + Thread.CurrentThread.ManagedThreadId + " [Step]" + (ushort)workStep.step + " [Value]" + (ushort)workStep.value + " [MoveComplete]" + Globals.moveComplete + " [curNode]" + Globals.currentWork.curNode);

            bool result = false;

            int fromDistance = 2000;
            if (workStep.value > 0)
            {
                fromDistance = workStep.value;
            }

            long startX = Globals.NavigationSystem.NAV_X;
            long startY = Globals.NavigationSystem.NAV_Y;
            while (Globals.currentWork.alarm == Alarms.NO_ALARM || Globals.currentWork.alarm >= Alarms.WARNING)
            {

                float distanceFromStart = (float)Math.Sqrt(Math.Pow(Globals.NavigationSystem.NAV_X - startX, 2) + Math.Pow(Globals.NavigationSystem.NAV_Y - startY, 2));

                //Globals.nWhileWorkCount++;
                //if (Globals.nWhileWorkCount > 200) // 1초
                //{
                //    Globals.nWhileWorkCount = 0;
                //    logger.Info("   Working MoveFromStation [moveComplete] " + Globals.moveComplete + " [workQueue.Count] " + Globals.currentWork.workQueue.Count + " [distanceFromStart] " + distanceFromStart + " [NodeType] " + Astar.FindNodeInfo(Globals.currentWork.curNode).GetNodeType());
                //}

                if (Globals.currentWork.workQueue == null)
                {
                    logger.Info("  No CurrentWork workQueue");
                    break;
                }

                if (TrafficControl.IsInterruptedTraffic())
                {
                    break;
                }

                /*&& Astar.FindNodeInfo(Globals.currentWork.curNode).GetNodeType() != Globals.MAP_NODE_STATION */
                if ((distanceFromStart <= 30000 && distanceFromStart >= fromDistance && Astar.FindNodeInfo(Globals.currentWork.curNode).GetNodeType() != Globals.MAP_NODE_EQUIPMENT) || Globals.moveComplete)
                {
                    logger.Info("  Distance From Start [Distance] " + distanceFromStart + " [NodeType] " + Astar.FindNodeInfo(Globals.currentWork.curNode).GetNodeType() + " [moveComplete] " + Globals.moveComplete);
                    result = true;
                    break;
                }
                Thread.Sleep(50);
            }
            return result;
        }

        public static bool MoveToStation(WorkStep workStep)
        {
            logger.Info(">>>>[START]" + workStep.name + " [ThreadID]" + Thread.CurrentThread.ManagedThreadId + " [Step]" + (ushort)workStep.step + " [Value]" + (ushort)workStep.value + " [MoveComplete]" + Globals.moveComplete + " [curNode]" + Globals.currentWork.curNode);

            bool result = false;
            if (Globals.currentWork.realPath.Count > 2)
            {
                while (Globals.currentWork.alarm == Alarms.NO_ALARM || Globals.currentWork.alarm >= Alarms.WARNING)
                {
                    //Globals.nWhileWorkCount++;
                    //if (Globals.nWhileWorkCount > 200) // 1초
                    //{
                    //    Globals.nWhileWorkCount = 0;
                    //    logger.Info("   Working MoveToStation [moveComplete] " + Globals.moveComplete + " [workQueue.Count] " + Globals.currentWork.workQueue.Count + " [curNode] " + Globals.currentWork.curNode + " [Globals.currentWork.realPath[1].GetCurrentNodeID()] " + Globals.currentWork.realPath[1].GetCurrentNodeID());
                    //}

                    if (Globals.currentWork.workQueue == null)
                    {
                        logger.Info("  No CurrentWork workQueue");
                        break;
                    }

                    if (TrafficControl.IsInterruptedTraffic())
                    {
                        break;
                    }
                    if ((Globals.currentWork.realPath[1].GetCurrentNodeID() == Globals.currentWork.curNode) || Globals.moveComplete)
                    //if (Globals.currentWork.realPath.Count < 2 || Globals.moveComplete)
                    //if ((Globals.currentWork.realPath[1].GetCurrentNodeID() == Globals.currentWork.curNode && 
                    //Globals.currentWork.realPath[1].GetParentNodeID() == Globals.currentWork.realPath[0].GetCurrentNodeID()) 
                    //|| Globals.moveComplete) //kdy 테스트 필요(test_kdy)
                    //if ((Astar.FindNodeInfo(Globals.currentWork.curNode).GetNodeType() != Globals.MAP_NODE_STATION && Globals.currentWork.realPath[2].GetCurrentNodeID() == Globals.currentWork.curNode) || Globals.moveComplete) // 현재위치가 station 이 아니면 
                    {
                        result = true;
                        break;
                    }
                    Thread.Sleep(50);
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public static bool MoveComplete(WorkStep workStep)
        {
            logger.Info(">>>>[START]" + workStep.name + " [ThreadID]" + Thread.CurrentThread.ManagedThreadId + " [Step]" + (ushort)workStep.step + " [Value]" + (ushort)workStep.value + " [MoveComplete]" + Globals.moveComplete + " [curNode]" + Globals.currentWork.curNode);
            bool result = false;

            //if (Globals.bFirstPathComplete)
            //{
            //    Globals.bFirstPathComplete = false;
            //    logger.Info("   First Path Move Complete Start");
            //}
            //if (Globals.bSecondPathComplete)
            //{
            //    Globals.bSecondPathComplete = false;
            //    logger.Info("   Second Path Move Complete Start");
            //}


            while (Globals.currentWork.alarm == Alarms.NO_ALARM || Globals.currentWork.alarm >= Alarms.WARNING)
            {
                //Globals.nWhileWorkCount++;
                //if (Globals.nWhileWorkCount > 200) // 1초
                //{
                //    Globals.nWhileWorkCount = 0;
                //    logger.Info("   Working MoveComplete [moveComplete] " + Globals.moveComplete + " [workQueue.Count] " + Globals.currentWork.workQueue.Count);
                //}


                if (Globals.currentWork.workQueue == null)
                {
                    logger.Info("  No CurrentWork workQueue");
                    break;
                }


                if (TrafficControl.IsInterruptedTraffic())
                {
                    break;
                }

                if (Globals.moveComplete)
                {
                    Globals.currentWork.realPath.Clear();
                    Globals.currentWork.getNextNodePath.Clear();
                    result = true;

                    Globals.moveComplete = false;
                    break;
                }
                Thread.Sleep(50);
            }

            return result;
        }

        public static bool MoveEnd(WorkStep workStep)
        {
            logger.Info(">>>>[START]" + workStep.name + " [ThreadID]" + Thread.CurrentThread.ManagedThreadId + " [Step]" + (ushort)workStep.step + " [Value]" + (ushort)workStep.value + " [MoveComplete]" + Globals.moveComplete + " [curNode]" + Globals.currentWork.curNode);
            bool result = false;

            while (Globals.currentWork.alarm == Alarms.NO_ALARM || Globals.currentWork.alarm >= Alarms.WARNING)
            {
                if (TrafficControl.IsInterruptedTraffic())
                {
                    break;
                }

                // if (CalcDistance(Convert.ToInt32(Globals.NavigationSystem.NAV_X), Convert.ToInt32(Globals.NavigationSystem.NAV_Y), Astar.FindNodeInfo(Globals.currentWork.toNode).GetNodePoint().X, Astar.FindNodeInfo(Globals.currentWork.toNode).GetNodePoint().Y) <= 100 || Globals.currentWork.toNode == Globals.currentWork.curNode)
                //{
                //logger.Info("  Target is Current Node");
                result = true;
                break;
                //}
                Thread.Sleep(50);
            }
            return result;
        }

        private static bool isCalcCurentLocationeHMI()
        {
            bool bReturn = false;

            if (CalcDistance((int)Globals.NavigationSystem.NAV_X, (int)Globals.NavigationSystem.NAV_Y, Astar.FindNodeInfo(Globals.currentWork.realPath[Globals.currentWork.realPath.Count - 1].GetCurrentNodeID()).GetNodePoint().X, Astar.FindNodeInfo(Globals.currentWork.realPath[Globals.currentWork.realPath.Count - 1].GetCurrentNodeID()).GetNodePoint().Y) <= 100)
            {
                bReturn = true;
            }

            return bReturn;
        }

        public static int CalcDistance(int x1, int y1, int x2, int y2)
        {
            int nResult = 10000;

            nResult = Convert.ToInt32(Math.Sqrt(Math.Pow(x1 - x2, 2) + Math.Pow(y1 - y2, 2)));

            return nResult;
        }

        public static bool IsMoveComplete()
        {
            if ((Globals.NavigationSystem.ModeState == (ushort)MovingState.AUTO_STATE_MOVE_COMPLETE) || (Globals.NavigationSystem.ModeState == (ushort)MovingState.SEMI_AUTO_STATE_MOVE_COMPLETE))
            {
                logger.Info("  Receive Move Complete from Firmware [realPath.Count] " + Globals.currentWork.realPath.Count.ToString() + " [NavigationSystem.Alarm] " + Globals.NavigationSystem.Alarm + "NAV [X] " + Globals.NavigationSystem.NAV_X + " [Y] " + Globals.NavigationSystem.NAV_Y + " [Theta] " + Globals.NavigationSystem.NAV_PHI + " [NODE] " + Globals.currentWork.curNode);

                if (Globals.HMISystem.PalletFrontEnable == true)
                {
                    logger.Info("Front [X] " + Globals.RecognitionSystem.TargetX + " [Y] " + Globals.RecognitionSystem.TargetY + " [Theta] " + Globals.RecognitionSystem.TargetTheta);
                }

                if (Globals.HMISystem.PalletStackingEnable == true)
                {
                    logger.Info("Stacking [X] " + Globals.RecognitionSystem.TargetX + " [Y] " + Globals.RecognitionSystem.TargetY + " [Theta] " + Globals.RecognitionSystem.TargetTheta);
                }

                logger.Info("@@@@@ [NAV_MODES] " + Globals.NavigationSystem.ModeState + " [bFirstPathComplete] " + Globals.bFirstPathComplete + " [bSecondPathComplete] " + Globals.bSecondPathComplete + " [Distance] " + CalcDistance((int)Globals.NavigationSystem.NAV_X, (int)Globals.NavigationSystem.NAV_Y, Astar.FindNodeInfo(Globals.nLastNodeID).GetNodePoint().X, Astar.FindNodeInfo(Globals.nLastNodeID).GetNodePoint().Y));
                logger.Info("@@@@@ [HMISystem.MoveComplete] " + Globals.HMISystem.MoveComplete + " [moveComplete] " + Globals.moveComplete);
                logger.Info("@@@@@ [Traffic] " + Globals.HMISystem.SystemTraffic + " [ACStoHMI] " + Globals.HMISystem.ACStoHMI_TrafficPause + " [HMItoACS] " + Globals.HMISystem.HMItoACS_TrafficPause);

                logger.Info("NAV    [X] " + Globals.NavigationSystem.NAV_X + " [Y] " + Globals.NavigationSystem.NAV_Y + " [Theta] " + Globals.NavigationSystem.NAV_PHI + " [NODE] " + Globals.currentWork.curNode);
                logger.Info("TARGET [X] " + Astar.FindNodeInfo(Globals.nLastNodeID).GetNodePoint().X + " [Y] " + Astar.FindNodeInfo(Globals.nLastNodeID).GetNodePoint().Y + " [TARGET_NODE] " + Globals.nLastNodeID);

                //kdy 23.07.10 목적지 다 도착했을때 만약 현재노드랑 목적지랑 다른데 좌표가 같으면 노드 강제 변환
                if (Globals.currentWork.curNode != (ushort)Globals.nLastNodeID)
                {
                    logger.Info(" [currentWork.curNode] " + Globals.currentWork.curNode + " [nLastNodeID] " + Globals.nLastNodeID);
                    if (CalcDistance((int)Globals.NavigationSystem.NAV_X, (int)Globals.NavigationSystem.NAV_Y, Astar.FindNodeInfo(Globals.nLastNodeID).GetNodePoint().X, Astar.FindNodeInfo(Globals.nLastNodeID).GetNodePoint().Y) < 200)
                    {
                        Globals.currentWork.curNode = (ushort)Globals.nLastNodeID;
                        logger.Info(" [NodeChange][currentWork.curNode] " + Globals.currentWork.curNode + " [nLastNodeID] " + Globals.nLastNodeID);
                    }
                }

                if (CalcDistance((int)Globals.NavigationSystem.NAV_X, (int)Globals.NavigationSystem.NAV_Y, Astar.FindNodeInfo(Globals.nLastNodeID).GetNodePoint().X, Astar.FindNodeInfo(Globals.nLastNodeID).GetNodePoint().Y) > 1000 //회전 후 노드가 목적지 일 경우 500 보다 더 차이날수 있어 1000으로 수정
                    && Globals.NavigationSystem.Alarm == 0 && Globals.nLastNodeID != 0) //내비게이션 알람 있을때는 주행완료알람 안치게 수정 23.05.18 kdy
                {

                    logger.Info("FINAL  [X] : " + (Globals.NavigationSystem.NAV_X - (int)Astar.FindNodeInfo(Globals.nLastNodeID).GetNodePoint().X) + " [Y] : " + (Globals.NavigationSystem.NAV_Y - (int)Astar.FindNodeInfo(Globals.nLastNodeID).GetNodePoint().Y));
                    logger.Info("@@@@@@@@@@ Planning Move Complete Error");

                    Globals.nLastNodeID = 0;
                    Globals.HMISystem.MoveComplete = false;
                    Globals.moveComplete = false;
                    Globals.currentWork.alarm = Alarms.ALARM_MOVE_COMPLETE_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1목적지미도착/C1  에    러 !]";
                }

                DateTime StartTime = DateTime.Now;
                while (Globals.currentWork.alarm == Alarms.NO_ALARM || Globals.currentWork.alarm >= Alarms.WARNING)
                {
                    Globals.HMISystem.MoveComplete = true;
                    Globals.moveComplete = true;

                    logger.Info("@@@@@@@@@@ [NAV_MODES] " + Globals.NavigationSystem.ModeState);

                    if (Globals.NavigationSystem.ModeState != (ushort)MovingState.AUTO_STATE_MOVE_COMPLETE && Globals.NavigationSystem.ModeState != (ushort)MovingState.SEMI_AUTO_STATE_MOVE_COMPLETE)
                    {
                        logger.Info("   NAV [X] " + Globals.NavigationSystem.NAV_X + " [Y] " + Globals.NavigationSystem.NAV_Y + " [Theta] " + Globals.NavigationSystem.NAV_PHI);
                        logger.Info("   MoveComplete [B] " + Globals.HMISystem.MoveComplete + " [TargetNode] " + Globals.nLastNodeID + " [CurrnentNode] " + Globals.currentWork.curNode);
                        Globals.HMISystem.MoveComplete = false;
                        Globals.nLastNodeID = 0;
                        break;
                    }

                    if (TrafficControl.IsInterruptedTraffic())
                    {
                        logger.Info("@@@@@@@@@@ [Traffic] " + Globals.HMISystem.SystemTraffic + " [ACStoHMI] " + Globals.HMISystem.ACStoHMI_TrafficPause + " [HMItoACS] " + Globals.HMISystem.HMItoACS_TrafficPause);
                        Globals.nLastNodeID = 0;
                        break;
                    }

                    DateTime EndTime = DateTime.Now;
                    long elapsedTicks = EndTime.Ticks - StartTime.Ticks;
                    TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);
                    if (elapsedSpan.TotalSeconds >= 10)
                    {
                        logger.Info("@@@@@@@@@@ [TIMER] ALARM");

                        Globals.nLastNodeID = 0;
                        Globals.HMISystem.MoveComplete = false;
                        Globals.moveComplete = false;
                        Globals.currentWork.alarm = Alarms.ALARM_MOVE_COMPLETE_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1목적지미도착/C1  에    러 !]";
                        break;
                    }

                    Thread.Sleep(100);
                }


            }
            return Globals.HMISystem.MoveComplete;
        }
        //형우과장님 한화 버젼 적용 kdy 23.10.17
        //public static void GetCurrentNode()
        //{
        //    try
        //    {
        //        if (Globals.NavigationSystem.CurrentNode != 0 && Globals.NavigationSystem.CurrentLink != 0)
        //        {
        //            Globals.nChangeNodeMode = 2;
        //            Globals.toTmpNode = 0;
        //            Globals.bNoRocFromFirmware = false;

        //            if (Globals.currentWork.curNode != Globals.NavigationSystem.CurrentNode)
        //            {
        //                logger.Info("   Current Node Change [curNode] " + Globals.NavigationSystem.CurrentNode + " [curLink] " + Globals.NavigationSystem.CurrentLink + "[NAV_X] " + Globals.NavigationSystem.NAV_X + " [NAV_Y] " + Globals.NavigationSystem.NAV_Y + " [NAV_PHI] " + Globals.NavigationSystem.NAV_PHI + " [NAVI_STATE]" + Globals.NavigationSystem.ModeState);
        //            }

        //            Globals.curTmpNode = Globals.NavigationSystem.CurrentNode;
        //            Globals.curTmpLink = Globals.NavigationSystem.CurrentLink;

        //            Globals.currentWork.curNode = Globals.NavigationSystem.CurrentNode;
        //            Globals.currentWork.curLink = Globals.NavigationSystem.CurrentLink;
        //        }
        //        else
        //        {
        //            Globals.toTmpNode = 0;
        //            Globals.nChangeNodeMode = 0;
        //            if (Globals.bNodeLinkCheck && Globals.NavigationSystem.NAV_X != 0 && Globals.NavigationSystem.NAV_Y != 0)
        //            {
        //                Globals.nChangeNodeMode = 3;
        //                logger.Info("   Start Current Rocation using Func");
        //                Astar.FindCurrenPosition(Globals.NavigationSystem.NAV_X, Globals.NavigationSystem.NAV_Y, Globals.NavigationSystem.NAV_PHI);
        //                Globals.currentWork.curNode = Astar.GetCurrentNode();
        //                Globals.currentWork.curLink = Astar.GetCurrentLink();
        //                // Astar.GetCurrentNodeAndLink(Globals.NavigationSystem.NAV_X, Globals.NavigationSystem.NAV_Y, Globals.NavigationSystem.NAV_PHI, ref Globals.currentWork.curNode, ref Globals.currentWork.curLink);

        //                logger.Info("   Get Current Rocation using Func [curNode] " + Globals.currentWork.curNode + " [curLink] " + Globals.currentWork.curLink + " [NAV_curNode] " + Globals.NavigationSystem.CurrentNode + " [NAV_curLink] " + Globals.NavigationSystem.CurrentLink + " [NAVI_STATE]" + Globals.NavigationSystem.ModeState);

        //                Globals.bNodeLinkCheck = false;

        //                Globals.curTmpNode = Globals.currentWork.curNode;
        //                Globals.curTmpLink = Globals.currentWork.curLink;
        //            }
        //            // 주행 완료치면 타겟노드 강제로 현재노드로 박기
        //            else if (Globals.currentWork.workState == (byte)WorkState.MOVE_COMPLETE ||
        //                    Globals.currentWork.workState == (byte)WorkState.LOAD_COMPLETE ||
        //                    Globals.currentWork.workState == (byte)WorkState.UNLOAD_COMPLETE)
        //            {
        //                if (Globals.nLastNodeID != 0)
        //                {
        //                    Globals.currentWork.curNode = (ushort)Globals.nLastNodeID;
        //                    Globals.currentWork.curLink = (ushort)Globals.nLastLinkID;

        //                    Globals.curTmpNode = Globals.currentWork.curNode;
        //                    Globals.curTmpLink = Globals.currentWork.curLink;

        //                    logger.Info("   Current Node Change when Work Complete [nLastNodeID]" + Globals.nLastNodeID + "[nLastLinkID]" + Globals.nLastLinkID);

        //                    Globals.nLastNodeID = 0;
        //                    Globals.nLastLinkID = 0;
        //                }
        //            }
        //            else
        //            {
        //                Globals.nChangeNodeMode = 4;
        //                if (Globals.currentWork.curNode != Globals.curTmpNode)
        //                {
        //                    logger.Info("   Current Node Change [curTmpNode]" + Globals.curTmpNode + " [curTmpLink]" + Globals.curTmpLink);
        //                    Globals.nCorrectMoving = 0;
        //                }
        //                Globals.currentWork.curNode = Globals.curTmpNode;
        //                Globals.currentWork.curLink = Globals.curTmpLink;
        //            }
        //            Globals.bNoRocFromFirmware = true;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        logger.Error("   Get Current Node Error [NAV_X] " + Globals.NavigationSystem.NAV_X +
        //                                       " [NAV_Y] " + Globals.NavigationSystem.NAV_Y);
        //        logger.Error(e.ToString());
        //        Globals.currentWork.alarm = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
        //        Globals.currentWork.alarm_name = "![000/E5151/C1현 재 위 치 /C1  에    러 !]";
        //    }
        //}
        // 노드 강제 변환 코드 넣기 전 함수 kdy 23.10.17
        public static void GetCurrentNode()
        {
            try
            {
                //작업완료되면 목표노드를 현재노드로 강제
                //if (Globals.toTmpNode != 0 && Globals.currentWork.workQueue.Count == 0)
                //{
                //    Globals.nChangeNodeMode = 1;
                //    if (Globals.currentWork.curNode != Globals.toTmpNode)
                //        logger.Info("   Force Set Current Rocation [curNode]" + Globals.currentWork.curNode + " [toNode]" + Globals.toTmpNode);
                //    Globals.currentWork.curNode = Globals.toTmpNode;
                //    Globals.curTmpNode = Globals.toTmpNode;
                //}
                //else
                if (Globals.NavigationSystem.CurrentNode != 0 && Globals.NavigationSystem.CurrentLink != 0)
                {
                    Globals.nChangeNodeMode = 2;
                    Globals.bNoRocFromFirmware = false;
                    Globals.toTmpNode = 0;

                    if (Globals.currentWork.curNode != Globals.NavigationSystem.CurrentNode)
                    {
                        logger.Info("   Current Node Change [curNode] " + Globals.NavigationSystem.CurrentNode + " [curLink] " + Globals.NavigationSystem.CurrentLink + "[NAV_X] " + Globals.NavigationSystem.NAV_X + " [NAV_Y] " + Globals.NavigationSystem.NAV_Y + " [NAV_PHI] " + Globals.NavigationSystem.NAV_PHI);
                        Globals.nCorrectMoving = 0;
                    }

                    Globals.curTmpNode = Globals.NavigationSystem.CurrentNode;
                    Globals.curTmpLink = Globals.NavigationSystem.CurrentLink;

                    Globals.currentWork.curNode = Globals.NavigationSystem.CurrentNode;
                    Globals.currentWork.curLink = Globals.NavigationSystem.CurrentLink;
                }
                else //맨처음 제어에서 커런트노드, 커런트링크를 0으로 줄때 일로 빠짐
                {
                    Globals.toTmpNode = 0;
                    Globals.nChangeNodeMode = 0;

                    if (Globals.bNodeLinkCheck && Globals.NavigationSystem.NAV_X != 0 && Globals.NavigationSystem.NAV_Y != 0) // x,y값이 0이 아닌 어떠한 값이라도 들어오는지 확인
                    {
                        Globals.nChangeNodeMode = 3;
                        logger.Info("   Start Current Rocation using Func");
                        Astar.FindCurrenPosition(Globals.NavigationSystem.NAV_X, Globals.NavigationSystem.NAV_Y, Globals.NavigationSystem.NAV_PHI);
                        Globals.currentWork.curNode = Astar.GetCurrentNode();
                        Globals.currentWork.curLink = Astar.GetCurrentLink();
                        // Astar.GetCurrentNodeAndLink(Globals.NavigationSystem.NAV_X, Globals.NavigationSystem.NAV_Y, Globals.NavigationSystem.NAV_PHI, ref Globals.currentWork.curNode, ref Globals.currentWork.curLink);

                        logger.Info("   Get Current Rocation using Func [curNode] " + Globals.currentWork.curNode + " [curLink] " + Globals.currentWork.curLink + " [NAVI_curNode] " + Globals.NavigationSystem.CurrentNode + " [NAVI_curLink] " + Globals.NavigationSystem.CurrentLink);

                        Globals.bNodeLinkCheck = false;

                        Globals.curTmpNode = Globals.currentWork.curNode;
                        Globals.curTmpLink = Globals.currentWork.curLink;
                    }
                    else
                    {
                        Globals.nChangeNodeMode = 4;
                        if (Globals.currentWork.curNode != Globals.curTmpNode)
                        {
                            logger.Info("   Current Node Change [curTmpNode]" + Globals.curTmpNode + " [curTmpLink]" + Globals.curTmpLink);
                            Globals.nCorrectMoving = 0;
                        }
                        Globals.currentWork.curNode = Globals.curTmpNode;
                        Globals.currentWork.curLink = Globals.curTmpLink;
                    }
                    Globals.bNoRocFromFirmware = true;
                }
            }
            catch (Exception e)
            {
                logger.Error("   Get Current Node Error [Mode] " + Globals.nChangeNodeMode +
                                               " [NAV_X] " + Globals.NavigationSystem.NAV_X +
                                               " [NAV_Y] " + Globals.NavigationSystem.NAV_Y);
                logger.Error(e.ToString());
                Globals.currentWork.alarm = Alarms.ALARM_SYSTEM_UNKNOWN_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1현 재 위 치 /C1  에    러 !]";
            }
        }


        public static void SetCurrentNode()
        {
            if (SerialPortBoard.SendSetCurrentNode((ushort)Globals.currentWork.curNode, (ushort)Globals.currentWork.curLink))
            {
                logger.Info("   Set CurrentNode [CURNODE] " + Globals.currentWork.curNode + " [CURLINK] " + Globals.currentWork.curLink);
            }
        }

        //public static void GetNextNode()
        //{
        //    try
        //    {
        //        if (Globals.currentWork.getNextNodePath != null)
        //        {

        //            if (Globals.currentWork.getNextNodePath.Count > 0)
        //            {
        //                //if (isCalcCurentLocationeHMI())
        //                //{
        //                //    Globals.currentWork.realPath.RemoveAt(0);
        //                //}

        //                if (Globals.currentWork.workState == (byte)WorkState.READY)
        //                {
        //                    Globals.currentWork.nextNode = 0;
        //                }
        //                else
        //                {                         

        //                    for (int i = Globals.currentWork.getNextNodePath.Count - 1; i >= 0; i--)
        //                    {
        //                        //if (Globals.currentWork.realPath[i].GetLinkNumber() == Globals.currentWork.curLink)
        //                        if (Globals.currentWork.getNextNodePath[i].GetParentNodeID() == Globals.currentWork.curNode)
        //                        {
        //                            Globals.currentWork.nextNode = (ushort)Globals.currentWork.getNextNodePath[i].GetCurrentNodeID();
        //                            if (Globals.currentWork.nextNode == Globals.currentWork.curNode && i > 0)
        //                            {
        //                                Globals.currentWork.nextNode = (ushort)Globals.currentWork.getNextNodePath[i - 1].GetCurrentNodeID();
        //                            }
        //                            Globals.currentWork.getNextNodePath.RemoveAt(i);
        //                            break;
        //                        }
        //                    }
        //                }
        //            }
        //        }


        //    }
        //    catch (Exception e)
        //    {
        //        logger.Error("   GetNextNode Error : " + e.ToString());
        //    }


        //}
        public static void GetNextNode()
        {
            try
            {
                if (Globals.currentWork.getNextNodePath != null)
                {

                    if (Globals.currentWork.getNextNodePath.Count > 0)
                    {
                        //if (isCalcCurentLocationeHMI())
                        //{
                        //    Globals.currentWork.realPath.RemoveAt(0);
                        //}


                        if (Globals.currentWork.workState == (byte)WorkState.READY)
                        {
                            Globals.currentWork.nextNode = 0;
                        }
                        else
                        {

                            for (int i = Globals.currentWork.getNextNodePath.Count - 1; i >= 0; i--)
                            {
                                //if (Globals.currentWork.realPath[i].GetLinkNumber() == Globals.currentWork.curLink)
                                if (Globals.currentWork.getNextNodePath[i].GetParentNodeID() == Globals.currentWork.curNode && Globals.curNodeforNextNode != Globals.currentWork.curNode)
                                {
                                    Globals.currentWork.nextNode = (ushort)Globals.currentWork.getNextNodePath[i].GetCurrentNodeID();

                                    Globals.curNodeforNextNode = Globals.currentWork.curNode;

                                    if (Globals.currentWork.nextNode == Globals.currentWork.curNode && i > 0)
                                    {
                                        Globals.currentWork.nextNode = (ushort)Globals.currentWork.getNextNodePath[i - 1].GetCurrentNodeID();
                                    }
                                    Globals.currentWork.getNextNodePath.RemoveAt(i);
                                    break;
                                }
                            }
                        }
                    }
                }


            }
            catch (Exception e)
            {
                logger.Error("   GetNextNode Error : " + e.ToString());
            }


        }
        public static void GenerateVirtualLink(int VehicleX, int VehicleY, float VehicleAng, int PalletX, int PalletY, float PalletAng, ref Point Target1, ref Point WorldPallet, ref Point Target2)
        {
            Point Start_Point = new Point();

            double dx, dy = 0;

            dx = (Math.Cos(VehicleAng * Math.PI / 180) * PalletX - Math.Sin(VehicleAng * Math.PI / 180) * PalletY);
            dy = (Math.Sin(VehicleAng * Math.PI / 180) * PalletX + Math.Cos(VehicleAng * Math.PI / 180) * PalletY);

            if (Globals.currentWork.frontRetry < 1)
            {
                if (dy < 0)
                    dy -= 1000;
                else if (dy > 0)
                    dy += 1000;
            }

            int World_Pallet_X = (int)(VehicleX + dx);
            int World_Pallet_Y = (int)(VehicleY + dy);


            float World_Pallet_Ang = VehicleAng + PalletAng;

            if (World_Pallet_Ang > 180)
                World_Pallet_Ang -= 360;
            else if (World_Pallet_Ang < -180)
                World_Pallet_Ang += 360;

            World_Pallet_Ang = (float)(World_Pallet_Ang * Math.PI / 180);

            double d = 0;

            PalletAng = Math.Abs(PalletAng);

            //if (PalletAng > 4.0 && PalletAng < 7.5)
            //    d = 3300; //6300 5500
            //else if (PalletAng > 7.5 && PalletAng < 10.0)
            //    d = 3500; // 6000
            //else if (PalletAng > 10.0 && PalletAng < 15.0)
            //    d = 4000; // 7000
            //else if (PalletAng > 15.0)
            //    d = 4300;
            d = 5599;

            double fIncrease1 = Math.Tan(-World_Pallet_Ang);
            double fConstant1 = World_Pallet_Y - fIncrease1 * World_Pallet_X;

            double m1 = fIncrease1;
            double a1 = 1 + (m1 * m1);
            double b1 = 2 * fConstant1 * m1 - 2 * World_Pallet_Y * m1 - 2 * World_Pallet_X;
            double c1 = fConstant1 * fConstant1 + World_Pallet_Y * World_Pallet_Y + World_Pallet_X * World_Pallet_X - 2 * fConstant1 * World_Pallet_Y - d * d;

            Point tmpNodeChange1 = new Point();
            Point tmpNodeChange2 = new Point();

            tmpNodeChange1.X = (int)((-b1 + Math.Sqrt(b1 * b1 - 4 * a1 * c1)) / (2 * a1));
            tmpNodeChange1.Y = (int)(fIncrease1 * tmpNodeChange1.X + fConstant1);
            tmpNodeChange2.X = (int)((-b1 - Math.Sqrt(b1 * b1 - 4 * a1 * c1)) / (2 * a1));
            tmpNodeChange2.Y = (int)(fIncrease1 * tmpNodeChange2.X + fConstant1);

            float tmpdis1 = (float)Math.Sqrt(Math.Pow(tmpNodeChange1.X - VehicleX, 2) + Math.Pow(tmpNodeChange1.Y - VehicleY, 2));
            float tmpdis2 = (float)Math.Sqrt(Math.Pow(tmpNodeChange2.X - VehicleX, 2) + Math.Pow(tmpNodeChange2.Y - VehicleY, 2));

            if (tmpdis1 > tmpdis2)
                Start_Point = tmpNodeChange2;
            else
                Start_Point = tmpNodeChange1;

            Target1 = Start_Point;


            d = 2700;

            m1 = fIncrease1;
            a1 = 1 + (m1 * m1);
            b1 = 2 * fConstant1 * m1 - 2 * World_Pallet_Y * m1 - 2 * World_Pallet_X;
            c1 = fConstant1 * fConstant1 + World_Pallet_Y * World_Pallet_Y + World_Pallet_X * World_Pallet_X - 2 * fConstant1 * World_Pallet_Y - d * d;

            tmpNodeChange1 = new Point();
            tmpNodeChange2 = new Point();

            tmpNodeChange1.X = (int)((-b1 + Math.Sqrt(b1 * b1 - 4 * a1 * c1)) / (2 * a1));
            tmpNodeChange1.Y = (int)(fIncrease1 * tmpNodeChange1.X + fConstant1);
            tmpNodeChange2.X = (int)((-b1 - Math.Sqrt(b1 * b1 - 4 * a1 * c1)) / (2 * a1));
            tmpNodeChange2.Y = (int)(fIncrease1 * tmpNodeChange2.X + fConstant1);

            tmpdis1 = (float)Math.Sqrt(Math.Pow(tmpNodeChange1.X - VehicleX, 2) + Math.Pow(tmpNodeChange1.Y - VehicleY, 2));
            tmpdis2 = (float)Math.Sqrt(Math.Pow(tmpNodeChange2.X - VehicleX, 2) + Math.Pow(tmpNodeChange2.Y - VehicleY, 2));

            if (tmpdis1 > tmpdis2)
                Start_Point = tmpNodeChange2;
            else
                Start_Point = tmpNodeChange1;

            Target2 = Start_Point;

            WorldPallet.X = World_Pallet_X;
            WorldPallet.Y = World_Pallet_Y;
            // linkoption 4 
        }

    }
}

