﻿
//using MapDLL;
using EditorLibrary;
using MobyusTouch.src.Communication;
using MobyusTouch.src.Control;
using MobyusTouch.src.Control.Sub;
using MobyusTouch.src.Global;
using MobyusTouch.src.Main.Sub;
using NLog;
using System;
using System.Runtime.Remoting.Messaging;
using System.Threading;


namespace MobyusTouch.src.Main
{
    class MainControl
    {
        private static Logger logger = LogManager.GetLogger("MainControl");

        private int onThreadSyncCnt = 0;
        private int workCnt = 0;
        private bool doNextWork = false;
        public bool isThreadRun = false;
        

        public MainControl()
        {
            Globals.preSystemMode = SystemMode.OFFLINE;
            Globals.HMISystem.SystemMode = SystemMode.OFFLINE;
            Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_FIRMWARE_NONE;
            Globals.HMISystem.ACStoHMI_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_ACS_NONE;
            Globals.currentWork.workType = Globals.TRANSFER_READY;

            Thread mainProcess = new Thread(MainThread);
            isThreadRun = true;
            mainProcess.Start();
        }

        public void Close()
        {
            isThreadRun = false;

            CanForklift.Disconnect();
            TcpServerFMS.Disconnect();
            SerialPortBoard.Disconnect();
        }

        public void CallbackResult(IAsyncResult iAsyncResult)
        {
           //Monitor.Enter(Globals.callbackLock);
            try
            {
                bool resultWork = false;
                WorkStep workStep = new WorkStep();

                AsyncResult async = (AsyncResult)iAsyncResult;
                workStep = (WorkStep)async.AsyncState;
                resultWork = workStep.function.EndInvoke(iAsyncResult);
                logger.Info("[CALLBACK] [CURRENT]" + workStep.name + " resultWork : " + resultWork);


                lock (this)
                {
                    onThreadSyncCnt--;
                    workCnt++;
                }

                if (Globals.HMISystem.SystemTraffic == SystemTraffic.TRANSFER_TRAFFIC_STOP)
                {
                    logger.Info("<<<<[END] CANCEL WORK" + " [WORK]" + Globals.currentWork.ToString());
                    Thread.Sleep(1000);
                    Globals.HMISystem = new HMISystem(Globals.HMISystem.SystemMode, SystemTraffic.TRANSFER_TRAFFIC_ACS_NONE);
                    Globals.currentWork.Clear();
                }
                else
                {
                    if (resultWork)
                    {
                        Globals.currentWork.workQueueCompleted.Enqueue(workStep);

                        while (workStep.isSynconized == true)
                        {
                            if (Globals.currentWork.workQueue == null)
                            {
                                break;
                            }

                            if (TrafficControl.IsInterruptedTraffic())
                            {
                                break;
                            }

                            //logger.Info("[CURRENT]" + workStep.name + "onThreadSyncCnt : " + onThreadSyncCnt + " Count: " + Globals.currentWork.workQueue.Count);
                            if (Globals.currentWork.workQueue != null && Globals.currentWork.workQueue.Count > 0 && onThreadSyncCnt == 0)
                            {
                                lock (this)
                                {
                                    Globals.currentWork.workQueue.Dequeue();
                                    doNextWork = true;
                                }
                                break;
                            }
                            Thread.Sleep(100);
                        }

                    }
                    else
                    {
                        logger.Info("[ERROR] [CALLBACK] [CURRENT]" + workStep.name + " [ALARM]" + Globals.currentWork.alarm);
                        doNextWork = false;
                    }

                    if (Globals.bRetry_FirstPathRetry && workStep.name == "WORK_MOVE_COMPLETE")
                    {
                        Globals.bRetry_FirstMoveComplete = true;
                        //logger.Info("   [Globals.bFirstMoveComplete] " + Globals.bRetry_FirstMoveComplete);
                    }

                    logger.Info("<<<<[END]" + workStep.name + " [WORK_CNT]" + workCnt + " [RESULT]" + resultWork + " [ALARM]" + Globals.currentWork.alarm + " [MODE]" + Globals.HMISystem.SystemMode
                         + " [ThreadID]" + Thread.CurrentThread.ManagedThreadId + " [STEP]" + workStep.step + " [VALUE]" + workStep.value + " [SYNC]" + workStep.isSynconized
                        + " [NEXT]" + doNextWork + " [AFL_STATE]" + Globals.currentWork.workState + " [NAVI_STATE]" + Globals.NavigationSystem.ModeState
                        + " [STATE_UNIT]" + Globals.currentWork.workStateUnit + " [CUR_NODE]" + Globals.currentWork.curNode + " [NEXT_NODE]" + Globals.currentWork.nextNode
                        + " [TO_NODE]" + Globals.currentWork.toNode + " [NAV_X]" + Globals.NavigationSystem.NAV_X + " [NAV_Y]" + Globals.NavigationSystem.NAV_Y
                        + " [NAV_PHI]:" + Globals.NavigationSystem.NAV_PHI + " [LIFT]" + Globals.ForkLiftSystem.LiftPosition.ToString() + "[TILT]" + Globals.ForkLiftSystem.TiltPosition
                        + "[SIDESHIFT]" + ((Globals.ForkLiftSystem.ShiftRightPosition - Globals.ForkLiftSystem.ShiftLeftPosition) / 2).ToString()
                        + "[REACH]" + Globals.ForkLiftSystem.ReachPosition.ToString());
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString() + "[ERROR]");
            }
            //finally
            //{
            //    Monitor.Exit(Globals.callbackLock);
            //}
        }

        private void MainThread()
        {
            while (isThreadRun)
            {
                if (Globals.HMISystem.SystemMode == SystemMode.AUTO || Globals.HMISystem.SystemMode == SystemMode.SEMI_AUTO)
                {
                    switch (Globals.currentWork.workType)
                    {
                        case Globals.TRANSFER_LOAD:
                        case Globals.TRANSFER_UNLOAD:
                        case Globals.TRANSFER_MOVE:

                            if (doNextWork && (Globals.HMISystem.SystemTraffic == SystemTraffic.TRANSFER_TRAFFIC_FIRMWARE_NONE || Globals.HMISystem.SystemTraffic == SystemTraffic.TRANSFER_TRAFFIC_RESUME))
                            {
                                if (Globals.currentWork.workQueue != null && Globals.currentWork.workQueue.Count > 0)
                                {
                                    doNextWork = false;
                                    onThreadSyncCnt++;
                                    Globals.workStep = Globals.currentWork.workQueue.Peek();
                                    Globals.workStep.function.BeginInvoke(Globals.workStep, this.CallbackResult, Globals.workStep);
                                    if (Globals.workStep.isSynconized == false)
                                    {
                                        Globals.currentWork.workQueue.Dequeue();
                                        doNextWork = true;
                                    }
                                }
                            }

                            DriveControl.IsMoveComplete();
                            StateControl.UpdateWorkState();
                            StateControl.UpdateWorkStateUnit();
                            break;

                        case Globals.TRANSFER_READY:
                            doNextWork = true;
                            onThreadSyncCnt = 0;
                            workCnt = 0;
                            if (Globals.currentWork.curNode == 0)
                            {
                                //DriveControl.SetCurrentNode();
                            }
                            break;
                    }
                }

                DriveControl.GetCurrentNode();
                DriveControl.GetNextNode();
                StateControl.UpdateLEDState();
                //StateControl.UpdateManualLoaded();
                StateControl.GetChargingState();
                ModeControl.IsChangedSystemMode();

                ModeControl.IsInterruptedAlarm();
                ModeControl.IsReleaseCheck();
                ModeControl.PushButton(); // 1129 kdy 버튼 누를시 모드 변경
                TrafficControl.IsInterruptedTrafficRemote();
                StateControl.FirmWareLog(); // 펌웨어 로그, 배터리 로그, 2md이후 카메라 데이터 로그

                Thread.Sleep(100);
                //logger.Info("@[MODE]" + Globals.HMISystem.SystemMode + " [TRAFFIC]" + Globals.HMISystem.SystemTraffic + " [STATE]" + Globals.currentWork.workState + " [WORKTYPE]" + Globals.currentWork.workType + " [STEP]" + Globals.workStep.step + " [NEXT]" + doNextWork + " [LEVEL]" + Globals.currentWork.workLevel + " [NAVISTATE]" + Globals.NavigationSystem.ModeState + " [CURNODE]" + Globals.currentWork.curNode + " [NEXTNODE]" + Globals.currentWork.nextNode + " [TONODE]" + Globals.currentWork.toNode);
            }
        }
    }
}