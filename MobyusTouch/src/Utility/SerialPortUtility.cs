﻿using System;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using NLog;

namespace MobyusTouch
{
    public class SerialPortUtility
    {
        private static Logger logger = LogManager.GetLogger("SerialPortUtility");

        private static SerialPort _serialPort;
        static Thread _readThread;
        static volatile bool _keepReading;

        public static bool IsOpen()
        {
            bool isOpen = false;
            if(_serialPort!= null)
            {
                isOpen = _serialPort.IsOpen;
            }
            return isOpen;
        }

        public delegate void EventHandler(byte[] param);
        public static EventHandler DataReceived;


        private static void StartReading()
        {
            if (!_keepReading)
            {
                _keepReading = true;
                _readThread = new Thread(ReadPort);
                _readThread.Start();
            }
        }

        private static void StopReading()
        {
            if (_keepReading)
            {
                _keepReading = false;
                _readThread.Join();
                _readThread = null;
            }
        }

        private static void ReadPort()
        {
            while (_keepReading)
            {
                if (_serialPort.IsOpen)
                {
                    byte[] readBuffer = new byte[_serialPort.ReadBufferSize + 1];
                    try
                    {
                        int count = _serialPort.BytesToRead;
                        byte[] data = new byte[count];
                        _serialPort.Read(data, 0, count);

                        if (data.Length > 0)
                            DataReceived(data);
                    }
                    catch (Exception te)
                    {
                        logger.Info(te.Message);
                    }
                }
                else
                {
                    TimeSpan waitTime = new TimeSpan(0, 0, 0, 0, 10);
                    Thread.Sleep(waitTime);
                }
            }
        }

        public static bool Open(string portName, int baudRate, int dataBits)
        {
            bool result = true;

            try
            {
                if (_serialPort == null)
                {
                    _serialPort = new SerialPort();
                    _readThread = null;
                    _keepReading = false;
                }
                _serialPort.PortName = portName;
                _serialPort.BaudRate = baudRate;
                _serialPort.Parity = Parity.None;
                _serialPort.DataBits = dataBits;
                _serialPort.StopBits = StopBits.One;
                _serialPort.Handshake = Handshake.None;

                // Set the read/write timeouts
                _serialPort.ReadTimeout = 100;
                _serialPort.WriteTimeout = 100;

                if (SerialPort.GetPortNames().Length > 0)
                {
                    _serialPort.Open();
                    if (_serialPort.IsOpen)
                    {
                        StartReading();
                    }
                }
                else
                {
                    result = false;
                    logger.Info(String.Format("{0} already in use", _serialPort.PortName));
                }
            }
            catch(Exception ex)
            {
                result = false;
            }
            return result;
        }

        public static void Close()
        {
            StopReading();
            _serialPort.Close();
        }

        public static void Send(byte[] data, int length)
        {
            if (IsOpen())
            {
                _serialPort.Write(data, 0, length);
                //logger.Fatal(" 70 Send [data] " + data + " [length] " + length);
            }
        }
    }
}
