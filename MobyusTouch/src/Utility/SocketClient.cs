﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using NLog;

namespace MobyusTouch.src.Utility
{
    public delegate void ClientHandlePacketData(byte[] data, int bytesRead);

    public class NetworkBuffer
    {
        public byte[] WriteBuffer;
        public byte[] ReadBuffer;
        public int CurrentWriteByteCount;
    }


    public class SocketClient
    {
        private static Logger logger = LogManager.GetLogger("SocketClient");

        public event ClientHandlePacketData OnDataReceived;

        private TcpClient tcpClient;
        private NetworkStream clientStream;
        private NetworkBuffer buffer;
        private int writeBufferSize = 1024;
        private int readBufferSize = 1024;
        private bool started = false;
        AutoResetEvent autoEvent = new AutoResetEvent(true);

        public SocketClient()
        {
            buffer = new NetworkBuffer();
            buffer.WriteBuffer = new byte[writeBufferSize];
            buffer.ReadBuffer = new byte[readBufferSize];
            buffer.CurrentWriteByteCount = 0;
        }

        public void ConnectToServer(string ipAddress, int port)
        {
            try
            {
                tcpClient = new TcpClient();
                IAsyncResult result = tcpClient.BeginConnect(ipAddress, port, null, null);

                bool success = result.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(1));
                if (success)
                {
                    clientStream = tcpClient.GetStream();

                    Thread t = new Thread(new ThreadStart(ListenForPackets));
                    started = true;
                    t.Start();
                }
            }
            catch (Exception ex)
            {
                tcpClient.Close();
                logger.Info("TCP Connection Fail" + ex.ToString());
                throw ex;
            }

        }

        /// <summary>
        /// This method runs on its own thread, and is responsible for
        /// receiving data from the server and raising an event when data
        /// is received
        /// </summary>
        private void ListenForPackets()
        {
            int bytesRead;
            int count = 0;
            while (started)
            {
                bytesRead = 0;

                try
                {
                    //Blocks until a message is received from the server
                    bytesRead = clientStream.Read(buffer.ReadBuffer, 0, readBufferSize);
                }
                catch
                {
                    //A socket error has occurred
                    count++;
                    logger.Info("A socket error has occurred with the client socket " + tcpClient.ToString());
                    if(count > 30)
                    {
                        count = 0;
                        break;
                    }
                    
                }

                if (bytesRead == 0)
                {
                    count++;
                    logger.Info("bytesRead data is 0 " + tcpClient.ToString());
                    //The server has disconnected
                    if (count > 30)
                    {
                        count = 0;
                        break;
                    }
                }

                if (OnDataReceived != null)
                {
                    //Send off the data for other classes to handle
                    OnDataReceived(buffer.ReadBuffer, bytesRead);
                }

                Thread.Sleep(15);
            }

            started = false;
            Disconnect();
        }

        /// <summary>
        /// Adds data to the packet to be sent out, but does not send it across the network
        /// </summary>
        /// <param name="data">The data to be sent</param>
        public void AddToPacket(byte[] data)
        {
            if (buffer.CurrentWriteByteCount + data.Length > buffer.WriteBuffer.Length)
            {
                FlushData();
            }

            Array.ConstrainedCopy(data, 0, buffer.WriteBuffer, buffer.CurrentWriteByteCount, data.Length);
            buffer.CurrentWriteByteCount += data.Length;
        }

        /// <summary>
        /// Flushes all outgoing data to the server
        /// </summary>
        public void FlushData()
        {
            clientStream.Write(buffer.WriteBuffer, 0, buffer.CurrentWriteByteCount);
            clientStream.Flush();
            buffer.CurrentWriteByteCount = 0;
        }

        /// <summary>
        /// Sends the byte array data immediately to the server
        /// </summary>
        /// <param name="data"></param>
        public void SendImmediate(byte[] data)
        {
            AddToPacket(data);
            FlushData();
        }

        /// <summary>
        /// Tells whether we're connected to the server
        /// </summary>
        /// <returns></returns>
        public bool IsConnected()
        {
            return tcpClient!= null && started && tcpClient.Connected;
        }

        /// <summary>
        /// Disconnect from the server
        /// </summary>
        public void Disconnect()
        {
            if (tcpClient == null)
            {
                return;
            }

            tcpClient.Close();

            started = false;
        }
    }
}
