﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json;

namespace MobyusTouch
{
    public static class Restful
    {

        public static void POST(string url)
        { 
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            
            //UTF8Encoding encoding = new UTF8Encoding();
            //Byte[] byteArray = encoding.GetBytes(jsonContent);

            string param = "AA";
            string postString = string.Format("forkliftid={0}", param);

            request.Method = "POST";
            request.ContentLength = postString.Length;
            request.ContentType = @"application/x-www-form-urlencoded";

            StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());
            requestWriter.Write(postString);
            requestWriter.Close();

            try
            {
                // JSON Obaject Parsing
                //string obj = "{\"WORK_PLS_CD\":\"W0001\",\"ST_CROSS\":\"C0-00\",\"END_CROSS\":\"C0-01\",\"ST_CROSS_X\":0,\"ST_CROSS_Y\":0.0,\"END_CROSS_X\":0,\"END_CROSS_Y\":2.75,\"DIST\":2.75,\"MAX_SPEED\":1,\"MIN_SPEED\":1,\"RATIO\":1,\"VALID_ST_DT\":\"2018.1.1\",\"VALID_END_DT\":\"9999.12.31\",\"CRE_DTTM\":\"2018.1.1\",\"CRE_USR_ID\":\"admin\",\"UPD_DTTM\":null,\"UPD_USR_ID\":null,\"USE_YN\":\"Y\"}";
                //TempObjects item = JsonConvert.DeserializeObject<TempObject>(obj);

                // JSON List Parsing
                //string objList = "{\"TempObject\":[{\"WORK_PLS_CD\":\"W0001\",\"ST_CROSS\":\"C0-00\",\"END_CROSS\":\"C0-01\",\"ST_CROSS_X\":0,\"ST_CROSS_Y\":0.0,\"END_CROSS_X\":0,\"END_CROSS_Y\":2.75,\"DIST\":2.75,\"MAX_SPEED\":1,\"MIN_SPEED\":1,\"RATIO\":1,\"VALID_ST_DT\":\"2018.1.1\",\"VALID_END_DT\":\"9999.12.31\",\"CRE_DTTM\":\"2018.1.1\",\"CRE_USR_ID\":\"admin\",\"UPD_DTTM\":null,\"UPD_USR_ID\":null,\"USE_YN\":\"Y\"},{\"WORK_PLS_CD\":\"W0001\",\"ST_CROSS\":\"C0-00\",\"END_CROSS\":\"C1-00\",\"ST_CROSS_X\":0,\"ST_CROSS_Y\":0.0,\"END_CROSS_X\":7,\"END_CROSS_Y\":0.0,\"DIST\":7.0,\"MAX_SPEED\":1,\"MIN_SPEED\":1,\"RATIO\":1,\"VALID_ST_DT\":\"2018.1.1\",\"VALID_END_DT\":\"9999.12.31\",\"CRE_DTTM\":\"2018.1.1\",\"CRE_USR_ID\":\"admin\",\"UPD_DTTM\":null,\"UPD_USR_ID\":null,\"USE_YN\":\"Y\"}]}";
                //TempObjects item = JsonConvert.DeserializeObject<TempObjects>(objList);

                StreamReader responseReader = new StreamReader(request.GetResponse().GetResponseStream());
                string responseData = responseReader.ReadToEnd();

                //TempObjects item = JsonConvert.DeserializeObject<TempObjects>(responseData);
                //MessageBox.Show(String.Format("{0}", item.TempObject[0].WORK_PLS_CD )); 
                
                //MessageBox.Show(TempObject.WORK_PLS_CD);

                responseReader.Close();
                request.GetResponse().Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("[ERROR]" + ex.ToString());
            }
                            
        }

        public static class RequestConstants
        {
            public const string UserAgent = "User-Agent";
            public const string UserAgentValue = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36";
        }


        public static string GetReleases(string url)
        {
            var client = new WebClient();
            client.Headers.Add(RequestConstants.UserAgent, RequestConstants.UserAgentValue);

            var response = client.DownloadString(url);

            MessageBox.Show(response.Substring(0,10));

            return response;
        }

        public static string GetReleases2(string url)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add(RequestConstants.UserAgent, RequestConstants.UserAgentValue);

                var response = httpClient.GetStringAsync(new Uri(url)).Result;

                return response;
            }
        }
    }
}
