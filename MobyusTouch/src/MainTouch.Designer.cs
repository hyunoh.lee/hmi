﻿namespace MobyusTouch
{
    partial class MainTouch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainTouch));
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonZoomUp = new System.Windows.Forms.Button();
            this.buttonZoomDown = new System.Windows.Forms.Button();
            this.StatusAlarm = new System.Windows.Forms.Button();
            this.StatusCAN = new System.Windows.Forms.Button();
            this.StatusSerial = new System.Windows.Forms.Button();
            this.buttonSetting = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.buttonMinimize = new System.Windows.Forms.Button();
            this.buttonAlarm = new System.Windows.Forms.Button();
            this.StatusFMS = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.buttonMap = new System.Windows.Forms.Button();
            this.MainLogo = new System.Windows.Forms.Panel();
            this.panelMain = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button8 = new System.Windows.Forms.Button();
            this.LoadedBtn = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.WorkUnitBtn = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.ForkCenterBtn = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.ObstacleBtn = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.TrafficBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.ChargeBtn = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.WorkBtn = new System.Windows.Forms.Button();
            this.BatteryBtn = new System.Windows.Forms.Button();
            this.ForkWidthBtn = new System.Windows.Forms.Button();
            this.TiltBtn = new System.Windows.Forms.Button();
            this.LiftBtn = new System.Windows.Forms.Button();
            this.SpeedBtn = new System.Windows.Forms.Button();
            this.SteerBtn = new System.Windows.Forms.Button();
            this.lableTopName = new System.Windows.Forms.Label();
            this.labelCurrentDate = new System.Windows.Forms.Label();
            this.touchMaintop = new System.Windows.Forms.Panel();
            this.buttonWorkOrder = new System.Windows.Forms.Button();
            this.buttonUnitOrder = new System.Windows.Forms.Button();
            this.buttonModeSelect = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.touchMaintop.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.buttonZoomUp);
            this.panel1.Controls.Add(this.buttonZoomDown);
            this.panel1.Controls.Add(this.StatusAlarm);
            this.panel1.Controls.Add(this.StatusCAN);
            this.panel1.Controls.Add(this.StatusSerial);
            this.panel1.Controls.Add(this.buttonSetting);
            this.panel1.Controls.Add(this.buttonExit);
            this.panel1.Controls.Add(this.buttonMinimize);
            this.panel1.Controls.Add(this.buttonAlarm);
            this.panel1.Controls.Add(this.StatusFMS);
            this.panel1.Controls.Add(this.button28);
            this.panel1.Controls.Add(this.buttonMap);
            this.panel1.Controls.Add(this.MainLogo);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(106, 800);
            this.panel1.TabIndex = 0;
            // 
            // buttonZoomUp
            // 
            this.buttonZoomUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonZoomUp.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonZoomUp.ForeColor = System.Drawing.Color.White;
            this.buttonZoomUp.Location = new System.Drawing.Point(2, 549);
            this.buttonZoomUp.Name = "buttonZoomUp";
            this.buttonZoomUp.Size = new System.Drawing.Size(100, 50);
            this.buttonZoomUp.TabIndex = 9;
            this.buttonZoomUp.Text = "확대";
            this.buttonZoomUp.UseVisualStyleBackColor = true;
            this.buttonZoomUp.Click += new System.EventHandler(this.buttonZoomUp_Click);
            // 
            // buttonZoomDown
            // 
            this.buttonZoomDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonZoomDown.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonZoomDown.ForeColor = System.Drawing.Color.White;
            this.buttonZoomDown.Location = new System.Drawing.Point(2, 600);
            this.buttonZoomDown.Name = "buttonZoomDown";
            this.buttonZoomDown.Size = new System.Drawing.Size(100, 50);
            this.buttonZoomDown.TabIndex = 8;
            this.buttonZoomDown.Text = "축소";
            this.buttonZoomDown.UseVisualStyleBackColor = true;
            this.buttonZoomDown.Click += new System.EventHandler(this.buttonZoomDown_Click);
            // 
            // StatusAlarm
            // 
            this.StatusAlarm.BackColor = System.Drawing.Color.DarkRed;
            this.StatusAlarm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StatusAlarm.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusAlarm.ForeColor = System.Drawing.Color.White;
            this.StatusAlarm.Location = new System.Drawing.Point(2, 362);
            this.StatusAlarm.Name = "StatusAlarm";
            this.StatusAlarm.Size = new System.Drawing.Size(100, 47);
            this.StatusAlarm.TabIndex = 7;
            this.StatusAlarm.Text = "ALARM";
            this.StatusAlarm.UseVisualStyleBackColor = false;
            // 
            // StatusCAN
            // 
            this.StatusCAN.BackColor = System.Drawing.Color.DarkRed;
            this.StatusCAN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StatusCAN.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusCAN.ForeColor = System.Drawing.Color.White;
            this.StatusCAN.Location = new System.Drawing.Point(2, 500);
            this.StatusCAN.Name = "StatusCAN";
            this.StatusCAN.Size = new System.Drawing.Size(100, 47);
            this.StatusCAN.TabIndex = 6;
            this.StatusCAN.Text = "CAN";
            this.StatusCAN.UseVisualStyleBackColor = false;
            // 
            // StatusSerial
            // 
            this.StatusSerial.BackColor = System.Drawing.Color.DarkRed;
            this.StatusSerial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StatusSerial.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusSerial.ForeColor = System.Drawing.Color.White;
            this.StatusSerial.Location = new System.Drawing.Point(2, 454);
            this.StatusSerial.Name = "StatusSerial";
            this.StatusSerial.Size = new System.Drawing.Size(100, 47);
            this.StatusSerial.TabIndex = 5;
            this.StatusSerial.Text = "SERIAL";
            this.StatusSerial.UseVisualStyleBackColor = false;
            // 
            // buttonSetting
            // 
            this.buttonSetting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.buttonSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSetting.Font = new System.Drawing.Font("Leelawadee UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSetting.ForeColor = System.Drawing.Color.Black;
            this.buttonSetting.Image = ((System.Drawing.Image)(resources.GetObject("buttonSetting.Image")));
            this.buttonSetting.Location = new System.Drawing.Point(3, 226);
            this.buttonSetting.Name = "buttonSetting";
            this.buttonSetting.Size = new System.Drawing.Size(98, 85);
            this.buttonSetting.TabIndex = 1;
            this.buttonSetting.Text = "Setting";
            this.buttonSetting.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonSetting.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonSetting.UseVisualStyleBackColor = false;
            this.buttonSetting.Click += new System.EventHandler(this.buttonSetting_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExit.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExit.ForeColor = System.Drawing.Color.White;
            this.buttonExit.Location = new System.Drawing.Point(2, 725);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(100, 73);
            this.buttonExit.TabIndex = 4;
            this.buttonExit.Text = "종료";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // buttonMinimize
            // 
            this.buttonMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMinimize.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMinimize.ForeColor = System.Drawing.Color.White;
            this.buttonMinimize.Location = new System.Drawing.Point(2, 651);
            this.buttonMinimize.Name = "buttonMinimize";
            this.buttonMinimize.Size = new System.Drawing.Size(100, 73);
            this.buttonMinimize.TabIndex = 4;
            this.buttonMinimize.Text = "최소화";
            this.buttonMinimize.UseVisualStyleBackColor = true;
            this.buttonMinimize.Click += new System.EventHandler(this.buttonMinimize_Click);
            // 
            // buttonAlarm
            // 
            this.buttonAlarm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.buttonAlarm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAlarm.Font = new System.Drawing.Font("Leelawadee UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAlarm.ForeColor = System.Drawing.Color.Black;
            this.buttonAlarm.Image = global::MobyusTouch.Properties.Resources.alarm;
            this.buttonAlarm.Location = new System.Drawing.Point(3, 139);
            this.buttonAlarm.Name = "buttonAlarm";
            this.buttonAlarm.Size = new System.Drawing.Size(98, 85);
            this.buttonAlarm.TabIndex = 1;
            this.buttonAlarm.Text = "Alarm";
            this.buttonAlarm.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonAlarm.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonAlarm.UseVisualStyleBackColor = false;
            this.buttonAlarm.Click += new System.EventHandler(this.buttonAlarm_Click);
            // 
            // StatusFMS
            // 
            this.StatusFMS.BackColor = System.Drawing.Color.DarkRed;
            this.StatusFMS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StatusFMS.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusFMS.ForeColor = System.Drawing.Color.White;
            this.StatusFMS.Location = new System.Drawing.Point(2, 408);
            this.StatusFMS.Name = "StatusFMS";
            this.StatusFMS.Size = new System.Drawing.Size(100, 47);
            this.StatusFMS.TabIndex = 4;
            this.StatusFMS.Text = "FMS";
            this.StatusFMS.UseVisualStyleBackColor = false;
            // 
            // button28
            // 
            this.button28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button28.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.button28.ForeColor = System.Drawing.Color.White;
            this.button28.Location = new System.Drawing.Point(2, 313);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(100, 50);
            this.button28.TabIndex = 4;
            this.button28.Text = "STATUS";
            this.button28.UseVisualStyleBackColor = true;
            // 
            // buttonMap
            // 
            this.buttonMap.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.buttonMap.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMap.Font = new System.Drawing.Font("Leelawadee UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMap.ForeColor = System.Drawing.Color.Black;
            this.buttonMap.Image = global::MobyusTouch.Properties.Resources.map1;
            this.buttonMap.Location = new System.Drawing.Point(3, 52);
            this.buttonMap.Name = "buttonMap";
            this.buttonMap.Size = new System.Drawing.Size(98, 85);
            this.buttonMap.TabIndex = 1;
            this.buttonMap.Text = "Map";
            this.buttonMap.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonMap.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.buttonMap.UseVisualStyleBackColor = false;
            this.buttonMap.Click += new System.EventHandler(this.buttonMap_Click);
            // 
            // MainLogo
            // 
            this.MainLogo.BackColor = System.Drawing.Color.White;
            this.MainLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MainLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.MainLogo.Location = new System.Drawing.Point(0, 0);
            this.MainLogo.Name = "MainLogo";
            this.MainLogo.Size = new System.Drawing.Size(104, 47);
            this.MainLogo.TabIndex = 0;
            // 
            // panelMain
            // 
            this.panelMain.BackColor = System.Drawing.Color.LightGray;
            this.panelMain.Location = new System.Drawing.Point(106, 48);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(1280, 680);
            this.panelMain.TabIndex = 8;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.panel2.Controls.Add(this.button8);
            this.panel2.Controls.Add(this.LoadedBtn);
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.WorkUnitBtn);
            this.panel2.Controls.Add(this.button7);
            this.panel2.Controls.Add(this.ForkCenterBtn);
            this.panel2.Controls.Add(this.button5);
            this.panel2.Controls.Add(this.ObstacleBtn);
            this.panel2.Controls.Add(this.button4);
            this.panel2.Controls.Add(this.TrafficBtn);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.ChargeBtn);
            this.panel2.Controls.Add(this.button27);
            this.panel2.Controls.Add(this.button18);
            this.panel2.Controls.Add(this.button30);
            this.panel2.Controls.Add(this.button22);
            this.panel2.Controls.Add(this.button20);
            this.panel2.Controls.Add(this.button6);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.button9);
            this.panel2.Controls.Add(this.WorkBtn);
            this.panel2.Controls.Add(this.BatteryBtn);
            this.panel2.Controls.Add(this.ForkWidthBtn);
            this.panel2.Controls.Add(this.TiltBtn);
            this.panel2.Controls.Add(this.LiftBtn);
            this.panel2.Controls.Add(this.SpeedBtn);
            this.panel2.Controls.Add(this.SteerBtn);
            this.panel2.Location = new System.Drawing.Point(105, 726);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1175, 74);
            this.panel2.TabIndex = 1;
            // 
            // button8
            // 
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button8.ForeColor = System.Drawing.Color.White;
            this.button8.Location = new System.Drawing.Point(840, 0);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(72, 28);
            this.button8.TabIndex = 17;
            this.button8.Text = "LOADED";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // LoadedBtn
            // 
            this.LoadedBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LoadedBtn.Font = new System.Drawing.Font("Century Gothic", 7.5F);
            this.LoadedBtn.ForeColor = System.Drawing.Color.White;
            this.LoadedBtn.Location = new System.Drawing.Point(840, 27);
            this.LoadedBtn.Name = "LoadedBtn";
            this.LoadedBtn.Size = new System.Drawing.Size(72, 46);
            this.LoadedBtn.TabIndex = 18;
            this.LoadedBtn.Text = "OFF";
            this.LoadedBtn.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(130, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(72, 28);
            this.button3.TabIndex = 15;
            this.button3.Text = "ACTION";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // WorkUnitBtn
            // 
            this.WorkUnitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.WorkUnitBtn.Font = new System.Drawing.Font("Century Gothic", 7F);
            this.WorkUnitBtn.ForeColor = System.Drawing.Color.White;
            this.WorkUnitBtn.Location = new System.Drawing.Point(130, 27);
            this.WorkUnitBtn.Name = "WorkUnitBtn";
            this.WorkUnitBtn.Size = new System.Drawing.Size(72, 46);
            this.WorkUnitBtn.TabIndex = 16;
            this.WorkUnitBtn.Text = "-";
            this.WorkUnitBtn.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Location = new System.Drawing.Point(556, 0);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(72, 28);
            this.button7.TabIndex = 13;
            this.button7.Text = "F-CENTER";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // ForkCenterBtn
            // 
            this.ForkCenterBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ForkCenterBtn.Font = new System.Drawing.Font("Century Gothic", 7.5F);
            this.ForkCenterBtn.ForeColor = System.Drawing.Color.White;
            this.ForkCenterBtn.Location = new System.Drawing.Point(556, 27);
            this.ForkCenterBtn.Name = "ForkCenterBtn";
            this.ForkCenterBtn.Size = new System.Drawing.Size(72, 46);
            this.ForkCenterBtn.TabIndex = 14;
            this.ForkCenterBtn.Text = "0 mm";
            this.ForkCenterBtn.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(698, 0);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(72, 28);
            this.button5.TabIndex = 11;
            this.button5.Text = "OBSTACLE";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // ObstacleBtn
            // 
            this.ObstacleBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ObstacleBtn.Font = new System.Drawing.Font("Century Gothic", 7.5F);
            this.ObstacleBtn.ForeColor = System.Drawing.Color.White;
            this.ObstacleBtn.Location = new System.Drawing.Point(698, 27);
            this.ObstacleBtn.Name = "ObstacleBtn";
            this.ObstacleBtn.Size = new System.Drawing.Size(72, 46);
            this.ObstacleBtn.TabIndex = 12;
            this.ObstacleBtn.Text = "OFF";
            this.ObstacleBtn.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(201, 0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(72, 28);
            this.button4.TabIndex = 9;
            this.button4.Text = "TRAFFIC";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // TrafficBtn
            // 
            this.TrafficBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TrafficBtn.Font = new System.Drawing.Font("Century Gothic", 7.5F);
            this.TrafficBtn.ForeColor = System.Drawing.Color.White;
            this.TrafficBtn.Location = new System.Drawing.Point(201, 27);
            this.TrafficBtn.Name = "TrafficBtn";
            this.TrafficBtn.Size = new System.Drawing.Size(72, 46);
            this.TrafficBtn.TabIndex = 10;
            this.TrafficBtn.Text = "-";
            this.TrafficBtn.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(769, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(72, 28);
            this.button1.TabIndex = 5;
            this.button1.Text = "CHARGE";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // ChargeBtn
            // 
            this.ChargeBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ChargeBtn.Font = new System.Drawing.Font("Century Gothic", 7.5F);
            this.ChargeBtn.ForeColor = System.Drawing.Color.White;
            this.ChargeBtn.Location = new System.Drawing.Point(769, 27);
            this.ChargeBtn.Name = "ChargeBtn";
            this.ChargeBtn.Size = new System.Drawing.Size(72, 46);
            this.ChargeBtn.TabIndex = 6;
            this.ChargeBtn.Text = "OFF";
            this.ChargeBtn.UseVisualStyleBackColor = true;
            // 
            // button27
            // 
            this.button27.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button27.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button27.ForeColor = System.Drawing.Color.White;
            this.button27.Location = new System.Drawing.Point(59, 0);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(72, 28);
            this.button27.TabIndex = 4;
            this.button27.Text = "WORK";
            this.button27.UseVisualStyleBackColor = true;
            // 
            // button18
            // 
            this.button18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button18.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold);
            this.button18.ForeColor = System.Drawing.Color.White;
            this.button18.Location = new System.Drawing.Point(0, 0);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(60, 73);
            this.button18.TabIndex = 4;
            this.button18.Text = "AFL";
            this.button18.UseVisualStyleBackColor = true;
            // 
            // button30
            // 
            this.button30.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button30.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button30.ForeColor = System.Drawing.Color.White;
            this.button30.Location = new System.Drawing.Point(911, 1);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(72, 28);
            this.button30.TabIndex = 4;
            this.button30.Text = "BATTERY";
            this.button30.UseVisualStyleBackColor = true;
            // 
            // button22
            // 
            this.button22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button22.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button22.ForeColor = System.Drawing.Color.White;
            this.button22.Location = new System.Drawing.Point(627, 0);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(72, 28);
            this.button22.TabIndex = 4;
            this.button22.Text = "F-WIDTH";
            this.button22.UseVisualStyleBackColor = true;
            // 
            // button20
            // 
            this.button20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button20.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button20.ForeColor = System.Drawing.Color.White;
            this.button20.Location = new System.Drawing.Point(485, 0);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(72, 28);
            this.button20.TabIndex = 4;
            this.button20.Text = "F-TILT";
            this.button20.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(414, 0);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(72, 28);
            this.button6.TabIndex = 4;
            this.button6.Text = "F-LIFT";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(343, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(72, 28);
            this.button2.TabIndex = 4;
            this.button2.Text = "SPEED";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Century Gothic", 7F, System.Drawing.FontStyle.Bold);
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.Location = new System.Drawing.Point(272, 0);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(72, 28);
            this.button9.TabIndex = 4;
            this.button9.Text = "STEER";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // WorkBtn
            // 
            this.WorkBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.WorkBtn.Font = new System.Drawing.Font("Century Gothic", 7F);
            this.WorkBtn.ForeColor = System.Drawing.Color.White;
            this.WorkBtn.Location = new System.Drawing.Point(59, 27);
            this.WorkBtn.Name = "WorkBtn";
            this.WorkBtn.Size = new System.Drawing.Size(72, 46);
            this.WorkBtn.TabIndex = 4;
            this.WorkBtn.Text = "-";
            this.WorkBtn.UseVisualStyleBackColor = true;
            // 
            // BatteryBtn
            // 
            this.BatteryBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BatteryBtn.Font = new System.Drawing.Font("Century Gothic", 7.5F);
            this.BatteryBtn.ForeColor = System.Drawing.Color.White;
            this.BatteryBtn.Location = new System.Drawing.Point(911, 28);
            this.BatteryBtn.Name = "BatteryBtn";
            this.BatteryBtn.Size = new System.Drawing.Size(72, 46);
            this.BatteryBtn.TabIndex = 4;
            this.BatteryBtn.Text = "0 %";
            this.BatteryBtn.UseVisualStyleBackColor = true;
            this.BatteryBtn.Click += new System.EventHandler(this.BatteryBtn_Click);
            // 
            // ForkWidthBtn
            // 
            this.ForkWidthBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ForkWidthBtn.Font = new System.Drawing.Font("Century Gothic", 7.5F);
            this.ForkWidthBtn.ForeColor = System.Drawing.Color.White;
            this.ForkWidthBtn.Location = new System.Drawing.Point(627, 27);
            this.ForkWidthBtn.Name = "ForkWidthBtn";
            this.ForkWidthBtn.Size = new System.Drawing.Size(72, 46);
            this.ForkWidthBtn.TabIndex = 4;
            this.ForkWidthBtn.Text = "0 mm";
            this.ForkWidthBtn.UseVisualStyleBackColor = true;
            // 
            // TiltBtn
            // 
            this.TiltBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TiltBtn.Font = new System.Drawing.Font("Century Gothic", 7.5F);
            this.TiltBtn.ForeColor = System.Drawing.Color.White;
            this.TiltBtn.Location = new System.Drawing.Point(485, 27);
            this.TiltBtn.Name = "TiltBtn";
            this.TiltBtn.Size = new System.Drawing.Size(72, 46);
            this.TiltBtn.TabIndex = 4;
            this.TiltBtn.Text = "0 deg";
            this.TiltBtn.UseVisualStyleBackColor = true;
            // 
            // LiftBtn
            // 
            this.LiftBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LiftBtn.Font = new System.Drawing.Font("Century Gothic", 7.5F);
            this.LiftBtn.ForeColor = System.Drawing.Color.White;
            this.LiftBtn.Location = new System.Drawing.Point(414, 27);
            this.LiftBtn.Name = "LiftBtn";
            this.LiftBtn.Size = new System.Drawing.Size(72, 46);
            this.LiftBtn.TabIndex = 4;
            this.LiftBtn.Text = "0 mm";
            this.LiftBtn.UseVisualStyleBackColor = true;
            // 
            // SpeedBtn
            // 
            this.SpeedBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SpeedBtn.Font = new System.Drawing.Font("Century Gothic", 7.5F);
            this.SpeedBtn.ForeColor = System.Drawing.Color.White;
            this.SpeedBtn.Location = new System.Drawing.Point(343, 27);
            this.SpeedBtn.Name = "SpeedBtn";
            this.SpeedBtn.Size = new System.Drawing.Size(72, 46);
            this.SpeedBtn.TabIndex = 4;
            this.SpeedBtn.Text = "0 km/h";
            this.SpeedBtn.UseVisualStyleBackColor = true;
            // 
            // SteerBtn
            // 
            this.SteerBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SteerBtn.Font = new System.Drawing.Font("Century Gothic", 7.5F);
            this.SteerBtn.ForeColor = System.Drawing.Color.White;
            this.SteerBtn.Location = new System.Drawing.Point(272, 27);
            this.SteerBtn.Name = "SteerBtn";
            this.SteerBtn.Size = new System.Drawing.Size(72, 46);
            this.SteerBtn.TabIndex = 4;
            this.SteerBtn.Text = "0 deg";
            this.SteerBtn.UseVisualStyleBackColor = true;
            // 
            // lableTopName
            // 
            this.lableTopName.AutoSize = true;
            this.lableTopName.Font = new System.Drawing.Font("Century Gothic", 18F);
            this.lableTopName.ForeColor = System.Drawing.Color.White;
            this.lableTopName.Location = new System.Drawing.Point(16, 7);
            this.lableTopName.Name = "lableTopName";
            this.lableTopName.Size = new System.Drawing.Size(143, 30);
            this.lableTopName.TabIndex = 2;
            this.lableTopName.Text = "AFL System";
            // 
            // labelCurrentDate
            // 
            this.labelCurrentDate.AutoSize = true;
            this.labelCurrentDate.Location = new System.Drawing.Point(819, 2);
            this.labelCurrentDate.Name = "labelCurrentDate";
            this.labelCurrentDate.Size = new System.Drawing.Size(96, 42);
            this.labelCurrentDate.TabIndex = 2;
            this.labelCurrentDate.Text = "2019-06-28 \n  10:40:20";
            // 
            // touchMaintop
            // 
            this.touchMaintop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(44)))), ((int)(((byte)(51)))));
            this.touchMaintop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.touchMaintop.Controls.Add(this.lableTopName);
            this.touchMaintop.Controls.Add(this.labelCurrentDate);
            this.touchMaintop.Controls.Add(this.buttonWorkOrder);
            this.touchMaintop.Controls.Add(this.buttonUnitOrder);
            this.touchMaintop.Controls.Add(this.buttonModeSelect);
            this.touchMaintop.ForeColor = System.Drawing.Color.White;
            this.touchMaintop.Location = new System.Drawing.Point(106, 0);
            this.touchMaintop.Name = "touchMaintop";
            this.touchMaintop.Size = new System.Drawing.Size(1174, 47);
            this.touchMaintop.TabIndex = 6;
            // 
            // buttonWorkOrder
            // 
            this.buttonWorkOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonWorkOrder.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold);
            this.buttonWorkOrder.ForeColor = System.Drawing.Color.White;
            this.buttonWorkOrder.Location = new System.Drawing.Point(527, 5);
            this.buttonWorkOrder.Name = "buttonWorkOrder";
            this.buttonWorkOrder.Size = new System.Drawing.Size(125, 36);
            this.buttonWorkOrder.TabIndex = 4;
            this.buttonWorkOrder.Text = "작업 명령";
            this.buttonWorkOrder.UseVisualStyleBackColor = true;
            this.buttonWorkOrder.Click += new System.EventHandler(this.buttonWorkOrder_Click);
            // 
            // buttonUnitOrder
            // 
            this.buttonUnitOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUnitOrder.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold);
            this.buttonUnitOrder.ForeColor = System.Drawing.Color.White;
            this.buttonUnitOrder.Location = new System.Drawing.Point(667, 5);
            this.buttonUnitOrder.Name = "buttonUnitOrder";
            this.buttonUnitOrder.Size = new System.Drawing.Size(125, 36);
            this.buttonUnitOrder.TabIndex = 4;
            this.buttonUnitOrder.Text = "단위 명령";
            this.buttonUnitOrder.UseVisualStyleBackColor = true;
            this.buttonUnitOrder.Click += new System.EventHandler(this.buttonUnitOrder_Click);
            // 
            // buttonModeSelect
            // 
            this.buttonModeSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonModeSelect.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold);
            this.buttonModeSelect.ForeColor = System.Drawing.Color.White;
            this.buttonModeSelect.Location = new System.Drawing.Point(290, 5);
            this.buttonModeSelect.Name = "buttonModeSelect";
            this.buttonModeSelect.Size = new System.Drawing.Size(222, 36);
            this.buttonModeSelect.TabIndex = 4;
            this.buttonModeSelect.Text = "OFFLINE";
            this.buttonModeSelect.UseVisualStyleBackColor = true;
            this.buttonModeSelect.Click += new System.EventHandler(this.buttonModeSelect_Click);
            // 
            // MainTouch
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1280, 800);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.touchMaintop);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(120)))), ((int)(((byte)(138)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainTouch";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.touchMaintop.ResumeLayout(false);
            this.touchMaintop.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel MainLogo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lableTopName;
        private System.Windows.Forms.Button buttonMap;
        private System.Windows.Forms.Label labelCurrentDate;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonMinimize;
        private System.Windows.Forms.Panel touchMaintop;
        private System.Windows.Forms.Button buttonSetting;
        private System.Windows.Forms.Button SteerBtn;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button WorkBtn;
        private System.Windows.Forms.Button StatusFMS;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.Button buttonModeSelect;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button LiftBtn;
        private System.Windows.Forms.Button SpeedBtn;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button TiltBtn;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button ForkWidthBtn;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button BatteryBtn;
        private System.Windows.Forms.Button buttonUnitOrder;
        private System.Windows.Forms.Button buttonWorkOrder;
        private System.Windows.Forms.Button StatusSerial;
        private System.Windows.Forms.Button StatusCAN;
        private System.Windows.Forms.Button StatusAlarm;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button ChargeBtn;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button TrafficBtn;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button ObstacleBtn;
        private System.Windows.Forms.Button buttonAlarm;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button ForkCenterBtn;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button WorkUnitBtn;
        private System.Windows.Forms.Button buttonZoomDown;
        private System.Windows.Forms.Button buttonZoomUp;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button LoadedBtn;
    }
}

