﻿using LumenWorks.Framework.IO.Csv;

//using MapDLL;
using EditorLibrary;
using MobyusTouch.src.Control.Sub;
using MobyusTouch.src.Main.Sub;
using NLog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.IO;



namespace MobyusTouch.src.Global.Entity
{
    public class WorkEntity
    {
        private static Logger logger = LogManager.GetLogger("WorkEntity");

        public string workStartTime;

        public string transferID;
        public string workID;
        public string workMode; // A: AMR Path Planning, M: ACS Path Planning
        public int workType; // LOAD, UNLOAD, MOVE
        public byte workState;
        public byte workStateUnit;
        public int workLevel;
        public int workDuration;
        public int workLastLinkType;


        public ushort curLink;
        public ushort curNode;


        public ushort toNode; 
        public ushort nextNode;
        public short obstacle;

        public List<AstarNodeData> realPath;

        public List<AstarNodeData> getNextNodePath;

        public Queue<WorkStep> workQueue;
        public Queue<WorkStep> workQueueCompleted;

        public DataTable workDataTable;

        public int palletType;
        public int frontRetry;
        public int alarm;
        public string alarm_name =  "";

        public WorkEntity()
        {
            transferID = "";

            this.workID = "";
            this.workMode = Globals.PATH_PLANNING_AMR;
            this.workType = Globals.TRANSFER_READY;
            this.workState = (byte)WorkState.READY;
            this.workStateUnit = (byte)WorkStateUnit.READY;
            this.workLevel = 0;
            this.workDuration = 0;
            this.workLastLinkType = 0;

            this.curLink = 0;
            this.curNode = 0;
            this.toNode = 0;
            this.nextNode = 0;

            this.workStartTime = System.DateTime.Now.ToString("yyyyMMddHHmmss");

            this.realPath = new List<AstarNodeData>();
            this.getNextNodePath = new List<AstarNodeData>();
            this.workQueue = new Queue<WorkStep>();
            this.workQueueCompleted = new Queue<WorkStep>();
            this.workDataTable = new DataTable();

            this.frontRetry = 0;
            this.palletType = 0;
            this.alarm = Alarms.NO_ALARM;
        }

        public WorkEntity(int workType, int palletType, int level, ushort targetNode)
        {
            this.transferID = "";

            this.workID = "";
            this.workMode = Globals.PATH_PLANNING_AMR;
            this.workType = workType;
            this.workLevel = level;
            this.workState = (byte)WorkState.READY;
            this.workStateUnit = (byte)WorkStateUnit.READY;
            this.workDuration = 0;
            this.workLastLinkType = 0;

            this.workStartTime = System.DateTime.Now.ToString("yyyyMMddHHmmss");

            this.curLink = Globals.currentWork.curLink;
            this.curNode = Globals.currentWork.curNode;
            this.toNode = targetNode;
            this.nextNode = Globals.currentWork.nextNode;

            this.realPath = new List<AstarNodeData>();
            this.getNextNodePath = new List<AstarNodeData>();

            this.workQueue = new Queue<WorkStep>();
            this.workQueueCompleted = new Queue<WorkStep>();
            this.workDataTable = new DataTable();

            this.frontRetry = 0;

            this.palletType = palletType;
            this.alarm = Alarms.NO_ALARM;
        }
        public void Clear()
        {
            this.transferID = "";

            this.workID = "";
            this.workMode = Globals_Hyundai.PATH_PLANNING_AMR;
            this.workType = Globals.TRANSFER_READY;
            this.workLevel = 0;
            this.workState = (byte)WorkState.READY;
            this.workStateUnit = (byte)WorkStateUnit.READY;
            this.workDuration = 0;
            this.workLastLinkType = 0;

            this.curLink = 0;
            //this.curNode = 0; 0914 kdy
            this.toNode = 0;
            this.nextNode = 0;

            this.realPath = null;
            this.getNextNodePath = null;
            this.workQueue = null;
            this.workQueueCompleted = null;
            this.workDataTable = null;

            this.frontRetry = 0;

            this.palletType = 0;
            this.alarm = Alarms.NO_ALARM;

            Globals.bRetry_FirstMoveComplete = false;
            Globals.bRetry_FirstPathRetry = false;
            Globals.bRetry_SecondPathRetry = false;
            Globals.bRetry_LiftingRetry = true;
            Globals.nRetry_HeadTypeLiftingRetry = 0;

            Globals.nCorrectMoving = 0;

            Globals.bFirstPathComplete = false;
            Globals.bSecondPathComplete = false;
            Globals.bDisplaySecondPathWork = false;
            //Globals.bDisplayPauseCheck = true;

            Globals.nWhileLiftWorkCount = 0;
            Globals.nWhileTiltWorkCount = 0;
            Globals.nWhileSideShiftWorkCount = 0;
            Globals.nWhileForkMoverWorkCount = 0;

            Globals.curNodeforNextNode = 0;

        }

        public void Clear_exceptstate() // kdy 작업완료시 지게차 상태 대기로 안바꾸고 계속 완료상태로 남기기위해 추가 
        {
            this.transferID = "";

            this.workID = "";
            this.workMode = Globals_Hyundai.PATH_PLANNING_AMR;
            this.workType = Globals.TRANSFER_READY;
            this.workLevel = 0;
            //this.workState = (byte)WorkState.READY;
            this.workStateUnit = (byte)WorkStateUnit.READY;
            this.workDuration = 0;
            this.workLastLinkType = 0;

            this.curLink = 0;
            //this.curNode = 0; 0914 kdy
            this.toNode = 0;
            this.nextNode = 0;

            this.realPath = null;
            this.getNextNodePath = null;
            this.workQueue = null;
            this.workQueueCompleted = null;
            this.workDataTable = null;

            this.frontRetry = 0;

            this.palletType = 0;
            this.alarm = Alarms.NO_ALARM;

            Globals.bRetry_FirstMoveComplete = false;
            Globals.bRetry_FirstPathRetry = false;
            Globals.bRetry_SecondPathRetry = false;
            Globals.bRetry_LiftingRetry = true;
            Globals.nRetry_HeadTypeLiftingRetry = 0;

            Globals.nCorrectMoving = 0;

            Globals.bFirstPathComplete = false;
            Globals.bSecondPathComplete = false;
            Globals.bDisplaySecondPathWork = false;
            //Globals.bDisplayPauseCheck = true;

            Globals.nWhileLiftWorkCount = 0;
            Globals.nWhileTiltWorkCount = 0;
            Globals.nWhileSideShiftWorkCount = 0;
            Globals.nWhileForkMoverWorkCount = 0;

            Globals.curNodeforNextNode = 0;
        }

        override public String ToString()
        {
            return "transferID:" + transferID +
                " workStartTime:" + workStartTime +
                " workID:" + workID +
                " workMode:" + workMode +
                " workType:" + workType +
                " workLevel:" + workLevel +
                " workState:" + workState +
                " workStateUnit:" + workStateUnit +
                " curLink:" + curLink +
                " curNode:" + curNode +
                " toNode:" + toNode +
                " nextNode:" + nextNode +
                " palletType:" + palletType +
                " NAV_X:" + Globals.NavigationSystem.NAV_X +
                " NAV_Y:" + Globals.NavigationSystem.NAV_Y +
                " NAV_PHI:" + Globals.NavigationSystem.NAV_PHI +
                " alarm:" + alarm;
        }
        public bool EnqueueWork()
        {
            bool result = true;
            try
            {
                logger.Info("   EnqueueWork [workLevel] "+ workLevel + " [workType] " + workType);
                if (workLevel > 0 && workType == Globals.TRANSFER_MOVE )
                {
                    result = false;
                    Globals.currentWork.alarm = Alarms.ALARM_WORK_WRONG_ORDER_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1세부작업오류/C1  에    러 !]";
                    logger.Error("[FAILED] WORK WRONG ORDER ERROR");
                }

                switch (workType)
                {
                    case Globals.TRANSFER_MOVE:
                        workID = "MOVE_0_0_" + toNode;
                        workState = (byte)WorkState.MOVING;
                        Globals.tmp_text = "이동";
                        break;

                    case Globals.TRANSFER_LOAD:
                        workID = "LOAD_" + palletType + "_" + workLevel + "_" + toNode;
                        workState = (byte)WorkState.LOADING;
                        Globals.tmp_text = "적재 " + workLevel + "단";
                        break;

                    case Globals.TRANSFER_UNLOAD:
                        workID = "UNLOAD_" + palletType + "_" + workLevel + "_" + toNode;
                        workState = (byte)WorkState.UNLOADING;
                        Globals.tmp_text = "이재 " + workLevel + "단";
                        break;

                    default:
                        workState = (byte)WorkState.READY;
                        Globals.tmp_text = "-";
                        result = false;
                        break;
                }

                if (result && !"".Equals(workID))
                {
                    FileInfo item = null;
                    try
                    {
                        item = new FileInfo(Globals.WORK_SettingDirPath + "\\" + workID + ".csv");
                        if (!item.Exists)
                        {
                            workID = workID.Remove(workID.LastIndexOf('_'), workID.Length - workID.LastIndexOf('_')) + "_0";
                            item = new FileInfo(Globals.WORK_SettingDirPath + "\\" + workID + ".csv");
                        }
                    }
                    catch (Exception ex)
                    {
                        Globals.currentWork.alarm = Alarms.ALARM_WORK_FILE_LIST_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1세부작업읽기/C1  에    러 !]";
                        logger.Error("[FAILED] NO FOUND WORK FILE ");
                    }

                    using (CsvReader csvReader = new CsvReader(new StreamReader(System.IO.File.OpenRead(item.FullName)), false, ';'))
                    {
                        workDataTable.Load(csvReader);
                        for (int i = 0; i <= 12; i++)
                        {
                            if (i >= workDataTable.Columns.Count)
                            {
                                workDataTable.Columns.Add(new DataColumn());
                            }
                            workDataTable.Columns[i].ReadOnly = false;
                        }
                    }

                    if (workDataTable.Rows.Count > 1)
                    {
                        string FILE_ID = workDataTable.Rows[1][0].ToString();
                        string FILE_WORK = workDataTable.Rows[1][1].ToString();
                        int FILE_PALLET_ID = int.Parse(workDataTable.Rows[1][2].ToString());
                        int FILE_LEVEL = int.Parse(workDataTable.Rows[1][3].ToString());
                        int FILE_NODE = int.Parse(workDataTable.Rows[1][4].ToString());
                        int FILE_CNT = int.Parse(workDataTable.Rows[1][6].ToString());

                        this.workLastLinkType = int.Parse(workDataTable.Rows[1][5].ToString());

                        string fileWorkID = FILE_WORK + "_" + FILE_PALLET_ID + "_" + FILE_LEVEL + "_" + FILE_NODE;
                        if (!fileWorkID.Equals(FILE_ID) || !fileWorkID.Equals(Path.GetFileNameWithoutExtension(item.Name)))
                        {
                            result = false;
                            Globals.currentWork.alarm = Alarms.ALARM_WORK_FILE_FORMAT_ERROR;
                            Globals.currentWork.alarm_name = "![000/E5151/C1세부작업형태/C1  에    러 !]";
                            logger.Error("[FAILED] WORK FILE FORMAT ERROR");
                        }

                        for (int i = 0; i < workDataTable.Rows.Count; i++)
                        {
                            if (i == 5)
                            {
                                /*
                                string PALLET_NAME = csvTable.Rows[i][0].ToString();
                                int PALLET_WIDTH = int.Parse(csvTable.Rows[i][1].ToString());
                                int PALLET_HEIGHT = int.Parse(csvTable.Rows[i][2].ToString());
                                int PALLET_LENGTH = int.Parse(csvTable.Rows[i][3].ToString());
                                int PALLET_LENGTH_HOLE = int.Parse(csvTable.Rows[i][4].ToString());
                                */
                            }
                            else if (i >= 10)
                            {
                                string name = workDataTable.Rows[i][0].ToString();
                                int value = int.Parse(workDataTable.Rows[i][1].ToString());
                                int sleep = int.Parse(workDataTable.Rows[i][2].ToString());
                                bool sync = bool.Parse(workDataTable.Rows[i][3].ToString());
                                
                                WorkStep workStep = GetWorkStep(name, value, sleep, sync);
                                if (workStep.step != 0)
                                {
                                    workQueue.Enqueue(workStep);
                                }
                            }
                        }

                        if (workQueue.Count != FILE_CNT)
                        {
                            result = false;
                            Globals.currentWork.alarm = Alarms.ALARM_WORK_FILE_FORMAT_ERROR;
                            Globals.currentWork.alarm_name = "![000/E5151/C1세부작업형태/C1  에    러 !]";
                            logger.Error("[FAILED] FILE WORK COUNT MISMATCH ");
                        }
                        else
                        {
                            logger.Info("[SUCCESS] READ WORK QUEUE [" + workID + "] to Node [" + toNode + "] Count ["+ workQueue.Count.ToString() + "]");
                        }
                    }
                    else
                    {
                        result = false;
                        Globals.currentWork.alarm = Alarms.ALARM_WORK_FILE_FORMAT_ERROR;
                        Globals.currentWork.alarm_name = "![000/E5151/C1세부작업형태/C1  에    러 !]";
                        logger.Error("[FAILED] FILE WORK ROWS 0 ");
                    }
                }
                else
                {
                    result = false;
                    Globals.currentWork.alarm = Alarms.ALARM_WORK_WRONG_ORDER_ERROR;
                    Globals.currentWork.alarm_name = "![000/E5151/C1세부작업오류/C1  에    러 !]";
                    logger.Error("[FAILED] WRONG WORK NAME");
                }
            }
            catch(Exception ex)
            {
                result = false;
                Globals.currentWork.alarm = Alarms.ALARM_WORK_WRONG_ORDER_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1세부작업오류/C1  에    러 !]";
                logger.Error("[ERROR]" + ex.ToString());
            }
            return result;
        }

        private static WorkStep GetWorkStep(string functionName, int value, int sleep, bool isSync)
        {
            WorkStep workStep = new WorkStep();
            switch (functionName)
            {
                //////////////////////////////////////////////////////////////////////////////////////////
                // CHARGE CONTROL //////////////////////////////////////////////////////////////////////// 
                case "WORK_CHARGE_DISABLE":
                    workStep = new WorkStep("WORK_CHARGE_DISABLE", Globals.WORK_CHARGE_DISABLE, value, sleep, isSync, ChargeControl.ChargeDisable);
                    break;
                case "WORK_CHARGE_ENABLE":
                    workStep = new WorkStep("WORK_CHARGE_ENABLE", Globals.WORK_CHARGE_ENABLE, value, sleep, isSync, ChargeControl.ChargeEnable);
                    break;

                //////////////////////////////////////////////////////////////////////////////////////////
                // MOVE CONTROL ////////////////////////////////////////////////////////////////////////// 
                case "WORK_MOVE_PATH_PLANNING":
                    workStep = new WorkStep("WORK_MOVE_PATH_PLANNING", Globals.WORK_MOVE_PATH_PLANNING, value, sleep, isSync, DriveControl.MovePathPlanning);
                    break;
                case "WORK_MOVE_1ST_PATH_PLANNING":
                    workStep = new WorkStep("WORK_MOVE_1ST_PATH_PLANNING", Globals.WORK_MOVE_1ST_PATH_PLANNING, value, sleep, isSync, DriveControl.Move1stPathPlanning);
                    break;
                case "WORK_MOVE_2ND_PATH_PLANNING":
                    workStep = new WorkStep("WORK_MOVE_2ND_PATH_PLANNING", Globals.WORK_MOVE_2ND_PATH_PLANNING, value, sleep, isSync, DriveControl.Move2ndPathPlanning);
                    break;
                case "WORK_MOVE_PATH_SENDING":
                    workStep = new WorkStep("WORK_MOVE_PATH_SENDING", Globals.WORK_MOVE_PATH_SENDING, value, sleep, isSync, DriveControl.MovePathSending);
                    break;
                case "WORK_MOVE_FROM_STATION":
                    workStep = new WorkStep("WORK_MOVE_FROM_STATION", Globals.WORK_MOVE_FROM_STATION, value, sleep, isSync, DriveControl.MoveFromStation);
                    break;
                case "WORK_MOVE_TO_STATION":
                    workStep = new WorkStep("WORK_MOVE_TO_STATION", Globals.WORK_MOVE_TO_STATION, value, sleep, isSync, DriveControl.MoveToStation);
                    break;
                case "WORK_MOVE_COMPLETE":
                    workStep = new WorkStep("WORK_MOVE_COMPLETE", Globals.WORK_MOVE_COMPLETE, value, sleep, isSync, DriveControl.MoveComplete);
                    break;
                case "WORK_MOVE_END":
                    workStep = new WorkStep("WORK_MOVE_END", Globals.WORK_MOVE_END, value, sleep, isSync, DriveControl.MoveEnd);
                    break;

                //////////////////////////////////////////////////////////////////////////////////////////
                // FORK CONTROL ////////////////////////////////////////////////////////////////////////// 
                case "WORK_FORK_LIFTING":
                    workStep = new WorkStep("WORK_FORK_LIFTING", Globals.WORK_FORK_LIFTING, value, sleep, isSync, ForkControl.Lifting);
                    break;
                case "WORK_FORK_TILTING":
                    workStep = new WorkStep("WORK_FORK_TILTING", Globals.WORK_FORK_TILTING, value, sleep, isSync, ForkControl.Tilting);
                    break;
                case "WORK_FORK_REACHING":
                    workStep = new WorkStep("WORK_FORK_REACHING", Globals.WORK_FORK_REACHING, value, sleep, isSync, ForkControl.Reaching);
                    break;
                case "WORK_FORK_SIDESHIFT":
                    workStep = new WorkStep("WORK_FORK_SIDESHIFT", Globals.WORK_FORK_SIDESHIFT, value, sleep, isSync, ForkControl.SideShift);
                    break;
                case "WORK_FORK_FORKMOVER":
                    workStep = new WorkStep("WORK_FORK_FORKMOVER", Globals.WORK_FORK_FORKMOVER, value, sleep, isSync, ForkControl.ForkMover);
                    break;

                //////////////////////////////////////////////////////////////////////////////////////////
                // CAMERA CONTROL //////////////////////////////////////////////////////////////////////// 
                case "WORK_PALLET_FRONT_DISABLE":
                    workStep = new WorkStep("WORK_PALLET_FRONT_DISABLE", Globals.WORK_PALLET_FRONT_DISABLE, value, sleep, isSync, CameraControl.PalletFrontDisable);
                    break;
                case "WORK_PALLET_FRONT_ENABLE":
                    workStep = new WorkStep("WORK_PALLET_FRONT_ENABLE", Globals.WORK_PALLET_FRONT_ENABLE, value, sleep, isSync, CameraControl.PalletFrontEnable);
                    break;
                case "WORK_PALLET_STACKING_DISABLE":
                    workStep = new WorkStep("WORK_PALLET_STACKING_DISABLE", Globals.WORK_PALLET_STACKING_DISABLE, value, sleep, isSync, CameraControl.PalletStackingDisable);
                    break;
                case "WORK_PALLET_STACKING_ENABLE":
                    workStep = new WorkStep("WORK_PALLET_STACKING_ENABLE", Globals.WORK_PALLET_STACKING_ENABLE, value, sleep, isSync, CameraControl.PalletStackingEnable);
                    break;
                case "WORK_PALLET_HEIGHT_DISABLE":
                    workStep = new WorkStep("WORK_PALLET_HEIGHT_DISABLE", Globals.WORK_PALLET_HEIGHT_DISABLE, value, sleep, isSync, CameraControl.PalletHeightDisable);
                    break;
                case "WORK_PALLET_HEIGHT_ENABLE":
                    workStep = new WorkStep("WORK_PALLET_HEIGHT_ENABLE", Globals.WORK_PALLET_HEIGHT_ENABLE, value, sleep, isSync, CameraControl.PalletHeightEnable);
                    break;
                case "WORK_PALLET_STACKING_SUCCESS_DISABLE":
                    workStep = new WorkStep("WORK_PALLET_STACKING_SUCCESS_DISABLE", Globals.WORK_PALLET_STACKING_SUCCESS_DISABLE, value, sleep, isSync, CameraControl.PalletStackingSuccessDisable);
                    break;
                case "WORK_PALLET_STACKING_SUCCESS_ENABLE":
                    workStep = new WorkStep("WORK_PALLET_STACKING_SUCCESS_ENABLE", Globals.WORK_PALLET_STACKING_SUCCESS_ENABLE, value, sleep, isSync, CameraControl.PalletStackingSuccessEnable);
                    break;
                case "WORK_PALLET_FRONT_EXIST_DISABLE":
                    workStep = new WorkStep("WORK_PALLET_FRONT_EXIST_DISABLE", Globals.WORK_PALLET_FRONT_EXIST_DISABLE, value, sleep, isSync, CameraControl.PalletFrontExistDisable);
                    break;
                case "WORK_PALLET_FRONT_EXIST_ENABLE":
                    workStep = new WorkStep("WORK_PALLET_FRONT_EXIST_ENABLE", Globals.WORK_PALLET_FRONT_EXIST_ENABLE, value, sleep, isSync, CameraControl.PalletFrontExistEnable);
                    break;
                //////////////////////////////////////////////////////////////////////////////////////////
                // CHECK CONTROL /////////////////////////////////////////////////////////////////////////
                case "WORK_CHECK_EXIST_PALLET_LOADED":
                    workStep = new WorkStep("WORK_CHECK_EXIST_PALLET_LOADED", Globals.WORK_CHECK_EXIST_PALLET_LOADED, value, sleep, isSync, CheckControl.IsLoadedPallet);
                    break;
                case "WORK_CHECK_EXIST_PALLET_FRONT":
                    workStep = new WorkStep("WORK_CHECK_EXIST_PALLET_FRONT", Globals.WORK_CHECK_EXIST_PALLET_FRONT, value, sleep, isSync, CheckControl.IsExistFrontPallet);
                    break;

                //////////////////////////////////////////////////////////////////////////////////////////
                // SAFETY CONTROL /////////////////////////////////////////////////////////////////////////
                case "WORK_SAFETY_CLAMP_ENABLE":
                    workStep = new WorkStep("WORK_SAFETY_CLAMP_ENABLE", Globals.WORK_SAFETY_CLAMP_ENABLE, value, sleep, isSync, SafetyControl.ClampEnable);
                    break;
                case "WORK_SAFETY_CLAMP_DISABLE":
                    workStep = new WorkStep("WORK_SAFETY_CLAMP_DISABLE", Globals.WORK_SAFETY_CLAMP_DISABLE, value, sleep, isSync, SafetyControl.ClampDisable);
                    break;

                default:
                    logger.Info("[WORK] NO WORK NAME: " + functionName);
                    break;
            }
            return workStep;
        }

    }
}
