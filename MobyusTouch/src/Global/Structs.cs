﻿using MobyusTouch.src.Global.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobyusTouch.src.Global
{
    public enum SystemMode
    {
        OFFLINE = 1,
        EMO,
        RESET,
        MANUAL,
        SEMI_AUTO,
        AUTO = 7
    }

    public enum SystemTraffic
    {
        TRANSFER_TRAFFIC_FIRMWARE_NONE = 0,
        TRANSFER_TRAFFIC_ACS_NONE = 2,
        TRANSFER_TRAFFIC_PAUSE = 1,
        TRANSFER_TRAFFIC_RESUME = 2,
        TRANSFER_TRAFFIC_STOP = 3,
        TRANSFER_TRAFFIC_EQUIPMENTCOMM_PAUSE = 11,
        TRANSFER_TRAFFIC_AUTO_PAUSE = 12,
        TRANSFER_TRAFFIC_USER_PAUSE = 13,
        TRANSFER_TRAFFIC_RCU_PAUSE = 14,
        TRANSFER_TRAFFIC_REMOTESWITCH_PAUSE = 15
    }

    public enum MovingState
    {
        OFFLINE = 10,

        EMERGENCY_STATE_STOP = 20,

        RESET = 30,

        MANUAL_STATE_STOP = 40,
        MANUAL_STATE_JOYSTICK,
        MANUAL_STATE_MOVE_FORWARD,
        MANUAL_STATE_MOVE_BACWARD,
        MANUAL_STATE_TURN_LEFT,
        MANUAL_STATE_TURN_RIGHT,

        SEMI_AUTO_STATE_STOP = 50,
        SEMI_AUTO_STATE_READY,
        SEMI_AUTO_STATE_MOVING_NORMAL,
        SEMI_AUTO_STATE_MOVING_STOP,
        SEMI_AUTO_STATE_PAUSING,
        SEMI_AUTO_STATE_MOVE_COMPLETE,
        SEMI_AUTO_STATE_ALARM_STOP,

        AUTO_STATE_STOP = 70,
        AUTO_STATE_READY,
        AUTO_STATE_MOVING_NORMAL,
        AUTO_STATE_MOVING_STOP,
        AUTO_STATE_PAUSING,
        AUTO_STATE_MOVE_COMPLETE,
        AUTO_STATE_ALARM_STOP,
    }

    public enum WorkState
    {
        READY = 0,
        MOVING,
        LOADING,
        UNLOADING,
        MOVE_COMPLETE,
        LOAD_COMPLETE,
        UNLOAD_COMPLETE,
        TRAFFIC,
        CHARGING
    }

    
    public enum WorkStateUnit
    {
        READY = 0,
        LIFTING,
        TILTING,
        SIDESHIFT,
        FORKMOVER,
        REACH,
        PATH_1ST_NODE,
        PATH_2ND_STATION,
        PATH_SENDING,

        PALLET_FRONT = 10,
        PALLET_STACKING,
        TAGGING,
        CHECKING
    }
    public enum ObstacleAreaState
    {
        NONE = 0,
        WARNING,
        STOP
    }


    public struct WorkStep
    {
        public delegate bool Function(WorkStep workStep);
        public void Clear()
        {
            this.name = "";
            this.step = 0;
            this.isSynconized = true;
            this.value = 0;
            this.sleep = 0;
            this.function = null;
        }
        public WorkStep(string name, int step, int value, int sleep, bool isSynconized, Function function)
        {
            this.name = name;
            this.step = step;
            this.isSynconized = isSynconized;
            this.value = value;
            this.sleep = sleep;
            this.function = function;
        }

        public Function function;
        public string name;
        public int step;
        public bool isSynconized;
        public int value;
        public int sleep;
    }

    public struct HMISystem
    {
        public HMISystem(SystemMode systemMode)
        {
            this.SystemMode = systemMode;
            this.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_FIRMWARE_NONE;
            this.ACStoHMI_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_ACS_NONE;
            this.HMItoACS_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_FIRMWARE_NONE;
            this.MoveComplete = false;

            this.ForkliftValue = 0;

            this.LiftEnable = false;
            this.TiltEnable = false;
            this.ReachEnable = false;
            this.SideShiftEnable = false;
            this.MoverEnable = false;
            this.AFLIDEnable = false;
            this.ChargeEnable = false;
            this.ClampEnable = false;
            this.TaggingEnable = false;

            this.PalletStackingEnable = false;
            this.PalletFrontEnable = false;
            this.PalletHeightEnable = false;
            this.PalletStackingSuccessEnable = false;
            this.PalletFrontExistEnable = false;

            this.WorkType = 0;

            this.PalletType = 0;
            this.PalletLevel = 0;

            this.SafeEnable = false;
            this.SafeLevel = 0;

            this.TiltingValue = 0;
            this.LiftingValue = 0;
            this.ReachingValue = 0;
            this.SideShiftValue = 0;
            this.MoverValue = 0;

            this.AlarmCode = 0;
        }

        public HMISystem(SystemMode systemMode, SystemTraffic systemTraffic)
        {
            this.SystemMode = systemMode;

            if (systemTraffic == SystemTraffic.TRANSFER_TRAFFIC_ACS_NONE)
                systemTraffic = SystemTraffic.TRANSFER_TRAFFIC_FIRMWARE_NONE;

            this.SystemTraffic = systemTraffic;
            this.ACStoHMI_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_ACS_NONE;
            this.HMItoACS_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_FIRMWARE_NONE;
            this.MoveComplete = false;

            this.ForkliftValue = 0;

            this.LiftEnable = false;
            this.TiltEnable = false;
            this.ReachEnable = false;
            this.SideShiftEnable = false;
            this.MoverEnable = false;
            this.AFLIDEnable = false;
            this.ChargeEnable = false;
            this.ClampEnable = false;
            this.TaggingEnable = false;

            this.PalletStackingEnable = false;
            this.PalletFrontEnable = false;
            this.PalletHeightEnable = false;
            this.PalletStackingSuccessEnable = false;
            this.PalletFrontExistEnable = false;

            this.WorkType = 0;

            this.PalletType = 0;
            this.PalletLevel = 0;

            this.SafeEnable = false;
            this.SafeLevel = 0;

            this.TiltingValue = 0;
            this.LiftingValue = 0;
            this.ReachingValue = 0;
            this.SideShiftValue = 0;
            this.MoverValue = 0;

            this.AlarmCode = 0;
        }

        public SystemMode SystemMode;
        public SystemTraffic SystemTraffic;
        public SystemTraffic ACStoHMI_TrafficPause;
        public SystemTraffic HMItoACS_TrafficPause;
        public bool MoveComplete;

        public byte ForkliftValue;

        public bool LiftEnable;
        public bool TiltEnable;
        public bool ReachEnable;
        public bool SideShiftEnable;
        public bool MoverEnable;
        public bool AFLIDEnable;
        public bool ChargeEnable;
        public bool ClampEnable;
        public bool TaggingEnable;

        public bool PalletStackingEnable;
        public bool PalletFrontEnable;
        public bool PalletHeightEnable;
        public bool PalletStackingSuccessEnable;
        public bool PalletFrontExistEnable;

        public byte WorkType;

        public byte PalletType;
        public byte PalletLevel;

        public bool SafeEnable;
        public byte SafeLevel;

        public sbyte TiltingValue;
        public UInt16 LiftingValue;
        public UInt16 ReachingValue;
        public Int16 SideShiftValue;
        public UInt16 MoverValue;

        public byte AlarmCode;
    }

    public struct ForkLiftSystem
    {
        public bool LiftState;
        public bool TiltState;
        public bool SideShiftState;
        public bool ReachState;
        public bool MoverState;


        public bool TouchCanErr;
        public bool TouchEnableErr;
        public bool ReleaseCheck;

        public bool LiftSensorAlarm;
        public bool TiltSensorAlarm;
        public bool ShiftLeftSensorAlarm;
        public bool ShiftRightSensorAlarm;

        public bool LiftControlAlarm;
        public bool TiltControlAlarm;
        public bool MoverControlAlarm;
        public bool ShiftControlAlarm;

        public sbyte TiltPosition;
        public UInt16 LiftPosition;
        public UInt16 ReachPosition;
        public UInt16 MoverPosition;
        public UInt16 ShiftRightPosition;
        public UInt16 ShiftLeftPosition;
    }

    public struct NavigationSystem
    {
        public byte ModeState;
        public byte VehicleState;
        public bool VStateMoving;
        public bool VStatePausing;
        public bool VStateMoveComplete;
        public bool VStateBeamLowArea;
        public bool VStateBeamStopArea;
        public bool VStateCommTouch;
        public bool VStateCharging;
        public bool VStateForking;

        public ushort NAVLayer;
        public ushort NAVState;
        public ushort Alarm;

        public ushort CurrentLink;
        public ushort NextLink;

        public ushort CheckAFLID;

        public long NAV_X;
        public long NAV_Y;
        public short NAV_PHI;

        public ushort SlamScore;

        public byte ObjectDtectionState;
        public bool bUltraSonic;
        public bool bStAreaDetect;
        public bool bWT100;
        public bool bResetButton;
        public bool bStartButton;
        public bool bStopButton;

        public bool LineOption_Rear;
        public bool LineOption_Front;
        public byte AreaDetectionState;

        public ushort CurrentNode;
    }

    public struct FirmwareLog
    {
        public short DSState;
        public short LocaltargetCount;
        public short CurrentNode;
        public short NextNode;
        public short Rho;
        public short NavError;
        public short NavWarning;
        public short DL;
        public short DiffAngle;
        public short TargetSteer;
        public short CurrentSteer;
        public short TargetSpeed;
        public short CurrentSpeed;
        public short LeftRPM;
        public short RightRPM;
        public short SpeedOutPut;
        public short SteerOutPut;
        public short SavedCurrentNode;
        public short AFLID;
        public short HornWarningEMO;
        public short TargetRPM;
        public short LeftRPMDev;
        public short RightRPMDev;
        public short AccCount;
        public short DecCount;
        public short SpdCount;
        public short UnnormalSpeedCheck;
        public short SteerDev;
        public short SteerDevData;
        public short SteerCheckCount;
        public short UnnormalSteerCheck;
    }

    public struct CrevisEMO
    {
        public bool FrontLeft;
        public bool FrontRight;
        public bool RearLeft;
        public bool RearRight;
    }

    public struct VehicleSystem
    {
        public bool SystemMode;
        public bool SystemEMO;

        public bool DSNAlarm;
        public bool DSNMode;
        public bool SystemCharge;
        public bool NxUseable;

        public short CurrentAngle;
        public short CurrentSpeed;

        public byte DSNTractionAlarm;
        public byte DSNPumpAlarm;
    }

    public struct RecognitionSystem
    {
        // CAN ID 290
        public short TargetX;
        public short TargetY;
        public short TargetTheta;
        public byte AliveCount;

        // CAN ID 291
        public short FrontPalletX;
        public short FrontPalletY;
        public short FrontPalletTheta;
        public byte FrontPalletType;
        public bool FrontPalletResult;
        public byte Alarm;

        // CAN ID 334
        public short LeftServoMotorAngle;
        public short RightServoMotorAngle;
        public short Keyence_Current_Pos;
        public short Keyence_Current_Pos_2;

        // CAN ID 292
        public short LineTargetY;
        public short LineTargetTheta;
        public byte AreaState;
        public byte LineAlarm;
    }

    public struct BMSSystem
    {
        // CAN ID 340
        public short PackCurrent;
        public ushort PackVolt;
        public ushort ChargeVolt;
        public ushort CellVolt;
        public ushort Alarm;

        public sbyte Temperature;
        public byte Battery;
        public byte Fault;
        public byte Warning;

        public byte Status;

        public short Bat_V;
        public short Bat_i;
        public short SOC;
        public short SOH;

        public short OPV_Flag; // 팩 과전압
        public short UPV_Flag; // 팩 저전압
        public short OCV_Flag; // 셀 과전압
        public short UCV_Flag; // 셀 저전압
        public short OT_Flag; // 과 온도
        public short UT_Flag; // 저 온도
        public short ODV_Flag; // 과 셀전압 편차
        public short ODT_Flag; // 과 온도 편차
        public short OC_Flag; // 과 전류
        public bool Charge_Complete; // 충전완료
        public bool Charging; // 충전중
    }

    public struct SafetySystem
    {
        public byte FrontOutNS3;
        public byte FrontInNS3;

        public byte LeftOutNS3;
        public byte LeftInNS3;

        public byte RightOutNS3;
        public byte RightInNS3;

        public byte TailOutNS3;
        public byte TailInNS3;

        public short Obstacle;
        public int NANOSCAN3State;
        public int NANOSCAN3State_Finally;
        public bool FrontNS3Slow;
        public bool LeftNS3Slow;
        public bool RightNS3Slow;
        public bool TailNS3Slow;

        public bool FrontNS3State;
        public bool LeftNS3State;
        public bool RightNS3State;
        public bool TailNS3State;
        public bool WORK_SAFETY_FRONTNS3_NONE;
        public bool WORK_SAFETY_FRONTNS3_SLOW;
        public bool WORK_SAFETY_FRONTNS3_STOP;
        public bool WORK_SAFETY_LEFTNS3_NONE;
        public bool WORK_SAFETY_LEFTNS3_SLOW;
        public bool WORK_SAFETY_LEFTNS3_STOP;
        public bool WORK_SAFETY_RIGHTNS3_NONE;
        public bool WORK_SAFETY_RIGHTNS3_SLOW;
        public bool WORK_SAFETY_RIGHTNS3_STOP;
        public bool WORK_SAFETY_TAILNS3_NONE;
        public bool WORK_SAFETY_TAILNS3_SLOW;
        public bool WORK_SAFETY_TAILNS3_STOP;

        public bool VisioneryT;
        public bool Loading_Fault;

        public byte LeftOutTIM;
        public byte LeftInTIM;

        public byte RightOutTIM;
        public byte RightInTIM;

        public byte RearOutTIM;
        public byte RearInTIM;

        public ushort PressureSensor;

        public bool LeftWT100;
        public bool RightWT100;

        public bool LeftPx;
        public bool RightPx;

        public bool LeftKeyence;
        public bool RightKeyence;

        public bool RemoteSwith;
    }

    public struct RecognitionParameter
    {
        public ushort MinDistance;
        public byte ReceiverGain;
        public byte PreSharpening;
        public byte PostSharpening;
        public byte Confidence;
        public byte NoiseFiltering;
        public byte PalletDistanceUp;
        public byte PalletDistanceDown;

        public ushort ObstacleX; // kdy
        public ushort ObstacleY; // kdy

        public bool UpdateFlag;
        public bool UpdateResult;
        public short Error_Response; // kdy
        public bool Obstacle_Detect; // kdy


        // Camera Motor Update
        public short RotateMotorLeft;
        public short RotateMotorRight;

        public bool RotateMotorUpdateFlag;

        public bool RotateMotorLeftUpdateResult;
        public bool RotateMotorRightUpdateResult;

    }

    public struct VehicleParameter
    {
        public byte StackingAngle;
        public byte StackingDistance;
        public byte MinSpeed;
        public byte MaxSpeed;

        public bool UpdateFlag;

        public bool UpdateResult;
    }

}
