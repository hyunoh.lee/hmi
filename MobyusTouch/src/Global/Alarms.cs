﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MobyusTouch.src.Global
{
    public static class Alarms
    {

        // 작업 성공 및 알람 정상 코드
        public const int WORK_SUCCESS = 0;
        public const int NO_ALARM = 0;
        public const int WARNING = 200;

        //////////////////////////////////////////////
        // Alarm
        // 제품 감지 알람
        public const int ALARM_PALLET_LOADED_ERROR = 10; // 명령이 적재고 지게차에 팔렛이 이미 적재되어있는 경우
        public const int ALARM_PALLET_LOADED_NOT_ERROR = 11; // 명령이 이재고 지게차에 팔렛이 없을 경우

        // 제품 위치 알람
        public const int ALARM_PALLET_FRONT_EXIST_ERROR = 12; // 명령이 이재이나, 목적지에 팔렛이 있는 경우
        public const int ALARM_PALLET_FRONT_NOT_EXIST_ERROR = 13; // 명령이 적재이나, 목적지에 팔렛이 없는 경우

        public const int ALARM_FORK_EXIST_OBASTACLE_ERROR = 14; // 포크 진입 시 장애물 감지

        // 경로 계획 알람
        public const int ALARM_MOVE_FRONT_PALLET_RETRY_ERROR = 35; // 전방 팔레트 틀어짐 재시도 초과 시 알람

        public const int ALARM_MOVE_1ST_PATH_PLANNING_ERROR = 40;  // 1차 경로 생성 실패 시(목적지 전 노드까지) 
        public const int ALARM_MOVE_2ND_PATH_PLANNING_ERROR = 41; // 2차 경로 생성 실패 시(목적지 전 노드부터 목적지까지)
        public const int ALARM_MOVE_PATH_SENDING_ERROR = 42; // Navigation Board에 Path 데이터 전달 실패 시
        public const int ALARM_MOVE_NO_PATH_DATA_ERROR = 43; // Path 데이터가 없을 경우
        public const int ALARM_MOVE_FRONT_PALLET_ANGLE_OVER_ERROR = 44; // 전방 팔레트 각도가 15도 이상일 경우
        public const int ALARM_MOVE_COMPLETE_ERROR = 45; // 노드 주행중 오류 시 

        public const int ALARM_MAP_SET_CURRENT_NODE_ERROR = 46; // 현재 노드를 찾을 수 없을 때
        public const int ALARM_MAP_SET_CURRENT_NODE_SEND_SERIAL_ERROR = 47; // 현재 노드를 Navgation Board에 전달 실패 시
        public const int ALARM_MAP_NOT_FOUND_NODE_INFO_ERROR = 48; // 현재 노드의 정보를 찾을 수 없을 때
        public const int ALARM_MAP_TRANSFER_HEADING_ERROR = 49; // 지게차 heading 에러

        // 충전 알람
        public const int ALARM_CHARGE_ERROR = 50; // 충전 시 알람

        // ACS 잘못된 명령
        public const int ALARM_ACS_WRONG_ORDER = 51; // 타겟이 스테이션일때 이동명령 안됨, 타겟이 일반노드일때 이적재 안됨

        // ACS 통신 에러 
        public const int ALARM_ACS_COMM_ERROR = 52; // GQ 수신 못받으면 RCU, Mqtt 연결 에러

        public const int ALARM_VEHICLE_BOARD_EMO_SWITCH_ERROR = 55; //EMO 스위치가 눌러 있을 경우

        // NAV 알람
        public const int ALARM_NAV_350_ERROR = 60; // NAV 알람
        public const int ALARM_NAV_GYRO_ERROR = 61; // NAV 자이로 알람 (NAV센서에서 주는 알람)
        public const int ALARM_NAV_NO_DATA_ERROR = 62; // NAV 데이터 에러
        public const int ALARM_NAV_CAN_ERROR = 63; // NAV CAN 에러

        // 이적재 상태 알람
        public const int ALARM_LOADED_STATE_ERROR = 64; // 이적재시 키엔에스 상태
        //
        public const int ALARM_GYRO_ERROR = 65; // 자이로 상태 (NAVI 보드에서 주는 알람)

        // 장애물 감지
        public const int ALARM_NAVI_BOARD_STACKING_ERROR = 68;  // 스태킹 시 알람
        public const int ALARM_NAVI_OBSTACLE_STOP_KEEP_ERROR = 69;  // 장애물 지속 감지 알람 3분 이상 시
        public const int ALARM_NAVI_OBSTACLE_WT100_STOP_KEEP_ERROR = 66;  // 장애물 지속 감지 알람 15초 이상 시

        // Navigation 관련 알람
        public const int ALARM_NAVI_LOADING_FAULT_ERROR = 67; // 포크 팔레트 진입 후 도착 전에 적재 시 리프트값 변화가 있으면 알람 발생
        public const int ALARM_NAVI_BOARD_EMO_ERROR = 70; //EMO 
        public const int ALARM_NAVI_BOARD_BUMPER_STOP_ERROR = 71; // Bumper 스위치가 눌렀을 경우
        public const int ALARM_NAVI_BOARD_MOTION_ERROR = 72; // 링크타입이 직선, 커브, 가상링크 외에 정의되지 않은 경우
        public const int ALARM_NAVI_BOARD_MAP_ERROR = 73; // 현재 노드 및 목적 노드가 0일 경우, 링크타입이 맞지 않을 때(Motion Error와 같이 발생)
        public const int ALARM_NAVI_BOARD_VIRTUAL_LINK_ERROR = 74; // Virtual Link 생성중 카메라 데이터가 들어오지 않을 때
        public const int ALARM_NAVI_BOARD_PATH_OVER_ERROR = 75; // 탈선
        public const int ALARM_NAVI_BOARD_HEADING_OVER_ERROR = 76; // 현재 방향과 경로의 이동방향이 다른 경우
        public const int ALARM_NAVI_BOARD_LOCAL_TARGET_ERROR = 77; // 주행중에 목적지까지의 거리가 점점 늘어날 경우
        public const int ALARM_NAVI_BOARD_NAV_ERROR = 78; // NAV 데이터가 들어오지 않을 경우
        public const int ALARM_NAVI_ALARM_STOP_ERROR = 79; // 주행중 알람이 발생했을 경우


        // Navigation 추가 알람 
        public const int ALARM_NAVI_BOARD_AFL_CAN_ERROR = 20; // 두산캔 통신 끊김 에러 
        public const int ALARM_NAVI_BOARD_LINE_DETECTION_ERROR = 21; // 라인 감지 에러
        public const int ALARM_NAVI_COMM_ERROR = 22; // HMI - 제어 간 통신 에러
        public const int ALARM_NAVI_UNNORMALSTEER_ERROR = 23; // STEER 에러 KDY 23.08.24 급발진 방어코드로 추가
        public const int ALARM_NAVI_CHANGE_AFL_ID_ERROR = 24; // 호차변환 에러 KDY 23.10.10 

        // 두산 자체에러
        public const int ALARM_DSN_ERROR = 25;

        // Vehicle 관련 알람 Traction
        public const int ALARM_VEHICLE_CAN_ERROR = 80;
        public const int ALARM_VEHICLE_PUMP_PDO_ERROR = 81;
        public const int ALARM_VEHICLE_THROTTLE_ON_STARTUP_PDO_ERROR = 82;
        public const int ALARM_VEHICLE_SRO_DIRECTION_BEFORE_SEAT_ERROR = 83;
        public const int ALARM_VEHICLE_THROTTLE_ERROR = 84;
        public const int ALARM_VEHICLE_STEER_POT_ERROR = 85;
        public const int ALARM_VEHICLE_LOW_BATTERY_VOLT_ERROR = 86;
        public const int ALARM_VEHICLE_CALIBRATION_DISPLAY_SELECTED_ERROR = 87;
        public const int ALARM_VEHICLE_DISPLAY_PDO_ERROR = 88;
        public const int ALARM_VEHICLE_PDO_ERROR = 89;
        public const int ALARM_VEHICLE_SEVERE_STEERING_ERROR = 90;
        public const int ALARM_VEHICLE_STEERING_ERROR = 91;
        public const int ALARM_VEHICLE_AGV_PDO_TIMEOUT_ERROR = 92;
        public const int ALARM_VEHICLE_PDO_TIMEOUT_BMS_ERROR = 93;

        public const int ALARM_VEHICLE_PUMP_SIGN_ON_ERROR = 94;
        public const int ALARM_VEHICLE_JOYSTICK_COMM_ERROR = 95;
        public const int ALARM_VEHICLE_JOYSTICK_LIFT_ERROR = 96;
        public const int ALARM_VEHICLE_JOYSTICK_INPUT_BEFORE_KEW_SWITCH_ERROR = 97;
        public const int ALARM_VEHICLE_JOYSTICK_TILT_ERROR = 98;
        public const int ALARM_VEHICLE_JOYSTICK_AUX1_ERROR = 99;
        public const int ALARM_VEHICLE_JOYSTICK_AUX2_ERROR = 100;
        public const int ALARM_VEHICLE_FINGERTIP_ENABLE_CHANGE_ERROR = 101;
        public const int ALARM_VEHICLE_EXM_TILT_BACK_ERROR = 102;
        public const int ALARM_VEHICLE_EXM_LIFT_SOL_ERROR = 103;
        public const int ALARM_VEHICLE_EXM_LOW_SOL_ERROR = 104;
        public const int ALARM_VEHICLE_EXM_AUX1_FWD_SOL_ERROR = 105;
        public const int ALARM_VEHICLE_EXM_TILT_FWD_SOL_ERROR = 106;
        public const int ALARM_VEHICLE_EXM_AUX1_BACK_SOL_ERROR = 107;
        public const int ALARM_VEHICLE_EXM_ERROR = 108;
        public const int ALARM_VEHICLE_EXM_PDO_ERROR = 109;

        // FORK 알람
        public const int ALARM_FORK_LIFT_RANGE_OVER_ERROR = 110;                // LIFT 최대 및 최소 범위 값을 넘었을 경우
        public const int ALARM_FORK_TILT_RANGE_OVER_ERROR = 111;                // TILT 최대 및 최소 범위 값을 넘었을 경우
        public const int ALARM_FORK_REACH_RANGE_OVER_ERROR = 112;               // REACH 최대 및 최소 범위 값을 넘었을 경우
        public const int ALARM_FORK_SIDESHIFT_RANGE_OVER_ERROR = 113;           // SIDESHIFT 최대 및 최소 범위 값을 넘었을 경우
        public const int ALARM_FORK_MOVER_RANGE_OVER_ERROR = 114;               // MOVER 최대 및 최소 범위 값을 넘었을 경우

        public const int ALARM_FORK_LIFT_SENSOR_ERROR = 115;                    // LIFT 센서에 문제가 있을 경우
        public const int ALARM_FORK_TILT_SENSOR_ERROR = 116;                    // TILT 센서에 문제가 있을 경우
        public const int ALARM_FORK_SHIFT_LEFT_SENSOR_ERROR = 117;              // LEFT SHIFT 센서에 문제가 있을 경우
        public const int ALARM_FORK_SHIFT_RIGHT_SENSOR_ERROR = 118;             // RIGHT SHIFT 센서에 문제가 있을 경우

        public const int ALARM_FORK_LIFT_CONTROL_ERROR = 119;                   // LIFT 제어 명령 시 오류가 있을 경우
        public const int ALARM_FORK_TILT_CONTROL_ERROR = 120;                   // TILT 제어 명령 시 오류가 있을 경우
        public const int ALARM_FORK_MOVER_CONTROL_ERROR = 121;                  // MOVER 제어 명령 시 오류가 있을 경우
        public const int ALARM_FORK_SHIFT_CONTROL_ERROR = 122;                  // SHIFT 제어 명령 시 오류가 있을 경우

        // 라인 인식 알람
        public const int ALARM_RECOGNITION_LINE_ERROR = 130;                    // 라인 인식 알람

        // 팔렛 인식 알람
        public const int ALARM_RECOGNITION_ERROR = 131;                         // 기본 인식 에러
        public const int ALARM_RECOGNITION_PALLET_BOTTOM_LEFT_ROI_ERROR = 132;  // 왼쪽 하단 초기 ROI 알람
        public const int ALARM_RECOGNITION_PALLET_BOTTOM_RIGHT_ROI_ERROR = 133; // 오른쪽 하단 초기 ROI 알람
        public const int ALARM_RECOGNITION_PALLET_BOTTOM_LEFT_ERROR = 134;      // 왼쪽 하단 인식 알람
        public const int ALARM_RECOGNITION_PALLET_BOTTOM_RIGHT_ERROR = 135;     // 오른쪽 하단 인식 알람
        public const int ALARM_RECOGNITION_PALLET_TOP_LEFT_ERROR = 136;         // 왼쪽 상단 인식 알람
        public const int ALARM_RECOGNITION_PALLET_TOP_RIGHT_ERROR = 137;        // 오른쪽 상단 인식 알람
        public const int ALARM_RECOGNITION_ITEM_HEIGHT_ERROR = 138;             // 제품 높이 불일치 알람
        public const int ALARM_RECOGNITION_PALLET_ANGLE_OVER_ERROR = 139;       // 팔레트 Angle Over 알람


        public const int ALARM_RECOGNITION_FRONT_ERROR = 140;                   // 3단 전방 물체 인식

        public const int ALARM_RECOGNITION_DRIVE_DATA_X_ERROR = 141;            // DATA X 오차 (주행중 200mm)
        public const int ALARM_RECOGNITION_DRIVE_DATA_Y_ERROR = 142;            // DATA Y 오차 (주행중 100mm)
        public const int ALARM_RECOGNITION_DRIVE_DATA_T_ERROR = 143;            // DATA Theta 오차 (주행중 10도)
        public const int ALARM_RECOGNITION_SAFETY_DATA_X_ERROR = 144;           // DATA X 오차 (안착감지 10mm)
        public const int ALARM_RECOGNITION_SAFETY_DATA_Y_ERROR = 145;           // DATA Y 오차 (안착감지 10mm)
        public const int ALARM_RECOGNITION_SAFETY_DATA_T_ERROR = 146;           // DATA Theta 오차 (안착감지 1도)

        public const int ALARM_RECOGNITION_CAN_RECEIVE_ERROR = 147;             // CAN 알람
        public const int ALARM_RECOGNITION_ROS_TOPIC_ERROR = 148;               // ROS 카메라 토픽 알람
        public const int ALARM_RECOGNITION_SYSTEM_ERROR = 149;                  // 시스템 알람



        // CAN 알람
        public const int ALARM_CAN_CONNECTION_ERROR = 150;
        public const int ALARM_CAN_290_RECOGNITION_CONNECTION_ERROR = 151;
        public const int ALARM_CAN_291_RECOGNITION_CONNECTION_ERROR = 152;
        public const int ALARM_CAN_293_RECOGNITION_CONNECTION_ERROR = 153;

        public const int ALARM_CAN_260_NAVIGATION_CONNECTION_ERROR = 154;
        public const int ALARM_CAN_261_NAVIGATION_CONNECTION_ERROR = 155;

        public const int ALARM_CAN_220_VEHICLE_CONNECTION_ERROR = 156;
        public const int ALARM_CAN_221_VEHICLE_CONNECTION_ERROR = 157;

        public const int ALARM_CAN_241_FORKLIFT_CONNECTION_ERROR = 158;
        public const int ALARM_CAN_242_FORKLIFT_CONNECTION_ERROR = 159;

        public const int ALARM_CAN_340_BMS_CONNECTION_ERROR = 160;
        public const int ALARM_CAN_341_BMS_CONNECTION_ERROR = 161;
        public const int ALARM_CAN_333_GATEWAY_CONNECTION_ERROR = 162;
        public const int ALARM_CAN_330_SAFETY_CONNECTION_ERROR = 163;
        public const int ALARM_CAN_334_SAFETY_CONNECTION_ERROR = 164;
        public const int ALARM_CAN_342_BMS_CONNECTION_ERROR = 168;

        public const int ALARM_TAMS_EQUIPMENT_FAULT_ERROR = 165;

        //kdy 23.05.18 PAUSE 3분 지속시 에러
        public const int ALARM_TRANSFER_TRAFFIC_REMOTESWITCH_ERROR = 166;

        //kdy 23.08.02 급발진 방어코드, 속도이상동작에러
        public const int ALARM_NAVI_BOARD_SPEED_ERROR = 167; // 추가한거 홍우과장한테 전달 필요

        public const int ALARM_BMS_TEMPERATURE_ERROR = 175; // 24도 이상
        public const int ALARM_BMS_ERROR = 176; // BMS자체 ERROR
        public const int ALARM_BMS_CURRENT_ERROR = 177; // BMS 전류 에러
        public const int ALARM_BMS_SOC_ERROR = 178; // BMS SOC 에러
        public const int ALARM_BMS_BATTERY_LOW_ERROR = 180; // 30% 미만
        public const int ALARM_BMS_COMM_ERROR = 181;
        public const int ALARM_BMS_MAIN_RELAY_ERROR = 182;
        public const int ALARM_BMS_PRE_CHARGE_RELAY_ERROR = 183;
        public const int ALARM_BMS_FUSE_OPEN_ERROR = 184;
        public const int ALARM_BMS_SHUTDOWN_ERROR = 185;
        public const int ALARM_BMS_MAIN_RELAY_STATUS_ERROR = 186;
        public const int ALARM_BMS_PRE_CHARGE_RELAY_STATUS_ERROR = 187;

        public const int ALARM_BMS_VOLTAGE_LOW_ERROR = 188;
        public const int ALARM_BMS_VOLTAGE_HIGH_ERROR = 189;

        public const int ALARM_WORK_FILE_LIST_ERROR = 190; // Work List 파일을 읽지 못하거나 파일이 없는 경우
        public const int ALARM_WORK_FILE_FORMAT_ERROR = 191; // 잘못된 Work 파일일 경우
        public const int ALARM_WORK_TIME_OVER_ERROR = 192; // 20분 이내 작업이 완료되지 않은 경우
        public const int ALARM_WORK_WRONG_ORDER_ERROR = 193; // 명령 정보가 일치하지 않을 경우

        public const int ALARM_MAP_PARSE_ERROR = 195; // 파싱을 하지 못한 경우

        public const int ALARM_SYSTEM_UNKNOWN_ERROR = 199; // 알수 없는 오류 알람


        ////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////
        // WARNING 경고
        public const int ALARM_PROTOCOL_FORMAT_ERROR = 210; // 프로토콜 포맷 에러
        public const int ALARM_PALLET_TYPE_INFO_ERROR = 212; // 잘못된 팔레트 정보일 경우
        public const int ALARM_MODE_WRONG_ERROR = 215; // 잘못된 SYSTEM MODE값일 경우

       

        public const int ALARM_TCP_CONNECTION_ERROR = 220;   // TCP 연결이 안될 경우
        public const int ALARM_SERIALPORT_CONNECTION_ERROR = 221; // 시리얼포트 연결이 안될 경우
        public const int ALARM_MQTT_CONNECTION_ERROR = 222; // MQTT 연결이 안될 경우
        public const int ALARM_RELEASE_CHECK = 223;
        public const int ALARM_RETRY = 224;
        // ACS RCU 통신 
        public const int ALARM_ACS_RCU_COMM_WARNING = 225; // GQ 수신 못받으면 RCU 연결 경고

        public const int ALARM_WORK_ALREADY_ING_ERROR = 230; // 이미 진행된 작업이 존재하는 경우

        public const int ALARM_BMS_OPV = 240; // 팩 과전압
        public const int ALARM_BMS_UPV = 241; // 팩 저전압
        public const int ALARM_BMS_OCV = 242; // 셀 과전압
        public const int ALARM_BMS_UCV = 243; // 셀 저전압
        public const int ALARM_BMS_OT = 244; // 과 온도
        public const int ALARM_BMS_UT = 245; // 저 온도
        public const int ALARM_BMS_ODV = 246; // 과 셀전압 편차
        public const int ALARM_BMS_ODT = 247; // 과 온도 편차
        public const int ALARM_BMS_OC = 248; // 과 전류
    }
}
