﻿
using MobyusTouch.src.Global.Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

//using MapDLL;
using EditorLibrary;
using System.Windows.Forms;

namespace MobyusTouch.src.Global
{
    public static class Globals
    {
        public const bool debug = false;

        public static string Site = "";
        public static string Language = "";
        public static string Type = "";
        public static string ID = "";
        public static int AFL_ID;
        public static int pre_AFL_ID;
        public static string VALUE = "";
        public static string LIFT_OFFSET = "";

        public const string SITE_SAMSUNG = "SAMSUNG";
        public const string SITE_HYUNDAI = "HYUNDAI";

        public const string SITE_SEJONG = "SEJONG";
        public const string SITE_BRAZIL = "BRAZIL";

        public const string LANG_EN = "EN";
        public const string LANG_PT = "PT";

        public const string TYPE_LIFT = "LIFT";
        public const string TYPE_LIFT_CRANK = "LIFT_CRANK";

        public const string TYPE_CV_2 = "CV_2";
        public const string TYPE_CV_3 = "CV_3";

        public static string BOARD_COMM_TIME = "";
        public static string ACS_TIU_COMM_TIME = "";
        public static string ACS_TCP_COMM_TIME = "";

        public static string Test = "";
        public static string TRAFFIC_PASS = "";

        public static string INFLUX_ADDR = "";
        public static string INFLUX_TOKEN = "";


        public static Hashtable alarmTable = new Hashtable();

        public static Dictionary<string, Object> workList = new Dictionary<string, Object>();

        // File Path Info
        public const string MOBYUS_SettingDirPath = "C:\\MOBYUS";
        public const string INFO_SettingDirPath = "C:\\MOBYUS";
        public const string LOG_SettingDirPath = "C:\\MOBYUS\\logs";
        public const string WORK_SettingDirPath = "C:\\MOBYUS\\work";

        public const string HMI_SettingFilePath = "C:\\MOBYUS\\HMI_INFO.ini";
        public const string MAP_SettingFilePath = "C:\\MOBYUS\\MAP.dat";

        public const string FMS_IP = "192.168.0.10";

        public const string PATH_PLANNING_AMR = "A";
        public const string PATH_PLANNING_ACS = "M";

        public const int MAP_NODE_NORMAL = 0; // 일반 노드
        public const int MAP_NODE_STATION = 1; // 스테이션 노드
        public const int MAP_NODE_CHARGE = 2; // 충전 노드
        public const int MAP_NODE_EQUIPMENT = 3; // 설비 노드

        //////////////////////////////////////////////
        //////////////////////////////////////////////
        // Main Work 관련
        public const int TRANSFER_READY = 100;
        public const int TRANSFER_MOVE = 110;
        public const int TRANSFER_LOAD = 120;
        public const int TRANSFER_UNLOAD = 130;
        public const int TRANSFER_CHARGE = 140;

        public const int TRANSFER_SCENARIO = 200;


        //////////////////////////////////////////////
        //////////////////////////////////////////////
        // Sub Work 관련
        // 주행
        public const int WORK_MOVE_PATH_PLANNING = 1000;
        public const int WORK_MOVE_1ST_PATH_PLANNING = 1100;
        public const int WORK_MOVE_2ND_PATH_PLANNING = 1200;
        public const int WORK_MOVE_PATH_SENDING = 1300;
        public const int WORK_MOVE_FROM_STATION = 1400;
        public const int WORK_MOVE_TO_STATION = 1500;
        public const int WORK_MOVE_COMPLETE = 1600;
        public const int WORK_MOVE_END = 1700;

        // 포크
        public const int WORK_FORK_LIFTING = 2100;


        public const int WORK_FORK_TILTING = 2200;
        public const int WORK_FORK_REACHING = 2300;
        public const int WORK_FORK_SIDESHIFT = 2400;
        public const int WORK_FORK_FORKMOVER = 2500;

        // 충전
        public const int WORK_CHARGE_ENABLE = 3100;
        public const int WORK_CHARGE_DISABLE = 3200;

        // 카메라
        public const int WORK_PALLET_STACKING_ENABLE = 4100;
        public const int WORK_PALLET_STACKING_DISABLE = 4101;
        public const int WORK_PALLET_FRONT_ENABLE = 4200;
        public const int WORK_PALLET_FRONT_DISABLE = 4201;
        public const int WORK_PALLET_HEIGHT_ENABLE = 4300;
        public const int WORK_PALLET_HEIGHT_DISABLE = 4301;
        public const int WORK_PALLET_STACKING_SUCCESS_ENABLE = 4400;
        public const int WORK_PALLET_STACKING_SUCCESS_DISABLE = 4401;
        public const int WORK_PALLET_FRONT_EXIST_ENABLE = 4500;
        public const int WORK_PALLET_FRONT_EXIST_DISABLE = 4501;

        // 검증
        public const int WORK_CHECK_EXIST_PALLET_FRONT = 5100;
        public const int WORK_CHECK_EXIST_PALLET_LOADED = 5200;

        // 안전
        public const int WORK_SAFETY_CLAMP_ENABLE = 6100;
        public const int WORK_SAFETY_CLAMP_DISABLE = 6200;

        //public const short WORK_SAFETY_FRONTNS3_NONE = 0;
        public const short WORK_SAFETY_FRONTNS3_SLOW = 1;
        public const short WORK_SAFETY_FRONTNS3_STOP = 2;
        //public const short WORK_SAFETY_LEFTNS3_NONE = 3;
        public const short WORK_SAFETY_LEFTNS3_SLOW = 4;
        public const short WORK_SAFETY_LEFTNS3_STOP = 8;
        //public const short WORK_SAFETY_RIGHTNS3_NONE = 6;
        public const short WORK_SAFETY_RIGHTNS3_SLOW = 16;
        public const short WORK_SAFETY_RIGHTNS3_STOP = 32;
        //public const short WORK_SAFETY_TAILNS3_NONE = 9;
        public const short WORK_SAFETY_TAILNS3_SLOW = 64;
        public const short WORK_SAFETY_TAILNS3_STOP = 128;


        //////////////////////////////////////////////////////
        //////////////////////////////////////////////////////
        // 전역 변수
        public static HMISystem HMISystem = new HMISystem(SystemMode.OFFLINE, SystemTraffic.TRANSFER_TRAFFIC_ACS_NONE);
        public static ForkLiftSystem ForkLiftSystem;
        public static NavigationSystem NavigationSystem;
        public static FirmwareLog FirmwareLog;
        public static VehicleSystem VehicleSystem;
        public static CrevisEMO CrevisEMO;
        public static RecognitionSystem RecognitionSystem;
        public static BMSSystem BMSSystem;
        public static SafetySystem SafetySystem;


        public static RecognitionParameter RecognitionParameter;
        public static VehicleParameter VehicleParameter;

        public static SystemMode tempSystemMode;
        public static SystemMode preSystemMode;

        public static SaveData MapData = new SaveData();

        public static List<NodeInfo> nodeData = new List<NodeInfo>();
        public static List<LinkInfo> linkData = new List<LinkInfo>();

        // Work
        public static WorkEntity currentWork = new WorkEntity();
        public static WorkStep workStep;
        public static bool moveComplete = false;
        public static int nMoveCompleteCount = 0;
        public static bool keepWork = false;
        public static bool loaded = false;

        public static bool bRefresh_worklistView = false;
        public static string strWL_LEVEL = "";
        public static string strWL_NODE = "";
        public static string strWL_PALLETE_ID = "";
        public static string strWL_WORK = "";
        public static string strWL_LINK_TYPE = "";

        //ACS 오더 정보
        public static int nACSCorrectCmd = 0;
        public static string strACSCmd = "";
        public static bool bACSOrderDlgShow = true;
        public static bool IgnoreACS = true;

        public static bool b1stPathPlanContinue = false;
        public static bool b2ndPathPlanContinue = false;
        public static bool bNoRocFromFirmware = false;

        //테스트
        public static bool bTest = false;

        //리트라이
        public static bool bRetry_ACSRetryCheck = false;
        public static bool bRetry_FirstPathRetry = false;
        public static bool bRetry_FirstMoveComplete = false;
        public static bool bRetry_SecondPathRetry = false;
        public static bool bRetry_LiftingRetry = true;
        public static int nRetry_HeadTypeLiftingRetry = 0;

        //무브컴플리트
        public static bool bFirstPathComplete = false;
        public static bool bSecondPathComplete = false;
        public static int nFirstPath_LastNode = 0;
        public static int nLastNodeID = 0;
        public static int nLastLinkID = 0;

        //전광판
        public static bool bDisplaySecondPathWork = false;
        public static bool bDisplayPauseCheck = true;

        //RCU 연결확인
        public static int nRCUConnectCheck = 0;
        public static bool bRCUAlarmCheck = false;

        public static int mqtt_connection_count = 0;

        //주행중 체크
        public static int nCorrectMoving = 0;
        public static ushort uTmpCurNode = 0;

        //HMI에서 현재 노드와 링크를 기억하기 위함
        //public static List<AstarNodeData> tmpPath = new List<AstarNodeData>();
        public static ushort curTmpLink = 0;
        public static ushort curTmpNode = 0;
        public static bool bNodeLinkCheck = true;
        public static int nChangeNodeMode = 0;

        //네비알람 확인
        public static int nNaviAlarm_Check = 0;

        //알람창
        public static bool bAlarmMemory = false;
        public static bool bDlgEMOShow = false;

        //AR 메세지 확인
        public static bool bACS_ARmsgCheck = false;

        //nav 스테이트확인
        public static bool bNAV_StateCheck = false;

        //매뉴얼컴플리트 
        public static int nManualComplete = 0;

        //강제 컴플리트
        public static int nForceComplete = 0;

        //HMI 프로그램 시작 확인
        public static int nAFL_Start = 0;

        //to노드 저장
        public static ushort toTmpNode = 0;

        //오토모드 AR 대기
        public static bool bAutoMode_AR_Wait = false;

        //PAUSE 카운트 
        public static int TrafficPauseCount = 0;
        public static int RemoteSwichCount = 0;
        public static bool RemoteSwichFlag = false;

        public static bool bStartPushWait = false;
        public static bool bResetPushWait = false;
        public static bool bStopPushWait = false;

        public static bool bAlarmSendACS = false;

        //동시 콜백 Race Condition 
        public static object callbackLock = new object();

        public static bool bAdd_newParam = false;
        public static bool bEdit_Param = false;
        public static string strWL_PR_NAME = "";
        public static string strWL_PR_SLEEP = "";
        public static string strWL_PR_SYNC = "";
        public static string strWL_PR_VALUE = "";

        // 충전 플레그
        public static bool bStartChargeFlag = true;
        public static int nChargingCount;
        public static int nFirstSOC;
        public static int nLastSOC;

        public static int SOCCount;
        public static int nFirstSOC2;
        public static int nLastSOC2;
        public static bool ChargeCheckFlag;

        public static int CurrentTimeCheck;

        //다음노드
        public static ushort curNodeforNextNode = 0;

        //gq로그 카운트
        public static ushort GQcount = 0;

        //firmware로그 카운트
        public static ushort FWCount = 0;

        // 장애물
        public static bool bObstacle_Puase = false;

        // 배터리 전압 카운트
        public static ushort BatVCount = 0;

        // 진행중인 명령 표시
        public static string tmp_text = "-";

        // Communication 
        public static bool CAN_181_FLAG = false;
        public static bool CAN_220_FLAG = false;
        public static bool CAN_221_FLAG = false;

        public static bool CAN_241_FLAG = false;
        public static bool CAN_242_FLAG = false;

        public static bool CAN_260_FLAG = false;
        public static bool CAN_261_FLAG = false;
        public static bool CAN_263_FLAG = false;
        public static bool CAN_264_FLAG = false;
        public static bool CAN_265_FLAG = false;
        public static bool CAN_266_FLAG = false;
        public static bool CAN_267_FLAG = false;
        public static bool CAN_268_FLAG = false;
        public static bool CAN_269_FLAG = false;

        public static bool CAN_290_FLAG = false;
        public static bool CAN_291_FLAG = false;
        public static bool CAN_293_FLAG = false;

        public static bool CAN_330_FLAG = false;
        public static bool CAN_331_FLAG = false;
        public static bool CAN_332_FLAG = false;
        public static bool CAN_333_FLAG = false;
        public static bool CAN_334_FLAG = false;

        public static bool CAN_340_FLAG = false;
        public static bool CAN_341_FLAG = false;
        public static bool CAN_342_FLAG = false;

        public static string fmsIpAddress;
        public static int fmsIpPort;
        public static string LED_IP;
        public static int LED_Port;

        public static string serialPortName;
        public static int serialBaudRate;
        public static int serialDataBits;
        public static Parity serialParityBits;
        public static StopBits serialStopBits;

        // Fork value before control
        public static int beforeLiftValue = 0;
        public static int beforeTiltValue = 0;
        public static int beforeReachValue = 0;
        public static int beforeSideShiftValue = 0;
        public static int beforeForkMoverValue = 0;

        public static int beforeLiftValue_ = 0;
        public static int beforeTiltValue_ = 0;
        public static int beforeReachValue_ = 0;
        public static int beforeSideShiftValue_ = 0;
        public static int beforeForkMoverValue_ = 0;

        // Work While Count
        public static int nWhileLiftWorkCount = 0;
        public static int nWhileTiltWorkCount = 0;
        public static int nWhileSideShiftWorkCount = 0;
        public static int nWhileForkMoverWorkCount = 0;


        // LIMIT OF FORK CONTROL
        public static int FORK_LIFT_MAX_LIMIT = 0;
        public static int FORK_LIFT_MIN_LIMIT = 0;

        public static int FORK_TILT_MAX_LIMIT = 0;
        public static int FORK_TILT_MIN_LIMIT = 0;

        public static int FORK_REACH_MAX_LIMIT = 0;
        public static int FORK_REACH_MIN_LIMIT = 0;

        public static int FORK_MOVER_MAX_LIMIT = 0;
        public static int FORK_MOVER_MIN_LIMIT = 0;

        public static int FORK_SHIFT_LIMIT = 0;
    }
}
