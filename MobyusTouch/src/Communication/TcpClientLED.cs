﻿using MobyusTouch.src.Global;
using MobyusTouch.src.Global.Entity;
using MobyusTouch.src.Main;
using MobyusTouch.src.Main.Sub;
using MobyusTouch.src.Utility;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MobyusTouch.src.Communication
{
    class TcpClientLED
    {
        private static Logger logger = LogManager.GetLogger("TcpClientLED");
        private static SocketClient socketClient = new SocketClient();

        public static bool Connect()
        {
            bool result = false;
            try
            {
                socketClient.ConnectToServer(Globals.LED_IP, Globals.LED_Port);
                socketClient.OnDataReceived += new ClientHandlePacketData(OnDataReceived);

                if (socketClient.IsConnected())
                {
                    logger.Info("LED Socket Client Connected.");
                    result = true;
                }
            }
            catch (Exception ex)
            {
                logger.Info(ex.ToString());
            }
            return result;
        }

        public static bool IsConnected()
        {
            return socketClient.IsConnected();
        }

        public static void Disconnect()
        {
            socketClient.Disconnect();
        }

        public static void SendText(string strdata)
        {
            byte[] data = Encoding.Default.GetBytes(strdata);
            if (IsConnected()) 
            { 
                socketClient.SendImmediate(data);
            }
        }

        private static void OnDataReceived(byte[] data, int bytesRead)
        {
            ASCIIEncoding encoder = new ASCIIEncoding();
            string MSG = encoder.GetString(data, 0, bytesRead);
        }
    }
}
