﻿using MobyusTouch.src.Global;
using MobyusTouch.Src.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.Drawing;
using MobyusTouch.src.Control;
using NLog;
//using MapDLL;
using EditorLibrary;

namespace MobyusTouch.src.Communication
{
    class SerialPortBoard
    {
        private static Logger logger = LogManager.GetLogger("SerialPortBoard");

        private SerialPortBoard instance;
        public static bool bSetConnect = true;

        private static bool serial_response_flag = false;
        private static int return_cmd = 0;
        private static int com_path_length = 15;
        private static int path_size = 20;

        public static bool bSendLogFlag = false;
        public static bool bRecvLogFirst = false;
        public static byte[] byteLog = new byte[1000];
        public static int nLogDataSize = 0;
        public static int nLogFirstSize = 0;
        public static int nLogSecondSize = 0;

        public SerialPortBoard GetInstance()
        {
            if (instance == null)
            {
                instance = new SerialPortBoard();
            }
            return instance;
        }

        public static bool IsConnected()
        {
            return SerialPortUtility.IsOpen();
        }

        public static void Disconnect()
        {
            if (SerialPortUtility.IsOpen())
            {
                SerialPortUtility.Close();
            }
        }

        public static bool Connect()
        {
            bool result = false;
            if (!SerialPortUtility.IsOpen())
            {
                SerialPortUtility.Open(Globals.serialPortName, Globals.serialBaudRate, Globals.serialDataBits);
                SerialPortUtility.DataReceived += OnDataReceived;
            }

            if (SerialPortUtility.IsOpen())
            {
                logger.Info("Serial Port Connected.");
                result = true;
            }
            return result;
        }

        public static void Send(byte[] data, int length)
        {
            if (IsConnected())
            {
                SerialPortUtility.Send(data, length);
            }
        }




        private static void calculate_checksum(ref byte[] send, int count)
        {
            byte check_sum = 0;

            for (int i = 0; i < count; i++)
            {
                check_sum ^= send[i];

            }
            send[count] = check_sum;
        }

        public static List<PathDataAFL> CreatePathSerial(List<AstarNodeData> realPath)
        {
            List<PathDataAFL> serialPathData = new List<PathDataAFL>();
            var tmp = new PathDataAFL();

            for (int i = 0; i < realPath.Count; i++)
            {
                int target_link = Astar.FindLinkIndex(realPath[i].GetLinkNumber());

                //logger.Info("   LinkNumber : " + realPath[i].GetLinkNumber() + " LinkType : " + realPath[i].GetLinkType());

                if (realPath[i].GetLinkType() == 1 || realPath[i].GetLinkType() == 2)
                {
                    int link_num_ = realPath[i].GetLinkNumber();
                    int curve_num_ = 0;

                    for (int l = 0; l < Globals.linkData.Count; l++)
                    {
                        if (link_num_ == Globals.linkData[l].GetLinkNumber())
                            curve_num_ = l;
                    }

                    Int16 offset_x = (Int16)(Globals.nodeData[Astar.FindNodeIndex(realPath[i].GetCurrentNodeID())].GetNodePoint().X - Globals.linkData[curve_num_].GetBasePoint().X);
                    Int16 offset_y = (Int16)(Globals.nodeData[Astar.FindNodeIndex(realPath[i].GetCurrentNodeID())].GetNodePoint().Y - Globals.linkData[curve_num_].GetBasePoint().Y);
                    //logger.Info("   CurNodeID : "+Globals.nodeData[Astar.FindNodeIndex(realPath[i].GetCurrentNodeID())] + " offset_x : "+offset_x + " offset_y : "+offset_y);
                    tmp = new PathDataAFL(realPath[i].GetCurrentNodeID(), realPath[i].GetLinkNumber(), Globals.nodeData[Astar.FindNodeIndex(realPath[i].GetCurrentNodeID())].GetNodePoint().X, Globals.nodeData[Astar.FindNodeIndex(realPath[i].GetCurrentNodeID())].GetNodePoint().Y, offset_x, offset_y, realPath[i].GetLinkDirection(), realPath[i].GetLinkOption(), realPath[i].GetLinkType(), realPath[i].GetMaxVelocity(), realPath[i].GetSafetyChannel(), Globals.nodeData[Astar.FindNodeIndex(realPath[i].GetParentNodeID())].GetLayerNumber());
                }
                else
                {
                    tmp = new PathDataAFL(realPath[i].GetCurrentNodeID(), realPath[i].GetLinkNumber(), Globals.nodeData[Astar.FindNodeIndex(realPath[i].GetCurrentNodeID())].GetNodePoint().X, Globals.nodeData[Astar.FindNodeIndex(realPath[i].GetCurrentNodeID())].GetNodePoint().Y, 0, 0, realPath[i].GetLinkDirection(), realPath[i].GetLinkOption(), realPath[i].GetLinkType(), realPath[i].GetMaxVelocity(), realPath[i].GetSafetyChannel(), Globals.nodeData[Astar.FindNodeIndex(realPath[i].GetParentNodeID())].GetLayerNumber());
                }
                serialPathData.Add(tmp);
            }

            tmp = new PathDataAFL(realPath[realPath.Count - 1].GetParentNodeID(), realPath[realPath.Count - 1].GetLinkNumber(), Globals.nodeData[Astar.FindNodeIndex(realPath[realPath.Count - 1].GetParentNodeID())].GetNodePoint().X, Globals.nodeData[Astar.FindNodeIndex(realPath[realPath.Count - 1].GetParentNodeID())].GetNodePoint().Y, 0, 0, 0, 0, 0, 0, 0, Globals.nodeData[Astar.FindNodeIndex(realPath[realPath.Count - 1].GetParentNodeID())].GetLayerNumber());
            serialPathData.Add(tmp);
            serialPathData.Reverse();

            return serialPathData;
        }

        public static List<PathDataAFL> CreateVirtualLinkSerial(Point StartP, Point PalletP, Point EndP)
        {
            List<PathDataAFL> serialPathData = new List<PathDataAFL>();
            var tmp = new PathDataAFL();
            Byte Linkdirection = 0;
            float heading = Globals.NavigationSystem.NAV_PHI;
            Point VehiclePose = new Point((int)Globals.NavigationSystem.NAV_X, (int)Globals.NavigationSystem.NAV_Y);

            tmp = new PathDataAFL(0, 0, (int)Globals.NavigationSystem.NAV_X, (int)Globals.NavigationSystem.NAV_Y, 0, 0, 0, 4, 0, 0, 2, 0);
            serialPathData.Add(tmp);

            Linkdirection = Astar.VirtualLinkDecisionDirection(VehiclePose, StartP, ref heading);
            tmp = new PathDataAFL(0, 0, StartP.X, StartP.Y, 0, 0, Astar.ChangeDirection(Linkdirection), 4, 0, 2, 2, 0);
            serialPathData.Add(tmp);

            Linkdirection = Astar.VirtualLinkDecisionDirection(StartP, EndP, ref heading);
            tmp = new PathDataAFL(0, 0, EndP.X, EndP.Y, 0, 0, Astar.ChangeDirection(Linkdirection), 4, 0, 2, 2, 0);

            serialPathData.Add(tmp);
            return serialPathData;
        }


        public static bool SendPathSerial(List<PathDataAFL> serialPathData)
        {
            Stopwatch stopwatch = new Stopwatch();
            int send_count = 0;
            bool com_flag = true;
            byte[] send = new byte[512];
            short cmd_last = 5003;
            short cmd_send = 5001;
            int error_count = 0;
            bool result = false;
            int block_num = 0;

            /// Test
            while (com_flag)
            {
                if (send_count + com_path_length > serialPathData.Count) // com_path_length = 15;
                {
                    int size = (serialPathData.Count - send_count) * 20 + 8;
                    Setting_path(ref send, size, cmd_last, block_num, serialPathData.Count - send_count);
                    int send_num = 7;
                    for (int i = send_count; i < serialPathData.Count; i++)
                    {
                        for (int j = 0; j < path_size; j++)
                        {
                            send[send_num] = serialPathData[i].GetByte(j);
                            send_num++;
                        }
                    }

                    calculate_checksum(ref send, send_num);
                    if (!serial_response_flag)
                    {
                        logger.Info("Serial Port send_cmd: " + cmd_last + ",count : " + serialPathData.Count);
                        Send(send, size);
                        stopwatch.Reset();
                        stopwatch.Start();
                    }

                    while (!serial_response_flag)
                    {
                        stopwatch.Stop();

                        if (stopwatch.ElapsedMilliseconds > 2000)
                        {
                            break;
                        }
                        stopwatch.Start();
                    }

                    if (!serial_response_flag)
                        break;
                    else
                    {
                        serial_response_flag = false;

                        if (return_cmd == 5004)
                        {
                            logger.Info("Serial Port return_cmd: " + return_cmd + ",count : " + serialPathData.Count);
                            com_flag = false;
                            return_cmd = 0;
                            result = true;
                        }
                        else
                        {
                            if (error_count > 10)
                                break;
                            else
                            {
                                return_cmd = 0;
                                error_count++;

                            }
                        }
                    }
                }
                else
                {
                    int send_num = 7;
                    Setting_path(ref send, 308, cmd_send, block_num, 15);
                    for (int i = send_count; i < send_count + com_path_length; i++)
                    {
                        for (int j = 0; j < path_size; j++)
                        {
                            send[send_num] = serialPathData[i].GetByte(j);
                            send_num++;
                        }
                    }

                    calculate_checksum(ref send, send_num);
                    if (!serial_response_flag)
                    {
                        stopwatch.Reset();
                        stopwatch.Start();

                        logger.Info("Serial Port send_cmd: " + cmd_send + ",count : " + serialPathData.Count);
                        Send(send, 308);

                    }

                    while (!serial_response_flag)
                    {
                        stopwatch.Stop();

                        if (stopwatch.ElapsedMilliseconds > 2000)
                            break;
                        stopwatch.Start();

                    }

                    if (!serial_response_flag)
                        break;

                    else
                    {
                        serial_response_flag = false;

                        if (return_cmd == 5002)
                        {
                            logger.Info("Serial Port return_cmd: " + return_cmd + ",count : " + serialPathData.Count);
                            block_num++;
                            return_cmd = 0;
                            send_count = send_count + com_path_length;
                            error_count = 0;
                        }
                        else

                        {
                            if (error_count > 10)
                                break;
                            else
                            {
                                error_count++;
                                return_cmd = 0;
                            }
                        }
                    }
                }
            }
            return result;
        }

        private static void Setting_path(ref byte[] send, int size, short cmd, int count, int path_info_size)
        {
            send[0] = 0x02;
            send[1] = (byte)((size) & 0xFF);
            send[2] = (byte)(size >> 8 & 0xFF);
            send[3] = (byte)((cmd) & 0xFF);
            send[4] = (byte)(cmd >> 8 & 0xFF);
            send[5] = (byte)count;
            send[6] = (byte)path_info_size; // path size = 20
        }

        public static bool SendSetCurrentNode(ushort curNode, ushort curLink)
        {
            bool result = false;
            int tryCnt = 0;
            serial_response_flag = false;

            int index;
            ushort size;
            byte[] buff = new byte[100];
            ushort cmd, node, link;

            // Header
            index = 0;
            buff[index++] = 0x02;
            buff[index++] = (byte)'B';  // Size H
            buff[index++] = 0;  // Size L

            cmd = 1301;
            buff[index++] = (byte)((cmd) & 0xFF);
            buff[index++] = (byte)(cmd >> 8 & 0xFF);

            // Current Node  
            node = curNode;
            buff[index++] = (byte)((node) & 0xFF);
            buff[index++] = (byte)(node >> 8 & 0xFF);
            //m_SendCurrentNode = 0 ; //전송완료후 m_SendCurrentNode => Reset 

            // Current Link
            link = curLink;
            buff[index++] = (byte)((link) & 0xFF);
            buff[index++] = (byte)(link >> 8 & 0xFF);
            //m_SendCurrentLink = 0 ; //전송완료후 m_SendCurrentLink => Reset 

            size = (ushort)(index + 1);
            buff[1] = (byte)((size) & 0xFF);
            buff[2] = (byte)(size >> 8 & 0xFF);

            byte bcc = 0;
            for (int i = 0; i < index; i++) bcc ^= buff[i];
            buff[index++] = bcc;

            while (true)
            {
                try
                {
                    Send(buff, size);
                }
                catch (Exception ex)
                {
                    result = false;
                    logger.Info("[SendSetCurrentNode] " + ex.ToString());
                }

                if (return_cmd == 1302)
                {
                    result = true;
                    break;
                }
                else
                {
                    tryCnt++;

                    if (tryCnt > 5)
                    {
                        result = false;
                        break;
                    }
                }
                Thread.Sleep(500);
            }
            return result;
        }

        //70 5020 -> 로그 수신 요청
        //72 size 내용 -> 처음 로그 수신
        //70 5030 -> 로그 수신 재요청
        //73 5050 -> 마지막 로그 수신 완료

        public static bool SendLogSerial(int nHead, int nCmd)
        {
            bSendLogFlag = true;

            Stopwatch stopwatch = new Stopwatch();
            //int send_count = 0;
            //bool com_flag = true;
            byte[] send = new byte[512];
            byte[] receive = new byte[512];
            //short cmd_last = 5003;
            //short cmd_send = 5001;
            // int error_count = 0;
            bool result = false;
            //int block_num = 0;
            send[0] = (byte)nHead;//0x70;
            send[1] = (byte)((5) & 0xFF);
            send[2] = (byte)(5 >> 8 & 0xFF);
            send[3] = (byte)((nCmd) & 0xFF);
            send[4] = (byte)(nCmd >> 8 & 0xFF);
            Send(send, 5);

            return result;
        }

        internal delegate void StringDelegate(byte[] data);
        private static void OnDataReceived(byte[] data)
        {
            try
            {
                int index = 1;
                //보드 로그 수신 완료
                if (data[0] == 0x72 && bSendLogFlag)
                {
                    int size = (data[1] << 8) | (data[2]);
                    nLogDataSize = size;
                    nLogFirstSize = data.Length;

                    bSendLogFlag = false;
                    bRecvLogFirst = true;

                    data.Reverse();
                    data = data.Take(size - 2).ToArray();
                    data.Reverse();

                    byteLog = (byte[])data.Clone();

                    logger.Info("   Firmware Log First Recv");
                    Thread.Sleep(500);
                }

                else if (bRecvLogFirst)
                {
                    if (data.Length > 0)
                    {
                        if (data[data.Length - 1] == 50 && data[data.Length - 2] == 50 && data[data.Length - 3] == 0x73)
                        {
                            int size = (data[1] << 8) | (data[2]);
                            bSendLogFlag = false;
                            bRecvLogFirst = false;

                            List<Byte> list = new List<Byte>();

                            list.AddRange(byteLog);
                            list.AddRange(data);

                            Byte[] resultLog = list.ToArray();
                            //logger.Fatal(Encoding.Default.GetString(resultLog));

                            logger.Info("   Firmware Log Receive End");
                            MessageBox.Show("펌웨어 로그 수신 완료");
                        }
                        else
                        {
                            int size = (data[1] << 8) | (data[2]);
                            bSendLogFlag = false;
                            bRecvLogFirst = false;

                            nLogSecondSize = data.Length;

                            logger.Info("   Second Firmware Log Data Recv Failed");
                            if (nLogDataSize != nLogFirstSize + nLogSecondSize)
                            {
                                SendLogSerial(0x70, 5030);
                                logger.Info("   Retry Get Firmware Log");

                            }

                        }
                    }
                    else
                    {
                        logger.Info("   Firmware Log Data 0");
                    }
                }
                //else if (bRecvLogFirst)
                //{
                //    int size = (data[1] << 8) | (data[2]);
                //    bSendLogFlag = false;
                //    bRecvLogFirst = false;

                //    nLogSecondSize = data.Length;

                //    logger.Info("   Second Firmware Log Data Recv Failed");
                //    if (nLogDataSize != nLogFirstSize + nLogSecondSize)
                //    {
                //        SendLogSerial(0x70, 5030);
                //        logger.Info("   Retry Get Firmware Log");

                //    }
                //}
                else if (!bSendLogFlag && !bRecvLogFirst)
                {
                    while ((data.Length > 5) && index == 1)
                    {
                        for (int i = 0; i < data.Length; i++)
                        {
                            if (data[i] == 2)
                                index = i;
                        }
                        return_cmd = data[index + 3] | data[index + 4] << 8;

                        index = 0;
                        serial_response_flag = true;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Info(ex.ToString()); 
            }
        }
    }
}
