﻿
using MobyusTouch.src.Global;
using MobyusTouch.src.Main;
using MobyusTouch.src.Main.Sub;
using MobyusTouch.src.Utility;
using NLog;
using SimpleACS;
using SimpleACS.Server;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using MobyusTouch.src.Global.Entity;
//using MapDLL;
using EditorLibrary;

namespace MobyusTouch.src.Communication
{
    class TcpServerFMS
    {
        private static Logger logger = LogManager.GetLogger("TcpServerFMS");

        private static TcpServerFMS instance = new TcpServerFMS();
        private static SimpleTcpServer tcpServer = new SimpleTcpServer();
        private static long lastTime;
        private static long curTime;

        private const char ACS_TRAFFIC = 'D';
        private const char ACS_STATUS = 'A';
        private const char ACS_WORK = 'W';

        private const int TRAFFIC_NONE = 0;
        private const int TRAFFIC_RESUME = 1;
        private const int TRAFFIC_PAUSE = 2;
        private const int TRAFFIC_STOP = 3;

        public static bool Connect()
        {
            bool result = false;

            if (!tcpServer.IsStarted) 
            { 
                try
                {
                    int tcpPort = 8000;
                    tcpServer.Start(tcpPort);
                    tcpServer.DataReceived += new EventHandler<Message>(OnDataReceived);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public static void Disconnect()
        {
            tcpServer.Stop();
        }
        public static long UnixTimeNow()
        {
            TimeSpan timeSpan = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0));
            return (long)timeSpan.TotalSeconds;
        }

        public static bool IsConnected()
        {
            bool result = true;
            curTime = UnixTimeNow();
            if (curTime - lastTime >= 3)
            {
                result = false;
            }
            
            return result;
        }

        private static byte MakeCheckSum(Byte[] RxBuf, int leng)
        {
            int result = 0;
            int Sum = 0;
            byte Check_Sum_Value = 0;

            byte[] temp_data = RxBuf;

            for (int i = 0; i < leng; i++)
            {
                if (i > 0 && i < 7)
                {
                    Sum += temp_data[i];
                }
            }
            result = Sum;
            result &= Convert.ToChar(0x0f);
            result = (~result + 1);

            Check_Sum_Value = Convert.ToByte(result &= Convert.ToChar(0x0f));
            return Check_Sum_Value;
        }

        private static int Check_Check_Sum(Byte[] RxBuf, int leng)
        {
            int result = 0;
            int Sum = 0;

            byte[] temp_data = RxBuf;

            for (int i = 0; i < leng; i++)
            {
                Sum += temp_data[i];
            }
            result = Sum;
            result &= Convert.ToChar(0x0f);
            result = (~result + 1);
            result = result + Sum;
            result &= Convert.ToChar(0x0f);
            return result;
        }

        private static void SendTransferStatus()
        {
        }

        
        private static void ReceiveTraffic(byte[] data, int bytesRead)
        {
            switch ((int)data[2])
            {
                case TRAFFIC_NONE:
                case TRAFFIC_RESUME:
                    Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_RESUME;
                    Globals.HMISystem.ACStoHMI_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_RESUME;
                    break;
                case TRAFFIC_PAUSE:
                    Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_PAUSE;
                    Globals.HMISystem.ACStoHMI_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_PAUSE;
                    break;
                case TRAFFIC_STOP:
                    Globals.HMISystem.SystemTraffic = SystemTraffic.TRANSFER_TRAFFIC_STOP;
                    Globals.HMISystem.ACStoHMI_TrafficPause = SystemTraffic.TRANSFER_TRAFFIC_STOP;
                    break;

            }
        }
        private static void ReceiveWork(byte[] data, int bytesRead)
        {

            if (Globals.currentWork.workType != Globals.TRANSFER_READY)
            {
                logger.Info("현재 작업이 존재합니다.");
                return;
            }

            WorkEntity workCommand = new WorkEntity();
            switch (data[4])
            {
                case 1:// 이동
                    workCommand.workType = Globals.TRANSFER_MOVE;
                    break;
                case 2:// 1층 적재
                    workCommand.workType = Globals.TRANSFER_LOAD;
                    workCommand.workLevel = 1;
                    break;
                case 3:// 1층 이재
                    workCommand.workType = Globals.TRANSFER_UNLOAD;
                    workCommand.workLevel = 1;
                    break;
                case 4:// 2층 적재
                    workCommand.workType = Globals.TRANSFER_LOAD;
                    workCommand.workLevel = 2;
                    break;
                case 5:// 2층 이재
                    workCommand.workType = Globals.TRANSFER_UNLOAD;
                    workCommand.workLevel = 2;
                    break;
                case 9:// 취출 위치 적재
                    workCommand.workType = Globals.TRANSFER_LOAD;
                    workCommand.workLevel = 9;
                    break;
                default:
                    //error
                    break;
            }
            workCommand.curNode = Globals.currentWork.curNode;
            workCommand.toNode = (ushort)(data[2] << 8 | data[3]);

            /*
            if (workCommand.curNode == workCommand.toNode)
            {
                if (workCommand.workType == Global.TRANSFER_LOAD)
                {
                    Global.currentWork.workState = 5;
                }
                else if (workCommand.workType == Global.TRANSFER_UNLOAD)
                {
                    Global.currentWork.workState = 6;
                }
                else
                {
                    if (Global.rx310Data.SystemCharge == true)
                    {
                        Global.currentWork.workState = 9;
                    }
                    else
                    {
                        Global.currentWork.workState = 4;
                    }
                }
                
                logger.Error("Same Node [TARGET]" + workCommand.toNode + "[CURRENT]" + workCommand.curNode + "[STATE]" + Global.currentWork.workState);
                return;
            }
            */

            if (Astar.FindNodeIndex(workCommand.toNode) <= 0)
            {
                logger.Error("Map에서 노드 정보를 찾을 수 없습니다. [NODE]" + workCommand.toNode);
                Globals.currentWork.alarm = Alarms.ALARM_MAP_NOT_FOUND_NODE_INFO_ERROR;
                Globals.currentWork.alarm_name = "![000/E5151/C1노드정보없음/C1  에    러 !]";
            }
            else
            {

            }

            Queue<WorkStep> workQueue = new Queue<WorkStep>();
            if (workCommand.workType == Globals.TRANSFER_MOVE)
            {
                //workQueue = workCommand.WorkMove();
            }
            else if (workCommand.workType == Globals.TRANSFER_LOAD)
            {
                //workQueue = workCommand.WorkLoad();
            }
            else if (workCommand.workType == Globals.TRANSFER_UNLOAD)
            {
                //workQueue = workCommand.WorkUnload();
            }
            else if (workCommand.workType == Globals.TRANSFER_CHARGE)
            {
                //workQueue = workCommand.WorkCharge();
            }
            else
            {
                logger.Info("존재 하지 않는 작업입니다.");
                return;
            }

            workCommand.workQueue = workQueue;
            Globals.currentWork = workCommand;
        }
        
        private static void OnDataReceived(Object sender, Message msg)
        {
            int checkSum = 0;
            int bytesRead = msg.Data.Length;
            byte[] data = msg.Data;
            if (bytesRead == 10 && data[0] == 0x02 && data[8] == 0x0D && data[9] == 0x0A)
            {
                checkSum = Check_Check_Sum(data, 10);
                switch ((char)data[1])
                {
                    case ACS_STATUS:
                        SendTransferStatus();
                        break;

                    case ACS_WORK:
                        if (Globals.HMISystem.SystemMode == SystemMode.AUTO)
                        {
                            logger.Info("[RECEIVE WORK] [DATA]" + BitConverter.ToString(data) + " [SIZE]" + bytesRead);
                            ReceiveWork(data, bytesRead);
                        }
                        break; 

                    case ACS_TRAFFIC:
                        if (Globals.HMISystem.SystemMode == SystemMode.AUTO)
                        {
                            logger.Info("[RECEIVE TRAFFIC] [DATA]" + BitConverter.ToString(data) + " [SIZE]" + bytesRead);
                            ReceiveTraffic(data, bytesRead);
                        }
                        break;

                    default:
                        logger.Error("[ERROR] TCP TYPE FORMAT [DATA]" + BitConverter.ToString(data) + " [SIZE]" + bytesRead);
                        Globals.currentWork.alarm = Alarms.ALARM_PROTOCOL_FORMAT_ERROR;
                        break;
                }
            }
            else
            {
                logger.Error("[ERROR] TCP FORMAT [DATA]" + BitConverter.ToString(data) + " [SIZE]" + bytesRead);
                Globals.currentWork.alarm = Alarms.ALARM_PROTOCOL_FORMAT_ERROR;
            }
        }
    }
}
