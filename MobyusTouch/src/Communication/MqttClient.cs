﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using Newtonsoft.Json;
using MobyusTouch.src.Global.Entity;
using MobyusTouch.src.Global;
using System.Threading;
using MobyusTouch.src.Global.Entity.Hyundai;
using MobyusTouch.src.Main.Sub;
using MobyusTouch.src.Main;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using uPLibrary.Networking.M2Mqtt.Exceptions;
using System.Net.Sockets;
using System.Windows.Media.Media3D;

namespace MobyusTouch.src.Communication
{
    public class MqttClient
    {
        private static Logger logger = LogManager.GetLogger("MqttClient");

        public static uPLibrary.Networking.M2Mqtt.MqttClient client = new uPLibrary.Networking.M2Mqtt.MqttClient("");

        public static bool Connect()
        {
            bool result = false;

            try
            {
                if (!IsConnected())
                {
                    client = new uPLibrary.Networking.M2Mqtt.MqttClient(Globals.fmsIpAddress);
                    client.Connect(Guid.NewGuid().ToString());
                    client.MqttMsgPublishReceived += SubscribeMessage;
                    client.Subscribe(new string[] { "ACS001>AMR001" }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
                }

                if (IsConnected())
                {
                    logger.Info("MQTT Client Connected.");
                    result = true;
                    //Publish_Result(new EIEntity()); // 연결되면 초기 위치 전송
                }
            }
            catch (MqttConnectionException ex)
            {

            }
            catch (Exception ex)
            {
                logger.Info(ex.ToString());
            }

            return result;
        }

        public static void Disconnect()
        {
            if(IsConnected())
            {
                client.Disconnect();
            }
        }

        public static bool IsConnected()
        {
            return client.IsConnected;
        }

        private static void SubscribeMessage(object sender, MqttMsgPublishEventArgs e)
        {
            try
            {
                string msg = Encoding.UTF8.GetString(e.Message);
                //logger.Info("[MODE]" + Globals.HMISystem.SystemMode + " [SubscribeMessage]" + msg);

                CmdEntity cmdEntity = JsonConvert.DeserializeObject<CmdEntity>(msg);
                switch (cmdEntity.Cmd)
                {
                    default:
                        cmdEntity.Result = Globals_Hyundai.RESULT_FAIL;
                        cmdEntity.ErrorCode = Alarms.ALARM_PROTOCOL_FORMAT_ERROR;
                        Publish_Result(cmdEntity);
                        break;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }

        public static void Publish_Result(Object obj)
        {
            String strValue = JsonConvert.SerializeObject(obj, Formatting.None);
            //logger.Info("[Publish]" + str Value);

            client.Publish("AMR001>ACS001", Encoding.UTF8.GetBytes(strValue), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
        }

    }
}