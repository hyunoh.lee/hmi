﻿
using MobyusTouch.src.Global;
using MobyusTouch.src.Global.Entity;
using MobyusTouch.src.Main;
using MobyusTouch.src.Main.Sub;
using MobyusTouch.src.Utility;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MobyusTouch.src.Communication
{
    class TcpClientFMS
    {

        private static Logger logger = LogManager.GetLogger("TcpClientFMS");

        //private static TcpClientFMS instance = new TcpClientFMS();
        private static SocketClient socketClient = new SocketClient();

        public static bool Connect()
        {
            bool result = false;
            try
            {
                socketClient.ConnectToServer(Globals.fmsIpAddress, Globals.fmsIpPort);
                socketClient.OnDataReceived += new ClientHandlePacketData(OnDataReceived);
            }
            catch (Exception ex)
            {
                logger.Info(ex.ToString());
            }

            if (socketClient.IsConnected())
            {
                logger.Info("Socket Client Connected.");
                result = true;
            }
            return result;
        }

        public static bool IsConnected()
        {
            return socketClient.IsConnected();
        }

        public static void Disconnect()
        {
            socketClient.Disconnect();
        }

        private static byte MakeCheckSum(Byte[] RxBuf, int leng)
        {
            int result = 0;
            int Sum = 0;
            byte Check_Sum_Value = 0;

            byte[] temp_data = RxBuf;

            for (int i = 0; i < leng; i++)
            {
                if (i > 0 && i < 7)
                {
                    Sum += temp_data[i];
                }

            }
            result = Sum;
            result &= Convert.ToChar(0x0f);
            result = (~result + 1);

            Check_Sum_Value = Convert.ToByte(result &= Convert.ToChar(0x0f));
            return Check_Sum_Value;
        }

        private int Check_Check_Sum(Byte[] RxBuf, int leng)
        {
            int result = 0;
            int Sum = 0;

            byte[] temp_data = RxBuf;

            for (int i = 0; i < leng; i++)
            {
                Sum += temp_data[i];
            }
            result = Sum;
            result &= Convert.ToChar(0x0f);
            result = (~result + 1);
            result = result + Sum;
            result &= Convert.ToChar(0x0f);
            return result;
        }

        private static void SendTransferStatus()
        {
            
        }

        private void SendWork()
        {
            
        }

        private static void OnDataReceived(byte[] data, int bytesRead)
        {
            /*
            ASCIIEncoding encoder = new ASCIIEncoding();
            string msgFMS = encoder.GetString(data, 0, bytesRead);
            logger.Info("Received a message: " + msgFMS);
            */

            if (bytesRead == 9 && data[0] == 0x02)
            {
                logger.Info("[RECEIVE]" + (char)data[1]);
                switch ((char)data[1])
                {
                    case 'A':
                        SendTransferStatus();
                        break;
                    case 'D':
                        ReceiveWork(data, bytesRead);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                logger.Info("Data Size ERROR");
            }
        }

        private static void ReceiveWork(byte[] data, int bytesRead)
        {
            WorkEntity workCommand = new WorkEntity();
            workCommand.toNode = (ushort)(data[2] | data[3] >> 8);
            // if () 맵 상에 존재하는 노드인지 체크

            switch (data[4])
            {
                case 1:
                    workCommand.workType = Globals.TRANSFER_MOVE;
                    break;
                case 2:
                    workCommand.workType = Globals.TRANSFER_LOAD;
                    break;
                case 3:
                    workCommand.workType = Globals.TRANSFER_UNLOAD;
                    break;
                default:
                    break;
            }
            
            logger.Info("workCommand.workType : " + workCommand.workType);
            logger.Info("workCommand.toNode : " + workCommand.toNode);
            

            if (Globals.currentWork.workType != Globals.TRANSFER_READY)
            {
                // if traffic check stop이 아닐경우 알람
                logger.Info("[ALARM] 현재 작업중입니다.");
            }

            if (workCommand.workType == Globals.TRANSFER_LOAD)
            {
                Queue<WorkStep> workQueue = new Queue<WorkStep>();
                /*
                // 목표 노드의 전 노드 path 생성
                workQueue.Enqueue(new WorkStep(Global.TRANSFER_MOVE_1ST_PATH_PLANNING, 0, false, MoveControl.Move1stPathPlanning));

                // 목표 노드의 전 노드 path 전달
                workQueue.Enqueue(new WorkStep(Global.TRANSFER_MOVE_PATH_SENDING, 0, true, MoveControl.MovePathSending));


                //목표 노드의 전 노드로 주행 (현재 위치가 Station인지 체크)
                workQueue.Enqueue(new WorkStep(Global.TRANSFER_MOVE_ING, 0, true, MoveControl.Moving));

                workQueue.Enqueue(new WorkStep(Global.TRANSFER_MOVE_2ND_PATH_PLANNING, 0, true, MoveControl.Move2ndPathPlanning));
                workQueue.Enqueue(new WorkStep(Global.TRANSFER_MOVE_PATH_SENDING, 0, true, MoveControl.MovePathSending));
                */
                /*
                // 이동을 위한 기본 자세
                workQueue.Enqueue(new WorkStep(Global.TRANSFER_REACHING, false, 0));
                workQueue.Enqueue(new WorkStep(Global.TRANSFER_LIFTING, false, 100));
                workQueue.Enqueue(new WorkStep(Global.TRANSFER_TILTING, false, 200));

                // 목표 노드의 전 노드 주행 완료
                workQueue.Enqueue(new WorkStep(Global.TRANSFER_MOVE_COMPLETE, true, 0));

                workQueue.Enqueue(new WorkStep(Global.TRANSFER_LIFTING, false, 500));
                workQueue.Enqueue(new WorkStep(Global.TRANSFER_TILTING, false, 0));
                workQueue.Enqueue(new WorkStep(Global.TRANSFER_REACHING, false, 1));

                workQueue.Enqueue(new WorkStep(Global.TRANSFER_MOVE_2ND_PATH_PLANNING, 0, true, MoveControl.Move2ndPathPlanning));
                workQueue.Enqueue(new WorkStep(Global.TRANSFER_MOVE_PATH_SENDING, false, 0));
                workQueue.Enqueue(new WorkStep(Global.TRANSFER_MOVE_ING, true, 0));

                workQueue.Enqueue(new WorkStep(Global.TRANSFER_LIFTING, false, 550));
                workQueue.Enqueue(new WorkStep(Global.TRANSFER_TILTING, false, 200));
                workQueue.Enqueue(new WorkStep(Global.TRANSFER_REACHING, false, 0));
                */
                workCommand.workQueue = workQueue;
            }
            else if (workCommand.workType == Globals.TRANSFER_MOVE)
            {
                Queue<WorkStep> workQueue = new Queue<WorkStep>();

                /*
                workQueue.Enqueue(new WorkStep(Global.TRANSFER_MOVE_1ST_PATH_PLANNING, false, 0));
                workQueue.Enqueue(new WorkStep(Global.TRANSFER_MOVE_PATH_SENDING, false, 0));
                workQueue.Enqueue(new WorkStep(Global.TRANSFER_MOVE_ING, true, 0));

                workQueue.Enqueue(new WorkStep(Global.TRANSFER_REACHING, false, 0));
                workQueue.Enqueue(new WorkStep(Global.TRANSFER_LIFTING, false, 100));
                workQueue.Enqueue(new WorkStep(Global.TRANSFER_TILTING, false, 200));

                workQueue.Enqueue(new WorkStep(Global.TRANSFER_MOVE_COMPLETE, true, 0));
                */
                workCommand.workQueue = workQueue;
            }
            else if (workCommand.workType == Globals.TRANSFER_UNLOAD)
            {
                
            }

            Globals.currentWork = workCommand;
        }
    }
}
