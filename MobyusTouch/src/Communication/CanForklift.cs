﻿using Peak.Can.Basic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TPCANHandle = System.UInt16;
using TPCANBitrateFD = System.String;
using TPCANTimestampFD = System.UInt64;
using System.Threading;
using MobyusTouch.src.Global;
using NLog;

namespace MobyusTouch.src.Communication
{
    class CanForklift
    {
        // CAN ID 정보
        public const uint CAN_181 = 0x181;
        public const uint CAN_210 = 0x210;
        public const uint CAN_211 = 0x211;

        public const uint CAN_220 = 0x220;
        public const uint CAN_221 = 0x221;

        public const uint CAN_241 = 0x241;
        public const uint CAN_242 = 0x242;
        public const uint CAN_260 = 0x260;
        public const uint CAN_261 = 0x261;
        public const uint CAN_263 = 0x263;
        public const uint CAN_264 = 0x264;
        public const uint CAN_265 = 0x265;
        public const uint CAN_266 = 0x266;
        public const uint CAN_267 = 0x267;
        public const uint CAN_268 = 0x268;
        public const uint CAN_269 = 0x269;

        public const uint CAN_290 = 0x290;
        public const uint CAN_291 = 0x291;
        public const uint CAN_292 = 0x292;
        public const uint CAN_293 = 0x293;

        public const uint CAN_330 = 0x330;
        public const uint CAN_331 = 0x331;
        public const uint CAN_332 = 0x332;
        public const uint CAN_333 = 0x333;
        public const uint CAN_334 = 0x334;

        public const uint CAN_340 = 0x340;
        public const uint CAN_341 = 0x341;
        public const uint CAN_342 = 0x342;

        public const uint CAN_401 = 0x401;
        public const uint CAN_402 = 0x402;

        public const uint CAN_410 = 0x410;
        public const uint CAN_411 = 0x411;
        public const uint CAN_412 = 0x412;

        private static Logger logger = LogManager.GetLogger("CanForklift");

        private static CanForklift instance = new CanForklift();
        private static TPCANHandle m_PcanHandle;
        private static TPCANBaudrate m_Baudrate;
        private static TPCANType m_HwType;
        private static Thread m_ReadThread;
        private static Thread m_WriteThread;
        private static AutoResetEvent autoEvent;
        public static bool isConnected = false;
        public static bool isWriteCAN_Flage = true;
        public static int nWriteCan_Cnt = 0;

        private static TPCANMsg CANMsg = new TPCANMsg();

        private CanForklift()
        {
            autoEvent = new System.Threading.AutoResetEvent(true);
        }

        public static void Connect()
        {
            TPCANStatus stsResult = TPCANStatus.PCAN_ERROR_UNKNOWN;
            UInt32 ioPort = 0100;
            UInt16 interrupt = 3;
            m_Baudrate = TPCANBaudrate.PCAN_BAUD_500K;
            m_HwType = TPCANType.PCAN_TYPE_ISA;
            m_PcanHandle = PCANBasic.PCAN_USBBUS1;

            try
            {
                PCANBasic.Reset(m_PcanHandle);
                stsResult = PCANBasic.Initialize(m_PcanHandle, m_Baudrate, m_HwType, ioPort, interrupt);
                if (stsResult == TPCANStatus.PCAN_ERROR_OK)
                {
                    isConnected = true;

                    ThreadStart threadDelegateRead = new ThreadStart(ReadMessage);
                    m_ReadThread = new Thread(threadDelegateRead);
                    m_ReadThread.Start();

                    ThreadStart threadDelegateWrite = new ThreadStart(WriteMessage);
                    m_WriteThread = new Thread(threadDelegateWrite);
                    m_WriteThread.Start();

                    logger.Info("CAN Protocol Connected.");
                }
                else
                {
                    isConnected = false;
                }
            }
            catch (Exception ex)
            {
                isConnected = false;
                logger.Error(ex.ToString());
            }
        }

        public static void Disconnect()
        {
            if (m_ReadThread != null)
            {
                m_ReadThread.Abort();
                m_ReadThread.Join();
                m_ReadThread = null;
            }

            TPCANStatus stsResult;
            stsResult = PCANBasic.Uninitialize(m_PcanHandle);
        }

        public static bool IsConnected()
        {
            TPCANStatus status = PCANBasic.GetStatus(m_PcanHandle);
            switch (status)
            {
                case TPCANStatus.PCAN_ERROR_OK:
                case TPCANStatus.PCAN_ERROR_BUSLIGHT:
                    break;
                default:
                    PCANBasic.Reset(m_PcanHandle);
                    isConnected = false;
                    Disconnect();
                    Connect();
                    break;
            }
            return isConnected;
        }

        private static void ReadMessage()
        {
            UInt32 iBuffer;
            TPCANStatus stsResult;

            iBuffer = Convert.ToUInt32(autoEvent.SafeWaitHandle.DangerousGetHandle().ToInt32());
            stsResult = PCANBasic.SetValue(m_PcanHandle, TPCANParameter.PCAN_RECEIVE_EVENT, ref iBuffer, sizeof(UInt32));

            if (stsResult != TPCANStatus.PCAN_ERROR_OK)
            {
                isConnected = false;
                return;
            }

            // Filter Start 
            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_181, CAN_181, TPCANMode.PCAN_MODE_STANDARD);
            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_220, CAN_220, TPCANMode.PCAN_MODE_STANDARD);
            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_221, CAN_221, TPCANMode.PCAN_MODE_STANDARD);

            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_241, CAN_241, TPCANMode.PCAN_MODE_STANDARD);
            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_242, CAN_242, TPCANMode.PCAN_MODE_STANDARD);

            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_260, CAN_260, TPCANMode.PCAN_MODE_STANDARD);
            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_261, CAN_261, TPCANMode.PCAN_MODE_STANDARD);
            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_263, CAN_263, TPCANMode.PCAN_MODE_STANDARD);

            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_264, CAN_264, TPCANMode.PCAN_MODE_STANDARD);
            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_265, CAN_265, TPCANMode.PCAN_MODE_STANDARD);
            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_266, CAN_266, TPCANMode.PCAN_MODE_STANDARD);
            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_267, CAN_267, TPCANMode.PCAN_MODE_STANDARD);
            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_268, CAN_268, TPCANMode.PCAN_MODE_STANDARD);
            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_269, CAN_269, TPCANMode.PCAN_MODE_STANDARD);


            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_290, CAN_290, TPCANMode.PCAN_MODE_STANDARD);
            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_291, CAN_291, TPCANMode.PCAN_MODE_STANDARD);
            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_293, CAN_293, TPCANMode.PCAN_MODE_STANDARD);

            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_330, CAN_330, TPCANMode.PCAN_MODE_STANDARD);
            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_331, CAN_331, TPCANMode.PCAN_MODE_STANDARD);
            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_332, CAN_332, TPCANMode.PCAN_MODE_STANDARD);
            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_333, CAN_333, TPCANMode.PCAN_MODE_STANDARD);
            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_334, CAN_334, TPCANMode.PCAN_MODE_STANDARD);

            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_340, CAN_340, TPCANMode.PCAN_MODE_STANDARD);
            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_341, CAN_341, TPCANMode.PCAN_MODE_STANDARD);
            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_342, CAN_342, TPCANMode.PCAN_MODE_STANDARD);

            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_410, CAN_410, TPCANMode.PCAN_MODE_STANDARD);
            stsResult = PCANBasic.FilterMessages(m_PcanHandle, CAN_411, CAN_411, TPCANMode.PCAN_MODE_STANDARD);

            while (isConnected)
            {
                autoEvent.WaitOne();

                TPCANMsg CANMsg;
                TPCANTimestamp CANTimeStamp;
                do
                {
                    stsResult = PCANBasic.Read(m_PcanHandle, out CANMsg, out CANTimeStamp);
                    if (stsResult == TPCANStatus.PCAN_ERROR_OK)
                    {
                        switch (CANMsg.ID)
                        {
                            // Crevis EMO
                            case CAN_181: ReadCAN_181(CANMsg.DATA); break;

                            // Vehicle System
                            case CAN_220: ReadCAN_220(CANMsg.DATA); break;
                            case CAN_221: ReadCAN_221(CANMsg.DATA); break;

                            // Forklift System
                            case CAN_241: ReadCAN_241(CANMsg.DATA); break;
                            case CAN_242: ReadCAN_242(CANMsg.DATA); break;

                            // Navigation System
                            case CAN_260: ReadCAN_260(CANMsg.DATA); break;
                            case CAN_261: ReadCAN_261(CANMsg.DATA); break;
                            case CAN_263: ReadCAN_263(CANMsg.DATA); break;

                            // firmware log
                            case CAN_264: ReadCAN_264(CANMsg.DATA); break;
                            case CAN_265: ReadCAN_265(CANMsg.DATA); break;
                            case CAN_266: ReadCAN_266(CANMsg.DATA); break;
                            case CAN_267: ReadCAN_267(CANMsg.DATA); break;
                            case CAN_268: ReadCAN_268(CANMsg.DATA); break;
                            case CAN_269: ReadCAN_269(CANMsg.DATA); break;

                            // Recognition System
                            case CAN_290: ReadCAN_290(CANMsg.DATA); break;
                            case CAN_291: ReadCAN_291(CANMsg.DATA); break;
                            case CAN_293: ReadCAN_293(CANMsg.DATA); break;

                            // Safety & WT-100 Sensor
                            case CAN_330: ReadCAN_330(CANMsg.DATA); break;
                            //case CAN_331: ReadCAN_331(CANMsg.DATA); break;
                            //case CAN_332: ReadCAN_332(CANMsg.DATA); break;
                            //case CAN_333: ReadCAN_333(CANMsg.DATA); break;
                            case CAN_334: ReadCAN_334(CANMsg.DATA); break;

                            // BMS System
                            case CAN_340: ReadCAN_340(CANMsg.DATA); break;
                            case CAN_341: ReadCAN_341(CANMsg.DATA); break;
                            case CAN_342: ReadCAN_342(CANMsg.DATA); break;

                            // Parameter Setting
                            case CAN_410: ReadCAN_410(CANMsg.DATA); break;
                            case CAN_411: ReadCAN_411(CANMsg.DATA); break;

                        }
                    }
                } while (isConnected && (!Convert.ToBoolean(stsResult & TPCANStatus.PCAN_ERROR_QRCVEMPTY)));
            }
        }

        private static void WriteMessage()
        {
            while (isConnected)
            {
                WriteCAN_210();
                WriteCAN_211();
                if(nWriteCan_Cnt < 10)
                {
                    bool Recognition_reaction = false;
                    if (Recognition_reaction)
                    {
                        break;
                    }
                    else
                    {
                        WriteCAN_412();// kdy

                        if (Globals.RecognitionParameter.UpdateFlag)
                        {
                            WriteCAN_401(); // Recognition Parameter Setting
                        }
                        nWriteCan_Cnt++;
                    }
                }

                if (Globals.RecognitionParameter.RotateMotorUpdateFlag)
                {
                    WriteCAN_292(); // Recognition Parameter Camera motor Setting
                }                

                if (Globals.VehicleParameter.UpdateFlag) 
                {
                    WriteCAN_402(); // Vehicle Parameter Setting
                }

                Thread.Sleep(100);
            }
        }

        private static void WriteCAN_210()
        {
            //TPCANMsg CANMsg = new TPCANMsg();
            CANMsg.DATA = new byte[8];
            CANMsg.ID = CAN_210;
            CANMsg.LEN = Convert.ToByte(8);
            CANMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_STANDARD;

            CANMsg.DATA[0] = (byte)((byte)Globals.HMISystem.SystemTraffic << 6 | Globals.HMISystem.ForkliftValue << 4 | Convert.ToByte(Globals.HMISystem.MoveComplete) << 3 | (byte)Globals.HMISystem.SystemMode);
            CANMsg.DATA[1] = (byte)(Convert.ToByte(Globals.HMISystem.TaggingEnable) << 7 |
                                    Convert.ToByte(Globals.HMISystem.ClampEnable) << 6 |
                                    Convert.ToByte(Globals.HMISystem.ChargeEnable) << 5 |
                                    Convert.ToByte(Globals.HMISystem.PalletFrontExistEnable) << 3 |
                                    Convert.ToByte(Globals.HMISystem.PalletStackingSuccessEnable) << 2 |
                                    Convert.ToByte(Globals.HMISystem.TiltEnable) << 1 |
                                    Convert.ToByte(Globals.HMISystem.LiftEnable));
            CANMsg.DATA[2] = (byte)(Globals.HMISystem.WorkType << 4 | 
                                    Convert.ToByte(Globals.HMISystem.PalletHeightEnable) << 2 | 
                                    Convert.ToByte(Globals.HMISystem.PalletFrontEnable) << 1 | 
                                    Convert.ToByte(Globals.HMISystem.PalletStackingEnable));
            CANMsg.DATA[3] = (byte)(Convert.ToByte(Globals.HMISystem.PalletLevel) << 4 | Convert.ToByte(Globals.HMISystem.PalletType));
            CANMsg.DATA[4] = (byte)(byte)(Globals.HMISystem.LiftingValue);
            CANMsg.DATA[5] = (byte)(byte)(Globals.HMISystem.LiftingValue >> 8);
            CANMsg.DATA[6] = (byte)(Globals.HMISystem.TiltingValue);
            CANMsg.DATA[7] = (byte)Globals.HMISystem.AlarmCode;

            PCANBasic.Write(m_PcanHandle, ref CANMsg);
        }

        private static void WriteCAN_211()
        {
            //TPCANMsg CANMsg = new TPCANMsg();
            CANMsg.DATA = new byte[8];
            CANMsg.ID = CAN_211;
            CANMsg.LEN = Convert.ToByte(8);
            CANMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_STANDARD;
            
            CANMsg.DATA[0] = (byte)(Convert.ToByte(Globals.AFL_ID) << 4 |
                                    Convert.ToByte(Globals.HMISystem.AFLIDEnable) << 3 |
                                    Convert.ToByte(Globals.HMISystem.MoverEnable) << 2 |
                                    Convert.ToByte(Globals.HMISystem.ReachEnable) << 1 |
                                    Convert.ToByte(Globals.HMISystem.SideShiftEnable));
            CANMsg.DATA[1] = 0x00;
            CANMsg.DATA[2] = (byte)(Globals.HMISystem.ReachingValue);
            CANMsg.DATA[3] = (byte)(Globals.HMISystem.ReachingValue >> 8);
            CANMsg.DATA[4] = (byte)(Globals.HMISystem.SideShiftValue);
            CANMsg.DATA[5] = (byte)(Globals.HMISystem.SideShiftValue >> 8);
            CANMsg.DATA[6] = (byte)(Globals.HMISystem.MoverValue);
            CANMsg.DATA[7] = (byte)(Globals.HMISystem.MoverValue >> 8);

            PCANBasic.Write(m_PcanHandle, ref CANMsg);
        }
        private static void WriteCAN_292()
        {
            //TPCANMsg CANMsg = new TPCANMsg();
            CANMsg.DATA = new byte[8];
            CANMsg.ID = CAN_292;
            CANMsg.LEN = Convert.ToByte(8);
            CANMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_STANDARD;

            CANMsg.DATA[0] = (byte)(Globals.RecognitionParameter.RotateMotorLeft);
            CANMsg.DATA[1] = (byte)(Globals.RecognitionParameter.RotateMotorLeft >> 8);
            CANMsg.DATA[2] = (byte)(Globals.RecognitionParameter.RotateMotorRight);
            CANMsg.DATA[3] = (byte)(Globals.RecognitionParameter.RotateMotorRight >> 8);
            CANMsg.DATA[4] = (byte)Convert.ToByte(Globals.RecognitionParameter.RotateMotorUpdateFlag);
            CANMsg.DATA[5] = 0x00;
            CANMsg.DATA[6] = 0x00;
            CANMsg.DATA[7] = 0x00;

            PCANBasic.Write(m_PcanHandle, ref CANMsg);
        }

        public static void WriteCAN_401() // kdy
        {
            //TPCANMsg CANMsg = new TPCANMsg();
            CANMsg.DATA = new byte[8];
            CANMsg.ID = CAN_401;
            CANMsg.LEN = Convert.ToByte(8);
            CANMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_STANDARD;
            
            CANMsg.DATA[0] = (byte)(Globals.RecognitionParameter.MinDistance);
            CANMsg.DATA[1] = (byte)(Globals.RecognitionParameter.MinDistance >> 8);
            CANMsg.DATA[2] = (byte)((Globals.RecognitionParameter.ReceiverGain & 0x0F) << 4 | (Globals.RecognitionParameter.PostSharpening & 0x03) << 2 | (Globals.RecognitionParameter.Confidence & 0x03));
            CANMsg.DATA[3] = (byte)((Globals.RecognitionParameter.NoiseFiltering & 0x0F) << 4 | (Globals.RecognitionParameter.PreSharpening & 0x0F));
            CANMsg.DATA[4] = (byte)(Globals.RecognitionParameter.PalletDistanceUp);
            CANMsg.DATA[5] = (byte)(Globals.RecognitionParameter.PalletDistanceDown);
            CANMsg.DATA[6] = 0x00;
            CANMsg.DATA[7] = (byte)Convert.ToByte(Globals.RecognitionParameter.UpdateFlag);

            PCANBasic.Write(m_PcanHandle, ref CANMsg);

            //if (isWriteCAN_Flage == true)
            //{
            //    for (int i = 1; i <= repeat; i++)
            //    {
            //        bool Recognition_reaction = false;
            //        if (Recognition_reaction)
            //        {
            //            break;
            //        }
            //        PCANBasic.Write(m_PcanHandle, ref CANMsg);
            //        Thread.Sleep(100);
            //        if(i == repeat)
            //        {
            //            MessageBox.Show("보내기 완료");
            //            //logger.Error("[ERROR] 10번 보냈으나 can 연결 안됨");
            //        }
            //    }
            //}

        }

        private static void WriteCAN_402() 
        {
           // TPCANMsg CANMsg = new TPCANMsg();
            CANMsg.DATA = new byte[8];
            CANMsg.ID = CAN_402;
            CANMsg.LEN = Convert.ToByte(8);
            CANMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_STANDARD;

            CANMsg.DATA[0] = (byte)Globals.VehicleParameter.StackingAngle;
            CANMsg.DATA[1] = (byte)Globals.VehicleParameter.StackingDistance;
            CANMsg.DATA[2] = (byte)Globals.VehicleParameter.MinSpeed;
            CANMsg.DATA[3] = (byte)Globals.VehicleParameter.MaxSpeed;
            CANMsg.DATA[4] = 0x00;
            CANMsg.DATA[5] = 0x00;
            CANMsg.DATA[6] = 0x00;
            CANMsg.DATA[7] = (byte)Convert.ToByte(Globals.VehicleParameter.UpdateFlag);

            PCANBasic.Write(m_PcanHandle, ref CANMsg);
        }
        public static void WriteCAN_412() // kdy
        {
            CANMsg.DATA = new byte[8];
            CANMsg.ID = CAN_412;
            CANMsg.LEN = Convert.ToByte(8);
            CANMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_STANDARD;

            CANMsg.DATA[0] = (byte)(Globals.RecognitionParameter.ObstacleX);
            CANMsg.DATA[1] = (byte)(Globals.RecognitionParameter.ObstacleX >> 8);
            CANMsg.DATA[2] = (byte)(Globals.RecognitionParameter.ObstacleY);
            CANMsg.DATA[3] = (byte)(Globals.RecognitionParameter.ObstacleY >> 8);
            CANMsg.DATA[4] = 0x00;
            CANMsg.DATA[5] = 0x00;
            CANMsg.DATA[6] = 0x00;
            CANMsg.DATA[7] = 0x00;

            PCANBasic.Write(m_PcanHandle, ref CANMsg);
            //if (isWriteCAN_Flage == true)
            //{
            //    for(int i = 1; i <= repeat; i++)
            //    {
            //        bool Recognition_reaction = false;
            //        if (Recognition_reaction)
            //        {
            //            break;
            //        }
            //        PCANBasic.Write(m_PcanHandle, ref CANMsg);
            //        Thread.Sleep(100);
            //        if (i == repeat)
            //        {
            //            MessageBox.Show("보내기 완료");
            //            //logger.Error("[ERROR] 10번 보냈으나 can 연결 안됨");
            //        }
            //    }
            //}
        }

        private static void ReadCAN_181(Byte[] data)
        {
            Globals.CAN_181_FLAG = true;

            Globals.CrevisEMO.RearLeft = (data[0] & 1) == 1 ? true : false;
            Globals.CrevisEMO.FrontLeft = (data[0] & 2) == 2 ? true : false;
            Globals.CrevisEMO.FrontRight = (data[0] & 4) == 4 ? true : false;
            Globals.CrevisEMO.RearRight = (data[0] & 8) == 8 ? true : false;
            Globals.SafetySystem.RemoteSwith = (data[1] & 1) == 1 ? true : false;
        }

        private static void ReadCAN_220(Byte[] data)
        {
            Globals.CAN_220_FLAG = true;

            Globals.VehicleSystem.SystemMode = (data[0] & 1) == 1 ? true : false;
            Globals.VehicleSystem.SystemEMO = (data[0] & 2) == 2 ? true : false;
            Globals.VehicleSystem.SystemCharge = (data[0] & 32) == 32 ? true : false;
            Globals.VehicleSystem.DSNAlarm = (data[0] & 64) == 64 ? true : false;
            Globals.VehicleSystem.NxUseable = (data[0] & 128) == 128 ? true : false;

            Globals.VehicleSystem.CurrentSpeed = (short)((data[3] << 8) | (data[2]));
            Globals.VehicleSystem.CurrentAngle = (short)((data[5] << 8) | (data[4]));

            //logger.Info("[CAN_220] [SystemEMO] : " + Globals.VehicleSystem.SystemEMO + "  [MODE] : " + Globals.HMISystem.SystemMode + "  [ALARM] : " + Globals.currentWork.alarm);
        }

        private static void ReadCAN_221(Byte[] data)
        {
            Globals.CAN_221_FLAG = true;

            Globals.VehicleSystem.DSNTractionAlarm = (byte)(data[2]);
            Globals.VehicleSystem.DSNPumpAlarm = (byte)(data[3]);

        }

        private static void ReadCAN_241(Byte[] data)
        {
            Globals.CAN_241_FLAG = true;

            Globals.ForkLiftSystem.LiftPosition = (UInt16)((data[1] << 8) | (data[0]));
            Globals.ForkLiftSystem.TiltPosition = (sbyte)((data[3] << 8) | (data[2]));
            Globals.ForkLiftSystem.ShiftLeftPosition = (UInt16)((data[5] << 8) | (data[4]));
            Globals.ForkLiftSystem.ShiftRightPosition = (UInt16)((data[7] << 8) | (data[6]));

            /*
            logger.Info("[CAN_241] [LiftPosition] : " + Globals.ForkLiftSystem.LiftPosition +
                " [TiltPosition] : " + Globals.ForkLiftSystem.TiltPosition +
                " [ForkLeftPosition] : " + Globals.ForkLiftSystem.ShiftLeftPosition + 
                " [ForkRightPosition] : " + Globals.ForkLiftSystem.ShiftRightPosition);
            */
        }

        private static void ReadCAN_242(Byte[] data)
        {
            Globals.CAN_242_FLAG = true;

            Globals.ForkLiftSystem.LiftState = (data[0] & 16) == 16 ? true : false;
            Globals.ForkLiftSystem.TiltState = (data[0] & 32) == 32 ? true : false;
            Globals.ForkLiftSystem.SideShiftState = (data[0] & 64) == 64 ? true : false;
            Globals.ForkLiftSystem.MoverState = (data[0] & 128) == 128 ? true : false;

            Globals.ForkLiftSystem.LiftSensorAlarm = (data[1] & 1) == 1 ? true : false;
            Globals.ForkLiftSystem.TiltSensorAlarm = (data[1] & 2) == 2 ? true : false;
            Globals.ForkLiftSystem.ShiftLeftSensorAlarm = (data[1] & 4) == 4 ? true : false;
            Globals.ForkLiftSystem.ShiftRightSensorAlarm = (data[1] & 8) == 8 ? true : false;

            Globals.ForkLiftSystem.LiftControlAlarm = (data[1] & 16) == 16 ? true : false;
            Globals.ForkLiftSystem.TiltControlAlarm = (data[1] & 32) == 32 ? true : false;
            Globals.ForkLiftSystem.MoverControlAlarm = (data[1] & 64) == 64 ? true : false;
            Globals.ForkLiftSystem.ShiftControlAlarm = (data[1] & 128) == 128 ? true : false;


            Globals.ForkLiftSystem.TouchCanErr = (data[2] & 1) == 1 ? true : false;
            Globals.ForkLiftSystem.TouchEnableErr = (data[2] & 2) == 2 ? true : false;
            Globals.ForkLiftSystem.ReleaseCheck = (data[2] & 4) == 4 ? true : false;
            Globals.ForkLiftSystem.ReachState = (data[2] & 128) == 128 ? true : false;

            Globals.ForkLiftSystem.MoverPosition = (UInt16)((data[5] << 8) | (data[4]));

            Globals.ForkLiftSystem.ReachPosition = (UInt16)((data[7] << 8) | (data[6]));

            /*
            Console.WriteLine("LiftState : " + Globals.ForkLiftSystem.LiftState + " TiltState : " + Globals.ForkLiftSystem.LiftState 
                + " SideShiftState : " + Globals.ForkLiftSystem.SideShiftState + " ForkMoverState : " + Globals.ForkLiftSystem.ForkMoverState);
            */
        }

        private static void ReadCAN_260(Byte[] data)
        {
            Globals.CAN_260_FLAG = true;

            Globals.NavigationSystem.ModeState = data[0];
            // Vehicle State Start
            Globals.NavigationSystem.VehicleState = data[1];
            Globals.NavigationSystem.VStateMoving = (data[1] & 1) == 1 ? true : false;
            Globals.NavigationSystem.VStatePausing = (data[1] & 2) == 2 ? true : false;
            Globals.NavigationSystem.VStateMoveComplete = (data[1] & 4) == 4 ? true : false;
            Globals.NavigationSystem.VStateBeamLowArea = (data[1] & 8) == 8 ? true : false;
            Globals.NavigationSystem.VStateBeamStopArea = (data[1] & 16) == 16 ? true : false;
            Globals.NavigationSystem.VStateCommTouch = (data[1] & 32) == 32 ? true : false;
            Globals.NavigationSystem.VStateCharging = (data[1] & 64) == 64 ? true : false;
            Globals.NavigationSystem.VStateForking = (data[1] & 128) == 128 ? true : false;
            // Vehicle State End

            //Globals.NavigationSystem.NAVLayer = (byte)(data[2] & 0x0F);
            //Globals.NavigationSystem.NAVState = (byte)(data[2] >> 4 & 0x0F);

            Globals.NavigationSystem.Alarm = (ushort)((data[4] << 16) | (data[3] << 8) | (data[2]));

            Globals.NavigationSystem.CurrentLink = (ushort)((data[6] & 0x0F) << 8 | (data[5]));
            Globals.NavigationSystem.NextLink = (ushort)(data[7] << 4 | ((data[6] & 0xF0) >> 4));

            //logger.Info("[CAN_260] [VStateMoving]" + Globals.NavigationSystem.VStateMoving + " [VStatePausing]" + Globals.NavigationSystem.VStatePausing + " [VStateMoveComplete]" + Globals.NavigationSystem.VStateMoveComplete
            //      + " [VStateBeamLowArea]" + Globals.NavigationSystem.VStateBeamLowArea + " [VStateBeamStopArea]" + Globals.NavigationSystem.VStateBeamStopArea + " [VStateCommViu]" + Globals.NavigationSystem.VStateCommViu
            //      + " [VStatePalletOn]" + Globals.NavigationSystem.VStatePalletOn + " [VStateLineStart]" + Globals.NavigationSystem.VStateLineStart);
            //logger.Info("[CAN_260] [ModeState] : " + Globals.NavigationSystem.ModeState + " [NAVLayer] : " + Globals.NavigationSystem.NAVLayer + " [NAVState] : " + Globals.NavigationSystem.NAVState + " [Alarm] : " + Globals.NavigationSystem.Alarm);
            //logger.Info("Global.NavigationSystem.CurrentLink : " + Global.NavigationSystem.CurrentLink + " Global.NavigationSystem.NextLink : " + Global.NavigationSystem.NextLink);
        }

        private static void ReadCAN_261(Byte[] data)
        {
            try
            {
                Globals.CAN_261_FLAG = true;

                Globals.NavigationSystem.NAV_X = checked((long)((data[2] << 24 | data[1] << 16 | data[0] << 8)) >> 8);
                Globals.NavigationSystem.NAV_Y = checked((long)((data[5] << 24 | data[4] << 16 | data[3] << 8)) >> 8);
                Globals.NavigationSystem.NAV_PHI = (short)((short)(data[7] << 8 | data[6]) / 10);
            }
            catch (Exception e)
            {
                logger.Info("[CAN_261] [NAV_X] : " + Globals.NavigationSystem.NAV_X + " [NAV_Y] :" + Globals.NavigationSystem.NAV_Y + " [NAV_PHI] :" + Globals.NavigationSystem.NAV_PHI);
                logger.Error("[CAN_261] Error : "+ e.ToString());
            }
        }
        private static void ReadCAN_263(Byte[] data)
        {
            Globals.CAN_263_FLAG = true;

            Globals.NavigationSystem.NAVLayer = (byte)(data[0] & 0x0F);
            Globals.NavigationSystem.NAVState = (byte)(data[0] >> 4 & 0x0F);
            Globals.NavigationSystem.bWT100 = (data[2] & 1) == 1 ? true : false;
            Globals.NavigationSystem.bUltraSonic = (data[2] & 2) == 2 ? true : false;
            Globals.NavigationSystem.bStAreaDetect = (data[2] & 4) == 4 ? true : false;
            Globals.NavigationSystem.LineOption_Rear = (data[3] & 1) == 1 ? true : false;
            Globals.NavigationSystem.LineOption_Front = (data[3] & 2) == 2 ? true : false;
            Globals.NavigationSystem.bResetButton = (data[4] & 1) == 1 ? true : false;
            Globals.NavigationSystem.bStartButton = (data[4] & 2) == 2 ? true : false;
            Globals.NavigationSystem.bStopButton = (data[4] & 4) == 4 ? true : false;
            Globals.NavigationSystem.CheckAFLID = (ushort)(data[5] & 2);//(ushort)((data[5] & 2) >> 1 | (data[5] & 1) >> 1);
            Globals.NavigationSystem.CurrentNode = (ushort)(data[7] << 8 | data[6]);
            //logger.Info("[CAN_261] [NAV_X] : " + Global.rx261Data.NAV_X + " [NAV_Y] :" + Global.rx261Data.NAV_Y + " [NAV_PHI] :" + Global.rx261Data.NAV_PHI);
        }

        //264~267 펌웨어 로그관련정보
        private static void ReadCAN_264(Byte[] data)
        {
            Globals.CAN_264_FLAG = true;

            Globals.FirmwareLog.DSState = (short)(data[0] & 1);
            Globals.FirmwareLog.LocaltargetCount = (short)(data[0] >> 4 & 0x0F);
            Globals.FirmwareLog.CurrentNode = (short)(data[2] << 8 | data[1]);
            Globals.FirmwareLog.NextNode = (short)(data[4] << 8 | data[3]);
            Globals.FirmwareLog.Rho = (short)(data[6] << 8 | data[5]);
            Globals.FirmwareLog.NavError = (short)(data[7] & 0x0F);
            Globals.FirmwareLog.NavWarning = (short)(data[7] >> 4 & 0x0F);
            //logger.Info("[CAN_264] [DS_State] " + Globals.FirmwareLog.DSState + " [LocaltargetCount] " + Globals.FirmwareLog.LocaltargetCount + " [CurrentNode] " + Globals.FirmwareLog.CurrentNode + " [NextNode] " + Globals.FirmwareLog.NextNode + " [Rho] " + Globals.FirmwareLog.Rho + " [NavError] " + Globals.FirmwareLog.NavError + " [NavWarning] " + Globals.FirmwareLog.NavWarning);
        }

        private static void ReadCAN_265(Byte[] data)
        {
            Globals.CAN_265_FLAG = true;

            Globals.FirmwareLog.DL = (short)(data[1] << 8 | data[0]);
            Globals.FirmwareLog.DiffAngle = (short)(data[3] << 8 | data[2]);
            Globals.FirmwareLog.TargetSteer = (short)(data[5] << 8 | data[4]);
            Globals.FirmwareLog.CurrentSteer = (short)(data[7] << 8 | data[6]);
            //logger.Info("[CAN_265] [DL] " + Globals.FirmwareLog.DL + " [DiffAngle] " + Globals.FirmwareLog.DiffAngle + " [TargetSteer] " + Globals.FirmwareLog.TargetSteer + " [CurrentSteer] " + Globals.FirmwareLog.CurrentSteer);
        }

        private static void ReadCAN_266(Byte[] data)
        {
            Globals.CAN_266_FLAG = true;

            Globals.FirmwareLog.TargetSpeed = (short)(data[1] << 8 | data[0]);
            Globals.FirmwareLog.CurrentSpeed = (short)(data[3] << 8 | data[2]);
            Globals.FirmwareLog.LeftRPM = (short)(data[5] << 8 | data[4]);
            Globals.FirmwareLog.RightRPM = (short)(data[7] << 8 | data[6]);
            //logger.Info("[CAN_266] [TargetSpeed] " + Globals.FirmwareLog.TargetSpeed + " [CurrentSpeed] " + Globals.FirmwareLog.CurrentSpeed + " [LeftRPM] " + Globals.FirmwareLog.LeftRPM + " [RightRPM] " + Globals.FirmwareLog.RightRPM);
        }

        private static void ReadCAN_267(Byte[] data)
        {
            Globals.CAN_267_FLAG = true;

            Globals.FirmwareLog.SpeedOutPut = (short)(data[1] << 8 | data[0]);
            Globals.FirmwareLog.SteerOutPut = (short)(data[3] << 8 | data[2]);
            Globals.FirmwareLog.SavedCurrentNode = (short)(data[5] << 8 | data[4]);
            Globals.FirmwareLog.AFLID = (short)((data[6] & 4) | (data[6] & 2) | (data[6] & 1));
            Globals.FirmwareLog.HornWarningEMO = (short)(data[6] & 8);

            //logger.Info("[CAN_267] [SpeedOutPut] " + Globals.FirmwareLog.SpeedOutPut + " [SteerOutPut] " + Globals.FirmwareLog.SteerOutPut + " [SavedCurrentNode] " + Globals.FirmwareLog.SavedCurrentNode);
        }
        private static void ReadCAN_268(Byte[] data)
        {
            Globals.CAN_268_FLAG = true;

            Globals.FirmwareLog.TargetRPM = (short)(data[1] << 8 | data[0]);
            Globals.FirmwareLog.LeftRPMDev = (short)(data[3] << 8 | data[2]);
            Globals.FirmwareLog.RightRPMDev = (short)(data[5] << 8 | data[4]);
            Globals.FirmwareLog.AccCount = (short)(data[6] & 0x0F);
            Globals.FirmwareLog.DecCount = (short)(data[6] >> 4 & 0x0F);
            Globals.FirmwareLog.SpdCount = (short)(data[7] & 0x0F);
            Globals.FirmwareLog.UnnormalSpeedCheck = (short)(data[7] & 16);

            //logger.Info("[CAN_267] [SpeedOutPut] " + Globals.FirmwareLog.SpeedOutPut + " [SteerOutPut] " + Globals.FirmwareLog.SteerOutPut + " [SavedCurrentNode] " + Globals.FirmwareLog.SavedCurrentNode);
        }
        private static void ReadCAN_269(Byte[] data)
        {
            Globals.CAN_269_FLAG = true;

            Globals.FirmwareLog.SteerDev = (short)(data[1] << 8 | data[0]);
            Globals.FirmwareLog.SteerDevData = (short)(data[3] << 8 | data[2]);
            Globals.FirmwareLog.SteerCheckCount = (short)data[4];
            Globals.FirmwareLog.UnnormalSteerCheck = (short)(data[5] & 1);

            //logger.Info("[CAN_267] [SpeedOutPut] " + Globals.FirmwareLog.SpeedOutPut + " [SteerOutPut] " + Globals.FirmwareLog.SteerOutPut + " [SavedCurrentNode] " + Globals.FirmwareLog.SavedCurrentNode);
        }
        private static void ReadCAN_290(Byte[] data)
        {
            Globals.CAN_290_FLAG = true;

            Globals.RecognitionSystem.TargetX = (short)(data[1] << 8 | data[0]);
            Globals.RecognitionSystem.TargetY = (short)(data[3] << 8 | data[2]);
            Globals.RecognitionSystem.TargetTheta = (short)(data[5] << 8 | data[4]);
            Globals.RecognitionSystem.Alarm = (byte)(data[6]);
            Globals.RecognitionSystem.AliveCount = data[7];
            
            //logger.Info("[CAN_290] [Alarm] : " + Globals.RecognitionSystem.Alarm);
        
        }

        private static void ReadCAN_291(Byte[] data)
        {
            /*
            Globals.CAN_291_FLAG = true;

            Globals.RecognitionSystem.FrontPalletX = (short)(data[1] << 8 | data[0]);
            Globals.RecognitionSystem.FrontPalletY = (short)(data[3] << 8 | data[2]);
            Globals.RecognitionSystem.FrontPalletTheta = (short)(data[5] << 8 | data[4]);

            Globals.RecognitionSystem.FrontPalletType = (byte)(data[6] & 0xF0);
            Globals.RecognitionSystem.FrontPalletResult = (data[6] & 1) == 1 ? true : false;

            

            
            logger.Info("[CAN_291] [FrontPalletX] : " + Globals.RecognitionSystem.FrontPalletX +
                " [FrontPalletY] :" + Globals.RecognitionSystem.FrontPalletY +
                " [FrontPalletTheta] :" + Globals.RecognitionSystem.FrontPalletTheta +
                " [FrontPalletType] :" + Globals.RecognitionSystem.FrontPalletType +
                " [FrontPalletResult] :" + Globals.RecognitionSystem.FrontPalletResult +
                " [Alarm] :" + Globals.RecognitionSystem.Alarm);
            */
        }

        
        private static void ReadCAN_293(Byte[] data)
        {
            Globals.CAN_293_FLAG = true;

            Globals.RecognitionSystem.LineTargetY = (short)(data[1] << 8 | data[0]);
            Globals.RecognitionSystem.LineTargetTheta = (short)(data[3] << 8 | data[2]);
            Globals.RecognitionSystem.AreaState = (byte)(data[4] & 0xFF);
            Globals.RecognitionSystem.LineAlarm = (byte)(data[5] & 0xFF);

            //logger.Info("[CAN_293] [AreaState] : " + Globals.RecognitionSystem.AreaState);
        }
         
        private static void ReadCAN_330(Byte[] data)
        {
            Globals.CAN_330_FLAG = true;

            Globals.SafetySystem.FrontOutNS3 = data[0];
            Globals.SafetySystem.LeftOutNS3 = data[1];
            Globals.SafetySystem.RightOutNS3 = data[2];
            Globals.SafetySystem.TailOutNS3 = data[3];

            Globals.SafetySystem.LeftWT100 = (data[4] & 0x04) == 4 ? true : false;
            Globals.SafetySystem.RightWT100 = (data[4] & 0x08) == 8 ? true : false;
            //Globals.SafetySystem.FrontNS3State = (short)((data[5] & 2) << 1 | (data[5] & 1));
            //Globals.SafetySystem.LeftNS3State = (short)((data[5] & 8) << 1 | (data[5] & 4));
            //Globals.SafetySystem.RightNS3State = (short)((data[5] & 32) << 1 | (data[5] & 16));
            //Globals.SafetySystem.TailNS3State = (short)((data[5] & 128) << 1 | (data[5] & 64));
            //기존
            Globals.SafetySystem.NANOSCAN3State = data[5];
            Globals.SafetySystem.FrontNS3Slow = (data[5] & 1) == 1 ? true : false;
            Globals.SafetySystem.FrontNS3State = (data[5] & 2) == 2 ? true : false;

            Globals.SafetySystem.LeftNS3Slow = (data[5] & 4) == 4 ? true : false;
            Globals.SafetySystem.LeftNS3State = (data[5] & 8) == 8 ? true : false;

            Globals.SafetySystem.RightNS3Slow = (data[5] & 16) == 16 ? true : false;
            Globals.SafetySystem.RightNS3State = (data[5] & 32) == 32 ? true : false;

            Globals.SafetySystem.TailNS3Slow = (data[5] & 64) == 64 ? true : false;
            Globals.SafetySystem.TailNS3State = (data[5] & 128) == 128 ? true : false;

            Globals.SafetySystem.VisioneryT = (data[6] & 2) == 2 ? true : false;
            Globals.SafetySystem.Loading_Fault = (data[6] & 4) == 4 ? true : false;


            //logger.Info("[CAN_330] [FrontOutNS3] : " + Globals.SafetySystem.FrontOutNS3 + 
            //            " [LeftOutNS3] :" + Globals.SafetySystem.LeftOutNS3 + 
            //            " [RightOutNS3] :" + Globals.SafetySystem.RightOutNS3 + 
            //            " [TailOutNS3] :" + Globals.SafetySystem.TailOutNS3);
        }

        //private static void ReadCAN_331(Byte[] data)
        //{
        //    Globals.CAN_331_FLAG = true;

        //    Globals.SafetySystem.LeftOutTIM = data[0];
        //    Globals.SafetySystem.LeftInTIM = data[1];

        //    //logger.Info("[CAN_331] [LeftOutTIM] : " + Globals.SafetySystem.LeftOutTIM + " [LeftInTIM] :" + Globals.SafetySystem.LeftInTIM);
        //}

        //private static void ReadCAN_332(Byte[] data)
        //{
        //    Globals.CAN_332_FLAG = true;

        //    Globals.SafetySystem.RightOutTIM = data[0];
        //    Globals.SafetySystem.RightInTIM = data[1];
        //    Globals.SafetySystem.PressureSensor = (ushort)(data[3] << 8 | data[2]);

        //    //logger.Info("[CAN_332] [RightOutTIM] : " + Globals.SafetySystem.RightOutTIM + " [RightInTIM] :" + Globals.SafetySystem.RightInTIM + " [PressureSensor] :" + Globals.SafetySystem.PressureSensor);
        //}

        //private static void ReadCAN_333(Byte[] data)
        //{
        //    Globals.CAN_333_FLAG = true;

        //    Globals.SafetySystem.RearOutTIM = data[0];
        //    Globals.SafetySystem.RearInTIM = data[1];

        //    //logger.Info("[CAN_333] [RearOutTIM] : " + Globals.SafetySystem.RearOutTIM + " [RearInTIM] :" + Globals.SafetySystem.RearInTIM);
        //}

        private static void ReadCAN_334(Byte[] data)
        {
            Globals.CAN_334_FLAG = true;

            //Globals.SafetySystem.RightWT100 = (data[0] & 0x04) == 4 ? true : false;
            //Globals.SafetySystem.LeftWT100 = (data[0] & 0x08) == 8 ? true : false;

            Globals.RecognitionSystem.Keyence_Current_Pos = (short)(data[2] << 8 | data[1]);
            Globals.RecognitionSystem.Keyence_Current_Pos_2 = (short)(data[4] << 8 | data[3]);

            Globals.SafetySystem.LeftPx = (data[5] & 0x04) == 4 ? true : false;
            Globals.SafetySystem.RightPx = (data[5] & 0x08) == 8 ? true : false;

            //Globals.RecognitionParameter.RotateMotorRightUpdateResult = (data[6] & 1) == 1 ? true : false;
            //Globals.RecognitionParameter.RotateMotorLeftUpdateResult = (data[6] & 2) == 2 ? true : false;
            Globals.SafetySystem.LeftKeyence = (data[6] & 1) == 1 ? true : false;
            Globals.SafetySystem.RightKeyence = (data[6] & 2) == 2 ? true : false;

            //logger.Info("[CAN_334] [RightWT100] : " + Globals.SafetySystem.RightWT100 + " [LeftWT100] :" + Globals.SafetySystem.LeftWT100);
        }

        private static void ReadCAN_340(Byte[] data)
        {
            Globals.CAN_340_FLAG = true;

            Globals.BMSSystem.Temperature = (sbyte)(data[1] << 8 | data[0]);
            Globals.BMSSystem.CellVolt = (ushort)(data[3] << 8 | data[2]);
            Globals.BMSSystem.Alarm = (ushort)data[4];
            //Globals.BMSSystem.CellVolt = (ushort)(data[5] << 8 | data[4]);
            //Globals.BMSSystem.ChargeVolt = (ushort)(data[7] << 8 | data[6]);
        }

        private static void ReadCAN_341(Byte[] data)
        {
            Globals.CAN_341_FLAG = true;

            //Globals.BMSSystem.Temperature = (sbyte)data[0];
            //Globals.BMSSystem.Battery = (byte)data[1];
            //Globals.BMSSystem.Fault = (byte)(0xF0 & data[2] >> 4);
            //Globals.BMSSystem.Warning = (byte)(0x0F & data[2]);
            //Globals.BMSSystem.Status = (byte)(0x03 & data[3]);

            Globals.BMSSystem.Bat_V = (short)(data[0] << 8 | data[1]);
            Globals.BMSSystem.Bat_i = (short)(data[2] << 8 | data[3]);
            Globals.BMSSystem.SOC = (short)(data[4] << 8 | data[5]);
            Globals.BMSSystem.SOH = (short)(data[6] << 8 | data[7]);
        }
        private static void ReadCAN_342(Byte[] data)
        {
            Globals.CAN_342_FLAG = true;

            Globals.BMSSystem.OPV_Flag = (short)((data[0] & 2) | (data[0] & 1));
            Globals.BMSSystem.UPV_Flag = (short)((data[0] & 8) >> 2 | (data[0] & 4) >> 2);
            Globals.BMSSystem.OCV_Flag = (short)((data[0] & 32) >> 4 | (data[0] & 16) >> 4);
            Globals.BMSSystem.UCV_Flag = (short)((data[0] & 128) >> 6 | (data[0] & 64) >> 6);
            Globals.BMSSystem.OT_Flag = (short)((data[1] & 32) >> 4 | (data[1] & 16) >> 4);
            Globals.BMSSystem.UT_Flag = (short)((data[1] & 128) >> 6 | (data[1] & 64) >> 6);
            Globals.BMSSystem.ODV_Flag = (short)((data[2] & 2) | (data[2] & 1));
            Globals.BMSSystem.ODT_Flag = (short)((data[2] & 8) >> 2 | (data[2] & 4) >> 2);
            Globals.BMSSystem.OC_Flag = (short)((data[2] & 32) >> 4 | (data[2] & 16) >> 4);
            Globals.BMSSystem.Charge_Complete = (data[2] & 64) == 1 ? true : false;
            Globals.BMSSystem.Charging = (data[2] & 128) == 1 ? true : false;

        }
        private static void ReadCAN_410(Byte[] data) // kdy
        {
            Globals.RecognitionParameter.UpdateResult= (data[0] & 1) == 1 ? true : false;
            Globals.RecognitionParameter.UpdateResult = (data[0] & 2) == 2 ? true : false;
            Globals.RecognitionParameter.Error_Response = (short)((data[0] & 8) << 1 | (data[0] & 4));
            Globals.RecognitionParameter.Obstacle_Detect = (data[0] & 16) == 16 ? true : false;

            //Console.WriteLine("Globals.ParameterResult.RecognitionResult : " + Globals.ParameterResult.RecognitionResult);
        }

        private static void ReadCAN_411(Byte[] data)
        {
            Globals.VehicleParameter.UpdateResult = (data[0] & 1) == 1 ? true : false;

            //Console.WriteLine("Globals.ParameterResult.VehicleResult : " + Globals.ParameterResult.VehicleResult);
        }
    }
}
