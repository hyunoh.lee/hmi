﻿using LumenWorks.Framework.IO.Csv;
using MobyusTouch.src;
using MobyusTouch.src.Control.Sub;
using MobyusTouch.src.Global;
using MobyusTouch.src.Main.Sub;
using MobyusTouch.src.Site;
using MobyusTouch.Src.Utility;
using NLog;
using NLog.Config;
using NLog.Targets;
using NLog.Targets.Wrappers;
using NLog.Windows.Forms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Media.Media3D;

namespace MobyusTouch
{
    static class Program
    {
        

        [STAThread]
        static void Main()
        {
            Thread restartSignal = new Thread(WaitRestartSignal);
            restartSignal.Start();

            Process[] procs = Process.GetProcessesByName("MobyusTouch");

            if (procs.Length > 1)
            {
                MessageBox.Show("프로그램이 이미 실행되고 있습니다.\n다시 한번 확인해주시기 바랍니다.");
                Environment.Exit(0);
            }
            else
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(true);


                if (!(Globals.Site.Equals(Globals.SITE_SEJONG)))
                {
                    if (!ReadConfigInfo())
                    {
                        MessageBox.Show("환경 정보를 읽을 수 없습니다. \n다시 한번 확인해주시기 바랍니다.");
                        Environment.Exit(0);
                    }
                }

                if (Globals.Site.Equals(Globals.SITE_HYUNDAI))
                {
                    Application.Run(new MainHyundai());
                }
                else if (Globals.Site.Equals(Globals.SITE_SAMSUNG))
                {
                    Application.Run(new MainSamsung());
                }             
                else
                {
                    Application.Run(new MainTouch());
                }

            }
        }

        private static void WaitRestartSignal()
        {
            string target = "C:\\Mobyus\\Restart.bat";
            DateTime endTime, startTime;
            FileInfo fileInfo;
            try
            {
                fileInfo = new FileInfo(target);
                startTime = fileInfo.LastAccessTime;
            }
            catch(Exception ex)
            {
                startTime = new DateTime();
            }

            while (true)
            {
                try
                {
                    fileInfo = new FileInfo(target);
                    endTime = fileInfo.LastAccessTime;

                    if (!startTime.Equals(endTime))
                    {
                        System.Diagnostics.Process.Start("Restart.bat");
                        break;
                    }
                    Thread.Sleep(1000);
                }
                catch(Exception ex)
                {
                    endTime = startTime;
                }

            }
        }

        public static bool ReadConfigInfo()
        {
            bool result = true;

            try
            {
                if (File.Exists(Globals.HMI_SettingFilePath))
                {
                    IniFileParsing.SetFilePath(Globals.HMI_SettingFilePath);

                    Globals.Site = IniFileParsing.ReadValue("Site", "NAME", "");
                    Globals.Language = IniFileParsing.ReadValue("Language", "NAME", "");
                    Globals.Type = IniFileParsing.ReadValue("Type", "NAME", "");
                    Globals.ID = IniFileParsing.ReadValue("Type", "ID", "");
                    switch(Globals.ID)
                    {
                        case "AMR001":
                            Globals.AFL_ID = 1;
                            break;
                        case "AMR002":
                            Globals.AFL_ID = 2;
                            break;
                        case "AMR003":
                            Globals.AFL_ID = 3;
                            break;
                        default:
                            Globals.AFL_ID = 1;
                            break;

                    }
                    Globals.VALUE = IniFileParsing.ReadValue("Type", "VALUE", "");
                    Globals.LIFT_OFFSET = IniFileParsing.ReadValue("Type", "LIFT_OFFSET", "");
                    if (Globals.LIFT_OFFSET == "") Globals.LIFT_OFFSET = "-5";
                    Globals.BOARD_COMM_TIME = IniFileParsing.ReadValue("Type", "BOARD_COMM_TIME", "");
                    Globals.ACS_TIU_COMM_TIME = IniFileParsing.ReadValue("Type", "ACS_TIU_COMM_TIME", "");
                    Globals.ACS_TCP_COMM_TIME = IniFileParsing.ReadValue("Type", "ACS_TCP_COMM_TIME", "");
                    Globals.Test = IniFileParsing.ReadValue("Type", "TEST", "");
                    Globals.TRAFFIC_PASS = IniFileParsing.ReadValue("Type", "TRAFFIC_PASS", "");
                    Globals.INFLUX_ADDR = IniFileParsing.ReadValue("Type", "INFLUX_ADDR", "");
                    Globals.INFLUX_TOKEN = IniFileParsing.ReadValue("Type", "INFLUX_TOKEN", "");

                    Globals.FORK_LIFT_MAX_LIMIT = IniFileParsing.ReadValue("Forklift", "FORK_LIFT_MAX_LIMIT", 0);
                    Globals.FORK_LIFT_MIN_LIMIT = IniFileParsing.ReadValue("Forklift", "FORK_LIFT_MIN_LIMIT", 0);
                    Globals.FORK_TILT_MAX_LIMIT = IniFileParsing.ReadValue("Forklift", "FORK_TILT_MAX_LIMIT", 0);
                    Globals.FORK_TILT_MIN_LIMIT = IniFileParsing.ReadValue("Forklift", "FORK_TILT_MIN_LIMIT", 0);
                    Globals.FORK_REACH_MAX_LIMIT = IniFileParsing.ReadValue("Forklift", "FORK_REACH_MAX_LIMIT", 0);
                    Globals.FORK_REACH_MIN_LIMIT = IniFileParsing.ReadValue("Forklift", "FORK_REACH_MIN_LIMIT", 0);
                    Globals.FORK_MOVER_MAX_LIMIT = IniFileParsing.ReadValue("Forklift", "FORK_MOVER_MAX_LIMIT", 0);
                    Globals.FORK_MOVER_MIN_LIMIT = IniFileParsing.ReadValue("Forklift", "FORK_MOVER_MIN_LIMIT", 0);
                    Globals.FORK_SHIFT_LIMIT = IniFileParsing.ReadValue("Forklift", "FORK_SHIFT_LIMIT", 0);

                    if (Globals.FORK_LIFT_MAX_LIMIT == 0)
                    {
                        MessageBox.Show("[HMI_INFO.ini] FORK_LIFT_MAX_LIMIT 값에 오류가 있습니다.");
                        result = false;
                    }

                    if (Globals.FORK_TILT_MAX_LIMIT == 0)
                    {
                        MessageBox.Show("[HMI_INFO.ini] FORK_TILT_MAX_LIMIT 값에 오류가 있습니다.");
                        result = false;
                    }

                    if (Globals.FORK_REACH_MAX_LIMIT == 0)
                    {
                        MessageBox.Show("[HMI_INFO.ini] FORK_REACH_MAX_LIMIT 값에 오류가 있습니다.");
                        result = false;
                    }

                    if (Globals.FORK_MOVER_MAX_LIMIT == 0)
                    {
                        MessageBox.Show("[HMI_INFO.ini] FORK_MOVER_MAX_LIMIT 값에 오류가 있습니다.");
                        result = false;
                    }

                    if (Globals.FORK_SHIFT_LIMIT == 0)
                    {
                        MessageBox.Show("[HMI_INFO.ini] FORK_SHIFT_LIMIT 값에 오류가 있습니다.");
                        result = false;
                    }

                    Globals.serialPortName = IniFileParsing.ReadValue("SerialPort", "PORT_NAME", "");
                    Globals.serialBaudRate = Int32.Parse(IniFileParsing.ReadValue("SerialPort", "PORT_BAUDRATE", ""));

                    Globals.serialDataBits = IniFileParsing.ReadValue("SerialPort", "PORT_DATABITS", 0);
                    Globals.serialParityBits = (Parity)IniFileParsing.ReadValue("SerialPort", "PORT_PARITY", (int)Parity.None);
                    Globals.serialStopBits = (StopBits)IniFileParsing.ReadValue("SerialPort", "PORT_STOPBITS", (int)StopBits.One);

                    Globals.fmsIpAddress = IniFileParsing.ReadValue("FmsTcp", "FMS_IP", "");
                    Globals.fmsIpPort = Int32.Parse(IniFileParsing.ReadValue("FmsTcp", "FMS_PORT", ""));
                    Globals.LED_IP = IniFileParsing.ReadValue("FmsTcp", "LED_IP", "");
                    Globals.LED_Port = Int32.Parse(IniFileParsing.ReadValue("FmsTcp", "LED_Port", ""));

                    Globals.RecognitionParameter.MinDistance = ushort.Parse(IniFileParsing.ReadValue("Recognition", "MIN_DISTANCE", "0"));
                    Globals.RecognitionParameter.ReceiverGain = byte.Parse(IniFileParsing.ReadValue("Recognition", "RECEIVER_GAIN", "0"));
                    Globals.RecognitionParameter.PostSharpening = byte.Parse(IniFileParsing.ReadValue("Recognition", "POST_SHARPENING", "0"));
                    Globals.RecognitionParameter.PreSharpening = byte.Parse(IniFileParsing.ReadValue("Recognition", "PRE_SHARPENING", "0"));
                    Globals.RecognitionParameter.Confidence = byte.Parse(IniFileParsing.ReadValue("Recognition", "CONFIDENCE", "0"));
                    Globals.RecognitionParameter.NoiseFiltering = byte.Parse(IniFileParsing.ReadValue("Recognition", "NOISE_FILTERING", "0"));
                    Globals.RecognitionParameter.PalletDistanceUp = byte.Parse(IniFileParsing.ReadValue("Recognition", "PALLET_DISTANCE_UP", "0"));
                    Globals.RecognitionParameter.PalletDistanceDown = byte.Parse(IniFileParsing.ReadValue("Recognition", "PALLET_DISTANCE_DOWN", "0"));

                    Globals.VehicleParameter.StackingAngle = byte.Parse(IniFileParsing.ReadValue("Vehicle", "STACKING_ANGLE", "0"));
                    Globals.VehicleParameter.StackingDistance = byte.Parse(IniFileParsing.ReadValue("Vehicle", "STACKING_DISTANCE", "0"));
                    Globals.VehicleParameter.MinSpeed = byte.Parse(IniFileParsing.ReadValue("Vehicle", "MIN_SPEED", "0"));
                    Globals.VehicleParameter.MaxSpeed = byte.Parse(IniFileParsing.ReadValue("Vehicle", "MAX_SPEED", "0"));
                }
                else
                {
                    IniFileParsing.SetFilePath(Globals.HMI_SettingFilePath);

                    // FORKLIFT PARAMETER
                    IniFileParsing.WriteValue("Forklift", "FORK_LIFT_MAX_LIMIT", "0");
                    IniFileParsing.WriteValue("Forklift", "FORK_LIFT_MIN_LIMIT", "0");
                    IniFileParsing.WriteValue("Forklift", "FORK_TILT_MAX_LIMIT", "0");
                    IniFileParsing.WriteValue("Forklift", "FORK_TILT_MIN_LIMIT", "0");
                    IniFileParsing.WriteValue("Forklift", "FORK_REACH_MAX_LIMIT", "0");
                    IniFileParsing.WriteValue("Forklift", "FORK_REACH_MIN_LIMIT", "0");
                    IniFileParsing.WriteValue("Forklift", "FORK_MOVER_MAX_LIMIT", "0");
                    IniFileParsing.WriteValue("Forklift", "FORK_MOVER_MIN_LIMIT", "0");
                    IniFileParsing.WriteValue("Forklift", "FORK_SHIFT_LEFT_LIMIT", "0");
                    IniFileParsing.WriteValue("Forklift", "FORK_SHIFT_RIGHT_LIMIT", "0");

                    Globals.FORK_LIFT_MAX_LIMIT = IniFileParsing.ReadValue("Forklift", "FORK_LIFT_MAX_LIMIT", 0);
                    Globals.FORK_LIFT_MIN_LIMIT = IniFileParsing.ReadValue("Forklift", "FORK_LIFT_MIN_LIMIT", 0);
                    Globals.FORK_TILT_MAX_LIMIT = IniFileParsing.ReadValue("Forklift", "FORK_TILT_MAX_LIMIT", 0);
                    Globals.FORK_TILT_MIN_LIMIT = IniFileParsing.ReadValue("Forklift", "FORK_TILT_MIN_LIMIT", 0);
                    Globals.FORK_REACH_MAX_LIMIT = IniFileParsing.ReadValue("Forklift", "FORK_REACH_MAX_LIMIT", 0);
                    Globals.FORK_REACH_MIN_LIMIT = IniFileParsing.ReadValue("Forklift", "FORK_REACH_MIN_LIMIT", 0);
                    Globals.FORK_MOVER_MAX_LIMIT = IniFileParsing.ReadValue("Forklift", "FORK_MOVER_MAX_LIMIT", 0);
                    Globals.FORK_MOVER_MIN_LIMIT = IniFileParsing.ReadValue("Forklift", "FORK_MOVER_MIN_LIMIT", 0);
                    Globals.FORK_SHIFT_LIMIT = IniFileParsing.ReadValue("Forklift", "FORK_SHIFT_LIMIT", 0);

                    // SERIALPORT PARAMETER
                    IniFileParsing.WriteValue("SerialPort", "PORT_NAME", "COM1");
                    IniFileParsing.WriteValue("SerialPort", "PORT_BAUDRATE", "115200");
                    IniFileParsing.WriteValue("SerialPort", "PORT_DATABITS", "8");
                    IniFileParsing.WriteValue("SerialPort", "PORT_PARITY", "0");
                    IniFileParsing.WriteValue("SerialPort", "PORT_STOPBITS", "1");

                    Globals.serialPortName = IniFileParsing.ReadValue("SerialPort", "PORT_NAME", "");
                    Globals.serialBaudRate = Int32.Parse(IniFileParsing.ReadValue("SerialPort", "PORT_BAUDRATE", ""));
                    Globals.serialDataBits = IniFileParsing.ReadValue("SerialPort", "PORT_DATABITS", 0);
                    Globals.serialParityBits = (Parity)IniFileParsing.ReadValue("SerialPort", "PORT_PARITY", (int)Parity.None);
                    Globals.serialStopBits = (StopBits)IniFileParsing.ReadValue("SerialPort", "PORT_STOPBITS", (int)StopBits.One);

                    // ACS PARAMETER
                    IniFileParsing.WriteValue("FmsTcp", "FMS_IP", "127.0.0.1");
                    IniFileParsing.WriteValue("FmsTcp", "FMS_PORT", "1883");
                    IniFileParsing.WriteValue("FmsTcp", "LED_IP", "192.168.0.201");
                    IniFileParsing.WriteValue("FmsTcp", "LED_Port", "5000");

                    // AFL_ID
                    IniFileParsing.WriteValue("FmsTcp", "LED_IP", "192.168.0.201");


                    // RECOGNITION PARAMETER
                    IniFileParsing.WriteValue("Recognition", "MIN_DISTANCE", "0");
                    IniFileParsing.WriteValue("Recognition", "RECEIVER_GAIN", "0");
                    IniFileParsing.WriteValue("Recognition", "POST_SHARPENING", "0");
                    IniFileParsing.WriteValue("Recognition", "PRE_SHARPENING", "0");
                    IniFileParsing.WriteValue("Recognition", "CONFIDENCE", "0");
                    IniFileParsing.WriteValue("Recognition", "NOISE_FILTERING", "0");
                    IniFileParsing.WriteValue("Recognition", "PALLET_DISTANCE_UP", "0");
                    IniFileParsing.WriteValue("Recognition", "PALLET_DISTANCE_DOWN", "0");

                    Globals.RecognitionParameter.MinDistance = ushort.Parse(IniFileParsing.ReadValue("Recognition", "MIN_DISTANCE", "0"));
                    Globals.RecognitionParameter.ReceiverGain = byte.Parse(IniFileParsing.ReadValue("Recognition", "RECEIVER_GAIN", "0"));
                    Globals.RecognitionParameter.PostSharpening = byte.Parse(IniFileParsing.ReadValue("Recognition", "POST_SHARPENING", "0"));
                    Globals.RecognitionParameter.PreSharpening = byte.Parse(IniFileParsing.ReadValue("Recognition", "PRE_SHARPENING", "0"));
                    Globals.RecognitionParameter.Confidence = byte.Parse(IniFileParsing.ReadValue("Recognition", "CONFIDENCE", "0"));
                    Globals.RecognitionParameter.NoiseFiltering = byte.Parse(IniFileParsing.ReadValue("Recognition", "NOISE_FILTERING", "0"));
                    Globals.RecognitionParameter.PalletDistanceUp = byte.Parse(IniFileParsing.ReadValue("Recognition", "PALLET_DISTANCE_UP", "0"));
                    Globals.RecognitionParameter.PalletDistanceDown = byte.Parse(IniFileParsing.ReadValue("Recognition", "PALLET_DISTANCE_DOWN", "0"));


                    // VEHICLE PARAMETER
                    IniFileParsing.WriteValue("Vehicle", "STACKING_ANGLE", "0");
                    IniFileParsing.WriteValue("Vehicle", "STACKING_DISTANCE", "0");
                    IniFileParsing.WriteValue("Vehicle", "MIN_SPEED", "0");
                    IniFileParsing.WriteValue("Vehicle", "MAX_SPEED", "0");

                    Globals.VehicleParameter.StackingAngle = byte.Parse(IniFileParsing.ReadValue("Vehicle", "STACKING_ANGLE", "0"));
                    Globals.VehicleParameter.StackingDistance = byte.Parse(IniFileParsing.ReadValue("Vehicle", "STACKING_DISTANCE", "0"));
                    Globals.VehicleParameter.MinSpeed = byte.Parse(IniFileParsing.ReadValue("Vehicle", "MIN_SPEED", "0"));
                    Globals.VehicleParameter.MaxSpeed = byte.Parse(IniFileParsing.ReadValue("Vehicle", "MAX_SPEED", "0"));
                }

                if (Globals.Site.Equals(Globals.SITE_HYUNDAI) || Globals.Site.Equals(Globals.SITE_SAMSUNG))
                {
                    if (!System.IO.Directory.Exists(Globals.MOBYUS_SettingDirPath))
                    {
                        System.IO.Directory.CreateDirectory(Globals.MOBYUS_SettingDirPath);
                    }

                    if (!System.IO.Directory.Exists(Globals.LOG_SettingDirPath))
                    {
                        System.IO.Directory.CreateDirectory(Globals.LOG_SettingDirPath);
                    }

                    if (!System.IO.Directory.Exists(Globals.INFO_SettingDirPath))
                    {
                        System.IO.Directory.CreateDirectory(Globals.INFO_SettingDirPath);
                    }

                    if (!File.Exists(Globals.MAP_SettingFilePath))
                    {
                        MessageBox.Show("[MAP.dat] 파일을 찾을 수 없습니다.");
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

    }
}
